## skaion_pvc_new.py
""" 
Goal here was to test roc v. pvc method on skaion data. there aren't enough internal ips w/ attacks to work 

Note: this is meant to be run from the directory ../code-probabilistic-alert-rate-paper 
"""

import os, datetime
import numpy as np 
# from matplotlib import pyplot as plt 
import netaddr as na
from collections import defaultdict

import SkaionOperations as sop
import SkaionReader as sr
import SkaionReader as sr
from generalFunctions import *


## ---- functions ---- ## 
def make_cm(truth, predicted): 
	""" 
	input:	- truth = list of 1 and 0 s. This gives the groundtruth classification 
			- predicted = list of 1 and 0s. this gives the predicted classification 
		NOTE: truth, predicted should be the same length!
	output: - metrics = {tp: count of true positives, fp: , ...}
			computes and stores the number of true positives (TP), etc. and computes recall (TPR), false positive rate (FPR), precision (PPV), and neg. predictive value (NPV), and base rate = number of positive/number of samples
	""" 
	if len(truth) != len(predicted): 
		print "input lists have different lenghts (must be same)"
		return 
	l = zip(truth, predicted)
	tp = len([(t,p) for t,p in l if t==1 and p ==1 ] )
	fp = len([(t,p) for t,p in l if t==0 and p ==1 ] )
	tn = len([(t,p) for t,p in l if t==0 and p ==0 ] )
	fn = len([(t,p) for t,p in l if t==1 and p ==0 ] )
	
	if float( tp + fn) >0: 
		tpr = float(tp) / float( tp + fn)
	else: 
		tpr = 'na'
	
	if float( fp + tn ) >0: 
		fpr = float(fp) / float( fp + tn )
	else: 
		fpr = 'na'

	if float( tp + fp) > 0 : 
		ppv = float(tp) / float( tp + fp)
	else: 
		ppv = 'na'
	
	if float( tn + fn) > 0 :
		npv = float(tn) / float( tn + fn)
	else: 
		npv = 'na'

	br = float( tp + fn )/ (tp + fp + tn + fn ) ## base rate
	
	metrics = {
				'tp': tp, 
				'fp': fp, 
				'tn': tn, 
				'fn': fn, 
				'tpr': tpr, 
				'fpr': fpr, 
				'ppv': ppv, 
				'npv': npv, 
				'br': br
	}
	return metrics 


def distance(x,y):
	return ((x[0]-y[0])**2 + (x[1]-y[1])**2)**0.5

f = lambda cm, alpha : cm['br']* cm['tpr']/alpha ## for pv<= alpha threshold


def classify(pv, alpha): 
	if pv <= alpha: 
		return 1
	else: 
		return 0


# ---- paths ---- ##
skaion_path = os.path.join( os.path.abspath( os.path.join( os.getcwd(), '..')) , 'data-skaion')
folder_path = os.path.join(skaion_path , 'results')
if not os.path.isdir(folder_path): 
	os.mkdir(folder_path)


## ---- load the data ---- ## 
print '\n\nloading data ... '
# skaion = sop.DataBase.PopulateDataBase('/Users/ikj/Downloads/new_skaion.txt')
skaion = sop.DataBase.PopulateDataBase( os.path.join( os.path.join( skaion_path, 'new_skaion.txt')))

srcIPs=[]
destIPs=[]
times = []
scores = []
pvt = []
ppt = []
bppt = []

count = 0
for i in range(len(skaion.dataList)):
	srcIPs.append(skaion.dataList[i].srcIP)
	destIPs.append(skaion.dataList[i].dstIP)
	times.append(skaion.dataList[i].time)
	scores.append(skaion.dataList[i].score)
	pvt.append(skaion.dataList[i].scoreData.pv_T)
	ppt.append(skaion.dataList[i].scoreData.PrivPorts_T)
	bppt.append(skaion.dataList[i].scoreData.BytesPerPacket_T)
	count = count+1
	if count % 1000 == 0:
		print"\t %s" %(count)

derp = np.vstack((srcIPs, destIPs, times, scores, pvt, ppt, bppt, np.zeros(909179)))

skaionnumpy = np.transpose(derp)
## so column labels are [srcip, dstip, time, scores, pvt, ppt, bppt, label]


## ---- apply labels ---- ## 
print "\n\napplying labels to data ... "
attackstart = datetime.datetime(2006, 10, 1, 21, 9, 30, 0)

# attacks
for i in range(909179):
	if skaionnumpy[i,0] == '34.190.45.188':
		d=skaionnumpy[i,2]-attackstart
		if d.days >= 0 and d.seconds >= 0: ## should be or?
			skaionnumpy[i,7] = 1
	if skaionnumpy[i,1] == '34.190.45.188': ## should be or?
		d=skaionnumpy[i,2]-attackstart
		if d.days >= 0 and d.seconds >= 0:
			skaionnumpy[i,7] = 1


## ---- define networks ---- ## 
networks = map( na.IPNetwork, ['100.0.0.0/8'])

## ---- define training and testing sets ---- ##
print "Making training and testing sets ... "
nn = 600000 ## chosen so some positives in both train, test w/ roughly similar class bias in train/test 
skaiontrain = skaionnumpy[300000:nn, :]
skaiontest = skaionnumpy[nn:,:]

# pre_ips = []
train = []
for x in skaiontrain: 
	srcip = na.IPAddress(x[0])
	dstip = na.IPAddress(x[1])
	time = x[2] 
	score = 10**( -x[3] )
	pcr = 10**( - x[4] )	
	# ppt = 10**( -x[5]) 
	# bppt = 10**( -x[6])	
	label = x[-1]
	if any( map( lambda a: srcip in a , networks) ): 	
		train.append((srcip, time, score,  label))
		pre_ips.append(srcip)
	if any( map(lambda a: dstip in a, networks) ): 	
		train.append((dstip, time, score, label))
		pre_ips.append(dstip)

# pre_ips = sorted(set(ips)) ## list of all internal ips seen in training. 

test = []
for x in skaiontest: 
	srcip = na.IPAddress(x[0])
	dstip = na.IPAddress(x[1])
	time = x[2] 
	score = x[3]
	pcr = 10**( - x[4] )
	label = x[-1]
	if any( map(lambda x: srcip in x, networks) ): 	
		test.append( (srcip, time, score, label))
	if any( map(lambda x: dstip in x, networks) ): 	
		test.append((dstip, time, score, label))

# In [26]: len(test)
# Out[26]: 426011
# In [27]: len(train)
# Out[27]: 751312

train_br = float(len([x for x in train if x[-1]==1]))/len(train) ## class base rate
test_br = float(len([x for x in test if x[-1]==1]))/len(test) ## class base rate
print "The training base rate = %s, testing base rate = %s " %(train_br, test_br)


# In [28]: test_br
# Out[28]: 0.010384708376074796
# In [29]: train_br
# Out[29]: 0.011184434695572546


## ---- apply predicted labels for each alpha ---- ## 
print 'Making confusion matrices for all alphas ...'
alpha_list = np.arange(.39, .44, .001)

## make the confusion matrices for each alpha: 
cms_train = defaultdict(dict)
for alpha in alpha_list: 
	## for each alpha make the confusion matrices
	true_y = [ x[-1] for x in train]
	pred_y = map(lambda x: classify(x[2], alpha), train)
	cm = make_cm( true_y, pred_y)
	cms_train[alpha] = cm 
jsonify( { str(ip):y for ip,y in cms_train.iteritems()} , os.path.join(folder_path, 'cms_train.json'))


## now make the roc curve and optimize
roc = { alpha: (cms_train[alpha]['fpr'], cms_train[alpha]['tpr']) for alpha in alpha_list}
alpha_roc = sorted( alpha_list, key = lambda alpha: distance( ( 0., 1.), roc[alpha] ) )[0] ## want the smallest dist 

## same for pvc alpha: 
pvc = {alpha: f( cms_train[alpha] , alpha) for alpha in alpha_list}
alpha_pvc = sorted( alpha_list, key = lambda alpha: pvc[alpha] , reverse = True )[0] ## want the biggest 

## store the two alpha: 
print "alpha_roc = %s\nalpha_pvc = %s" %(alpha_roc, alpha_pvc)
jsonify({"alpha_roc": alpha_roc, "alpha_pvc": alpha_pvc}, os.path.join(folder_path, 'alphas.json'))


## moment of truth! test them: 
cm_test = {}

true_y = [ x[-1] for x in test ] ## list of true labels
## roc alpha prediction: 
pred_y = map(lambda x: classify(x[2], alpha_roc), test ) ## list of predicted labels
cm = make_cm(true_y, pred_y)
cm_test['roc'] = cm 

## pvc alpha prediction: 
pred_y = map(lambda x: classify(x[2], alpha_pvc), test ) ## list of predicted labels
cm = make_cm(true_y, pred_y)
cm_test['pvc'] = cm 




# c = 0
# for ip in pre_ips: 
# 	c += 1
# 	if c%50 == 0:
# 		print "on IP # %s of %s" %(c , len(pre_ips))
# 	train_ip = [ x for x in train if ip == x[0] ]
# 	true_y = [ x[-1] for x in train_ip ]
# 	if 1 in true_y and 0 in true_y: # need both positive and negative classes 

# 		for alpha in alpha_list: 
# 			## for each alpha make the confusion matrices
# 			pred_y = map(lambda x: classify(x[2], alpha), train_ip )
# 			cm = make_cm( true_y, pred_y)
# 			cms_train[ip][alpha] = cm 
# 		## now make the roc curve and optimize
# 		roc = {alpha: (cms_train[ip][alpha]['fpr'], cms_train[ip][alpha]['tpr']) for alpha in alpha_list}
# 		alpha_roc[ip] = sorted( alpha_list, key = lambda alpha: distance( ( 0., 1.), roc[alpha] ) )[0] ## want the smallest dist 
# 		## same for pvc alpha: 
# 		pvc = {alpha: f( cms_train[ip][alpha] , alpha) for alpha in alpha_list}
# 		alpha_pvc[ip] = sorted( alpha_list, key = lambda alpha: pvc[alpha] , reverse = True )[0] ## want the biggest 

 
# jsonify( { str(ip): y for ip, y in alpha_roc.iteritems()} , os.path.join(folder_path, 'alpha_roc.json'))
# jsonify( { str(ip): y for ip, y in alpha_pvc.iteritems()} , os.path.join(folder_path, 'alpha_pvc.json'))

# alpha_roc = unjsonify(os.path.join(folder_path, 'alpha_roc.json'))
# alpha_pvc = unjsonify(os.path.join(folder_path, 'alpha_pvc.json'))
# ips = alpha_pvc.keys()

## now for test set evaluation: 
# cm_test = defaultdict(dict)
# for ip in ips: 
# 	test_ip = [ x for x in train if ip == x[0] ] ## test set for this ip 
# 	if test_ip: 

# 		true_y = [ x[-1] for x in test_ip ] ## list of true labels
		
# 		## roc alpha prediction: 
# 		alpha = alpha_roc[ip]
# 		pred_y = map(lambda x: classify(x[2], alpha), test_ip ) ## list of predicted labels
# 		cm = make_cm(true_y, pred_y)
# 		cm_test[ip]['roc'] = cm 

# 		## pvc alpha prediction: 
# 		alpha = alpha_pvc[ip]
# 		pred_y = map(lambda x: classify(x[2], alpha), test_ip ) ## list of predicted labels
# 		cm = make_cm(true_y, pred_y)
# 		cm_test[ip]['pvc'] = cm 



# In [26]: len(test)
# Out[26]: 426011
# In [27]: len(train)
# Out[27]: 751312







## ---- plot ROC and PPV ---- ##
# print "\n\n plottin ... "
# tprs = [ confusion_train_pvt[alpha]['tpr'] for alpha in alpha_list]
# fprs = [ confusion_train_pvt[alpha]['fpr'] for alpha in alpha_list]
# ppvs = [ confusion_train_pvt[alpha]['ppv'] for alpha in alpha_list]
# npvs = [ confusion_train_pvt[alpha]['npv'] for alpha in alpha_list]

## Four axes, returned as a 2-d array
# f, axarr = plt.subplots(1, 2)
# axarr[0].plot(fprs, tprs)
# axarr[0].plot(roc[alpha_roc][0], roc[alpha_roc][1], 'ro' )
# axarr[0].set_title('ROC')

# axarr[1].scatter(alpha_list, [pvc[alpha] for alpha in alpha_list])
# axarr[1].plot(alpha_pvc, pvc[alpha_pvc], 'ro' )
# axarr[1].set_title('PVC')
# # Fine-tune figure; hide x ticks for top plots and y ticks for right plots
# # plt.setp([a.get_yticklabels() for a in axarr[:]], visible=False)
# # plt.xlim(0,1)
# # plt.ylim(0,1)
# plt.show()



## ---- now test the two alphas on the test set ---- ## 
# skaiontest = skaionnumpy[576167:,:]

# predicted_labels_pvt_test = {}
# for alpha in (alpha_roc, alpha_pvc) :
# 	temp_y = {}
# 	for i in range( skaionnumpy.shape[0] - 576167 ):
# 		if skaiontrain[ i, 4]>alpha:
# 			temp_y[i]=1
# 		else:
# 			temp_y[i] = 0
# 	predicted_labels_pvt_test[alpha] = temp_y

# confusion_test_pvt = {}
# for alpha, pred_y in predicted_labels_pvt_test.items():
# 	pred_y_list =  [pred_y[i] for i in sorted(pred_y.keys())] # list(pred_y.values()) changed b/c RAB's not sure .values() gives the list sorted by keys which is what i think you're assuming. 
# 	confusion_test_pvt[alpha] = make_cm(list(skaiontest[:,7]), pred_y_list) # true and then predicted


# print "\n Test Results are ..."
# print "\n alpha_roc = %s" %alpha_roc
# for key, v in confusion_test_pvt[alpha_roc].iteritems(): 
# 	print "\t", key, v 
# print "\n alpha_pvc = %s" %alpha_pvc
# for key,v in confusion_test_pvt[alpha_pvc].iteritems():
# 	print "\t", key, v


########
# confusion = {}
# for alpha, pred_y in predicted_labels_pvt.items():
#     pred_y_list = list(pred_y.values())
#     confusion[alpha]=make_cm(list(skaionnumpy[:,7]), pred_y_list) # true and then predicted

#            ...
#jsonify(confusion, 'confusioin matricies.json')

# Now we need to compute some rates. 

# for just the training data
# rates_train = {}
# for alpha, matrix in confusion_train_pvt.items():
# 	true_negative = matrix[0,0]
# 	false_positive = matrix[0,1]
# 	false_negative = matrix[1,0]
# 	true_positive = matrix[1,1]
# 	actual_positive = true_positive + false_negative
# 	actual_negative = false_positive + true_negative
# 	predicted_positive = true_positive + false_positive
# 	predicted_negative = true_negative + false_negative

# 	tpr = true_positive/float(actual_positive)
# 	fpr = false_positive/float(actual_negative)
# 	tnr = true_negative/float(actual_negative)
# 	fnr = false_negative/float(actual_positive)
# 	ppv = true_positive/float(predicted_positive)
# 	npv = true_negative/float(predicted_negative)

# 	rates_train[alpha] = {"TPR":tpr, "FPR":fpr, "TNR": tnr, "FNR": fnr, "PPV":ppv, "NPV": npv}


# # for the whole data set
# rates = {}
# for alpha, matrix in confusion.items():
#     true_negative = matrix[0,0]
#     false_positive = matrix[0,1]
#     false_negative = matrix[1,0]
#     true_positive = matrix[1,1]
#     actual_positive = true_positive + false_negative
#     actual_negative = false_positive + true_negative
#     predicted_positive = true_positive + false_positive
#     predicted_negative = true_negative + false_negative

#     tpr = true_positive/float(actual_positive)
#     fpr = false_positive/float(actual_negative)
#     tnr = true_negative/float(actual_negative)
#     fnr = false_negative/float(actual_positive)
#     ppv = true_positive/float(predicted_positive)
#     npv = true_negative/float(predicted_negative)

#     rates[alpha] = {"TPR":tpr, "FPR":fpr, "TNR": tnr, "FNR": fnr, "PPV":ppv, "NPV": npv}




# def alpha_selector(rates, curve = 'roc', plot = False):
# 	"""
# 	Input:
# 		rates = dict of form {alphas : {dict of rates}}
# 		curve = 'roc' or 'ppv' or 'both', default is roc
# 		plot = False, whether to plot or not
# 	Output:
# 		the alpha that is the best threshold for the curve type
# 		plot (if plot = True)
# 	"""
# 	if curve == 'roc':
# 		temprates = rates # don't want to remove things from the input list, just in case
# 		nan_alphas = [alpha for alpha in list(rates.keys()) if np.isnan(list(rates[alpha].values())).any()]
# 		for alpha in nan_alphas:
# 			del temprates[alpha] # let's get rid of any nan's
# 		points_of_interest = [(rates[alpha]["FPR"], rates[alpha]["TPR"]) for alpha in list(rates.keys())] # here we want the tpr vs fpr
# 		ideal = (0,1) # point on ROC if perfect classification
# 		closest_point = min(points_of_interest, key=lambda x:distance(x,ideal))
# 		smallest_distance = distance(closest_point, ideal) # what is the closest we can get?
# 		best_alphas = [alpha for alpha in list(rates.keys()) if distance(ideal, (rates[alpha]["FPR"], rates[alpha]["TPR"]) ) == smallest_distance]

# 		if plot:
# 			x_r = [rates[alpha]["FPR"] for alpha in list(temprates.keys())]
# 			y_r = [rates[alpha]["TPR"] for alpha in list(temprates.keys())]
# 			plt.scatter(x_r, y_r, c = list(temprates.keys()), alpha = 0.5)
# 			plt.xlim(-0.01, max(x_r)+0.01)
# 			plt.ylim(-0.1, 1.1)
# 			plt.xlabel('False Positive Rate')
# 			plt.xticks(np.arange(0, max(x_r)+0.01, 0.01), np.arange(0, max(x_r)+0.01, 0.01), rotation = 'vertical')
# 			plt.ylabel('True Positive Rate')
# 			plt.yticks(np.arange(0, 1.01, 0.1))
# 			plt.title("ROC Curve, Skaion Training Data")
# 			plt.show(block=False)

# 		return best_alphas

# 	if curve == 'pvc':
# 		temprates = rates
# 		nan_alphas = [alpha for alpha in list(rates.keys()) if np.isnan(list(rates[alpha].values())).any()]
# 		for alpha in nan_alphas:
# 			del temprates[alpha]
# 		points_of_interest = [(temprates[alpha]["NPV"], temprates[alpha]["PPV"]) for alpha in list(temprates.keys())]
# 		ideal = (1,1) # point on ROC if perfect classification
# 		closest_point = min(points_of_interest, key=lambda x:distance(x,ideal))
# 		smallest_distance = distance(closest_point, ideal)
# 		best_alphas = [alpha for alpha in list(rates.keys()) if distance(ideal, (rates[alpha]["NPV"], rates[alpha]["PPV"]) ) == smallest_distance]

		

# 		if plot:
# 			x_p = [rates[alpha]["NPV"] for alpha in list(temprates.keys())]
# 			x_p_rounded =[np.round(rates[alpha]["NPV"],4) for alpha in list(temprates.keys())]
# 			y_p = [rates[alpha]["PPV"] for alpha in list(temprates.keys())]
# 			plt.scatter(x_p, y_p, c = list(temprates.keys()), alpha = 0.5)
# 			plt.xticks(x_p_rounded, x_p_rounded, rotation = 'vertical')
# 			plt.xlim(min(x_p)-0.01, max(x_p)+0.01)
# 			plt.ylim(-0.05, max(y_p)+0.01)
# 			#plt.yticks(np.arange(0, 1.1, 0.1))
# 			plt.xlabel('Negative Predictive Value')
# 			plt.ylabel('Positive Predictive Value')
# 			plt.title("Predictive Value Curve, Skaion Training Data")
# 			plt.show(block=False)

# 		return best_alphas

# 	if curve == 'both':
# 		temprates = rates
# 		nan_alphas = [alpha for alpha in list(rates.keys()) if np.isnan(list(rates[alpha].values())).any()]
# 		for alpha in nan_alphas:
# 			del temprates[alpha]
# 		points_of_interest_r = [(rates[alpha]["FPR"], rates[alpha]["TPR"]) for alpha in list(rates.keys())]
# 		ideal_r = (0,1) # point on ROC if perfect classification
# 		closest_point_r = min(points_of_interest_r, key=lambda x:distance(x,ideal_r))
# 		smallest_distance_r = distance(closest_point_r, ideal_r)
# 		best_alphas_r = [alpha for alpha in list(rates.keys()) if\
# 		 distance(ideal_r, (rates[alpha]["FPR"], rates[alpha]["TPR"]) ) == smallest_distance_r]
# 		points_of_interest_p = [(temprates[alpha]["NPV"], temprates[alpha]["PPV"]) for alpha in list(temprates.keys())]
# 		ideal_p = (1,1) # point on ROC if perfect classification
# 		closest_point_p = min(points_of_interest_p, key=lambda x:distance(x,ideal_p))
# 		smallest_distance_p = distance(closest_point_p, ideal_p)
# 		best_alphas_p = [alpha for alpha in list(rates.keys()) if\
# 		 distance(ideal_p, (rates[alpha]["NPV"], rates[alpha]["PPV"]) ) == smallest_distance_p]

# 		if plot:
# 			x_r = [rates[alpha]["FPR"] for alpha in list(temprates.keys())]
# 			y_r = [rates[alpha]["TPR"] for alpha in list(temprates.keys())]
# 			x_p = [rates[alpha]["NPV"] for alpha in list(temprates.keys())]
# 			y_p = [rates[alpha]["PPV"] for alpha in list(temprates.keys())]
# 			f, (ax_1, ax_2) = plt.subplots(1, 2, sharey = True)
# 			ax_1.scatter(x_r, y_r, c = list(temprates.keys()), alpha = 0.5)
# 			ax_1.set_title("ROC Curve, Skaion Training Data")
# 			ax_1.set_xlabel('False Positive Rate')
# 			ax_1.set_ylabel('True Positive Rate')
# 			ax_2.scatter(x_p, y_p, c = list(temprates.keys()), alpha = 0.5)
# 			ax_2.set_title("Predictive Value Curve, Skaion Training Data")
# 			ax_2.set_xlabel('Negative Predictive Value')
# 			ax_2.set_ylabel('Positive Predictive Value')
# 			plt.show(block=False)

# 		return best_alphas_r, best_alphas_p

# best_alphas_r, best_alphas_p = alpha_selector(rates, curve = 'both', plot = True)

# best_alphas_r = alpha_selector(rates_train, curve = 'roc', plot = True)
# best_alphas_p = alpha_selector(rates_train, curve = 'pv', plot = True)


# # avg_alpha_r = sum(best_alphas_r)/len(best_alphas_r) # in case there's multiple alpha values

# # avg_alpha_p = sum(best_alphas_p)/len(best_alphas_p)

# #  Now that we have an alpha that works, let's do the testing with that alpha.

# X_test_pvt = skaiontest[:,4]
# y_test_pvt = skaiontest[:,7]

# testing_probs = {} 
# for i,(x, y) in enumerate(zip(X_test, y_test)): 
# 	testing_probs[i] = {'x': x.tolist(), "y": y.tolist()}
# 	x_ = np.concatenate( [np.array( [ 1] ), x], axis = 0 )
# 	p = sigma(np.dot(x_ , v ) ) ## probability that x is in class 1. 
# 	testing_probs[i]['p'] = p  # Here we used the testing set


# testing_outcomes = {"ROC alpha": {"value":avg_alpha_r}, "PVC alpha": {"value":avg_alpha_p}}

# for label, label_value in testing_outcomes.items(): 
# 	temp_y = {}
# 	for i,d in testing_probs.items(): 
# 		if d['p']> label_value["value"]:
# 			temp_y[i] = 1
# 		else:
# 			temp_y[i] = 0
# 	testing_outcomes[label]["Predicted Labels"] = temp_y

# #skaion

# for label, label_value in testing_outcomes.items(): 
# 	temp_y = {}
# 	for i in range(len(list(X_test_pvt))):
# 		if X_test_pvt[i]> label_value["value"]:
# 			temp_y[i] = 1
# 		else:
# 			temp_y[i] = 0
# 	testing_outcomes[label]["Predicted Labels"] = temp_y

# testing_outcomes["ROC alpha"]["Confusion Matrix"] = make_cm(list(y_test_pvt), list(testing_outcomes["ROC alpha"]["Predicted Labels"].values()))
# testing_outcomes["PVC alpha"]["Confusion Matrix"] = make_cm(list(y_test_pvt), list(testing_outcomes["PVC alpha"]["Predicted Labels"].values()))

# for label, label_value in testing_outcomes.items():
# 	matrix = label_value['Confusion Matrix']
# 	true_negative = matrix[0,0]
# 	false_positive = matrix[0,1]
# 	false_negative = matrix[1,0]
# 	true_positive = matrix[1,1]
# 	actual_positive = true_positive + false_negative
# 	actual_negative = false_positive + true_negative
# 	predicted_positive = true_positive + false_positive
# 	predicted_negative = true_negative + false_negative

# 	tpr = true_positive/float(actual_positive)
# 	fpr = false_positive/float(actual_negative)
# 	tnr = true_negative/float(actual_negative)
# 	fnr = false_negative/float(actual_positive)
# 	ppv = true_positive/float(predicted_positive)
# 	npv = true_negative/float(predicted_negative)

# 	label_value["rates"]={"TPR":tpr, "FPR":fpr, "TNR": tnr, "FNR": fnr, "PPV":ppv, "NPV": npv}
# 	print(label_value['rates'])