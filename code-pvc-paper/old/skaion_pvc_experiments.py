import os, datetime
import numpy as np 
from matplotlib import pyplot as plt 

import SkaionOperations as sop
import SkaionReader as sr
import SkaionReader as sr


## ---- functions ---- ## 
def cm(truth, predicted): 
	""" 
	input:	- truth = list of 1 and 0 s. This gives the groundtruth classification 
			- predicted = list of 1 and 0s. this gives the predicted classification 
		NOTE: truth, predicted should be the same length!
	output: - metrics = {tp: count of true positives, fp: , ...}
			computes and stores the number of true positives (TP), etc. and computes recall (TPR), false positive rate (FPR), precision (PPV), and neg. predictive value (NPV)
	""" 
	if len(truth) != len(predicted): 
		print "input lists have different lenghts (must be same)"
		return 
	l = zip(truth, predicted)
	tp = len([(t,p) for t,p in l if t==1 and p ==1 ] )
	fp = len([(t,p) for t,p in l if t==0 and p ==1 ] )
	tn = len([(t,p) for t,p in l if t==0 and p ==0 ] )
	fn = len([(t,p) for t,p in l if t==1 and p ==0 ] )
	tpr = float(tp) / float( tp + fn)
	fpr = float(fp) / float( fp + tn)
	ppv = float(tp) / float( tp + fp)
	npv = float(tn) / float( tn + fn)

	metrics = {
				'tp': tp, 
				'fp': fp, 
				'tn': tn, 
				'fn': fn, 
				'tpr': tpr, 
				'fpr': fpr, 
				'ppv': ppv, 
				'npv': npv
	}
	return metrics 


def distance(x,y):
	return ((x[0]-y[0])**2 + (x[1]-y[1])**2)**0.5



## ---- load the data ---- ## 
print '\n\nloading data ... '
# skaion = sop.DataBase.PopulateDataBase('/Users/ikj/Downloads/new_skaion.txt')
skaion = sop.DataBase.PopulateDataBase( os.path.join( os.path.join( os.path.join( os.getcwd(), '..'), "data-skaion") , 'new_skaion.txt'))

srcIPs=[]
destIPs=[]
times = []
scores = []
pcrt = []
ppt = []
bppt = []

count = 0
for i in range(len(skaion.dataList)):
	srcIPs.append(skaion.dataList[i].srcIP)
	destIPs.append(skaion.dataList[i].dstIP)
	times.append(skaion.dataList[i].time)
	scores.append(skaion.dataList[i].score)
	pcrt.append(skaion.dataList[i].scoreData.Pcr_T)
	ppt.append(skaion.dataList[i].scoreData.PrivPorts_T)
	bppt.append(skaion.dataList[i].scoreData.BytesPerPacket_T)
	count = count+1
	if count % 1000 == 0:
		print"\t %s" %(count)

derp = np.vstack((srcIPs, destIPs, times, scores, pcrt, ppt, bppt, np.zeros(909179)))

skaionnumpy = np.transpose(derp)



## ---- apply labels ---- ## 
print "\n\napplying labels to data ... "
attackstart = datetime.datetime(2006, 10, 1, 21, 9, 30, 0)

# attacks
for i in range(909179):
	if skaionnumpy[i,0] == '34.190.45.188':
		d=skaionnumpy[i,2]-attackstart
		if d.days >= 0 and d.seconds >= 0: ## should be or?
			skaionnumpy[i,7] = 1
	if skaionnumpy[i,1] == '34.190.45.188': ## should be or?
		d=skaionnumpy[i,2]-attackstart
		if d.days >= 0 and d.seconds >= 0:
			skaionnumpy[i,7] = 1

## ---- training set labels ---- ## 
skaiontrain = skaionnumpy[:576167:, :]
skaiontest = skaionnumpy[576167:,:]


alpha_list = np.arange(.39, .41, .001)

predicted_labels_pcrt_train = {}
for alpha in alpha_list :
	temp_y = {}
	for i in range(576167):
		if skaiontrain[i,4]>alpha:
			temp_y[i]=1
		else:
			temp_y[i] = 0
	predicted_labels_pcrt_train[alpha] = temp_y

# #want to use the whole matrix?
# predicted_labels_pcrt = {}
# for alpha in np.arange(0,1.01, .01):
#     temp_y = {}
#     for i in range(909179):
#         if skaionnumpy[i,4]>alpha:
#             temp_y[i]=1
#         else:
#             temp_y[i] = 0
#     predicted_labels_pcrt[alpha] = temp_y

confusion_train_pcrt = {}
for alpha, pred_y in predicted_labels_pcrt_train.items():
	if alpha >0 and alpha <1: 
		pred_y_list =  [pred_y[i] for i in sorted(pred_y.keys())] # list(pred_y.values()) changed b/c RAB's not sure .values() gives the list sorted by keys which is what i think you're assuming. 
		confusion_train_pcrt[alpha] = cm(list(skaiontrain[:,7]), pred_y_list[:576167]) # true and then predicted


## ---- find optimal alphas ---- ## 
print "\n\nfinding optimal alphas ... "
roc = {alpha: (confusion_train_pcrt[alpha]['fpr'], confusion_train_pcrt[alpha]['tpr']) for alpha in alpha_list}
pvc = {alpha: (confusion_train_pcrt[alpha]['npv'], confusion_train_pcrt[alpha]['ppv']) for alpha in alpha_list}

alpha_roc = sorted( alpha_list, key = lambda alpha: distance( (0.,1.), roc[alpha] ) )[0] 
print "\n\t ROC-optimal alpha = %s with distance to (0,1) of %s " %( alpha_roc, distance((0.,1.), roc[alpha_roc]))
print confusion_train_pcrt[alpha_roc]
alpha_pvc = sorted( alpha_list, key = lambda alpha: distance( (1.,1.), pvc[alpha] ) )[0]
print "\n\t PVC-optimal alpha = %s with distance to (1,1) of %s " %( alpha_pvc, distance((1.,1.), pvc[alpha_pvc]))
print confusion_train_pcrt[alpha_pvc]

print "\n There are %s less false positives w/ pvc-optimal threshold " %(confusion_train_pcrt[alpha_roc]['fp'] - confusion_train_pcrt[alpha_pvc]['fp'] )



## ---- plot ROC and PPV ---- ##
print "\n\n plottin ... "
tprs = [ confusion_train_pcrt[alpha]['tpr'] for alpha in alpha_list]
fprs = [ confusion_train_pcrt[alpha]['fpr'] for alpha in alpha_list]
ppvs = [ confusion_train_pcrt[alpha]['ppv'] for alpha in alpha_list]
npvs = [ confusion_train_pcrt[alpha]['npv'] for alpha in alpha_list]

# Four axes, returned as a 2-d array
f, axarr = plt.subplots(1, 2)
axarr[0].plot(fprs, tprs)
axarr[0].plot(roc[alpha_roc][0], roc[alpha_roc][1], 'ro' )
axarr[0].set_title('ROC')

axarr[1].scatter(npvs, ppvs)
axarr[1].plot(pvc[alpha_pvc][0], pvc[alpha_pvc][1], 'ro' )
axarr[1].set_title('PVC')
# Fine-tune figure; hide x ticks for top plots and y ticks for right plots
# plt.setp([a.get_yticklabels() for a in axarr[:]], visible=False)
# plt.xlim(0,1)
# plt.ylim(0,1)
plt.show()



## ---- now test the two alphas on the test set ---- ## 
skaiontest = skaionnumpy[576167:,:]

predicted_labels_pcrt_test = {}
for alpha in (alpha_roc, alpha_pvc) :
	temp_y = {}
	for i in range( skaionnumpy.shape[0] - 576167 ):
		if skaiontrain[i,4]>alpha:
			temp_y[i]=1
		else:
			temp_y[i] = 0
	predicted_labels_pcrt_test[alpha] = temp_y

confusion_test_pcrt = {}
for alpha, pred_y in predicted_labels_pcrt_test.items():
	pred_y_list =  [pred_y[i] for i in sorted(pred_y.keys())] # list(pred_y.values()) changed b/c RAB's not sure .values() gives the list sorted by keys which is what i think you're assuming. 
	confusion_test_pcrt[alpha] = cm(list(skaiontest[:,7]), pred_y_list) # true and then predicted




print "\n Test Results are ..."
print "\n alpha_roc = %s" %alpha_roc
for key, v in confusion_test_pcrt[alpha_roc].iteritems(): 
	print "\t", key, v 
print "\n alpha_pvc = %s" %alpha_pvc
for key,v in confusion_test_pcrt[alpha_pvc].iteritems():
	print "\t", key, v


########
# confusion = {}
# for alpha, pred_y in predicted_labels_pcrt.items():
#     pred_y_list = list(pred_y.values())
#     confusion[alpha]=cm(list(skaionnumpy[:,7]), pred_y_list) # true and then predicted

#            ...
#jsonify(confusion, 'confusioin matricies.json')

# Now we need to compute some rates. 

# for just the training data
# rates_train = {}
# for alpha, matrix in confusion_train_pcrt.items():
# 	true_negative = matrix[0,0]
# 	false_positive = matrix[0,1]
# 	false_negative = matrix[1,0]
# 	true_positive = matrix[1,1]
# 	actual_positive = true_positive + false_negative
# 	actual_negative = false_positive + true_negative
# 	predicted_positive = true_positive + false_positive
# 	predicted_negative = true_negative + false_negative

# 	tpr = true_positive/float(actual_positive)
# 	fpr = false_positive/float(actual_negative)
# 	tnr = true_negative/float(actual_negative)
# 	fnr = false_negative/float(actual_positive)
# 	ppv = true_positive/float(predicted_positive)
# 	npv = true_negative/float(predicted_negative)

# 	rates_train[alpha] = {"TPR":tpr, "FPR":fpr, "TNR": tnr, "FNR": fnr, "PPV":ppv, "NPV": npv}


# # for the whole data set
# rates = {}
# for alpha, matrix in confusion.items():
#     true_negative = matrix[0,0]
#     false_positive = matrix[0,1]
#     false_negative = matrix[1,0]
#     true_positive = matrix[1,1]
#     actual_positive = true_positive + false_negative
#     actual_negative = false_positive + true_negative
#     predicted_positive = true_positive + false_positive
#     predicted_negative = true_negative + false_negative

#     tpr = true_positive/float(actual_positive)
#     fpr = false_positive/float(actual_negative)
#     tnr = true_negative/float(actual_negative)
#     fnr = false_negative/float(actual_positive)
#     ppv = true_positive/float(predicted_positive)
#     npv = true_negative/float(predicted_negative)

#     rates[alpha] = {"TPR":tpr, "FPR":fpr, "TNR": tnr, "FNR": fnr, "PPV":ppv, "NPV": npv}




# def alpha_selector(rates, curve = 'roc', plot = False):
# 	"""
# 	Input:
# 		rates = dict of form {alphas : {dict of rates}}
# 		curve = 'roc' or 'ppv' or 'both', default is roc
# 		plot = False, whether to plot or not
# 	Output:
# 		the alpha that is the best threshold for the curve type
# 		plot (if plot = True)
# 	"""
# 	if curve == 'roc':
# 		temprates = rates # don't want to remove things from the input list, just in case
# 		nan_alphas = [alpha for alpha in list(rates.keys()) if np.isnan(list(rates[alpha].values())).any()]
# 		for alpha in nan_alphas:
# 			del temprates[alpha] # let's get rid of any nan's
# 		points_of_interest = [(rates[alpha]["FPR"], rates[alpha]["TPR"]) for alpha in list(rates.keys())] # here we want the tpr vs fpr
# 		ideal = (0,1) # point on ROC if perfect classification
# 		closest_point = min(points_of_interest, key=lambda x:distance(x,ideal))
# 		smallest_distance = distance(closest_point, ideal) # what is the closest we can get?
# 		best_alphas = [alpha for alpha in list(rates.keys()) if distance(ideal, (rates[alpha]["FPR"], rates[alpha]["TPR"]) ) == smallest_distance]

# 		if plot:
# 			x_r = [rates[alpha]["FPR"] for alpha in list(temprates.keys())]
# 			y_r = [rates[alpha]["TPR"] for alpha in list(temprates.keys())]
# 			plt.scatter(x_r, y_r, c = list(temprates.keys()), alpha = 0.5)
# 			plt.xlim(-0.01, max(x_r)+0.01)
# 			plt.ylim(-0.1, 1.1)
# 			plt.xlabel('False Positive Rate')
# 			plt.xticks(np.arange(0, max(x_r)+0.01, 0.01), np.arange(0, max(x_r)+0.01, 0.01), rotation = 'vertical')
# 			plt.ylabel('True Positive Rate')
# 			plt.yticks(np.arange(0, 1.01, 0.1))
# 			plt.title("ROC Curve, Skaion Training Data")
# 			plt.show(block=False)

# 		return best_alphas

# 	if curve == 'pvc':
# 		temprates = rates
# 		nan_alphas = [alpha for alpha in list(rates.keys()) if np.isnan(list(rates[alpha].values())).any()]
# 		for alpha in nan_alphas:
# 			del temprates[alpha]
# 		points_of_interest = [(temprates[alpha]["NPV"], temprates[alpha]["PPV"]) for alpha in list(temprates.keys())]
# 		ideal = (1,1) # point on ROC if perfect classification
# 		closest_point = min(points_of_interest, key=lambda x:distance(x,ideal))
# 		smallest_distance = distance(closest_point, ideal)
# 		best_alphas = [alpha for alpha in list(rates.keys()) if distance(ideal, (rates[alpha]["NPV"], rates[alpha]["PPV"]) ) == smallest_distance]

		

# 		if plot:
# 			x_p = [rates[alpha]["NPV"] for alpha in list(temprates.keys())]
# 			x_p_rounded =[np.round(rates[alpha]["NPV"],4) for alpha in list(temprates.keys())]
# 			y_p = [rates[alpha]["PPV"] for alpha in list(temprates.keys())]
# 			plt.scatter(x_p, y_p, c = list(temprates.keys()), alpha = 0.5)
# 			plt.xticks(x_p_rounded, x_p_rounded, rotation = 'vertical')
# 			plt.xlim(min(x_p)-0.01, max(x_p)+0.01)
# 			plt.ylim(-0.05, max(y_p)+0.01)
# 			#plt.yticks(np.arange(0, 1.1, 0.1))
# 			plt.xlabel('Negative Predictive Value')
# 			plt.ylabel('Positive Predictive Value')
# 			plt.title("Predictive Value Curve, Skaion Training Data")
# 			plt.show(block=False)

# 		return best_alphas

# 	if curve == 'both':
# 		temprates = rates
# 		nan_alphas = [alpha for alpha in list(rates.keys()) if np.isnan(list(rates[alpha].values())).any()]
# 		for alpha in nan_alphas:
# 			del temprates[alpha]
# 		points_of_interest_r = [(rates[alpha]["FPR"], rates[alpha]["TPR"]) for alpha in list(rates.keys())]
# 		ideal_r = (0,1) # point on ROC if perfect classification
# 		closest_point_r = min(points_of_interest_r, key=lambda x:distance(x,ideal_r))
# 		smallest_distance_r = distance(closest_point_r, ideal_r)
# 		best_alphas_r = [alpha for alpha in list(rates.keys()) if\
# 		 distance(ideal_r, (rates[alpha]["FPR"], rates[alpha]["TPR"]) ) == smallest_distance_r]
# 		points_of_interest_p = [(temprates[alpha]["NPV"], temprates[alpha]["PPV"]) for alpha in list(temprates.keys())]
# 		ideal_p = (1,1) # point on ROC if perfect classification
# 		closest_point_p = min(points_of_interest_p, key=lambda x:distance(x,ideal_p))
# 		smallest_distance_p = distance(closest_point_p, ideal_p)
# 		best_alphas_p = [alpha for alpha in list(rates.keys()) if\
# 		 distance(ideal_p, (rates[alpha]["NPV"], rates[alpha]["PPV"]) ) == smallest_distance_p]

# 		if plot:
# 			x_r = [rates[alpha]["FPR"] for alpha in list(temprates.keys())]
# 			y_r = [rates[alpha]["TPR"] for alpha in list(temprates.keys())]
# 			x_p = [rates[alpha]["NPV"] for alpha in list(temprates.keys())]
# 			y_p = [rates[alpha]["PPV"] for alpha in list(temprates.keys())]
# 			f, (ax_1, ax_2) = plt.subplots(1, 2, sharey = True)
# 			ax_1.scatter(x_r, y_r, c = list(temprates.keys()), alpha = 0.5)
# 			ax_1.set_title("ROC Curve, Skaion Training Data")
# 			ax_1.set_xlabel('False Positive Rate')
# 			ax_1.set_ylabel('True Positive Rate')
# 			ax_2.scatter(x_p, y_p, c = list(temprates.keys()), alpha = 0.5)
# 			ax_2.set_title("Predictive Value Curve, Skaion Training Data")
# 			ax_2.set_xlabel('Negative Predictive Value')
# 			ax_2.set_ylabel('Positive Predictive Value')
# 			plt.show(block=False)

# 		return best_alphas_r, best_alphas_p

# best_alphas_r, best_alphas_p = alpha_selector(rates, curve = 'both', plot = True)

# best_alphas_r = alpha_selector(rates_train, curve = 'roc', plot = True)
# best_alphas_p = alpha_selector(rates_train, curve = 'pcr', plot = True)


# # avg_alpha_r = sum(best_alphas_r)/len(best_alphas_r) # in case there's multiple alpha values

# # avg_alpha_p = sum(best_alphas_p)/len(best_alphas_p)

# #  Now that we have an alpha that works, let's do the testing with that alpha.

# X_test_pcrt = skaiontest[:,4]
# y_test_pcrt = skaiontest[:,7]

# testing_probs = {} 
# for i,(x, y) in enumerate(zip(X_test, y_test)): 
# 	testing_probs[i] = {'x': x.tolist(), "y": y.tolist()}
# 	x_ = np.concatenate( [np.array( [ 1] ), x], axis = 0 )
# 	p = sigma(np.dot(x_ , v ) ) ## probability that x is in class 1. 
# 	testing_probs[i]['p'] = p  # Here we used the testing set


# testing_outcomes = {"ROC alpha": {"value":avg_alpha_r}, "PVC alpha": {"value":avg_alpha_p}}

# for label, label_value in testing_outcomes.items(): 
# 	temp_y = {}
# 	for i,d in testing_probs.items(): 
# 		if d['p']> label_value["value"]:
# 			temp_y[i] = 1
# 		else:
# 			temp_y[i] = 0
# 	testing_outcomes[label]["Predicted Labels"] = temp_y

# #skaion

# for label, label_value in testing_outcomes.items(): 
# 	temp_y = {}
# 	for i in range(len(list(X_test_pcrt))):
# 		if X_test_pcrt[i]> label_value["value"]:
# 			temp_y[i] = 1
# 		else:
# 			temp_y[i] = 0
# 	testing_outcomes[label]["Predicted Labels"] = temp_y

# testing_outcomes["ROC alpha"]["Confusion Matrix"] = cm(list(y_test_pcrt), list(testing_outcomes["ROC alpha"]["Predicted Labels"].values()))
# testing_outcomes["PVC alpha"]["Confusion Matrix"] = cm(list(y_test_pcrt), list(testing_outcomes["PVC alpha"]["Predicted Labels"].values()))

# for label, label_value in testing_outcomes.items():
# 	matrix = label_value['Confusion Matrix']
# 	true_negative = matrix[0,0]
# 	false_positive = matrix[0,1]
# 	false_negative = matrix[1,0]
# 	true_positive = matrix[1,1]
# 	actual_positive = true_positive + false_negative
# 	actual_negative = false_positive + true_negative
# 	predicted_positive = true_positive + false_positive
# 	predicted_negative = true_negative + false_negative

# 	tpr = true_positive/float(actual_positive)
# 	fpr = false_positive/float(actual_negative)
# 	tnr = true_negative/float(actual_negative)
# 	fnr = false_negative/float(actual_positive)
# 	ppv = true_positive/float(predicted_positive)
# 	npv = true_negative/float(predicted_negative)

# 	label_value["rates"]={"TPR":tpr, "FPR":fpr, "TNR": tnr, "FNR": fnr, "PPV":ppv, "NPV": npv}
# 	print(label_value['rates'])