## simulation.py 
 
import numpy as np 
import os
from collections import defaultdict
import matplotlib.pyplot as plt

from generalFunctions import *

## ---- functions ---- ## 
def make_cm(truth, predicted): 
	""" 
	input:	- truth = list of 1 and 0 s. This gives the groundtruth classification 
			- predicted = list of 1 and 0s. this gives the predicted classification 
		NOTE: truth, predicted should be the same length!
	output: - metrics = {tp: count of true positives, fp: , ...}
			computes and stores the number of true positives (TP), etc. and computes recall (TPR), false positive rate (FPR), precision (PPV), and neg. predictive value (NPV)
	""" 
	if len(truth) != len(predicted): 
		print "input lists have different lenghts (must be same)"
		return 
	l = zip(truth, predicted)
	tp = len([(t,p) for t,p in l if t==1 and p ==1 ] )
	fp = len([(t,p) for t,p in l if t==0 and p ==1 ] )
	tn = len([(t,p) for t,p in l if t==0 and p ==0 ] )
	fn = len([(t,p) for t,p in l if t==1 and p ==0 ] )
	tpr = float(tp) / float( tp + fn)
	fpr = float(fp) / float( fp + tn)
	ppv = float(tp) / float( tp + fp)
	npv = float(tn) / float( tn + fn)

	metrics = {
				'tp': tp, 
				'fp': fp, 
				'tn': tn, 
				'fn': fn, 
				'tpr': tpr, 
				'fpr': fpr, 
				'ppv': ppv, 
				'npv': npv
	}
	return metrics 


def weighted_norm(x, w = False):
	"""
	Input: x, w both 0-d or 1-d arrays of same length. w_i should all be positive. 
			Note, if w is not passed it will just be unweighted ( w_i = 1)
	Output: float. returns the norm of x weighted by w ( sum(x_i**2 * w_i) ^.5)
	
	"""
	if not isinstance(w, np.ndarray): 
		w = np.ones_like(x)
	return np.dot( x*x , w )**(.5)


def objective_funciton(cm): 
	tot = float(cm['tp'] + cm['fp'] + cm['tn'] + cm['fn'])
	pos = float(cm['tp'] + cm['fn'])
	neg = float(cm['tn'] + cm['fp'])	
	return (cm['tp'] - cm['fp'] ) * tot/pos + (cm['tn'] - cm['fn'])*tot/neg

## ---- paths ---- ## 
pvcfolder = os.path.abspath(os.path.join(os.path.join(os.getcwd(), '..'), 'data-pvc-paper') )
if not os.path.isdir(pvcfolder): 
	os.mkdir(pvcfolder)

simfolder = os.path.join(pvcfolder, 'simulations')
if not os.path.isdir(simfolder): 
	os.mkdir(simfolder)

## ---- weights and optimal points ---- ##
roc_best = np.array([0., 1.])
pvc_best = np.array([0., 1., 1., 1.])

# w = np.array([1,5,1,5])

## ---- run the simulations and results ---- ## 

micro_average_roc = defaultdict(int) ## to be populated from each run 
micro_average_pvc = defaultdict(int)

for run in xrange(10):
	print "\n Beginning Run %s" %run
	runpath = os.path.join(simfolder, 'run-%s' %run)
	if not os.path.isdir(runpath):
		os.mkdir(runpath)
	
	## ---- generate some data ---- ##
	negatives = np.concatenate( ( np.random.normal(0,1, 1000), np.random.normal(-1, .4, 50), np.random.normal(1, .4, 50)), axis = 0)
	positives = np.concatenate( (np.random.normal(0,1,10), np.random.normal(1.3, .4, 100), np.random.normal(-1.3, .4, 100)) , axis = 0) 
	## add labels: 
	negatives = zip(negatives, np.zeros_like(negatives) )
	positives = zip(positives, np.ones_like(positives))
	data = negatives + positives
	np.random.shuffle( data )
	columns = ['data', 'label', 'prediction ']
	data = np.array(data)

	k = int( data.shape[0] * .8 )

	train = data[:k, :]
	test = data[k:, :]

	# k_n = np.sum(np.where(train[:,-1] == 0, 1, 0 ) )
	# k_p = np.sum(np.where(train[:,-1] == 1, 1, 0 ) )

	jsonify(train.tolist(), os.path.join(runpath, 'train.json'))
	jsonify(test.tolist(), os.path.join(runpath, 'test.json'))

	## ---- apply predictions to training set for each alpha ---- ##
	alpha_list = np.arange(.01, 2.21, .01)

	predicted_labels_train= {}
	for alpha in alpha_list :
		temp_y = {}
		for i in range( train.shape[0]):
			if np.abs( train[i, 0] ) > alpha: ## we just used distance to 0 as our feature. 
				temp_y[i]=1
			else:
				temp_y[i] = 0
		predicted_labels_train[alpha] = temp_y
	jsonify(predicted_labels_train, os.path.join( runpath, 'predicted_labels_train.json'))

	confusion_train = {}
	for alpha, pred_y in predicted_labels_train.iteritems():
		pred_y_list =  [pred_y[i] for i in sorted(pred_y.keys())] # list(pred_y.values()) changed b/c RAB's not sure .values() gives the list sorted by keys which is what i think you're assuming. 
		confusion_train[alpha] = make_cm(list(train[:,1]), pred_y_list) # true and then predicted
	jsonify(confusion_train , os.path.join( runpath, 'confusion_train.json'))

	## ---- find optimal alphas ---- ## 
	print "\n\n\tfinding optimal alphas ... "
	roc = {alpha: (confusion_train[alpha]['fpr'], confusion_train[alpha]['tpr']) for alpha in alpha_list}
	pvc = {alpha: (confusion_train[alpha]['fpr'], confusion_train[alpha]['tpr'], confusion_train[alpha]['npv'], confusion_train[alpha]['ppv']) for alpha in alpha_list}
	cmc = {alpha: objective_funciton(confusion_train[alpha]) for alpha in alpha_list}
	plt.plot(alpha_list, [cmc[alpha] for alpha in alpha_list] )
	plt.show()

	alpha_roc = sorted( alpha_list, key = lambda alpha: weighted_norm( roc_best - np.array(roc[alpha] )  ) )[0] 
	print "\n\t\t ROC-optimal alpha = %s with distance to %s of %s " %( alpha_roc, roc_best, weighted_norm( roc_best -  np.array(roc[alpha_roc])) ) 
	print confusion_train[alpha_roc]
	
	alpha_pvc = sorted( alpha_list, key = lambda alpha: weighted_norm( pvc_best - np.array(pvc[alpha]) ) )[0]
	print "\n\t\t PVC-optimal alpha = %s with distance to %s of %s " %( alpha_pvc, pvc_best, weighted_norm( pvc_best - np.array(pvc[alpha_pvc])))
	print confusion_train[alpha_pvc]

	alpha_cmc = sorted(cmc.keys(), key = lambda alpha: cmc[alpha], reverse = True)[0]
	print "\n\t\t CMC-optimal alpha = %s with objective_function =  %s " %( alpha_cmc, cmc[alpha_cmc])
	print confusion_train[alpha_cmc]
	# alpha_cmc_weighted = cm[alpha]
	

	# alpha_pvc_weighted = sorted( alpha_list, key = lambda alpha: weighted_norm( pvc_best - np.array(pvc[alpha]), w ) )[0]
	# print "\n\t\t PVC-optimal alpha = %s with distance to %s of %s " %( alpha_pvc_weighted , pvc_best, weighted_norm( pvc_best - np.array(pvc[alpha_pvc_weighted]), w))
	# print confusion_train[alpha_pvc_weighted]
	# alpha_cmc_weighted = cm[alpha]

	jsonify({'alpha_roc': alpha_roc, 'alpha_pvc': alpha_pvc, 'alpha_pvc_weighted': alpha_pvc_weighted}, os.path.join(runpath, 'optimal-alphas.json'))

	## ---- plot ROC and PPV ---- ##
	# print "\n\n\t plottin ... "
	# tprs = [ confusion_train[alpha]['tpr'] for alpha in alpha_list]
	# fprs = [ confusion_train[alpha]['fpr'] for alpha in alpha_list]
	# ppvs = [ confusion_train[alpha]['ppv'] for alpha in alpha_list]
	# npvs = [ confusion_train[alpha]['npv'] for alpha in alpha_list]

	# # Four axes, returned as a 2-d array
	# f, axarr = plt.subplots(1, 2)
	# axarr[0].plot(fprs, tprs)
	# axarr[0].plot(roc[alpha_roc][0], roc[alpha_roc][1], 'ro' )
	# axarr[0].set_title('ROC')

	# axarr[1].scatter(npvs, ppvs)
	# axarr[1].plot(pvc[alpha_pvc][0], pvc[alpha_pvc][1], 'ro' )
	# axarr[1].set_title('PVC')
	# # Fine-tune figure; hide x ticks for top plots and y ticks for right plots
	# # plt.setp([a.get_yticklabels() for a in axarr[:]], visible=False)
	# # plt.xlim(0,1)
	# # plt.ylim(0,1)
	# plt.show()


	## ---- now test the two alphas on the test set ---- ## 
	predicted_labels_test = {}
	for alpha in (alpha_roc, alpha_pvc) :
		temp_y = {}
		for i in range( test.shape[0] ):
			if np.abs(test[ i, 0 ]) > alpha:
				temp_y[i] = 1
			else:
				temp_y[i] = 0
		predicted_labels_test[alpha] = temp_y
	jsonify(predicted_labels_test, os.path.join( runpath, 'predicted_labels_test.json'))


	confusion_test = {}
	for alpha, pred_y in predicted_labels_test.items():
		pred_y_list =  [pred_y[i] for i in sorted(pred_y.keys())] # list(pred_y.values()) changed b/c RAB's not sure .values() gives the list sorted by keys which is what i think you're assuming. 
		confusion_test[alpha] = make_cm(list(test[:,1]), pred_y_list) # true and then predicted
	jsonify(confusion_test , os.path.join( runpath, 'confusion_test.json'))

	# print "\n\t Test Results are ..."
	# print "\n\t alpha_roc = %s" %alpha_roc
	# for key, v in confusion_test[alpha_roc].iteritems(): 
	# 	print "\t\t", key, v 
	# print "\n\t alpha_pvc = %s" %alpha_pvc
	# for key,v in confusion_test[alpha_pvc].iteritems():
	# 	print "\t\t", key, v

	## add results to the microaveraged counts: 
	for key in ['tp', 'fp', 'tn', 'fn']:
		micro_average_pvc[key] += confusion_test[alpha_pvc][key]
		micro_average_roc[key] += confusion_test[alpha_roc][key]


## ---- compute the micro averaged results after the 10 runs ---- ## 
for d in (micro_average_roc, micro_average_pvc): 
	d['tpr'] = float(d['tp'])/(d['tp']+d['fn'])
	d['fpr'] = float(d['fp'])/(d['fp']+d['tn'])
	d['ppv'] = float(d['tp'])/(d['tp']+d['fp'])
	d['npv'] = float(d['tn'])/(d['tn']+d['fn'])

jsonify(dict(micro_average_roc), os.path.join(simfolder, 'micro_average_roc.json'))
jsonify(dict(micro_average_pvc), os.path.join(simfolder, 'micro_average_pvc.json'))	

print "\n --- Micro-averaged results across 10 simulations: --- \n"
print '\t alpha_roc results ... '
for key, v in micro_average_roc.iteritems(): 
	print "\t\t", key, v 
print "\n\t alpha_pvc results ..." 
for key, v in micro_average_pvc.iteritems():
	print "\t\t", key, v













