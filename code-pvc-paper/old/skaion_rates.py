# File: skaion_rates.py

import numpy as np 
import SkaionOperations as sop 
import SkaionReader as sr 
import datetime as dt
from tabulate import tabulate 
# If you don't have this package, just pip3 install tabulate


# First, populate the database with all the skaion data. 
# NOTE: this may take a while!
allData = sop.DataBase.PopulateDataBase("/Users/Jessie/Dropbox/PythonScratch/Jessie/skaion_code/source/new_skaion.txt")


# Let's check- how many flows are there?

total_number_of_flows = len(allData.dataList) # should be 909179

# Attack info: started at

attackdate = dt.datetime.strptime("2006-10-01T21:20:01:232489", '%Y-%m-%dT%H:%M:%S:%f')

# from IP "34.190.45.188"

# how many "attack" flows were there? Let's find any flow
# that was recorded after the attack date in which the
# srcIP or dstIP is the attacker IP.

attackdict = {}

for i in range(total_number_of_flows):
	if allData.dataList[i].srcIP == "34.190.45.188":
		diff = allData.dataList[i].time - attackdate
		if diff.days >= 0 and diff.seconds >= 0:
			attackdict[i] = allData.dataList[i]
	if allData.dataList[i].dstIP == "34.190.45.188":
		diff = allData.dataList[i].time - attackdate
		if diff.days >=0 and diff.seconds >= 0:
			attackdict[i] = allData.dataList[i]

# This gives a dictionary of all the flows that are classified
# as an attack. The number of such flows, which will be the 
# number of actual positives, is given by

number_of_attack_flows = max(list(attackdict.keys()))-min(list(attackdict.keys()))

# Now let's do anomaly detection using the three thresholds 
# in the paper.

# First, set the time interval. For the paper, it was 1 minute.

deltaTime = dt.timedelta(minutes = 1)

# Bin the data by the 1 minute intervals

binnedData = sop.BinnedDataBase(allData, deltaTime)

# What's the max number of alerts we could handle? For
# the paper, it was one per minute.

alertRate = sop.TimeRate(1, dt.timedelta(minutes = 1))

# Now let's define the three thresholds we will use. We'll use
# adaptive, 0.003, and 0.0002152

threshold_003 = sop.ThresholdType(0.003)
threshold_tiny = sop.ThresholdType(0.0002152)
threshold_adaptive = sop.ThresholdType(alertRate, deltaTime)

thresholds = [threshold_003, threshold_tiny, threshold_adaptive]
thresholds_readable = ["0.003", "0.0002152", "Adaptive"]

# now we need to run the experiment and store the results for
# each threshold. Note: the option [1, 3] indicates we wish
# to do anomaly detection using the PCR_T and PrivPorts_T scores

detection_results = {}

for threshold in thresholds:
	tmpalertcounts, tmpalertthresholds, tmpalertdata, tmpalertstatus = binnedData.GenerateAlertData(threshold, [1,3])
	detection_results[threshold] = {"Alert Counts Per Bin": tmpalertcounts, "Alert Data": tmpalertdata}
	# for each instance, how many alerts were there?
	detection_results[threshold]["Number of Alerts"] = sum(detection_results[threshold]["Alert Counts Per Bin"])

# Now let's find the number of false positives. An alarm was a false positive
# if (1) it was an alert before the time the attack started, or
# (2) it was an alert after the attack started but did not involve
# the attacker IP. Let's find all of these.

for threshold, results in detection_results.items():
	results["Confusion Matrix"] = {}
	falsealarmcount = 0
	tmpalertdata = results["Alert Data"]
	for element in tmpalertdata:
		if len(element.dataList) > 0:
			for item in element.dataList:
				diff = item.time - attackdate
				if diff.days < 0 or diff.seconds < 0:
					falsealarmcount = falsealarmcount + 1
			for item in element.dataList:
				diff = item.time - attackdate
				if diff.days >= 0 and diff.seconds >= 0:
					IPs = [item.srcIP, item.dstIP]
					if "34.190.45.188" not in IPs:
						falsealarmcount = falsealarmcount + 1
	results["Confusion Matrix"]["False Positive Count"] = falsealarmcount


# Now we have all we need to build the confusion matrices and
# calculate all the rates we need. 

# The number of actual negatives is

negatives = total_number_of_flows - number_of_attack_flows

# for each threshold, the number of true positives will be the
# total number of alerts - false alarms. Further, the number of 
# false negatives will be the number of flows that should have
# been alerted but weren't: Total actual positive - true positive
# Finally, the true negative count will be the total actual negatives
# minus the false positives.

TP = ["TP"]
TN = ["TN"]
FP = ["FP"]
FN = ["FN"]
TPR = ["TPR"]
FPR = ["FPR"]
TNR = ["TNR"]
FNR = ["FNR"]
PPV = ["PPV"]
NPV = ["NPV"]

for threshold, results in detection_results.items():
	results["Confusion Matrix"]["True Positive Count"] = results["Number of Alerts"] - results["Confusion Matrix"]["False Positive Count"]
	results["Confusion Matrix"]["False Negative Count"] = number_of_attack_flows - results["Confusion Matrix"]["True Positive Count"]
	results["Confusion Matrix"]["True Negative Count"] = negatives - results["Confusion Matrix"]["False Positive Count"]
# Now that we have the confusion matrix, we just need to combine
# these values in the right way to get the rates we need.

for threshold, results in detection_results.items():
	tp = results["Confusion Matrix"]["True Positive Count"]
	tn = results["Confusion Matrix"]["True Negative Count"]
	fp = results["Confusion Matrix"]["False Positive Count"]
	fn = results["Confusion Matrix"]["False Negative Count"]
	
	# for pretty printing a table later
	TP.append(tp)
	TN.append(tn)
	FP.append(fp)
	FN.append(fn)

	results["Rates"] = {}
	results["Rates"]["True Positive Rate"] = tp/float(number_of_attack_flows)
	results["Rates"]["False Positive Rate"] = fp/float(negatives)
	results["Rates"]["True Negative Rate"] = tn/float(negatives)
	results["Rates"]["False Negative Rate"] = fn/float(number_of_attack_flows)
	results["Rates"]["Positive Predictive Value"] = tp/float(tp+fp)
	results["Rates"]["Negative Predictive Value"] = tn/float(tn+fn)

	# for pretty printing a table later
	TPR.append(results["Rates"]["True Positive Rate"])
	FPR.append(results["Rates"]["False Positive Rate"])
	TNR.append(results["Rates"]["True Negative Rate"])
	FNR.append(results["Rates"]["False Negative Rate"])
	PPV.append(results["Rates"]["Positive Predictive Value"])
	NPV.append(results["Rates"]["Negative Predictive Value"])

# Now just for readability, let's make sure the thresholds are readable.

for i in range(len(thresholds)):
	detection_results[thresholds[i]]["Threshold"] = thresholds_readable[i]


# To print the confusion matrices, do:

for threshold, results in detection_results.items():
	print(detection_results[threshold]["Threshold"], results["Confusion Matrix"])

# To print the rates, do:

for threshold, results in detection_results.items():
	print(detection_results[threshold]["Threshold"], results["Rates"])

# Printing everything in a pretty table

print(tabulate([TP, TN, FP, FN, TPR, FPR, TNR, FNR, PPV, NPV], headers = thresholds_readable))








