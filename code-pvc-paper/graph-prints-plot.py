# temp.py 

"""
Goal is to use graphprints data to show that w/ labeled data the disparity in PVC curve and the Precision can unmask a bad model

Data points 0-149 are used for training the models 
Data points 278-302 are ground truth positives 

"""
import os
import numpy as np 
from scipy.stats import chi2

from generalFunctions import * 


# ---- paths ---- #
gp_path = os.path.join( os.path.abspath( os.path.join( os.getcwd(), '..')), 'data-graphprints')


## ---- functions ---- ## 
def p_inv_h(S): 
    """ 
    Common trick for inverting a matrix that's not invertible (on the subspace where it is invertible and making it 0 else. )

    Input: S np.array, n by n, symmetric
    Output: S's p-inverse. 
    Note here we don't use np.linalg.pinv b/c solving the eigenvalue problem is more stable using eigh when the matrix is symmetric
    So we do it manually w/ eigh. 
    """ 
    if not np.allclose(S.T, S): 
        print "S is not symmetric, nothing returned"
        return 

    values, U = np.linalg.eigh( S )  
    ## so,  S = U np.diag(values) U.T. to check: 
    # np.allclose(S , np.dot(np.dot(U, np.diag(values)), U.T ) )
    l = np.zeros_like( values )
    for i in xrange(len(values)): 
        if  values[i] > 10**-8:  ## S is PSD matrix, hence eigenvalues non-neg. anyting less than zero is a rounding error. 
            l[i] = 1./values[i]
    S_pinv = np.dot( U, np.dot( np.diag(l), U.T ) ) 
    return S_pinv


def make_cm(truth, predicted): 
    """ 
    input:  - truth = dict of form {index: 1 or 0}  This gives the groundtruth classification 
            - predicted = dict of form {index: 1 or 0} this gives the predicted classification 
        NOTE: truth, predicted should be the same length!
    output: - metrics = {tp: count of true positives, fp: , ...}
            computes and stores the number of true positives (TP), etc. and computes recall (TPR), false positive rate (FPR), precision (PPV), and neg. predictive value (NPV), and base rate = number of positive/number of samples
    """ 
    if sorted(truth.keys()) != sorted(predicted.keys()): 
        print "input dicts have different lenghts (must be same)"
        return 

    true_y = [ truth[i] for i in sorted(truth.keys()) ]
    pred_y = [ predicted[i] for i in sorted(predicted.keys())]
        
    l = zip(true_y, pred_y)
    tp = len([(t,p) for t,p in l if t==1 and p ==1 ] )
    fp = len([(t,p) for t,p in l if t==0 and p ==1 ] )
    tn = len([(t,p) for t,p in l if t==0 and p ==0 ] )
    fn = len([(t,p) for t,p in l if t==1 and p ==0 ] )
    
    if float( tp + fn) >0: 
        tpr = float(tp) / float( tp + fn)
    else: 
        tpr = 'na'
    
    if float( fp + tn ) >0: 
        fpr = float(fp) / float( fp + tn )
    else: 
        fpr = 'na'

    if float( tp + fp) > 0 : 
        ppv = float(tp) / float( tp + fp)
    else: 
        ppv = 'na'
    
    if float( tn + fn) > 0 :
        npv = float(tn) / float( tn + fn)
    else: 
        npv = 'na'

    br = float( tp + fn )/ (tp + fp + tn + fn ) ## base rate
    
    metrics = {
                'tp': tp, 
                'fp': fp, 
                'tn': tn, 
                'fn': fn, 
                'tpr': tpr, 
                'fpr': fpr, 
                'ppv': ppv, 
                'npv': npv, 
                'br': br
    }
    return metrics 


def distance(x,y):
    return ((x[0]-y[0])**2 + (x[1]-y[1])**2)**0.5

f = lambda confusion_matrix, alpha : confusion_matrix['br']* confusion_matrix['tpr']/alpha ## for pv<= alpha threshold


def classify(pv, alpha): 
    if pv <= alpha: 
        return 1
    else: 
        return 0

## ---- plot PVC vs. Precision ---- ## 
## make pvalues
gdi = {int(x): y for x,y in unjsonify(os.path.join(os.path.join(gp_path, 'cisrc-paper-public-data'), 'graph_detection_info.json')).iteritems()}
## of the form {index: {mu: array, cov: array, m_dist: mahalanobis distance squared!} } note that this gives mahalanobis distance squared b/c mcd.mahalanobis gives m^2. 

## get the p-values: 
p_values = {int(x):y for x,y in  unjsonify(os.path.join(gp_path, 'p-values.json')).iteritems()}

p_values = {} ## to be populated
for i in sorted(gdi.keys()): ## only use 200 data points like they did in the paper. 
    mahal_squared = gdi[i]['m-dist']
    mu, cov = np.array(gdi[i]['mu']), np.array(gdi[i]['cov'])
    k = len(mu) ## chi^2 degree of freedom. 
    # cov_inv = p_inv_h(cov) ## inverse of cov
    pv = 1-chi2.cdf(mahal_squared, k)  ## see http://stackoverflow.com/a/39985727/2487607 
    p_values[i] = pv 
jsonify(p_values, os.path.join(gp_path, 'p-values.json'))

## populate dict of ground truth labels: 
truth = {}
for i in p_values.keys(): 
    if 278 <= i and i <= 302: 
        truth[i] = 1
    else: 
        truth[i] = 0 

cm = {} ## to be populated
alpha_list = sorted(set([x for x in p_values.values() if x<=.3]))
alpha_list.pop(0)

for alpha in alpha_list: 
    predicted = {}
    for i in sorted(p_values.keys()):
        if p_values[i] <= alpha: 
            predicted[i] = 1
        else: 
            predicted[i] = 0 
    cm[alpha] = make_cm(truth, predicted)

# ## now make the roc curve and optimize
# roc = { alpha: (cm[alpha]['fpr'], cm[alpha]['tpr']) for alpha in alpha_list}
# alpha_roc = sorted( alpha_list, key = lambda alpha: distance( ( 0., 1.), roc[alpha] ) )[0] ## want the smallest dist 

## pvc curve: 
pvc = {alpha :  f( cm[alpha] , alpha) for alpha in alpha_list}
# alpha_pvc = sorted( alpha_list, key = lambda alpha: pvc[alpha] , reverse = True )[0] ## want the biggest 
l =  sorted( alpha_list, key = lambda alpha: pvc[alpha] , reverse = True )
alpha_pvc = [x for x in l if pvc[x]<=1][0]

from matplotlib import pyplot as plt 
## ---- plot ROC and PPV ---- ##
print "\n\n plottin ... "
ll = [x for x in l if pvc[x]<=10]
tprs = [ cm[alpha]['tpr'] for alpha in ll]
fprs = [ cm[alpha]['fpr'] for alpha in ll]
ppvs = [ cm[alpha]['ppv'] for alpha in ll]
npvs = [ cm[alpha]['npv'] for alpha in ll]

plt.plot(ll, ppvs, label = 'Precision')
plt.plot(ll, [pvc[alpha] for alpha in ll ], label = 'Predicted Precision')
plt.xlabel('alpha')
plt.legend()
plt.show()

## image saved in JJ/papers/predictive-value-curve/images




# f, axarr = plt.subplots(1, 2)
# axarr[0].scatter(alpha_list, ppvs)
# # axarr[0].plot(roc[alpha_roc][0], roc[alpha_roc][1], 'ro' )
# axarr[0].set_title('PPV')
# axarr[1].scatter(alpha_list, [pvc[alpha] for alpha in alpha_list])
# axarr[1].plot(alpha_pvc, pvc[alpha_pvc], 'ro' )
# axarr[1].set_title('PVC')
# # Fine-tune figure; hide x ticks for top plots and y ticks for right plots
# # plt.setp([a.get_yticklabels() for a in axarr[:]], visible=False)
# # plt.xlim(0,1)
# # plt.ylim(0,1)
# plt.show()


## store the two alpha: 
# print "alpha_roc = %s\nalpha_pvc = %s" %(alpha_roc, alpha_pvc)
# jsonify({"alpha_roc": alpha_roc, "alpha_pvc": alpha_pvc}, os.path.join(folder_path, 'alphas.json'))







