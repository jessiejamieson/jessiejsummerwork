# temp.py 

"""
Goal is to use graphprints (w/ p-values from non-robust gaussians) to show PVC allows increased precison w/ not too much expense of TPR 

Data points 0-149 are used for training the models 
Data points 278-302 are ground truth positives 


to be written into the paper: 
Notice that in practice, since a labeled training set is finite, there's a epsilon >0 so that for all alpha<=epsilon, TPR(alpha) is fixed. 
At this point PVC curve estimate grows without bound (as br and tpr are fixed). 
Further, TPR(alpha) is fixed between the realized p-values of training data points. 
Hence, in searching for the "best" alpha given labeled training data, we consider alphas in teh realized p-values, 
and for values where the estimate is bounded above by 1. 

"""

import os
import numpy as np 
from scipy.stats import chi2
from sklearn import decomposition
# from sklearn.covariance import MinCovDet
from sklearn.neighbors import KernelDensity
from matplotlib import pyplot as plt 

from generalFunctions import * 


# ---- paths ---- #
gp_path = os.path.join( os.path.abspath( os.path.join( os.getcwd(), '..')), 'data-graphprints')
pub_path = os.path.join( gp_path, 'cisrc-paper-public-data')
pvc_path = os.path.join(os.path.join( os.path.abspath( os.path.join( os.getcwd(), '..')), 'data-pvc-paper'), 'graph-prints')

## ---- functions ---- ## 
def p_inv_h(S): 
	""" 
	Common trick for inverting a matrix that's not invertible (on the subspace where it is invertible and making it 0 else. )

	Input: S np.array, n by n, symmetric
	Output: S's p-inverse. 
	Note here we don't use np.linalg.pinv b/c solving the eigenvalue problem is more stable using eigh when the matrix is symmetric
	So we do it manually w/ eigh. 
	""" 
	if not np.allclose(S.T, S): 
		print "S is not symmetric, nothing returned"
		return 

	values, U = np.linalg.eigh( S )  
	## so,  S = U np.diag(values) U.T. to check: 
	# np.allclose(S , np.dot(np.dot(U, np.diag(values)), U.T ) )
	l = np.zeros_like( values )
	for i in xrange(len(values)): 
		if  values[i] > 10**-8:  ## S is PSD matrix, hence eigenvalues non-neg. anyting less than zero is a rounding error. 
			l[i] = 1./values[i]
	S_pinv = np.dot( U, np.dot( np.diag(l), U.T ) ) 
	return S_pinv


def make_cm(truth, predicted): 
	""" 
	input:  - truth = dict of form {index: 1 or 0}  This gives the groundtruth classification 
			- predicted = dict of form {index: 1 or 0} this gives the predicted classification 
		NOTE: truth, predicted should be the same length!
	output: - metrics = {tp: count of true positives, fp: , ...}
			computes and stores the number of true positives (TP), etc. and computes recall (TPR), false positive rate (FPR), precision (PPV), and neg. predictive value (NPV), and base rate = number of positive/number of samples
	""" 
	if sorted(truth.keys()) != sorted(predicted.keys()): 
		print "input dicts have different lenghts (must be same)"
		return 

	true_y = [ truth[i] for i in sorted(truth.keys()) ]
	pred_y = [ predicted[i] for i in sorted(predicted.keys())]
		
	l = zip(true_y, pred_y)
	tp = len([(t,p) for t,p in l if t==1 and p ==1 ] )
	fp = len([(t,p) for t,p in l if t==0 and p ==1 ] )
	tn = len([(t,p) for t,p in l if t==0 and p ==0 ] )
	fn = len([(t,p) for t,p in l if t==1 and p ==0 ] )
	
	if float( tp + fn) >0: 
		tpr = float(tp) / float( tp + fn)
	else: 
		tpr = 'na'
	
	if float( fp + tn ) >0: 
		fpr = float(fp) / float( fp + tn )
	else: 
		fpr = 'na'

	if float( tp + fp) > 0 : 
		ppv = float(tp) / float( tp + fp)
	else: 
		ppv = 'na'
	
	if float( tn + fn) > 0 :
		npv = float(tn) / float( tn + fn)
	else: 
		npv = 'na'

	br = float( tp + fn )/ (tp + fp + tn + fn ) ## base rate
	
	metrics = {
				'tp': tp, 
				'fp': fp, 
				'tn': tn, 
				'fn': fn, 
				'tpr': tpr, 
				'fpr': fpr, 
				'ppv': ppv, 
				'npv': npv, 
				'br': br
	}
	return metrics 


def distance(x,y):
	return ((x[0]-y[0])**2 + (x[1]-y[1])**2)**0.5

f = lambda confusion_matrix, alpha : confusion_matrix['br']* confusion_matrix['tpr']/alpha ## for pv<= alpha threshold


def classify(pv, alpha): 
	if pv <= alpha: 
		return 1
	else: 
		return 0


## ---- read in graph info and make an array of feature vectors ---- ## 
graph_info = {int(k): v for k,v in unjsonify( os.path.join( pub_path, 'graph_info.json' ) ).iteritems()}
for i, d in graph_info.iteritems():
	for x in d['graphlet_count'].keys(): 
		d['graphlet_count'][x] = int(d['graphlet_count'][x]) 

## training will be range(290), so only use features seen here:
pre_features = []
for i in  xrange(290): 
	pre_features += [x for x in graph_info[i]['graphlet_count'].keys() if graph_info[i]['graphlet_count'][x]>0 and x.endswith("_3")]
pre_features = sorted(set (pre_features))

features = []
for x in pre_features: 
	if np.var([graph_info[i]['graphlet_count'][x] for i in xrange(290) ] ) >= .1:
		features.append(x)
		
row_labels = sorted(graph_info.keys())

X = np.zeros( (len(row_labels), len(features) ) )
for i in xrange(len(row_labels)): 
	for j,x in enumerate(features): 
		if x in graph_info[i]['graphlet_count']:
			X[i,j] = float(graph_info[i]['graphlet_count'][x])

X_train = X[:290, :]
X_test = X[290:, :]

# train = X_train
# test = X_test

# normalize training data 
maxes = np.max(X_train, axis = 0)
X_train_normalized = X_train/maxes
X_test_normalized = X_test/maxes 
# X_train_normalized = (X_train - np.mean(X_train, axis = 0))/np.std(X, axis = 0)

# ## for only 150 trainign points, we need to reduce dimension! 
pca = decomposition.PCA()
pca.fit(X_train_normalized)
plt.plot(range( len(pca.explained_variance_)), pca.explained_variance_)
plt.xlabel("Dimension")
plt.title("PCA Scree Plot")
plt.show()
## look at scree plot, chooose n_components 
n_components = 5
pca = decomposition.PCA(n_components = n_components)
pca.fit(X_train_normalized)
train = pca.transform(X_train_normalized)
test = pca.transform(X_test_normalized)


# ## KDE test w/ a toy set: 
# # train = np.array([[1,1], [1.1, .9], [.2,.2], [1.3, 1.9], [.9, 1.3]])
# train = np.array([[1],[1.1],[ .9], [.8],[ 1], [.2] ])
# n_components = 1
# width = np.max( map(np.var, [train[:,i] for i in xrange(train.shape[1])] ))
# kernel = "gaussian"  ### choices are ['gaussian', 'tophat', 'epanechnikov', 'exponential', 'linear', 'cosine']
# kde = KernelDensity(kernel = kernel, bandwidth = width).fit(train) ## fit model                     

# ## set up monte carlo: 
# a, b = float(np.min(train)) - width, float(np.max(train) + width) 
# num = 10000
# s = np.linspace(a, b, num).reshape((num, 1)) 
# x = np.meshgrid(*[s for i in range(n_components)])
# S = zip(*map( lambda a: list(a.ravel()), x ))

# delta = ( (b - a)/float(num) )**n_components ## area of the n_component cube w/ side length (b-a)/num
# B = np.exp( kde.score_samples(S) ) ## probability of sample points 
# print sum(B)*delta


# ## kde code: 
width = np.max( map(np.var, [train[:,i] for i in xrange(train.shape[1])] ))
kernel = "gaussian"  ### choices are ['gaussian', 'tophat', 'epanechnikov', 'exponential', 'linear', 'cosine']
kde = KernelDensity(kernel = kernel, bandwidth = width).fit(train[:278]) ## fit model                     

## need to sample points in d-dimensional space! for monte carlo
a, b = float(np.min(train)) - width, float(np.max(train) + width) 
num = 20 
s = np.linspace(a, b, num).reshape((num, 1)) 
x = np.meshgrid(*[s for i in range(n_components)])
S = zip(*map( lambda a: list(a.ravel()), x ))
delta = ( (b - a)/float(num) )**n_components ## area of the n_component cube w/ side length (b-a)/num
B = np.exp( kde.score_samples(S) ) ## probability of sample points 
print sum(B)*delta
## so the above sum is an estimate of 1 (the integral of the kde). so we will multiply probabilities by fudge to make everything sum to 1. 
fudge = 1./(sum(B)*delta)


pvals_train = { } ## to be populated: 
for i in xrange(train.shape[0]): 
	x = train[i,:].reshape(1,n_components) ## make an array w/ a single row vector
	try: 
		p = float(np.exp(kde.score_samples( x ))) # likelihood of the new data (score_samples gives the log(pdf))
		pv = float(np.sum(B[B <= p])*delta ) * fudge
	except: 
		pv = 0.
	pvals_train[i] = pv
jsonify(pvals_train, os.path.join( pvc_path, "train-p-values.json"))

pvals_test = { } ## to be populated: 
for i in xrange(test.shape[0]): 
	x = test[i,:].reshape(1, n_components) 
	try: 
		p = float(np.exp(kde.score_samples( x ))) # likelihood of the new data (score_samples gives the log(pdf))
		pv = float(np.sum(B[B <= p])*delta )* fudge
	except: 
		pv = 0.
	pvals_test[i] = pv
jsonify(pvals_test,  os.path.join( pvc_path, 'test-p-values.json'))


### below is for a gaussian estimate. 
# h = .99
# mcd = MinCovDet(support_fraction = h).fit(train)
# mu = mcd.location_ ## estimated robust mean
# cov = mcd.covariance_ ## estimated robust cov. 

# mu = np.mean(train[:278], axis = 0)
# k = len(mu) ## chi^2 degree of freedom. 
# cov = np.cov(train[:278].T) 
# cov_inv = p_inv_h(cov) ## inverse of cov
	
# pvals_train = { } ## to be populated: 
# for i in xrange(train.shape[0]): 
# 	v = train[i,:] 
# 	mahal_squared = np.dot( np.dot( cov_inv, v - mu ), v - mu )
# 	pv = 1 - chi2.cdf(mahal_squared, k)  ## see http://stackoverflow.com/a/39985727/2487607 
# 	pvals_train[i] = pv
# jsonify(pvals_train, os.path.join( pvc_path, "train-p-values.json"))

# pvals_test = { } ## to be populated: 
# for i in xrange(test.shape[0]): 
# 	v = test[i,:] 
# 	mahal_squared = np.dot( np.dot( cov_inv, v - mu ), v - mu )
# 	pv = 1 - chi2.cdf(mahal_squared, k)  ## see http://stackoverflow.com/a/39985727/2487607 
# 	pvals_test[i] = pv
# jsonify(pvals_test,  os.path.join( pvc_path, 'test-p-values.json'))



## now get the roc adn pvc alphas on the training data: 
train_truth = {}
for i in pvals_train.keys(): 
	if 278 <= i : 
		train_truth[i] = 1
	else: 
		train_truth[i] = 0 

cm = {} ## to be populated
alpha_list = sorted(set([x for x in pvals_train.values() if x<=.3]))
alpha_list.pop(0)
for alpha in alpha_list: 
	predicted = {}
	for i in sorted(pvals_train.keys()):
		if pvals_train[i] <= alpha: 
			predicted[i] = 1
		else: 
			predicted[i] = 0 
	cm[alpha] = make_cm(train_truth, predicted)

# ## now make the roc curve and optimize
roc = { alpha: (cm[alpha]['fpr'], cm[alpha]['tpr']) for alpha in alpha_list}
alpha_roc = sorted( alpha_list, key = lambda alpha: distance( ( 0., 1.), roc[alpha] ) )[0] ## want the smallest dist 
print "alpha_roc = %s w/ cm = \n%s" %(alpha_roc, cm[alpha_roc])

## pvc curve: 
pvc = {alpha :  f( cm[alpha] , alpha) for alpha in alpha_list}
# alpha_pvc = min(sorted( alpha_list, key = lambda alpha: pvc[alpha] , reverse = True )[0] ## want the biggest 
l = sorted( alpha_list, key = lambda alpha: pvc[alpha] , reverse = True )
alpha_pvc = max([x for x in l if x<=1])
print "alpha_pvc = %s w/ cm = \n%s" %(alpha_pvc, cm[alpha_pvc])



# from matplotlib import pyplot as plt 
## ---- plot ROC and PPV ---- ##
print "\n\n plotting ... "
tprs = [ cm[alpha]['tpr'] for alpha in alpha_list]
fprs = [ cm[alpha]['fpr'] for alpha in alpha_list]
ppvs = [ cm[alpha]['ppv'] for alpha in alpha_list]
npvs = [ cm[alpha]['npv'] for alpha in alpha_list]

plt.scatter(alpha_list, [pvc[alpha] for alpha in alpha_list], label = 'Predicted Precision')
plt.scatter(alpha_list, ppvs, label = 'Precision')
plt.xlabel('alpha')
plt.legend()
plt.show()

## now test: 
test_truth = {}
for i in pvals_test.keys(): 
	if  i <= 11 : 
		test_truth[i] = 1
	else: 
		test_truth[i] = 0 

pred_roc = {i:0 for i in pvals_test.keys()}
pred_pvc = {i:0 for i in pvals_test.keys()}
for i,pv in pvals_test.iteritems(): 
	if pv <= alpha_roc: 
		pred_roc[i] = 1
	if pv <= alpha_pvc: 
		pred_pvc[i] = 1

cm_roc = make_cm(test_truth, pred_roc)
cm_pvc = make_cm(test_truth, pred_pvc)

print "cm_roc = %s,\n\ncm_pvc = %s" %(cm_roc, cm_pvc) 


