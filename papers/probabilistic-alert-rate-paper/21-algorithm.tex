\subsection{Alert Rate Regulation Algorithm}
\label{sec:algorithm}
Under the assumption that data observations are samples from our distribution, we are mathematically equipped to design an algorithm that exploits the relationship between the alert rate and the threshold to prevent an over production of alerts. 
To illustrate this, suppose we receive $N$ data points per time interval $\delta_t$ (e.g., per minute), but operators only have resources to inspect the most anomalous $M \leq N$ in each time interval.  
Let $f_n$ be a PDF fit to all previous observations $\{x_1, ..., x_{n-1}\}$. 
Following the assumption that the next observation, $x_n$, will be sampled according to $f_n$, define the anomaly score, $A_n := h\circ pv_{f_n}$ where $h$ is a fixed, strictly decreasing bijection of the unit interval.  
Upon receipt of $x_n$ an alert is issued if $A_n(x_{n}) \geq \alpha$.
Equivalently, if $pv_{f_n}(x_n) \leq h^{-1}(\alpha) =: \beta$.   
Finally, we update $f_n$ to $f_{n+1},$ now including observation $x_n$ and repeat the cycle upon receipt of the next observation.  
Leveraging the theorem above, the expected number of alerts per interval is 
$ \sum_{n = 0}^{N-1} P_{f_n}( A_{n}(x_{n}) \geq \alpha ) \leq N h^{-1}(\alpha).$
Hence, choosing $\alpha = h(M/N)$ (equivalently, flagging if the p-value is below $\beta = M/N$) ensures that the operator's bound on the number of alerts will hold on average.

The method above is for a fixed time interval, or for constant rate data. 
As the speed of the data may vary, we now adapt the above method to dynamically change the alert rate to accommodate variable data speed.  
Let $t_i$ denote arrival time of $x_i$, and let $r$ (alerts per second) be the user-desired upper bound on the alert rate (the analogue on $M$).  
Next, for each time interval we periodically estimate the rate of data by letting $r_k = |\{x_i: t_i \in  [ (k-1)*\delta_t , k*\delta_t) \}|/\delta_t$, so that $r_k$ gives the number of observed events over the $k$\textsuperscript{th} $\delta_t$-length interval. 
Hence $r_k$ is a periodically computed, moving average data speed. 
Alternatively, one could compute $r_n$ on each data point's wait time, i.e.,  $r_n := 1 / (t_n-t_{n-1})$, which could experience much more variance.  
Finally, the new threshold at time $n$ shall be given by $\alpha_k = h( r / r_k )$ for the $k$\textsuperscript{th} interval, or equivalently, the p-value threshold is $\beta_k = r/r_k.$  
This new threshold is used for classifying $x_n$ as soon as $x_n$ is observed. 
This algorithm can be called at each iteration of a streaming algorithm to regulate the alert rate. 

Note that this choice of threshold depends only on the rate of the data ($N$ or $r_k$), and the operator's bound on the alert rate ($M$ or $r$). 
In particular, it is independent of the distribution, and therefore can be set a priori,  regardless of the distribution. 
This is especially applicable in a streaming setting, where $f_n$ is constantly changing. 
Next, this is not a hard bound on the number of alerts per day, but rather bounds the alert rate in expectation. 
The operational impact is that if there is an influx of anomalous data, all will indeed be flagged, but on average the alert rate will be bounded as desired. 
Consequently, it is possible to have greater than $M$ alerts in some time intervals. 
Finally, if one has the luxury of performing post-hoc analysis, such an algorithm is not needed; for example, one can prioritize a previous day's alerts by anomaly score, handling as many as possible. 
Yet, if real time analysis is necessary, e.g., only a fraction of the alerts can be processed, stored, etc., then such an algorithm allows real time prioritization of alerts with the bound preset.


The underlying assumption is that data is sampled from the model's probability distribution, so if this assumption fails, for example, if there is a change of state of the system and/or if $f_n$ poorly describes the observations to come, then these bounds may cease to hold. 
This failure gives an ancillary benefit of the mathematical machinery above, namely, that monitoring the actual versus expected alert rate gives a quantitative measure of how well the distribution fits the data.  
Our work to characterize conditions that ensure equality (Corollaries~\ref{cor:eq-1}, \ref{cor:continuous}, and \ref{cor:eq-2}) serve this purpose. 
Under these conditions, the alert rate bound is an actual equality, and deviations from the expected number of alerts over time (both below and above) indicate a poor model for the data.
On the other hand, when the bound is strict (and equality does not hold), only an on-average over-production of alerts will signal a model that does not fit the data. 
Our experiments on real data illustrate this phenomenon as well.   


Finally, we note that the adaptive threshold, $\beta = r/r_k$ with $r_k$ the data rate, is conveniently self tuning for variable speed data, it induces a vulnerability. 
Quite simply, an all-knowing adversary with the capability to increase $r_k$  at the time of attack, can force the threshold to zero, to mask otherwise alerted events. 
On the other hand, this is easily parried with a simply fixed-rate detector on the data rate, i.e., modeling the statistic $r_k$, (e.g., a denial of service flooding detector). 
Note that as $r_k$ is computed each interval, the proposed workaround is a fixed rate detector; hence, the alert rate can be regulated without the vulnerability induced. 


\subsection{Impact on Detection Accuracy} 
\label{sec:accuracy}
For high volume situations, the adaptive threshold will reduce the number of alerts during influxes of data. 
Consequently, the true/false positive rates (defined, respectively, as the percentage of positives/negatives that are alerts, and hereafter TPR, FPR) will drop, as the number of alerts (numerator) will be reduced with fixed denominator. 
The effect on the positive predictive value (PPV) also known as precision (defined as the percent of alerts that are positives), will depend on the distribution of anomaly scores to the positive events in the data set. 
In particular, if this distribution is uniform, then precision will be unaffected. 
In this case we note that our theorems give a sharp bound (and ability to regulate) the false detection rate (1-PPV). 




%{\color{BlueViolet} JESSIE, FIGURE OUT HOW TO REWRITE what's below IN ALGORITHM ENVIRONMENT OR MAYBE JUST ADD AN ALGORITHM TO IT? 
%Algorithm III.8 gives an algorithm for setting the alert rate threshold to realize an expected bound on the number of alerts. 
%Bring forward the notation $x_n$, $f_n$, and $h$ from above; let $t_i$ denote arrival time of $x_i$, and let $r$ be the user-given desired upper bound on the alert rate. \\

%Algorithm input: $t_{n-1}, t_n, r$ 
%Steps: 
%1. $r_n = (t_n - t_{n-1}){-1}$ is the rate of the data at time $n$. 
%2. $\alpha_n = h(r/r_n)$
%Algorithm outputs $\alpha_n$ the threshold used for classifying $x_n$ a soon as $x_n$ is observed. 


%\begin{raggedright}
%\textbf{Algorithm III.8: \url{Alert-Rate}}\\
%\label{alg:alertrate}
%\textbf{Inputs:}
%	\begin{itemize}
%		\item{$t_n$: arrival time of data point $x_{n}$.}
%		\item{\url{r}: desired alert rate upper bound.}
%	\end{itemize}
	
%	\textbf{Outputs:}
%	\begin{itemize}
%		\item{$\alpha_n$: the threshold used for classifying $x_n$.}
%	\end{itemize}
	

%\begin{algorithm}[]
%\SetAlgoLined
%\noindent
%{\bf begin} \ \ \ \\
%	\textbf{define}: $r_n=t_n-t_{n-1}$;\\
%	\quad \textbf{return}: $\alpha_n=h(r/r_n)$.\\
%	{\bf end}
%\end{algorithm}
%\end{raggedright}

%\textcolor{OliveGreen}{In the above algorithm, we had $r_n=(t_n-t_{n-1})-1$, but when I went back and checked using numbers I think we need to drop the $-1$. I think it was in there for python index issues.}



