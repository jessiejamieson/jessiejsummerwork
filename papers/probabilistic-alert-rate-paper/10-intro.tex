%1-intro.tex
\section{Introduction}
\label{sec:intro}
In general, anomaly detection (AD) refers to a large class of algorithms that characterize a notion of normality from observed data, and then detect sufficiently large deviations from normal. 
This is often viewed as a one-class or probabilistic modeling problem. 
Furthermore, these techniques enjoy a wide variety of applications including detection of machine defects, credit card fraud, and, our focus, intrusion detection for information systems.~\cite{bolton1999creditcard,chandola2009survey,king2002plant}


The current state of defense against cyber attacks is a layered defense, primarily of a variety of automated, signature-based detectors and secondarily via manual investigation by security analysts.
Typically large cyber operations (e.g., at government facilities) have widespread collection and query capabilities for an enormous amount of logging and alert data. 
For example, at the network level, firewalls and intrusion detection/prevention systems (IDS/IPS) such as Snort\footnote{\url{https://www.snort.org/}} produce logs, warnings, and alerts that are collected, in addition to the collection of network flow logs\footnote{Network flows record the metadata of IP-to-IP communications.
While specific definitions vary among the specific flow sensor used, a short time window of communications is logged, generally including timestamp, duration, protocol, as well as the source and destination IPs, ports, bytes, and number of packets, among other fields.}, and sometimes full packet captures are stored and/or analyzed.
\begin{wraptable}{r}{0.245\textwidth}
    \vspace{-.2cm}
    \begin{tabular}{cc}
    \multicolumn{2}{c}{\textbf{Flow Record Example}}\\
    \toprule    
    \textbf{Time} & 09:58:32.912\\%949 \\
    \textbf{Protocol} & tcp \\
    \textbf{SrcIP} & 192.168.1.100 \\
    \textbf{SrcPort} & 59860 \\
    %Direction & -> \\
    \textbf{DstIP} & 172.16.100.10 \\
    \textbf{DstPort} & 80 \\
    \textbf{SrcBytes} & 508526 \\
    \textbf{DstBytes} & 1186562 \\
    %Dest Packets & 595 \\
    %State & CON \\
    %\textbf{SrcCo} & US \\
    %\textbf{DstCo} & US \\
    \textbf{TotBytes} & 1695088 \\
    \bottomrule
    % \label{tab:flow}
	\end{tabular}
	\squeezeup
\end{wraptable} 
Additionally, situational awareness tools such as Nessus\footnote{\url{https://www.tenable.com/products/nessus-vulnerability-scanner}} provide lists of software, software version, and known vulnerabilities for each host. 
Finally, host-based IDS/IPS such as  McAfee\footnote{\url{https://www.mcafee.com/us/index.html}} anti-virus software and AMP\footnote{\url{http://www.cisco.com/c/en/us/products/security/advanced-malware-protection/index.html}} report alerts to cyber security operations. 
Hence, security analysts now have access to multiple streams of heterogeneous sources producing data in high volumes. 
For example, [Organization Redacted] 
%Oak Ridge National Laboratory (ORNL), 
comprised of $\approx4,500$ research support staff, monitors only a portion of their network flow logs, a volume of 4-7 GB/s,
%which does not include any of the packet contents, 
in addition to many other logging and alerting tools employed. 
(Example flow record in table on the right.) 
Consequently, manual investigation and automated processing of data/incidents must manage the large bandwidth. 


% \begin{minipage}{0.55\linewidth}
% \noindent 
% \vphantom{|}now have access to multiple streams of heterogeneous sources producing data in high volumes. 
% For example, Oak Ridge National Laboratory, comprised of $\approx4,500$ research  scientists and support staff, monitors only a portion of their network flow logs, 
% a volume of 4-7 GB/s, which does not include any of the packet contents,
% in addition to the many other logging and alerting collection tools employed. 
% (Example flow record in Table~\ref{tab:flow} on the right.) 
% Consequently, manual investigation of data and incidents are simply inefficient to scale to the current need.  
% \end{minipage}
% \begin{minipage}{.15\linewidth}
% \phantom{blah} 
% \end{minipage}
% \begin{minipage}{.30\linewidth}
%     \phantom{aaa\\}
%     \begin{tabular}{cc}
%     \multicolumn{2}{c}{\textbf{Flow Record Example}}\\
%     \toprule    
%     \textbf{Time} & 09:58:32.912\\%949 \\
%     \textbf{Protocol} & tcp \\
%     \textbf{SrcIP} & 192.168.1.100 \\
%     \textbf{SrcPort} & 59860 \\
%     %Direction & -> \\
%     \textbf{DstIP} & 173.16.100.10 \\
%     \textbf{DstPort} & 80 \\
%     \textbf{SrcBytes} & 508526 \\
%     \textbf{DstBytes} & 1186562 \\
%     %Dest Packets & 595 \\
%     %State & CON \\
%     %\textbf{SrcCo} & US \\
%     %\textbf{DstCo} & US \\
%     \textbf{TotBytes} & 1695088 \\
%     \bottomrule
%     \label{tab:flow}
% 	\end{tabular}
%     % \phantom{blah\\}
% \end{minipage}\\
    

Signature-based detection methods (e.g., anti-virus software, firewalls, IDS/IPS), operate by matching precise rules that identify known attack patterns within network and host data. 
As such, they promise relatively few false positives, high precision, and are the first line of defense for almost all networks. 
Yet, these methods are effective only against previously analyzed attack patterns; hence, their false negative rate is problematic, as evidenced by the repeated reporting of successful cyber attacks against high profile organizations.\footnote{E.g., see cyber security reports such as NCCIC/ICS-CERT's \url{https://ics-cert.us-cert.gov/sites/default/files/Annual_Reports/Year_in_Review_FY2015_Final_S508C.pdf}, Heritage Foundation's \url{http://www.heritage.org/defense/report/cyber-attacks-us-companies-2016}, or Symantec \url{https://www.symantec.com/en/ca/security-center/threat-report} among others.}
In response there is a large %and growing 
body of literature to provide AD systems for cyber intrusion detection~\cite{ ahmed2007multivariate, axelsson2000base, christodorescu2007can,        ferragut2011automatic,  ferragut2012new, fontugne2010mawilab, garcia2014empirical,   harshaw2016graphprints, joslyn2016information, krugel2002service, lakhina2004diagnosing, moore2017modeling, pevny2012identifying, rehak2009multiagent, scarfone2007guide, sexton2015attack, tandon2009tracking, thomas2010rapid,    wang2011statistical}. 
AD provides a complimentary monitoring tool that holds the promise of detecting novel attacks by identifying large deviations from normal behavior, and this concept has been proven in many of the previous works. 
Ideally, accurate detection with a low number of false positives is achieved. 
\textit{At a minimum, an AD IDS should isolate a manageable subset of events that are sufficiently abnormal to warrant next-step operating procedures}, such as, passing alerts to a downstream system (e.g., automated signature generator) or to an operator for manual investigation. 
%Such systems give a platform for situation awareness to aid manual investigations. 

While AD has garnered much research attention, such algorithms are met with many challenges when used in practice in the cyber security domain. 
Most notably, the unique nature of each network makes effective configuration of AD systems costly or impractical. 
% Previous research has generally focused on accuracy issues, with the primary goal of proving admissible false detection rates (percentage of alerts are false positives) are possible. 
How to design AD models for accuracy\textemdash exploring what statistics, algorithms, and data representations to use so that the detected events correspond with operator-defined positives\textemdash is the focus of many previous works~\cite{ahmed2007multivariate, axelsson2000base, christodorescu2007can,  ferragut2011automatic,  ferragut2012new, fontugne2010mawilab, garcia2014empirical,   harshaw2016graphprints, joslyn2014discrete, krugel2002service, sexton2015attack, tandon2009tracking, thomas2010rapid,    wang2011statistical} and in deployment is likely a network-specific task leveraging both domain expertise (understanding attacks, protocols, etc.) and tacit environmental knowledge (understanding configuration of network appliances and their behaviors). 
Common trends to increase accuracy involve the use of ensembles of heterogeneous detectors~\cite{christodorescu2007can, thomas2010rapid, ferragut2011automatic, ferragut2012new, fontugne2010mawilab, garcia2014empirical, kriegel2011interpreting, schubert2012evaluation, sexton2015attack} and/or online detection models that adapt in real time and/or upon observations of data~\cite{ahmed2007multivariate, bridges2015multi, bridges2016multi, ferragut2012new, harshaw2016graphprints, wang2011statistical}. 
In practice, the need for multiple detectors is enhanced by the diversity in (1) network components (models conditioned on each host, subnet, etc.), 
(2) data types (models conditioned on flow data, system logs, etc.),
 (3) features of interest (rate, distribution of ports used, ratio of data-in to data-out, etc.), and (4) the fact that each model can change in time. 
E.g., patents of Ferragut et al.~\cite{ferragut2016detection, ferragut2016real} detail AD systems using a fleet of dynamic models and producing near real-time alerts on high volume logging data. 

\subsection{Problem Addressed} 
In this work we do \textit{not} present novel methods for accurate detection of intrusions. 
Rather, we address a difficult but important question for AD in IDS applications, namely: 
How should the alert threshold be set in the case of a large number of heterogeneous detectors, possibly changing in real time, that are producing alerts on high-volume data?
Our organization's cyber operations' analysts have arrived at the problem of alert rate regulation from three secenarios that all require real-time prioritization of alerts that can accommodate influxes of data as well as the multitude of evolving models, namely, 
(1) manual alert investigation requires an online way to triage events; 
(2) data storage limitations, e.g., storing
packet captures (PCAPs)  from the most anomalous traffic,  requires a real-time algorithm for prioritizing events  as ``anomalous enough'';
(3) online automated alert processing (e.g.,
automated signature generation of anomalous activity) cannot handle influxes, i.e., downstream systems
require alert rate regulation to prevent a denial-of-service.


To illustrate the problem, consider the AD system and Skaion data used  in Section~\ref{sec:exp-skaion}. 
This AD system scores each network flow using two evolving probability models per internal IP, totaling about 2,500 dynamic detectors.  
It is simply not feasible to manually tune the threshold for each of these 2,500 models, and even so, since the models are changing in real time, reconfiguration would be periodically necessary. 
Furthermore, the consequences of misconfigured thresholds are substantial. 
Altogether, the system produces a collective $\approx$2,000,000 anomaly scores in about five hours; hence, a threshold that is only slightly too high can produce tens of thousands of alerts per hour!  
Moreover, this dataset is very small compared to many networks, and the detection ensemble grows linearly with the number of IPs to model (network size). %  but is only a third of the detectors currently used in operation. 

The specific problem of how to set the alert threshold for detection systems in these very realistic scenarios is difficult, underdeveloped, and when not properly addressed, leaves anomaly detectors useless, as their goal is to isolate the most abnormal events from the sea of data.  
% Further exacerbating the problem is the dynamic nature of  both network (the data volume is variable\textemdash and the probability models.
Hence, we arrive at our problem of interest.  
How does an operator set the threshold for an AD system, given that the method must adequately accommodate a multitude of possibly adaptive models operating on possibly variable speed, high volume data? 
Second, our work contributes to the related problem of detecting drift of adaptive models over time. 

\subsection{Background \& New Contributions} 
This alert-rate regulation problem is first specified by Ferragut et al.~\cite{ferragut2012new}, and they note that a principled notion of quantitative comparability across detectors is necessary to deal with multiple/dynamic models. 
Their relevant contribution is twofold. 
(1) By assuming data is sampled from an accessible probability distribution, they formulate a definition equivalent to Defn.~\ref{defn:a-score}. 
The upshot is that anomalies are events with low p-values (see Defn.~\ref{defn:p-value}), and this technique provides a distribution-independent, comparable anomaly score. 
(2) They provide a theorem equivalent to Lemma~\ref{lem:alert-rate} for alert-rate regulation. 
No alert rate algorithm nor experimental testing of the theorem's consequences are presented.% (although empirical testing of an AD's accuracy is presented). 

% We note that Kreigel et al.~\cite{kriegel2011interpreting} have addressed the problem of comparing multiple outlier detection methods by manually crafting transformation functions that convert the given output to a score to a comparable output in the interval [0,1].
% This is only done for a handful of outlier detection algorithms, indicating the obvious drawback of this approach, the necessity to manually investigate each model. 

Our contributions build on the work of Ferragut et al.~\cite{ferragut2012new} both in extending the mathematics and in converting these theorems into an operationally-viable solution to the problem. 
New theorems of Section~\ref{sec:math}  provide further mathematical advancements pertinent to understanding the relationship between the p-value threshold and the likelihood of an alert.  
These results informs an operational workflow.
Given (1) a detection capability that uses a probability distribution to score low p-value events as anomalies and (2) knowledge of the data's rate, the operator has a principled, distribution independent method for setting the threshold to regulate the number of alerts produced. 
Hence, the algorithm can be applied to an ensemble of possibly dynamic, heterogeneous detectors to prevent overproduction of alerts. 
Notably, the system will not suppress influxes of anomalies, but asymptotically the operator-given bound is respected. 
Our math results give hypotheses that ensure equality in the theorem;  operationally, this is the case when users can specify, rather than just bound, the number of alerts. 
As the theorems hold independent of the model (distribution) used, operators can set the threshold a priori. 
In particular, it remains valid in a streaming setting, where the detection model is updated in real time to accommodate new observations.
% We provide empirical testing of the algorithm on real data showing operational viability. 
% Furthermore, we include a discussion of drawbacks and counter-measures an adversary might take as well as a solution.  
Because the underlying assumption is that future observations are sampled from the model's distribution, the alert rate regulation will fail if the model distribution differs from the actual distribution.
%(e.g., there is a state change). 
Hence, the theorem's contrapositive gives an operational benefit, namely that violations of the operator-given alert rate indicate that the anomaly  detection model is not a good fit to to the data. 
In this case a period of relearning the distribution is necessary for the threshold-setting algorithm to remain effective. 
% We continue with empirical testing to illustrate the use of this on real data.  

We present empirical experiments testing our method in setting the alert rate in two scenarios, both using detectors on network flow data. 
The first (Section~\ref{sec:exp-skaion}) shows the efficacy of setting the threshold on approximately 2,500 simultaneous, dynamically updated detectors. 
In this scenario, multiple anomaly scores are computed per flow; hence, the data rate is high and varies according to network traffic.
The results show that the mathematics give a method for regulating the alert rate of dynamic detectors that is a priori (in the sense that no knowledge of the specific distribution is necessary for threshold configuration). 
Our second experiment (Section~\ref{sec:graphprints}) uses data from the network AD paper of Harshaw et al.~\cite{harshaw2016graphprints}, which fits a Gaussian to a vector describing the real network traffic every 30 seconds. 
Hence, it is a single, fixed rate detector. 
The results from this application illustrate the analytic capabilities for gauging model fit that are made possible by the theorems we develop. 

Altogether our work gives new mathematical results regarding the p-value distribution. 
This informs an algorithm that poses an alternative\textemdash Operators can accurately set the threshold of detection ensembles to bound the expected number of alerts or identify a misfit of the detection model.  
