%3-prob.tex
\section{Mathematical Results}% for Bounding the Alert Rate}
\label{sec:math}
In this section we present mathematical assumptions and results that are the foundation for bounding the alert rate of an anomaly detector. 
To proceed, we consider probabilistic anomaly detectors, which score data's anomalousness according to a probability distribution describing the data. 
We leverage the probabilistic description to provide a theorem that gives sharp bounds on the alert rate in terms of the threshold, independent of the distribution. 
This gives an a priori method to manage the expected number of alerts for any distribution. 
As the mathematics is presented with the necessary but possibly abstruse formality and rigor, we include easy-to-understand examples illustrating the results, their implications, and limitations. 

\subsection{Setting and Notation}
\label{sec:notation}
Our setting assumes data is sampled independently from a distribution with probability density function (PDF) $f: X \to [0,\infty)$. 
The ambient space, $(X, \mathfrak{M}, m),$ is assumed to be a $\sigma$-finite measure space with measure $m$ and $\mathfrak{M}$ the set of measurable subsets of $X.$ 
We define the probability of a measurable set $S\in \mathfrak{M}$ to be $P_f(S):= \int_S f dm$; i.e., $P_f$ denotes the corresponding probability measure with Radon-Nikodym derivative $f$. 
For almost all applications, $X$ is either a subset of $\mathbb{R}^n$ with $m$ Lebesgue measure, or $X$ is a discrete space with $m$ counting measure.

We say an anomaly score $A(x)$ respects the distribution $f$ if $A(x) \geq A(y)$ if and only if $f(x) \leq f(y)$\textemdash intuitively, $x$ is more anomalous that $y$ if and only if $x$ is less likely than $y$.  
While this can be attained by simply letting $A = h\circ f$ for a decreasing function $h$ (for example, see Tandon and Chan~\cite{tandon2009tracking} where $A:= -\log_2(f)$ ), the work of Ferragut et al.~\cite{ferragut2012new} notes that such an anomaly score inhibits comparability across detectors. 
That is, because such a definition puts $A$ in one-to-one correspondence with the values of $f$, anomaly scores can vary wildly for different distributions.  
The consequence is that setting a threshold is dependent on the distribution, which is problematic especially for two settings that are common. 
The first is a setting where multiple detectors (e.g., detectors for network traffic velocity, IP-distribution, etc.) are used in tandem as each require different models.
For instances in the literature using cooperative detectors see~\cite{christodorescu2007can, thomas2010rapid, ferragut2011automatic, ferragut2012new, fontugne2010mawilab, garcia2014empirical, kriegel2011interpreting, schubert2012evaluation, sexton2015attack}.   
The second setting is when dynamic models (where $f$ is updated upon new observations) are used, as this requires comparison of anomaly scores over time. 
For examples of streaming detection scenarios see~\cite{ahmed2007multivariate, bridges2015multi, bridges2016multi, ferragut2012new,  harshaw2016graphprints, wang2011statistical}.  

To circumvent this problem, we follow Ferragut  et al.~\cite{ferragut2012new} by assuming observations are sampled independently from $f$, a PDF, and define anomalies as events with low p-value (as do many detection capabilities).  
For any distribution the p-value gives the likelihood relative to the distribution. 
Hence, it always takes values in $[0,1]$, and in the specific case of a univariate Gaussian is just the two-sided z-score.  
\defn [P-Value]
	\label{defn:p-value}
	The {\it p-value} of $x\in X$ with respect to distribution $f$, is denoted $pv_f: X\to [0,1]$ and is defined as 
	$$ pv_f(x): = \int_{\{t: f(t)\leq f(x) \} } f dm  = P_f(\{t: f(t)\leq f(x) \} ).$$
\edefn 
It is clear from the definition that $pv_f(x) > pv_f(y) $ if and only if $x$ is more likely than $y$, since $f\geq 0$. 
Finally, in order to define an anomaly score, simply compose a decreasing function, say, $h$, with the p-value. 
\defn [Anomaly Score] 
	\label{defn:a-score}
	An anomaly score that respects a distribution $f$ is of the form  $A = h\circ pv_f(x)$, for strictly decreasing $h: [0,1] \to [0,1]$.  
\edefn
Since $h$ can be any strictly decreasing function, $h(x) = (1-x)$ is a natural choice for simply inverting [0,1] so that low p-values (anomalies) get high scores and conversely. 
For the theorems that follow, we use both the p-value threshold denoted by $\beta$, and the  corresponding anomaly score threshold is simply $\alpha:= h(\beta)$.% (for any fixed $h$ as above). 

% Our hypotheses are slightly restrictive, specifically because the setup requires an explicit probability distribution, but this assumption yields worthwhile advantages in the result that follow. 

\subsection{Theorems}
\label{sec:theorems}
In this section we present the mathematical results that make precise the relationship between an anomaly threshold and the likelihood of an alert. 
Theorem~\ref{thm:alert-rate} and ensuing corollaries %and Theorem~\ref{thm:pfp-rate} 
give sharp estimates for bounding the alert rate %and the false detection rate 
in terms of the threshold.  
% Provided the function $h$ in the definition of the anomaly score (\ref{defn:a-score}) is known, the theorems provide the following operational benefits. 
% \begin{enumerate}
% 	\item Theorem~\ref{thm:alert-rate} allows a priori regulation of the alert rate by setting the threshold.  
% 	\item If data is sampled according to $f$, and operator-defined positives are 
	
% 	one can quantify how fit the model is to the task in the sense of Definition~\ref{defn:model-fit}, then operators can regulate the false positive rate a priori by setting the threshold. In particular, if the model is precise then the false positive rate can be made as small as desired. 
% 	\item Furthermore, since the bounds in Lemma~\ref{lem:alert-rate} hold independent from the distribution, the same threshold will suffice in a dynamic setting where the $f$ changes in time for both alert rate and false positive regulation.  
% 	\end{enumerate}
% }

%Below we prove the results in turn and give examples of their use. 

% This section builds on work of Ferragut et al.~\cite{ferragut2012new}; in particular, Lemma~\ref{lem:alert-rate} is mathematically equivalent to the lone theorem in their work, and the algorithm following the theorems is outlined in their work, but is neither given explicitly, analyzed, nor tested. 
% Our contributions in this section include expanding the mathematical results, formulating an algorithm for alert-rate regulation via setting the threshold, and providing empirical results to test its efficacy on both synthetic and real data. 

\begin{lemma}
	\label{lem:alert-rate}
	Let $f$ denote a probability distribution.  For all $\beta \geq 0,$ 
	\begin{align*} 
	P_f(\{x: pv_f(x) \leq \beta\}) & \leq \beta \\
	P_f(\{x: pv_f(x) > \beta\}) & \geq 1 - \beta.
	\end{align*}
	Furthermore, equality holds in both if and only if $\beta = \sup \{x\in X : pv_f \leq \beta\}$.  
\end{lemma}

\begin{proof}
	Suppose for the moment there exists $y\in X$ such that $pv_f(y) = \beta$. 
	Then 
	\begin{align*} 
	\{pv_f \leq \beta\} & = \{x: pv_f(x) \leq pv_f(y)\}\\
						& = \{x: f(x)\leq f(y)\}.
	\end{align*} 
	It follows that 
	\begin{align}
	\label{eqn-sharp} 
	P_f(\{x: pv_f(x) \leq \beta \})	& = P_f(\{x: f(x) \leq f(y) \} ) \nonumber \\
								 	& = pv_f(y) = \beta. %\nonumber 
	\end{align} 
	Hence we have equality in this case, which shows the inequalities are sharp, once proven. 

	To prove the inequality let 
	$r = \sup \{x\in X : pv_f \leq \beta\}.$
	There exists $x_n \in X$ such that $pv_f(x_n) \nearrow r,$ hence 
	$$\{x: pv_f(x) \leq \beta\} = \bigcup \{x: pv_f(x) \leq pv_f(x_n)\}.$$ 
	Since the sets on the right are a nested, increasing family, we have 
	\begin{align*}
	P_f(\{x: pv_f(x) \leq \beta\})& = \lim_n P_f(\{x: pv_f(x) \leq pv_f(x_n)\}) \\ 
		& = \lim_n pv_f(x_n) \text{ \hphantom{aaaaa}\hfill by (\ref{eqn-sharp}) } \\ 
		& = r \leq \beta.
	\end{align*}
	This proves the first inequality, and establishes equality iff $\beta = r = \sup \{x\in X : pv_f \leq \beta\}.$ 
	Finally, 
	$P_f(\{x: pv_f(x) > \beta\}) = 1 - P_f(\{x: pv_f(x) \leq \beta\}) \geq 1 - \beta$.
% 	\begin{align*}
% 	P_f(\{x: pv_f(x) > \beta\})	& = 1 - P_f(\{x: pv_f(x) \leq \beta\}) \\
% 								& \geq 1 - \beta.
% 	\end{align*}
\end{proof}

\noindent Roughly speaking, the lemma says that if we sample $x$ from distribution $f$, and compute its p-values, $pv_f(x)$, the chance that the $pv_f(x)$ is less than a fixed number $\beta$ is less than or equal to $\beta$. 
The next theorem translates this to the AD setting.
%; to wit,  this bound on the chance of an anomaly implies a bound on the number of alerts in terms of the threshold. 

\thm 
	\label{thm:alert-rate}[Alert Rate Regulation Theorem]
	Let $h$ be strictly decreasing so that $A_f = h\circ pv_f$ is an anomaly score that respects the distribution $f$. 
	Let $\alpha$ denote the alert threshold (so $x$ is called ``anomalous'' iff $A_f(x) \geq \alpha$), and set $\beta = h^{-1}(\alpha)$.  
	Let $S \subset X$ be a set of independent samples from PDF $f$. 
	Then the expected number of alerts in $S$ is bounded above by $\beta |S|,$ i.e., 
	$$
	E[\{x\in S: A_f(x) \geq \alpha \}] \leq \beta |S|. 
	$$
\ethm
\begin{proof}
	By definition of $A_f$ and $\beta$, we have 
	\begin{align*}
	E[\{x\in S: A_f(x) \geq \alpha \}]	& = \sum_{x\in S} P_f(\{ A_f(x) \geq \alpha \})\\
										& = \sum_{x\in S} P_f(\{pv_f(x)\leq \beta \})\\
										& \leq \beta |S|, 
	\end{align*}
	with the last inequality provided by the Lemma. 
\end{proof}

\cor
\label{cor:eq-1}
If $pv_f:X\to[0,1]$ is surjective, then equality holds in the preceding theorem, lemma for all $\beta$.
\ecor

\cor
\label{cor:continuous}
If $X$ is a connected topological space, $f$ is not the uniform distribution, and  $pv_f$ is continuous, then $pv_f(X) = [a,1]$ for some $a\in [0,1]$; hence, equality holds in the preceding theorem and lemma for all $\beta \in [a,1)$. 
\ecor


\cor
\label{cor:eq-2}
Suppose $X$ is a topological space and, for all $ y > 0,$ $m \{x : f(x) = y \} = 0.$ Then equality holds in the preceding theorem and lemma. 
\ecor

\begin{proof} 
	Let $x_0, x_1\in X$ such that $0\leq f(x_0) \leq f(x_1)$. Then 
	\begin{align} 
	\label{eqn:cor-estimate}
	0 	& \leq pv_f(x_1) - pv_f(x_0) \nonumber \\
		& = \int_{\{ t: f(t)\in (f(x_0), f(x_1)]  \}} f dm \nonumber \\
		& \leq \| f \|_1 m \{ t: f(t)\in (f(x_0), f(x_1)]  \} \\ 
		& = m \{ t: f(t)\in (f(x_0), f(x_1)]  \} \text{ \hphantom{aaa}\hfill since $f$ a PDF.} \nonumber 
	\end{align}
	By hypothesis, for any $x_1\in X,$
	\begin{align*}
		0 	& = m \{ t: f(t) = f(x_1)\} \\
			& = \bigcap_{ \{ x_0 : f(x_0) < f(x_1)\} } m \{ t: f(t)\in (f(x_0), f(x_1)]  \}\\
			& = \lim_{ f(x_0) \nearrow f(x_1)}  m \{ t: f(t)\in (f(x_0), f(x_1)]  \}  
	\end{align*} 
	by continuity of measures from above. 
	Hence, for all $x_1 \in X$ and $\epsilon > 0$ there exists $x_0 \in X$ such that $m \{ t: f(t)\in (f(x_0), f(x_1)]  \} \leq \epsilon$. 
	
	It follows from Inequality~(\ref{eqn:cor-estimate}) that $ \sup \{x \in X : pv_f(x) < pv_f(x_1)\} = pv_f(x_1).$
	This establishes the condition of Lemma~\ref{lem:alert-rate} for equality with $\beta = pv_f(x_1)$.  
\end{proof}

\subsection{Examples and Explanations} 
%\textcolor{red}{include plot of multinomial w/ three bars and aligned p-value distribution just below it.  Caption: Trinomial distribution depicted with corresponding p-value distribution below. The p-value thresholds 1/6, 1/2, and 1 are the only values for which equality holds in the theorems. For these values the expected percentage of alerts are exactly 1/6, 1/2, and all the data. Yet for other values the inequality is strict and intermediate percentage of observations cannot be realized; e.g., using p-value threshold $\beta\in$ [0,1/6) will yield exactly 0 events with lower p-value and using $\beta = 1/2 - \epsilon$ will yield exactly 1/6 of the samples (for small $\epsilon$).  }


%\textcolor{red}{include plot of standard normal distribution w/ p-value distribution just below it. similar caption. }


See Figure~\ref{fig:f_pvf} depicting a simple trinomial distribution and corresponding p-value distribution.
\begin{wrapfigure}{r}{0.24\textwidth} 
	\vspace{-.1cm}
	\centering
	\includegraphics[scale=0.19]{f.png}
	\includegraphics[scale=0.2]{pvf.png}
	\caption[caption]{Trinomial distribution depicted with corresponding p-value distribution. P-value thresholds 1/6, 1/2, and 1 are the only values for which equality holds in the theorems. For these threshold values the expected percentage of alerts are exactly 1/6, 1/2, 1, and, moreover, these are the only percentages possible; e.g., using p-value threshold $\beta\in [0,1/6)$ will yield exactly 0 events and $\beta\in [1/6, 1/2)$ yields an expected 1/6 of the events as alerts. This illustrates a fundamental limitation of PDFs with plateaus. Note that this phenomenon can occur with continuous $f$ as well.}
	\label{fig:f_pvf}
	\vspace{-.3cm}
\end{wrapfigure}
This illustrates a fundamental limitation of distributions experiencing plateaus, namely, that the plateau forces a discontinuity in the rate of alerts as a function of the threshold. 
In this distribution the operator can either yield exactly none of all events or 1/6\textsuperscript{th} as the threshold changes from below to above $pv_f = 1/6$. 
As the extreme case, consider the uniform distribution in which all events are equally likely/anomalous. 
With the uniform distribution, the operator can yield exactly none or all events. 
Note that the limitation is independent of the method for choosing the threshold and poses a general problem for AD. 
We shall see this limitation appearing in our experiments with real data. 


% \begin{figure*}[ht]
% 	\begin{minipage}[b]{0.45\textwidth}
% 	\centering
% 	\includegraphics[scale=0.15]{f.png}
	
% 	\includegraphics[scale=0.15]{pvf.png}
% 	\caption[caption]{Trinomial distribution depicted (left) with corresponding p-value distribution (right). P-value thresholds 1/6, 1/2, and 1 are the only values for which equality holds in the theorems. For these threshold values the expected percentage of alerts are exactly 1/6, 1/2, and all the data, and, moreover, these are the only percentages possible; e.g., using p-value threshold $\beta\in [0,1/6)$ will yield exactly 0 events and $\beta\in [1/6, 1/2)$ yields an expected 1/6 of the events as alerts. This illustrates a fundamental limitation of PDFs with plateaus. Note that this phenomenon can occur with continuous $f$ as well.}
% 	\label{fig:f_pvf}
% 	\end{minipage}
% %\end{figure*}
% \hfill
% %\begin{figure*}[ht]
% 	\begin{minipage}[b]{0.45\textwidth}
% 	\centering
% 	\includegraphics[scale=0.5]{normal.png}    
	
% 	 \vspace{1cm}
% 	\includegraphics[scale=0.5]{normal_pv.png}
% 	\caption[caption]{The standard normal distribution PDF (left) is depicted with corresponding p-value distribution (right). In this case, the p-value is continuous, and the issue faced by the aforementioned trinomial distribution is avoided. Operationally, this means for any specified percentage $p$, the threshold can be set to isolate the most anomalous $p\%$ of the distribution. \newline}
% 	\label{fig:normal}
% 	\end{minipage}
% \end{figure*}



Corollaries~\ref{cor:eq-1}, \ref{cor:continuous}, and \ref{cor:eq-2}  are crafted to identify when this limitation is not present. 
\begin{wrapfigure}{r}{0.23\textwidth} 
    \vspace{-.1cm}
	\centering
	\includegraphics[scale=0.24]{normal.png}    
% 	 \vspace{.1cm}
	\includegraphics[scale=0.3]{normal_pv.png}
	\caption[caption]{The standard normal distribution PDF is depicted with corresponding p-value distribution. In this case, the p-value is continuous, and the issue faced by the aforementioned trinomial distribution is avoided. Operationally, this means for any specified percent $p$, a threshold can be set to isolate the most anomalous $p\%$ of the distribution. \newline}
	\label{fig:normal}
	\vspace{-.5cm}
\end{wrapfigure}
As a simple example, consider the standard normal distribution, Figure~\ref{fig:normal}. 
Regarding the plot of the corresponding p-value distribution is is easy to see continuity. 
Since $pv_f(x) = 2F(-|x|)$, where $F$ is the cumulative distribution function (CDF), it follows from Corollary~\ref{cor:continuous} that we have equality in Theorem~\ref{thm:alert-rate} for all $\beta\in[0,1].$ 
The same result follows from Corollary~\ref{cor:eq-2} and the fact that $f$ has no plateaus. 
Hence, the equality condition for all threshold values means that one can specify the expected number of alerts; quite explicitly, if one desires exactly the most anomalous 1/1000\textsuperscript{th} of the data, then simply setting the p-value threshold to $\beta = 0.001$ guarantees the result. 
Using the contrapositive, we see that if the model $f$ admits equality for some p-value threshold, $\beta$, then an average number of alerts above/below $\beta$\% indicates that the tails of $f$ are too small/big, respectively. 
Without equality one can only detect tails that are too thick. 

Finally, we note that while these examples are simple distributions chosen for illustrative purposes, the theorems hold under the specified, very general hypotheses. 
All that is needed is a known measure $m$ for which the probability measure is absolutely continuous. 


