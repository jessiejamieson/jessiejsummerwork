

% 4-prob.tex
\section{Mathematical Results in the Probabilistic Setting}
\label{sec-prob-setting}
Below we leverage an assumed probability distribution to (1) enumerate general criteria that any anomaly score should satisfy, (2) give an explicit definition of an anomaly score in terms of the probability distribution, and (3) provide a theorem giving sharp bounds on the alert rate in terms of the threshold. 
The work in this section closely follows the advancements of Ferragut et al., but generalizes results and definitions in manner that preserves their merit and eliminates unnecessarily restrictive hypotheses. 

We assume throught the section that observed data is sampled according to a probability density function (PDF) $f: X\to [0,\infty)$, $\|f\|_{L^1(\mu)} = 1$. Let $P_f(S) = \int_S fd\mu$ denote the corresponding probability measure. When no other PDF is in play, $P_f$ will be shortened to simply $P$. Similarly, inverse subsets such as $\{t: f(x) < y \}$ will be denoted $\{f<y\}$ when no ambiguity arises. We need the following definitions and results. 

\defn [p-value]
	\label{defn-p-value}
	The {\it p-value} of $x\in X$ with respect to distribution $f$, is denoted $pv_f: X\to [0,1]$ and is defined as 
	$$ pv_f(x): = \int_{\{t: f(t)\leq f(x) \} } f d\mu  = P_f(\{t: f(t)\leq f(x) \} ).$$
\edefn

\noindent Note that the p-value as defined above is exactly a generalization of the 2-sided p-value of a univariate Gaussian distribution. 

\prop 
	\label{prop-pv-me-f}
	Let $f, P$ denote a probability density function and corresponding measure as above. Then $pv_f(x)$ is monotonically equivalent to $f$.
	\eprop
\begin{proof}

\noindent Since $f\geq 0$ it follows that 
$$\int_{\{t : f(t) \geq f(x)\} } f d\mu < \int_{\{t : f(t) \geq f(y) \} } f d\mu \Leftrightarrow f(x) > f(y).$$ 
\end{proof}

\subsection{Defining Anomalies}
We now consider the problem of how to define an anomaly score in the probabilistic setting. 
Looking to the literature, a common strategy is to follow the intuition that the low probability events should correspond to anomalies, and hence define an anomaly score as a decreasing function of the probability. For example, Tanden and Chan ~\cite{tandon2009tracking} use $A_f(x) := -\log_2(f(x))$. As identified by Ferragut et al., the problem with this approach is that the score's values are dependent on the distribution, and hence are not comparable across detectors. Consequently, Ferragut et al. leverage the p-value of a distribution to ensure comparability, by defining $A_f(x):= -log_2 (pv_f(x))$.\footnote{To match our definition, which requires range $[0,1],$ one can precompose both previous definitions with $x\mapsto 1-2^{-x}$.} 
As we shall see in \ref{counter-example}, this definition is too restrictive for extending results to non-probabilistic dectors.
The main takeaway is that p-values provide a distribution-independent value indicating how rare an event is. 

In light of these observations, any anomaly score definition based on a probability distribution $f$ should satisfying the following three characteristics
\begin{enumerate}
	\label{important properties}
	\item A score is defined for any distribution.
	\item All such scores respects the distribution; that is, $f(x)>f(y) \Leftrightarrow A_f(x)<A_f(y).$
	\item Scores are comparable across distributions: $\forall \beta>0, \exists r>0 $ such that $P_f\{x: A_f(x)\geq \beta\} = r, \forall f.$  Roughly speaking this says that for fixed threshold, $\beta$ there is a $d\in[0,1]$, so that the set of anomalous data is the least likely $d$ (percent) of the events, for any distribution $f$. 
\end{enumerate}
\noindent while otherwise being as general as possible. 

\defn
	\label{defn-prob-ascore}
	An anomaly score $A_f$ respecting PDF $f$ is any function of the form $h\circ pv_f: X \to [0,1]$, where $h: [0,1]\to[0,1]$ is a strictly decreasing, surjective function.
\edefn

\noindent Note that given PDF $f$,  $A_f:= 1-pv_f$ satisfies Definition \ref{defn-prob-ascore}; hence, such a function always exists. Next (2) and (3) of \ref{important properties} are trivially satisfied by $A_f$. 
Lastly, to see that this definition coincides with the Definition \ref{defn-gen-ascore}, let $x_n\in X$ such that $f(x_n) \nearrow \sup f$. Then surjectivity of $h$ implies $h\circ  pv_f \searrow 0.$ This shows $\inf A_f = 0$ for any $A_f$ satisfying \ref{defn-prob-ascore}. 
The main advantage of the definition above is that $\{A_f(x) \geq \beta\}$ picks off the same proportion of most anomalous events regardless of the distribution. 

%that A that anomaly detection involves finding relatively rare subsets of data. This nuance is important. Suppose for the moment data is believed to be drawn from a known probability distribution. At first glance, anomalies seem to be thoses samples which have low likelihood, but this is of course relative to the distribution; e.g., a fair die with a million sides will produce every event with (low?) likelihood of 1/1000000, yet each is a maximally likely event! These considerations lead Ferragut et al. to propose that anomalous events are those with low p-values. More specifically, they propose an anomaly score as -log(p-value(x)) taking value in $[0,\infty]$ so that high scores indicate anomalous data point. This has an immediate benefit of comparability across distributions. For example, if two seemingly disparte distributrions are used in tandem, how can we 


%%%Mahalanobis example if I want to include it 
\defn[Mahalanobis Distance]
	\label{defn-mahal}
	Let $$f(x)=(2\pi)^{-\frac{n}{2}}|\Sigma|^{-\frac{1}{2}}e^{-\frac{1}{2}(x-\vec{\mu})^t\Sigma(x-\vec{\mu})},$$ 
	the PDF for $N(\vec{\mu}, \Sigma)$,  the Gaussian distribution with mean $\mu=(\mu_1, \dots, \mu_n)^t$ and covariance matrix $\Sigma$. Then the Mahalanobis distance is defined as $d(x,y)= ((x-y)^t\Sigma^{-1}(x-y))^{\frac{1}{2}}$.   
\edefn
\begin{example}
	\label{example-mahal}
	Perhaps one of the most common examples of unsupervised anomaly detection is to simply fit a Gaussian distribution to the observed data and use Mahalanobis distance from a given point to the mean, i.e., $ x\mapsto d(\vec{\mu},x) $ as an anomaly score. 
	Letting $f$ denote the PDF for a normal distribution, and $\phi(x) = d(\vec{\mu},x) $ it is clear from the definitions that $f \sim  - \phi$. 
	It follows from Proposition~\ref{prop-pv-me-f} that $pv_f(x) ~\sim f$; hence, there exists a strictly decreasing function $h$ such that $h\circ pv_f = \phi.$ 
	This shows that Definition~\ref{defn-prob-ascore} is a generalization of the well-accepted Mahalanobis distance score. 
\end{example}


\subsection{Theorems for regulating the alert rate \& Results }
In this section we present the main results for setting the threshold of an anomaly detector. In particular, Theorems \ref{thm-alert-rate} and Theorem \ref{thm-fp-rate} give sharp estimates for bounding the alert rate and the false positive rate in terms of the threshold. While these theorems still rely on the underlying probability distribution, our work in the next setting will extend these results to any anomaly detection system. At this point, provided the function $h$ in the definition of the anomaly score (\ref{defn-prob-ascore}) is known, the theorems provide the following operational benefits. 
\begin{enumerate}
	\item Theorem~\ref{thm-alert-rate} allows a priori regulation of the alert rate by setting the threshold.  
	\item If one can quantify how fit the model is to the task in the sense of Definition~\ref{defn-model-fit}, then operators can regulate the false positive rate a priori by setting the threshold. In particular, if the model is precise then the false positive rate can be made as small as desired. 
	\item Futhermore, since the bounds in Lemma \ref{lem-alert-rate} hold indpendent from the distribution, the same theshold will suffice in a dynamic setting where the $f$ changes in time for both alert rate and false positive regulation.  
	\end{enumerate}
\noindent Below we prove the results in turn and give examples of their use. 

\begin{lemma}
	\label{lem-alert-rate}
	Let $f$ denote a probability distribution.  For all $\beta \geq 0,$ 
	\begin{align*} 
	P_f(\{x: pv_f(x) \leq \beta\}) & \leq \beta \\
	P_f(\{x: pv_f(x) > \beta\}) & \geq 1 - \beta.
	\end{align*}
	Furthermore, equality holds in both if there exists $y \in X$ such that $pv_f(y) = \beta.$ 
\end{lemma}

\begin{proof}
	Suppose for the moment $\exists y\in X$ such that $pv_f(y) = \beta$. Then $\{pv_f \leq \beta\} = \{x: pv_f(x) \leq pv_f(y)\} = \{x: f(x)\leq f(y)\}$, with the last equality following from Propositions \ref{prop-pv-me-f} and \ref{prop-me-facts}. It follows then that 
	\begin{equation}
	\label{eqn-sharp} 
	P_f(\{x: pv_f(x) \leq \beta \}) = P_f(\{x: f(x) \leq f(y) \} ) = pv_f(y) = \beta. 
	\end{equation} 
	Hence we have equality in this case, which shows the inequalities are sharp, once proven. 

	To prove the inequality let $r = \sup_{\{pv_f \leq \beta\}} pv_f \leq \beta. $
	There exists $x_n \in X$ such that $pv_f(x_n) \nearrow r,$ and $\{x: pv_f(x) \leq \beta\} = \bigcup \{x: pv_f(x) \leq pv_f(x_n)\}.$ 
	Since the sets on the right are a nested, increasing family, we have 
	\begin{align*}
	P_f(\{x: pv_f(x) \leq \beta\})& = \lim_n P_f(\{x: pv_f(x) \leq pv_f(x_n)\}) \\ 
		& = \lim_n pv_f(x_n) \text{ \hphantom{aaaaa}\hfill by (\ref{eqn-sharp}) } \\ 
		& = r \\
		& \leq \beta.
	\end{align*}
	This proves the first inequality. 
	Finally, 
	\begin{align*}
	P_f(\{x: pv_f(x) > \beta\})	& = 1 - P_f(\{x: pv_f(x) \leq \beta\}) \\
								& \geq 1 - \beta.
	\end{align*}

\end{proof}

\noindent We now state our alert rate theorem, which follows immediately from the lemma. 
\thm 
	\label{thm-alert-rate}[Alert Rate Regulation Theorem]
	Let $h$ be strictly decreasing so that $A_f = h\circ pv_f$ is an anomaly score that respects the distribution $f$. Let $\alpha$ denote the alert threshold (so $x$ is called ``anomalous'' iff $A_f(x) \leq \alpha$), and set $\beta = h^{-1}(\alpha)$.  Then the expected alert rate is bounded above by $\beta.$
\ethm

\cor
In the case that $X$ is a topological space and \\
$pv_f:X\to[0,1]$ is continuous and surjective, then equality holds in the preceding theorem and lemma. 
\ecor

In order to use the theorems above, suppose we receive $N$ data points per day but operators only have resources to inspect the most anomalous $n \leq N$ each day. 
Let $f_n$ be a PDF fit to all observations up to but not including time $n$. 
Set the anomaly score, $A_n = h\circ pv_{f_n}$ where $h$ is any  (fixed) strictly decreasing bijection of the unit interval.  
Suppose that upon receipt of $x_{n+1}$, the data at time $n+1$, we check if $A_n(x_{n+1}) \geq \alpha$, then update $f_n$ to $f_{n+1}.$ 
Then the expected number of alerts per day is 
$$ \sum_{n = 0}^{N-1} P_{f_n}( A_{n}(x_{n+1}) \geq \alpha ) \leq N h^{-1}(\alpha).$$
Hence, choosing $\alpha = h(n/N)$ ensures that operators will not be overloaded by alerts. 

Below we present emperical experiments .. 
Experiment 1 - sample from a multinomial 
Experiment 2 - sample from a continuous distribution (use the corollarry)
Experiment 3 - use GraphPrints data w/ Gaussian  ... 
Experiment 4 - use multinomial on VAST data -- ask Joel!! 

The tables below present results for an experiment comparing untrained anomaly detection schemes with a trained anomaly detection scheme. We expect 40 alerts per day with an 8 hour day, with 1 data point per second and 100 additional training points. After $d$ days, this gives $d(8\cdot 60^2\cdot 1)+100$ points containing anomalies. The number of alerts per day is charted over a week, and the experiment was repeated 10 times. 

%The data in these tables were generated using the code
%UntrainedData.py and TrainedData.py, respectively.

\begin{table}[]
\centering
\caption{Alerts Per Day Over a 10 Week Period Using Untrained Methods}
\label{my-label}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|}
\hline
\multicolumn{11}{|c|}{Alerts Per Day Over a 10-week Period, Untrained} \\ \hline
\begin{tabular}[c]{@{}l@{}}Week Number/\\ Day\end{tabular} & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\ \hline
1 & 43 & 51 & 27 & 44 & 49 & 35 & 38 & 41 & 47 & 37 \\ \hline
2 & 27 & 39 & 55 & 31 & 48 & 47 & 43 & 50 & 44 & 52 \\ \hline
3 & 38 & 41 & 44 & 34 & 23 & 61 & 46 & 20 & 27 & 35 \\ \hline
4 & 23 & 34 & 29 & 25 & 33 & 19 & 3 & 51 & 45 & 39 \\ \hline
5 & 76 & 38 & 53 & 62 & 30 & 18 & 72 & 19 & 18 & 47 \\ \hline
6 & 40 & 30 & 16 & 10 & 79 & 39 & 23 & 78 & 70 & 25 \\ \hline
7 & 22 & 32 & 85 & 69 & 43 & 77 & 43 & 38 & 14 & 65 \\ \hline
Average Per Day & 38.4 & 37.8 & 44.1 & 39.3 & 43.5 & 42.2 & 38.2 & 42.4 & 37.8 & 42.8 \\ \hline
\end{tabular}
\end{table}

\begin{table}[]
\centering
\caption{Alerts Per Day Over a 10 Week Period Using Trained Methods}
\label{my-label}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|}
\hline
\multicolumn{11}{|c|}{Alerts Per Day Over a 10-week Period, Trained} \\ \hline
\begin{tabular}[c]{@{}l@{}}Week Number/\\ Day\end{tabular} & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\ \hline
1 & 38 & 49 & 44 & 41 & 36 & 41 & 40 & 38 & 44 & 46 \\ \hline
2 & 34 & 35 & 42 & 31 & 36 & 34 & 35 & 41 & 36 & 42 \\ \hline
3 & 46 & 26 & 40 & 30 & 36 & 38 & 53 & 40 & 38 & 40 \\ \hline
4 & 52 & 37 & 34 & 52 & 49 & 37 & 45 & 43 & 49 & 32 \\ \hline
5 & 46 & 42 & 47 & 39 & 37 & 45 & 41 & 35 & 40 & 36 \\ \hline
6 & 47 & 34 & 47 & 49 & 41 & 37 & 29 & 51 & 56 & 42 \\ \hline
7 & 44 & 43 & 34 & 35 & 42 & 43 & 41 & 39 & 42 & 43 \\ \hline
Average Per Day & 43.8 & 38.0 & 41.1 & 39.5 & 39.5 & 39.2 & 40.5 & 41.0 & 43.5 & 40.1 \\ \hline
\end{tabular}
\end{table}
% \begin{lemma}
% 	Suppose  $(X, \mathfrak{M}, \mu)$ is a topological space, and $f$ a PDF as before. For all $ y > 0$, suppose $\mu\{ f(x) = y \} = 0.$ Then $pv_f$ is continuous. 
% \end{lemma}

% \begin{proof} 
% 	Fix $x_0 \in X $ such that $f(x_0) > 0.$ We claim that for all $\epsilon > 0, \exists \delta > 0 $ such that $\mu \{ x : f(x_0) \leq f(x) \leq f(x_0 + \delta)$
% \end{proof}

\subsection{Theorems for Regulating the False Positive Rate}
As discussed above, the false-positive rate of an anomaly detectors is influenced by both the theshold and how well the model fits the task. 
The following developments make explict this relationship, and leverage Lemma \ref{lem-alert-rate} to provide bounds on the false positive rate. 

\defn[True Positive Function \& Model-Task Fit]
	\label{defn-model-fit}
	With PDF $f$ as above, and $A_f$ an anomaly score that respects $f$, we define the {\it True Positive Function, } $TP( \cdot ; f, A_f): [0,1] \to [0,1],$ as 
	\begin{align*}
	TP(\alpha) 	&= P_f(\text{ a true positive } | A_f(x) \geq \alpha)\\
				&= P_f( \{x : x \text{ a true positive } \}  \cap \{A_f \geq \alpha \} ) / P_f(\{ A_f \geq \alpha\}).
	\end{align*}
	\noindent As $TP$ is regarded as a function of $\alpha$, the $f, A_f$  will usually be supressed. 
	We say $(f,A_f)$ is a {\it precise} model iff $TP$ is increasing in $\alpha$ and $\sup TP = 1$. 
	Finally, we say $(f, A_f)$ is a {\it perfectly-fit} model iff  $ \exists  \alpha  > 0$ so that $\{ \text{true positives} \} = \{ A_f \geq \alpha \}.$
\edefn

Roughly speaking, $TP$ gives the probability an event is a true positive given the event is anomalous. 
Note that $TP$ is increasing means that the greater the anomalousness of the event, the greater the probability it is a true positive. Lastly, combining increasing with the hypothesis that $\sup TP = 1$ means that asymptotically, as $\alpha \nearrow 1$, anomalousness perfectly indicates a true positive. Hence the definition of a precise model is intuitive, as such models can have arbitrarily high precision. 
Finally, we note that perfectly-fit models are clearly precise models, but as a precise model may have a non-zero false-negative rate for all $\alpha$, the inclusion is strict. 

How well-fit a model is to the task depends on both the application and the operator. For example, in the cyber domain, one network analyst may identify any abnormal activity (e.g., an external port scan, which alone is non-malicious) as a true positive, while others may deem only bonafide intrusions as a true positive. Hence, $TP$ quantifies how often the relatively rare events according to the model ($f$) correspond to operator-defined true-positives. We expect that in practice the function $TP$ can be estimated from labeled data via a domain expert, or at least determine whether $A_f$ is precise, as this only entails looking at the subset of anomalous events, i.e., $\{A_f \geq \alpha\}$ for large $\alpha$. With access to both $TP$ and $h$, the theorems below prove a strict bound on the false positive rate. 

INCLUDE plot of $A_f$ vs. $x$. show precise model -- all high values are red points (TPs), but a few low ones too. show a perfect model, all high are TP, all low are TN.  


The theorem below allows operators with knowledge of the $TP$ for at least high values of $\alpha$ to bound the false-positive rate a priori. Furthermore, the corollary shows that in the presence of a precise model, we can guarantee the false positive rate is as small as desired. 


\thm[False Positive Regulation Theorem]
	\label{thm-fp-rate}
	Let $f$ denote a PDF, $A_f$ an anomaly score that respects $f$, and $TP$ the true-positive function. 
	Then $FP(\alpha)$, the false positive rate with threshold $\alpha$, is bounded above by $h^{-1}(\alpha)(1-TP(\alpha))$, and this inequality is sharp. 
\ethm

\begin{proof}
	Using Lemma \ref{lem-alert-rate}, we have 
	\begin{align*}
		FP(\alpha) 	&= P_f(\{x: A_f(x) \geq \alpha \} \cap \{x: x \text{ not a true-positive } \})\\
					&= P_f(\{A_f \geq \alpha\}) (1-TP(\alpha))\\
					& \leq h^{-1}(\alpha)(1-TP(\alpha))
	\end{align*}
\end{proof}

\cor
	If $A_f$ is precise, in the sense of Definition \ref{defn-model-fit}, then for all $\epsilon > 0, \exists \delta > 0$ such that $\alpha \geq 1-\delta$ implies $FP(\alpha) \leq \epsilon$. 
\ecor

 
Finally, we conclude with a discussion of the limitations of the method proposed above. The underlying assumption for all results is that data is sampled from the probability distribution $f$. Hence, if this assumption fails to hold, for example, if there is a change of state of the system or if $f$ poorly describes the data, then these bounds may cease to hold. 
This insight gives a useful ancilliary benefit of this mathematical machinary, namely, a method to measure how well a model fits data. This is to topic of Section \ref{}. 
