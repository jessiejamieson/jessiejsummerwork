%6-epsilon-sep-multi.tex

\section{$\epsilon$-Separated Multinomial Distributions}
\label{eps-sep}

In the case of anomaly detection when the probability distribution of incoming data is unknown, we wish to be able to recover, as closely as possible, the original probability distribution. This will allow us to more accurately identify anomalous data using their anomaly scores. To this end, we will examine and alter properties of multinomial distributions. 

\defn[Multinomial Distribution]
\label{multdist}
Let $x = (x_1,\dots, x_j)$ be random mutually exclusive outcomes. Assume these outcomes occur with respective nonzero probabilities $p=(p_1,\dots,p_j)$. Then if the random variables $k_i$ indicate the number of times outcome $x_i$ is observed over $n$ trials, the vector $k = (k_1, ..., k_m)$ follows a multinomial distribution. We will denote this by $k\sim Mult(n,p)$.
\edefn

An example of multinomially distributed events is the set of $r$ rolls of an $n$-sided die, where each side $x_i$ will occur with probability $p_i$. It may be the case that multiple events may occur with the same probability. In this case, we let $f_1,\dots, f_m$ denote the distinct values of $p$, and $k_j=|x_j|=|\{i:p_i=f_j\}|$; that is, $k_j$ is the number of events which occur with probability $f_j$ so that $\sum_j k_j=n$. Using these definitions, we may now define a multinomial distribution with specific restrictions on values of probabilities of events.

\defn[$\epsilon$-Separated Multinomial Distribution]
	\label{defn-me}
	Let $k=\\(k_1,\dots,k_m)\sim Mult(n,p)$ with $p=(p_1,\dots,p_n)$. Let $f_1,\dots, f_m$ denote the distinct 		values of $p$, where, without loss of generality, $0<f_1<\cdots<f_m$. We say $Mult(n,p)$ $\left(Mult(n,f)\right)$ is $\epsilon$-separated if and only if $f_i\geq \epsilon + f_{i-1}$ for some $\epsilon>0$.
\edefn

%From this definition, one can observe that $\epsilon$ is limited in size. If $f_1>\epsilon$ and $f_j\leq f_{j+1}-\epsilon$ for $j\geq 1$, then $\epsilon$ is maximal if and only if $f_j=j\epsilon$. In particular,
%\begin{align*}
%	1=\sum_{j=1}^m k_jf_j=\epsilon\sum_{j=1}^m jk_j\Rightarrow \epsilon\leq\left(\sum_{j=1}^m jk_j\right)^{-1}.
%\end{align*}
%Thus $\epsilon_{max}:=\left(\sum_{j=1}^m jk_j\right)^{-1}.$

These definitions will allow us to quantify the degree of separation between two probability distributions. The results toward this end are summarized in the following theorem.

\thm
	Let $k=(k_1,\dots,k_m)\sim Mult(n,f)$ be $\epsilon$-separated, with $f=\{f_i\}_{i=1}^m$ defined as above for some
	$\epsilon>0$ so that 
		\begin{align*}
		\begin{cases}
		\epsilon\leq f_1, \text{ and } \\
		f_j+\epsilon\leq f_{j+1}\text{ for all }j\geq 1. 	
		\end{cases}\tag{$\ast$}
		\end{align*}
	Then
	\begin{enumerate}
		\item[(a)]{$\epsilon\leq \epsilon_{max}:=\left(\sum_{j=1}^m jk_j\right)^{-1}.$}
		\item[(b)]{$f_j\in \left[  j\epsilon, \frac{\epsilon_{max}-\epsilon}{\sum_{i\geq j}^m k_i}+j\epsilon \right]$.}
		\item[(c)]{If $\hat{f}$ is another multinomial distribution satisfying $(\ast)$, then 
			\begin{align*}
				\|f-\hat{f}\|_{\ell^1}\leq (\epsilon_{max}-\epsilon)\sum_{j=1}^n\left(\sum_{i\geq j} k_i\right)^{-1}.
			\end{align*}}
		\item[(d)]{There exists a unique multinomial distribution satisfying $(\ast)$ when $\epsilon=\epsilon_{max}$.}
	\end{enumerate}
\ethm
\begin{proof}
	\begin{enumerate}
		\item[(a)]{Suppose there exists an $\epsilon>0$ such that $\epsilon\leq f_1$ and $f_j\leq f_{j+1}-\epsilon$ for $j\geq 1$. This implies that $\epsilon$ is maximized when $f_1=\epsilon$, $f_2=2\epsilon$, \dots, $f_m=m\epsilon$. Thus 
		\begin{align*}
		1=\sum_{j=1}^m k_jf_j=\epsilon\sum_{j=1}^m jk_j\Rightarrow \epsilon\leq\left(\sum_{j=1}^m jk_j\right)^{-1}:=\epsilon_{max}.
		\end{align*}}
		\item[(b)]{Next we wish to bound $f_i$. By assumption, we have that $\epsilon\leq f_1$ so that $f_{1,min}=\epsilon$. For an upper bound, observe that maximizing $f_1$ minimizes $f_j$ for $j>1$, so that $f_1$ is largest if and only if $f_2=f_1+\epsilon$, \dots, $f_m=f_1+(m-1)\epsilon$. Hence, 
		\begin{align*}
		1&=\sum_{j=1}^m k_jf_j\\
		&=\sum_{j=1}^m k_j(f_1+(j-1)\epsilon)\\
		&=f_j\sum_{j=1}^mk_j+\epsilon\sum_{j=1}^m(j-1)k_j\\
		&=nf_1+\epsilon\left[\sum_{j=1}^m jk_j-\sum_{j=1}^mk_j\right]\\
		&=nf_1+\epsilon\left(\epsilon_{max}^{-1}-n\right).
		\end{align*}
		Thus, $f_{1,max}=\frac{1-\epsilon\left(\epsilon_{max}^{-1}-n\right)}{n}$. 
		
		Now to extend the bound on $f_1$ to $f_j$ for arbitrary $j$, fix $j$ and note that $f_{j,min}=j\epsilon$, relying on the minimization of $f_i$ for $i<j$. On the other hand, $f_j$ is maximal if and only if for all $i\neq j$ $f_i$ is minimal. This occurs if and only if $f_i=i\epsilon$ for $i<j$ and $f_i=f_j+(i-j)\epsilon$ for $i>j$. Hence
		\begin{align*}
			1&=\sum_{i=1}^m k_if_i\\
			&=\sum_{i<j} k_if_i+\sum_{i\geq j}k_if_i\\
			&=\epsilon\sum_{i<j} i k_i+\sum_{i\geq j}\left(f_j+\epsilon(i-j)\right)k_i\\
			&=\epsilon\sum_{i<j}ik_i+f_j\sum_{i\geq j} k_i+\epsilon\sum_{i\geq j}(i-j)k_i.
		\end{align*}
		Solving this equation for $f_j$ gives that 
		\begin{align*}
			f_j&=\frac{1-\epsilon\left(\sum_{i<j}ik_i+\sum_{i\geq j}(i-j)k_i\right)}{\sum_{i\geq j}k_i}\\
			&=\frac{1-\epsilon\left(\sum_{i<j}ik_i+\sum_{i\leq j}i k_i-\sum_{i\geq j} j k_i\right)}{\sum_{i\geq j}k_i}\\
			&=\frac{1-\epsilon\left(\sum_{i=1}^m i k_i-\sum_{i\geq j} j k_i\right)}{\sum_{i\geq j} k_i}+ j\epsilon\\
			&=\frac{\epsilon_{max}-\epsilon}{\sum_{i\geq j}k_i}+j\epsilon,
		\end{align*}
		so that $f_j$ lies in the desired interval.}
		\item[(c)]{Now let $\hat{f}$ be another multinomial distribution satisfying $(\ast)$. Then 
		\begin{align*}
			\|f-\hat{f}\|_{\ell^1}&=\sum_{i=1}^m|f_i-\hat{f}_i|\\
			&\leq \sum_{i=1}^m(f_{i,max}-f_{i,min})\\
			&=\sum_{i\geq 1}^m\left(\frac{\epsilon_{max}-\epsilon}{\sum_{i\geq j}k_i}+i\epsilon-i\epsilon\right)\\
			&=(\epsilon_{max}-\epsilon)\sum_{i\geq 1}^m\left(\sum_{i\geq j}k_i\right)^{-1}
			\end{align*}
			as desired.
			}
		\item[(d)]{In light of (a) through (c), observe that if $\epsilon=\epsilon_{max}$, it must be the case that $\epsilon=f_1$, which from $(\ast)$ completely determines $f_i=i\epsilon$, giving uniqueness.}
	\end{enumerate}
\end{proof}

\textbf{Example.} Let $n=3$ and $m=2$, with $\epsilon$ maximal. Observe that given $m$ and $n$, either $\epsilon=(1+2\cdot 2)^{-1}=1/5$ or $\epsilon=(2+2\cdot 1)^{-1}=1/4$, and $f_1=\epsilon$, $f_2=2\epsilon$. Thus $\epsilon=\epsilon_{max}=1/4$, with $f_1=1/4$ and $f_2=1/2$. However, if $f=(1/4,1/2)$ and $\hat{f}=(1/5, 2/5)$, then 
	\begin{align*}
	\|f-\hat{f}\|_{\ell^1}&\leq (1/4-1/5)\sum_{j=1}^3\left(\sum_{i\geq j} k_i\right)^{-1}\\
	&=\frac{1}{20}\left[(2+1)^{-1}+(1)^{-1}\right]\\
	&=\frac{1}{15}.
	\end{align*}








