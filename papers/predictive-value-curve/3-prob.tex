%3-prob.tex
\section{Bounding the Alert Rate}
\label{sec:prob}
In this section we focus on bounding the expected number of alerts produced by an anomaly detection system. 
To do so, we consider probabilistic anomaly detectors, which score data's anomalousness according to a probability distribution describing the data. 
We leverage the probabilistic description to provide a theorem that gives sharp bounds on the alert rate in terms of the threshold, independent of the distribution. 
This gives an a priori method to manage the expected number of alerts for any distribution, which is tested on real and synthetic data. 
{\color{BrickRed}  Section blah extends these results to almost any detector. WRITE REST LATER}

\subsection{Setting and Notation}
\label{sec:prob-notation}
Our setting assumes data is sampled independently from a distribution with probability density function (PDF) $f: X \to [0,\infty)$. 
The ambient space, $(X, \mathfrak{M}, m),$ is assumed to be a $\sigma$-finite measure space with measure $m$ and $\mathfrak{M}$ the set of measurable subsets of $X.$ 
We define the probability of a measurable set $S\in \mathfrak{M}$ to be $P_f(S):= \int_S f dm$; i.e., $P_f$ denotes the corresponding probability measure with Radon-Nikodym derivative $f$. 
For almost all applications, $X$ is either a subset of $\mathbb{R}^n$ with $m$ Lebesgue measure, or $X$ is a discrete space with $m$ counting measure.

We say an anomaly score $A(x)$ respects the distribution $f$ if $A(x) \geq A(y)$ if and only if $f(x) \leq f(y)$\textemdash intuitively, $x$ is more anomalous that $y$ if and only if $x$ is less likely than $y$.  
While this can be attained by simply letting $A = h\circ f$ for a decreasing function $h$ (for example, see Tandon and Chan~\cite{tandon2009tracking} where $A:= -\log_2(f)$ ), the work of Ferragut et al.~\cite{ferragut2012new} notes that this inhibits comparability across detectors. 
Because such a definition puts $A$ in one-to-one correspondence with the values of $f$, anomaly scores can vary wildly for different distributions.  
The consequence is that setting a threshold is dependent on the distribution, which is problematic especially for two settings that are common. 
The first is a setting where multiple detectors (e.g., detectors for network traffic velocity, IP-distribution, etc.) are used in tandem as each require different models.
For instances in the literature of using cooperative detectors to reduce false positives see \cite{christodorescu2007can, fontugne2010mawilab}. 
The second setting is when dynamically changing models (where $f$ is updated in real time) are employed, as this requires comparison of anomaly scores over time even though $f$ is constantly evolving. For examples of streaming detection scenarios see \cite{ahmed2007multivariate, bridges2016multi, ferragut2012new, harshaw2016graphprints, wang2011statistical}. 

To circumvent this problem, we employ the p-value (as do many detection capabilities).  
For any distribution the p-value gives the likelihood relative to the distribution. 
Hence, it always takes values in $[0,1]$, and in the specific case of a univariate Gaussian is just the two-sided z-score.  
\defn [p-value]
	\label{defn:p-value}
	The {\it p-value} of $x\in X$ with respect to distribution $f$, is denoted $pv_f: X\to [0,1]$ and is defined as 
	$$ pv_f(x): = \int_{\{t: f(t)\leq f(x) \} } f dm  = P_f(\{t: f(t)\leq f(x) \} ).$$
\edefn 
It is clear from the definition that $pv_f(x) > pv_f(y) $ if and only if $x$ is more likely than $y$, since $f\geq 0$. 
Finally, in order to define an anomaly score, \textit{we assume the stronger condition that the anomaly score is a decreasing function of the p-value}. 
\defn [anomaly score] 
	\label{defn:a-score}
	An anomaly score that respects a distribution $f$ is of the form  $A = h\circ pv_f(x)$, for decreasing $h: [0,1] \to [0,1]$.  
\edefn
We note that this setup is not restrictive but gives advantages of comparability across distributions $f$ and a bounded neighborhood of anomaly scores. 

\subsection{Mathematical Results for Bounding the Alert Rate}
\label{sec:prob-alert-rate-math}
{\color{BrickRed} Rewrite this intro:   In this section we present the main results for setting the threshold of an anomaly detector to limit the expected number of alerts. 
In particular, Theorems \ref{thm:alert-rate} and Theorem \ref{thm:pfp-rate} give sharp estimates for bounding the alert rate and the predictive-false-positive rate in terms of the threshold. 
While these theorems still rely on an explicit probability distribution, our work in the next setting will extend these results to nearly any anomaly detection system (for example detectors relying on a non-probabilistic dimension reduction technique). 
At this point, provided the function $h$ in the definition of the anomaly score (\ref{defn:a-score}) is known, the theorems provide the following operational benefits. 
\begin{enumerate}
	\item Theorem~\ref{thm:alert-rate} allows a priori regulation of the alert rate by setting the threshold.  
	\item If one can quantify how fit the model is to the task in the sense of Definition~\ref{defn:model-fit}, then operators can regulate the false positive rate a priori by setting the threshold. In particular, if the model is precise then the false positive rate can be made as small as desired. 
	\item Furthermore, since the bounds in Lemma~\ref{lem:alert-rate} hold independent from the distribution, the same threshold will suffice in a dynamic setting where the $f$ changes in time for both alert rate and false positive regulation.  
	\end{enumerate}
}
%{\color{NavyBlue} I'll skip this part then, and when I put other stuff in I'll take a look.}


Below we prove the results in turn and give examples of their use. 
This section builds on work of Ferragut et al.~\cite{ferragut2012new}; in particular, Lemma~\ref{lem:alert-rate} is mathematically equivalent to the lone theorem in their work, and the algorithm following the theorems is outlined in their work, but is neither given explicitly, analyzed, nor tested. 
Our contributions in this section include expanding the mathematical results, formulating an algorithm for alert-rate regulation via setting the threshold, and providing empirical results to test its efficacy on both synthetic and real data. 

\begin{lemma}
	\label{lem:alert-rate}
	Let $f$ denote a probability distribution.  For all $\beta \geq 0,$ 
	\begin{align*} 
	P_f(\{x: pv_f(x) \leq \beta\}) & \leq \beta \\
	P_f(\{x: pv_f(x) > \beta\}) & \geq 1 - \beta.
	\end{align*}
	Furthermore, equality holds in both if and only if $\beta = \sup \{x\in X : pv_f \leq \beta\}$.  
\end{lemma}

\begin{proof}
	Suppose for the moment there exists $y\in X$ such that $pv_f(y) = \beta$. 
	Then 
	\begin{align*} 
	\{pv_f \leq \beta\} & = \{x: pv_f(x) \leq pv_f(y)\}\\
						& = \{x: f(x)\leq f(y)\}.
	\end{align*} 
	It follows that 
	\begin{align}
	\label{eqn-sharp} 
	P_f(\{x: pv_f(x) \leq \beta \})	& = P_f(\{x: f(x) \leq f(y) \} ) \nonumber \\
								 	& = pv_f(y)\\
								 	& = \beta. \nonumber 
	\end{align} 
	Hence we have equality in this case, which shows the inequalities are sharp, once proven. 

	To prove the inequality let 
	$r = \sup \{x\in X : pv_f \leq \beta\}.$
	There exists $x_n \in X$ such that $pv_f(x_n) \nearrow r,$ hence 
	$$\{x: pv_f(x) \leq \beta\} = \bigcup \{x: pv_f(x) \leq pv_f(x_n)\}.$$ 
	Since the sets on the right are a nested, increasing family, we have 
	\begin{align*}
	P_f(\{x: pv_f(x) \leq \beta\})& = \lim_n P_f(\{x: pv_f(x) \leq pv_f(x_n)\}) \\ 
		& = \lim_n pv_f(x_n) \text{ \hphantom{aaaaa}\hfill by (\ref{eqn-sharp}) } \\ 
		& = r \\
		& \leq \beta.
	\end{align*}
	This proves the first inequality, and establishes that equality occurs if and only if $\beta = r = \sup \{x\in X : pv_f \leq \beta\}.$ 
	Finally, 
	\begin{align*}
	P_f(\{x: pv_f(x) > \beta\})	& = 1 - P_f(\{x: pv_f(x) \leq \beta\}) \\
								& \geq 1 - \beta.
	\end{align*}

\end{proof}

\noindent We now state the alert rate theorem, which follows immediately from the lemma. 
\thm 
	\label{thm:alert-rate}[Alert Rate Regulation Theorem]
	Let $h$ be strictly decreasing so that $A_f = h\circ pv_f$ is an anomaly score that respects the distribution $f$. 
	Let $\alpha$ denote the alert threshold (so $x$ is called ``anomalous'' iff $A_f(x) \geq \alpha$), and set $\beta = h^{-1}(\alpha)$.  
	Let $S \subset X$ be a set of independent samples from PDF $f$. 
	Then the expected number of alerts in $S$ is bounded above by $\beta |S|,$ i.e., 
	$$
	E[\{x\in S: A_f(x) \geq \alpha \}] \leq \beta |S|. 
	$$
\ethm
\begin{proof}
	By definition of $A_f$ and $\beta$, we have 
	\begin{align*}
	E[\{x\in S: A_f(x) \geq \alpha \}]	& = \sum_{x\in S} P_f(\{ A_f(x) \geq \alpha \})\\
										& = \sum_{x\in S} P_f(\{pv_f(x)\leq \beta \})\\
										& \leq \beta |S|, 
	\end{align*}
	with the last inequality provided by the Lemma. 
\end{proof}

\cor
\label{cor:eq-1}
If $pv_f:X\to[0,1]$ is surjective, then equality holds in the preceding theorem and lemma for all $\beta$.
\ecor

\cor
\label{cor:continuous}
If $X$ is a connected topological space and  $pv_f$ is continuous, then $pv_f(X) = [a,1]$ for some $a\in [0,1)$; hence, equality holds in the preceding theorem and lemma for all $\beta \in [a,1]$. 
\ecor

\cor
\label{cor:eq-2}
Suppose $X$ is a topological space and for all $ y > 0$ $m \{x : f(x) = y \} = 0.$ Then equality holds in the preceding theorem and lemma. 
\ecor

\begin{proof} 
	Let $x_0, x_1\in X$ and assume WLOG $0\leq f(x_0) \leq f(x_1)$. Then 
	\begin{align} 
	\label{eqn:cor-estimate}
	0 	& \leq pv_f(x_1) - pv_f(x_0) \nonumber \\
		& = \int_{\{ t: f(t)\in (f(x_0), f(x_1)]  \}} f dm \nonumber \\
		& \leq \| f \|_1 m \{ t: f(t)\in (f(x_0), f(x_1)]  \} \\ 
		& = m \{ t: f(t)\in (f(x_0), f(x_1)]  \} \text{ \hphantom{aaa}\hfill since $f$ a PDF.} \nonumber 
	\end{align}
	By hypothesis, for any $x_1\in X,$
	\begin{align*}
		0 	& = m \{ t: f(t) = f(x_1)\} \\
			& = m\left[ \bigcap_{ \{ x_0 : f(x_0) < f(x_1)\} }  \{ t: f(t)\in (f(x_0), f(x_1)]  \} \right]\\
			& = \lim_{ f(x_0) \nearrow f(x_1)}  m \{ t: f(t)\in (f(x_0), f(x_1)]  \}  
	\end{align*} 
	by continuity of measures from above. 
	
	Hence, for all $x_1 \in X$ and $\epsilon > 0$ there exists $x_0 \in X$ such that $m \{ t: f(t)\in (f(x_0), f(x_1)]  \} \leq \epsilon$. 
	
	It follows from inequality~\ref{eqn:cor-estimate} that $ \sup \{x \in X : pv_f(x) < pv_f(x_1)\} = pv_f(x_1).$
	This establishes the condition of Lemma~\ref{lem:alert-rate} for equality with $\beta = pv_f(x_1)$. 
	% The continuity of $pv_f$ need not be true.. i think there could be a sequence x_n \to x_1 such that f(x_1) - f(x_n) is big! 

\end{proof}

For an example, consider $f$ the PDF of $N(0,1)$, the standard normal distribution. Since $pv_f(x) = 2F(-|x|)$, where $F$ is the cumulative distribution function (CDF), it follows from Corollary~\ref{cor:continuous} that we have equality in Theorem~\ref{thm:alert-rate} for all $\beta\in[0,1].$ 
The same result follows from Corollary~\ref{cor:eq-2} and the fact that $f$ has no plateaus. 

\subsection{Alert Rate Regulation Algorithm}
\label{sec:alert-rate-algorithm}
Under the assumption that data observations are samples from our distribution, we are mathematically equipped to design an algorithm that exploits the relationship between the alert rate and the threshold to prevent an over production of alerts. 
To express the problem under investigation, consider the work of Harshaw et al.~\cite{harshaw2016graphprints} where a network-level anomaly detection system using a dynamic model scores anomalousness of 30-second time windows of traffic and is shown effective in tests on a network of ~600 internal public IPs, which is about the traffic of 50 researchers.  
Suppose we deploy this system on an organization of ~1000 researchers (e.g., a relatively small lab or university). 
Then the system will score $(1000/50)$ instances $ \times 24 * 60 * 2 = 57,600$ data points per day. 
This begs the question of how to set the threshold knowing the model is constantly changing, and that misconfiguration can easily produce a flood of alerts.  
Below we give an illustrative example of the use of these theorems for bounding the alert rate. 
The independence of the bounds in the theorems above mean that the alert rate threshold can be chosen  
a priori, and the efficacy of the bounds gives insight into the model's fit to the data. 
This algorithm is then given explicitly and tested empirically. 

Generalizing the situation above and illustrating the algorithm under development, suppose we receive $N$ data points per day but operators only have resources to inspect the most anomalous $M \leq N$ each day. 
Let $f_n$ be a PDF fit to all observations $\{x_1, ..., x_{n-1}\}$. 
Following the assumption that the next observations $x_n$ will be sampled according to $f_n$, define the anomaly score, $A_n := h\circ pv_{f_n}$ where $h$ is any (fixed) strictly decreasing bijection of the unit interval.  
Upon receipt of $x_n$ an alert is issued if $A_n(x_{n}) \geq \alpha$. 
Finally, we update $f_n$ to $f_{n+1},$ now including observation $x_n$ and repeat the cycle upon receipt of the next observation.  
Leveraging the theorem above, the expected number of alerts per day is 
$$ \sum_{n = 0}^{N-1} P_{f_n}( A_{n}(x_{n}) \geq \alpha ) \leq N h^{-1}(\alpha).$$ 
Hence, choosing $\alpha = h(M/N)$ ensures that the operator's bound on the number of alerts will hold on average.

Note that this choice of threshold depends only on $h$, the rate of the data ($N$), and the operators bound on the alerts, $M$. 
In particular, it is independent of the distribution, and therefore can be set a priori,  regardless of the distribution, and is especially applicable in a streaming setting, where $f_n$ is constantly changing. 
Next, it is worth noting that this is not a hard bound on the number of alerts per day, but rather bounds the alert rate in expectation, which implies that if there is an influx of anomalous data, all will indeed be flagged. 
Consequently, it is possible to have greater than $M$ alerts on some days, but on average the alert rate will be bounded as desired. 
Finally, if one has the luxury of performing post-hoc analysis, such an algorithm is not needed; for example, one can prioritize a previous day's alerts by anomaly score, handling as many as possible. 
Yet, if realtime analysis is necessary, for example, if only a fraction of the data can be stored for analysis, then such an algorithm allows realtime prioritization of alerts with the bound preset. 

The underlying assumption is that data is sampled from the model's probability distribution, so if this assumption fails, for example, if there is a change of state of the system and / or if $f_n$ poorly describes the observations to come, then these bounds may cease to hold. 
This gives an ancillary benefit of the mathematical machinery above, namely, that monitoring the actual versus expected alert rate gives a quantitative measure of how well the distribution fits the data.  
Our work to characterize conditions that ensure equality (Corollaries~\ref{cor:eq-1}-\ref{cor:eq-2}) serve exactly this purpose. 
Under these conditions, the alert rate bound is an actual equality, and deviations from the expected number of alerts over time (both below and above) indicate a poor model for the data.


Bringing forward the notation $x_n$, $f_n$, and $h$ from above; let $t_i$ denote arrival time of $x_i$, and let $r$ be the user-given desired upper bound on the alert rate. Then the rate of data at time $n$ will be given by $r_n=(t_n-t_{n-1})$, so that the new threshold at time $n$ shall be given by $\alpha_n=h(r/r_n)$. This new threshold is used for classifying $x_n$ as soon as $x_n$ is observed. This algorithm can then be called at each iteration of a streaming anomaly detection algorithm to regulate the alert rate. 


%{\color{BlueViolet} JESSIE, FIGURE OUT HOW TO REWRITE what's below IN ALGORITHM ENVIRONMENT OR MAYBE JUST ADD AN ALGORITHM TO IT? 
%Algorithm III.8 gives an algorithm for setting the alert rate threshold to realize an expected bound on the number of alerts. 
%Bring forward the notation $x_n$, $f_n$, and $h$ from above; let $t_i$ denote arrival time of $x_i$, and let $r$ be the user-given desired upper bound on the alert rate. \\

%Algorithm input: $t_{n-1}, t_n, r$ 
%Steps: 
%1. $r_n = (t_n - t_{n-1}){-1}$ is the rate of the data at time $n$. 
%2. $\alpha_n = h(r/r_n)$
%Algorithm outputs $\alpha_n$ the threshold used for classifying $x_n$ a soon as $x_n$ is observed. 


%\begin{raggedright}
%\textbf{Algorithm III.8: \url{Alert-Rate}}\\
%\label{alg:alertrate}
%\textbf{Inputs:}
%	\begin{itemize}
%		\item{$t_n$: arrival time of data point $x_{n}$.}
%		\item{\url{r}: desired alert rate upper bound.}
%	\end{itemize}
	
%	\textbf{Outputs:}
%	\begin{itemize}
%		\item{$\alpha_n$: the threshold used for classifying $x_n$.}
%	\end{itemize}
	

%\begin{algorithm}[]
%\SetAlgoLined
%\noindent
%{\bf begin} \ \ \ \\
%	\textbf{define}: $r_n=t_n-t_{n-1}$;\\
%	\quad \textbf{return}: $\alpha_n=h(r/r_n)$.\\
%	{\bf end}
%\end{algorithm}
%\end{raggedright}

%\textcolor{OliveGreen}{In the above algorithm, we had $r_n=(t_n-t_{n-1})-1$, but when I went back and checked using numbers I think we need to drop the $-1$. I think it was in there for python index issues.}



\subsection{Experiments and Results for Alert Rate Regulation}

{\color{OliveGreen} I will take another look at D and IV. I will also need to wordsmith this section again.}

Below we present experiments which provide initial empirical verification that the proposed methods are adequate at preventing an over-production of alerts. %The first experiment samples data from a multinomial distribution, while the second experiment samples data from a continuous distribution. The final experiment uses non-synthetic GraphPrints data 
%The first experiment uses synthetic data sampled from a multinomial distribution, 
%Experiment 1 - sample from a multinomial 
%Experiment 2 - sample from a continuous distribution (use the corollary)
%Experiment 3 - use GraphPrints data w/ Gaussian  ... 
The tables in this section show results for an experiment comparing untrained anomaly detection schemes with a trained anomaly detection scheme. To create synthetic data for testing, data is sampled at a rate of 1 data point per second from a Gaussian mixture model; 90\% of the data is sampled from a 2-variate Gaussian with mean $(0,0)$ and covariance matrix $I_2$, the $2\times 2$ identity matrix. The remaining $10\%$ of the points are sampled from a second hidden 2-variate Gaussian distribution with mean $(2,2)$ and covariance matrix $2I_2$ in order to generate ``anomalous" data points. 

Suppose available resources can investigate 40 alerts per 8 hour day. To make a simple detector, a Gaussian distribution is fit to the previously observed data points, using the first 100 data points for the initial fit. Upon receipt of each data point, the point is scored for anomalousness, then the Gaussian is refit. Using previous notation, we use $h=1-x$, and the anomaly score is given by $1-pv_f(x)$\footnote{The p-value of a multivariate Gaussian can be explicitly computed from the likelihood of a $\chi^2$ variable.}. 

%We expect 40 alerts per day for an 8 hour day, with 1 data point per second and 100 additional training points. After $d$ days, this gives $d\cdot8\cdot 60^2\cdot 1+100$ points containing anomalies. The number of alerts per day is charted over a week, and the experiment was repeated 10 times. 

    Because we assume data is Gaussian distributed when detecting alerts, Corollary~\ref{cor:eq-1} ensures that equality in Theorem~\ref{thm:alert-rate} holds. Hence, because our average number of alerts over the week differs from the expected average, we know our assumed distribution is different from the actual.

%NOTE FROM JESSIE: 
%``The data in these tables were generated using the code
%UntrainedData.py and TrainedData.py, respectively.''
%PLEASE RESUNDERSTAND WHAT YOU DID, EXPLAIN BETTER, MAKE SURE WHAT YOU DID MATCHES THE ALGORITHM ABOVE. 
%SOMETHING LIKE: 
%To create synthetic data for testing, data is sampled w/ 9/10 probability from a hidden ``normal'' distribution which is a 2-variate Gaussian with mean.. cov... 
%The other 1/10 of the time, we sample from a second hidden 2-variate Gaussian with ... to generate  ``anomalous'' data points. 
% We then ... 
 %The goal here is not to test the efficacy of this anomaly detector at finding the seeded anomalous data points; rather it is to provide initial empirical verification that the method proposed above is a adequate at preventing an over-production of alerts, and also indicates that there is a disparity between our model's distribution and the actual data's. 

%CAN YOU CALCULATE THE ACTUAL $p(pv(X)\leq \alpha)$? with this data generation process? If so, then you know how far off the observed number of alerts should be! 

%You need to mention that bc we assume $f_n$ is Gaussian, we now by corrollary blah that equality in the theorem holds. Hence after seeing that our average number of alerts is different, we know our models a little off. 

%DON'T WORRY ABOUT MAKING THE TABLES FIT UNTIL WE FIGURE OUT THE LATEX SKIN (SO WHAT VENUE) WE WILL USE. 
%{\color{OliveGreen}
%There was a note saying that you didn't know what trained and untrained meant. I went back and looked at the code, and there was a note in the code saying we had decided not to use this because ``untrained" experiment sampled data from the multinomial as mentioned above, but just assumed a (0,0) centered gaussian as the base distribution for anomaly detection. To contrast, the ``trained" method used the MCD stuff- trained on 100 points and then we used the distribution from training to do anomaly detection. I'll leave the tables in for now.
%}
%\begin{table*}[]
%\centering
%\caption{Alerts Per Day Over a 10 Week Period Using Untrained Methods}
% \label{tab:}
%\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|}
%\hline
%\multicolumn{11}{|c|}{Alerts Per Day Over a 10-week Period, Untrained} \\ \hline
%\begin{tabular}[c]{@{}l@{}}Week Number/\\ Day\end{tabular} & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\ \hline
%1 & 43 & 51 & 27 & 44 & 49 & 35 & 38 & 41 & 47 & 37 \\ \hline
%2 & 27 & 39 & 55 & 31 & 48 & 47 & 43 & 50 & 44 & 52 \\ \hline
%3 & 38 & 41 & 44 & 34 & 23 & 61 & 46 & 20 & 27 & 35 \\ \hline
%4 & 23 & 34 & 29 & 25 & 33 & 19 & 3 & 51 & 45 & 39 \\ \hline
%5 & 76 & 38 & 53 & 62 & 30 & 18 & 72 & 19 & 18 & 47 \\ \hline
%6 & 40 & 30 & 16 & 10 & 79 & 39 & 23 & 78 & 70 & 25 \\ \hline
%7 & 22 & 32 & 85 & 69 & 43 & 77 & 43 & 38 & 14 & 65 \\ \hline
%Average Per Day & 38.4 & 37.8 & 44.1 & 39.3 & 43.5 & 42.2 & 38.2 & 42.4 & 37.8 & 42.8 \\ \hline
%\end{tabular}
%\end{table*}

\begin{table*}[]
\centering
\caption{Alerts Per Day Over a 10 Week Period Using Trained Methods}
% \label{tab:}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|}
\hline
\multicolumn{11}{|c|}{Alerts Per Day Over a 10-week Period, Trained} \\ \hline
\begin{tabular}[c]{@{}l@{}}Week Number/\\ Day\end{tabular} & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\ \hline
1 & 38 & 49 & 44 & 41 & 36 & 41 & 40 & 38 & 44 & 46 \\ \hline
2 & 34 & 35 & 42 & 31 & 36 & 34 & 35 & 41 & 36 & 42 \\ \hline
3 & 46 & 26 & 40 & 30 & 36 & 38 & 53 & 40 & 38 & 40 \\ \hline
4 & 52 & 37 & 34 & 52 & 49 & 37 & 45 & 43 & 49 & 32 \\ \hline
5 & 46 & 42 & 47 & 39 & 37 & 45 & 41 & 35 & 40 & 36 \\ \hline
6 & 47 & 34 & 47 & 49 & 41 & 37 & 29 & 51 & 56 & 42 \\ \hline
7 & 44 & 43 & 34 & 35 & 42 & 43 & 41 & 39 & 42 & 43 \\ \hline
Average Per Day & 43.8 & 38.0 & 41.1 & 39.5 & 39.5 & 39.2 & 40.5 & 41.0 & 43.5 & 40.1 \\ \hline
\end{tabular}
\end{table*}
%????


%Can you then put in the graph print results? 
Next we apply our methods to non-synthetic data, namely Skaion data. 
{\color{OliveGreen} I have no idea how to properly introduce the Skaion work or data, nor do I have a good idea of how to transition to the ``non-synthetic" data experiments. I'll need some help wordsmithing this and properly presenting the Skaion results. I'll explain the experiment below, though.
Also, we need to make a note that we will extend this to the non-probabilistic setting in Section 5.}

Data flows were scored in a roughly 35 second interval. Each second of data is used to calculate the data rate, in terms of flows per second. Using the aforementioned algorithm, the alert rate threshold is updated to control the number of alerts per second. In Figure 1(a) the number of alerts per second is charted over the scoring timeline for $M=100$ flows per second, the maximum number of alerts that can be feasibly checked by hand. The dashed central line represents the average number of alerts across the entire scoring timeline. Figure 1(b) shows the number of alerts if the maximum number of flows per second changes across a few values. 


\begin{figure*}[!tbp]
\centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{Alerts_Per_Second_Skaion.png}
%\caption[caption]{Actual Alerts, 100 Flow-Per-Second Maximum}
\label{skaion1}
\end{minipage}
\quad\quad
\begin{minipage}[b]{0.4\textwidth}
%\end{figure*}
%\begin{figure*}[!tbp]
    \includegraphics[width=\textwidth]{DifferentThresholdsSkaion.png}
    \label{skaion2}
    %\caption[caption]{Actual Alerts, Various Flow-Per-Second Maxima}
\end{minipage}
\caption{Figure 1(a) (L) indicates the number of alerts when considering a 100 flow-per-second maximum. Figure 1(b) (R) shows the number of alerts if the maximum ranges over several values.}
\end{figure*}

Observe that both figures indicate that flooding of alerts does not occur, and that the alerts is bounded well below the maximum across each value and each second.

\begin{figure*}[!tbp]
\centering
    \includegraphics[width=0.5\textwidth]{AlertRateThreshtime.png}
%\caption[caption]{Actual Alerts, 100 Flow-Per-Second Maximum}
\label{thresholds1}
\caption{This shows how the alert rate threshold changes over time as the data flow rate changes. $M=100$ flows per second in this scenario.}
\end{figure*}














