\section{Bounding the Number of False Alerts}
\label{sec:fp-rate}
Our goal in this section is now to consider the question, ``How to set the threshold?'', with an eye on optimizing number and percentage of false alerts. 
While anomaly detection systems are by definition designed to find abnormalities in data, they are evaluated in practice on the value of the detected events to the operator. 
Hence, the percentage of alerts that are false is dependent on both the threshold, as this determines what is detected, but also how well the  {\it model fits the task}, which, roughly speaking, is the amount of overlap between what the model considers anomalous and what the operator considers a ``positive'' event. 
% This consequence is that the relationship between the threshold and the false-alert  rate is dependent on how well the model fits the task. 

In addition to an obscure relationship between the threshold and predictive accuracy of the detector, further problems arise from the bias class problem in the data. 
We note that in many applications of anomaly detection and especially in cyber security applications, the number of negative events is far greater than the number of positive ones, often by orders of magnitude. 
For example, the amount of non-malicious files verses malware on a host or malicious network flows versus normal flows is generally heavily skewed. 
The consequence, known as the ``base-rate fallacy'' is two-fold. 
First, the {\it false-positive rate} (FPR), which is defined as the percentage of negative events that are false positives is near zero simply because the quantity of negative events is large and only the false positives are a subset of the relatively few anomalous events\textemdash although this seems beneficial, elation is premature as it means, proverbially, that we are looking for a needle in a haystack.
Second, perhaps the perennial problem for anomaly detection, is that the {\it false-alert rate} (FAR), giving the percentage of alerts that are false positives (or 1 - precision), is too high.   
We reference the reader to Axelsson~\cite{axelsson2000base} for a more thorough explanation of the base-rate fallacy in cyber applications\footnote{Our  terminology is separate from that of Gambino~\cite{gambino2006reflections}, which provides a more thorough treatment of their differences.}. 
{\color{BlueViolet} See Table~\ref{tbl:confusion-matrix} for definitions of the accuracy rates in relation to a confusion matrix. }  

A stereotypical approach (e.g., see Axelsson~\cite{axelsson2000base}) to the problem of setting the threshold even in light of the base-rate fallacy, is to use the ROC curve. 
Simply, the recall is plotted against the FPR  as the threshold changes, allowing a visualization for optimizing the FPR-recall balance.  
But this does not addresses base-rate fallacy problem, as it optimizes the FPR rate, which (while arithmetically related to the FAR via Bayes' rule) can be small while the FAR is high\textemdash exactly the problem.  

%{\color{BlueViolet} Jessie, can you put in the confusion matrix table here w/ false positive  rate, predictive false positive rate in the comment box. Also reference it in the paragraph above. }

%\begin{table}[]
%\centering
%\caption{My caption}
%\label{my-label}
%\resizebox{0.75\textwidth}{!}{%
%\begin{tabular}{lc|c}
%\multicolumn{1}{c}{}                                                    & Positive Event                                           & Negative Event                                                                \\ \cline{2-3} 
%\multicolumn{1}{l|}{Alert}                                              & \begin{tabular}[c]{@{}c@{}}True\\ Positive\end{tabular}  & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}False\\ Positive\end{tabular}} \\ \hline
%\multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}No\\ Alert\end{tabular}} & \begin{tabular}[c]{@{}c@{}}False\\ Positive\end{tabular} & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}True\\ Negative\end{tabular}}  \\ \cline{2-3} 
%\end{tabular}%
%}
%\end{table}

{\color{BrickRed}  AFTER JESSIE ADDS HER STUFF, GO THROUGH THIS SECTION AND TIGHTEN IT UP... 
Our contribution in this section is two-fold. .....
First, we directly address the base-rate fallacy by analysis and optimization of the FAR.
% We note and give examples that the FAR  is bounded by the model's fitness to the task, and hence for some models no acceptable rate will be possible. 
In particular, we analyze detectors by the asymptotic behavior of the precision-FARs, which give hypotheses for FAR optimization. 
Secondly, we show that the expected number of false positives can be bounded by an application of Lemma~\ref{lem:alert-rate}. 

Our treatment gives an operational benefit, namely that understanding the relationship between the threshold and optimal FARs only depends on labeling the few, most anomalous data points. 
Hence, as compared to the ROC method, which generally assumes labels on all data and does not provide direct regulation of the FAR, our method requires less labels and directly bounds the FAR. 
Finally, this section in conjunction with Section~\ref{sec:prob} gives a workflow for setting the threshold and gauging model fit.  
On can initially set the threshold to regulate the alert rate, and observation of the number of real versus expected alerts gives an indicator of goodness-of-fit in a probability sense. 
Manual inspection of the (manageable number of) alerts will provide labels for only the most anomalous events. 
By using the analysis proposed in this section, one can then tweak the threshold for an acceptable number of false positives, and gauge how well the model fits the classification task. 
}


\subsection{Bounding the Predictive False Positive Rate}
\label{sec:pfp-rate}
As before, we work in the probabilistic setting with $f$ a PDF and $A = h\circ pv_f$ an anomaly score that respects $f$ (so $h$ a decreasing bijection $[0,1]$). 
{\color{BrickRed} Theorems of Section~\ref{} will extend these results to other detectors. }
We refer to a sample $x\in X$ as ``positive'' (respectively, ``negative'') if the operator deems $x$ to be worthy of detection. {\color{OliveGreen} I am not sure what the previous sentence is saying.}

\defn[Classifier Performance Functions]
	\label{defn:functions}
	As above, let $f$ be a PDF, $P$ the probability measure with density $f$, $h$ a decreasing bijection of $[0,1]$, and $A = h\circ f,$ an anomaly score that respects $f$. 
	We define the {\it true positive, true negative, false positive, false negative} functions of the threshold $\alpha$ as follows: 
	\begin{align*} 
		TP(\alpha) &= P(\{x: x \text{ positive } \} \cap \{ x: A(x) \geq \alpha \}), \\
		TN(\alpha) &= P(\{x: x \text{ negative } \} \cap \{ x: A(x) < \alpha \}), \\
		FP(\alpha) &= P(\{x: x \text{ negative } \} \cap \{ x: A(x) \geq \alpha \}), \\
		FN(\alpha) &= P(\{x: x \text{ positive } \} \cap \{ x: A(x) < \alpha \}).
	\end{align*}
	We define the {\it precision} or {\it positive-predictive-value } as 
		$$PPV(\alpha) = \frac{TP(\alpha)}{ P_f(\{ A \geq \alpha\})}$$ 
	and the {\it false-alert function} as 
		$$PFP(\alpha) = 1-PPV(\alpha).$$	
	Similarly, the {\it negative-predictive-value function } is
		$$NPV(\alpha) = \frac{TN(\alpha)}{ P_f(\{ A < \alpha\})}.$$
	\edefn
Roughly speaking, $PPV$ returns the probability an event is positive given the event is anomalous, while $PFP$ returns the probability an event is a negative given the event is not anomalous. 

How well-fit a model is to the task depends on both the application and the operator. 
For example, in the cyber domain, one network analyst may identify any abnormal activity (e.g., an external port scan, which alone is non-malicious) as a true positive, while others may deem only bona fide intrusions as a true positive. Hence, $PPV$ quantifies how often the relatively rare events according to the model ($A$) correspond to operator-defined positives. 
We expect that in practice the function $PPV$ can be estimated from labeled data via a domain expert, or at least, as this only entails looking at the subset of anomalous events, i.e., $\{ A \geq \alpha \}$ for large $\alpha$. 
This motivates asymptotic definitions for characterizing an anomaly detection model's fitness to that labeling task. 

\defn[Model-Task Fitness]
	\label{defn:model-fit}
	We say anomaly score $A$  
	\begin{enumerate} 
		\item {\it respects precision } iff $PPV$ is increasing in $\alpha$.
		\item is a {\it precise model} iff $\sup PPV = 1$. 
		\item {\it respects negative precision } iff $NPV$ is decreasing in $\alpha$.
		\item is a {\it negatively precise} model iff  $\sup NPV = 1$. 
		\item and is a {\it perfect model} iff  $ \exists  \alpha  > 0$ so that $\{ \text{positives} \} = \{ A_f \geq \alpha \}.$
	\end{enumerate}
\edefn

Note that $PPV$ (respectively, $NPV$) is increasing means that the greater (respectively, less) the anomalousness of the event, the greater the probability it is a positive (respectively, negative). 
Further, if $\sup PPV = 1$ this means that asymptotically, as $\alpha \nearrow 1$, anomalousness perfectly indicates a positive; 
hence, the definition of a precise model is intuitive, as such models can have arbitrarily high precision. 
Similarly, $\sup NPV = 1$ means that for asymptotically, as $\alpha \searrow 0$, the non-detected events are guaranteed to be negatives. 

In general a model can be both precise and inversely precise, giving the operator a choice between a high threshold with near perfect precision, or a low threshold with near perfect negative-predictive value.   
In particular, perfectly fit models are both precise and inversely precise, with a single threshold giving perfect classification. 
Finally, as a precise (respectively, an inversely precise) model may have a non-zero number of false negatives (respectively, false positives) for all $\alpha$, the inclusion is strict. 
See figure~\ref{fig:models} for a figure depicting the model definitions. 
%{\color{BlueViolet} Jessie, 
%\begin{enumerate} 
%	\item add a third plot to include also an `inverse precise' model. This is one with red and black dots above the threshold, but only black below.
%	\item maybe also add a 4th plot with random points (just 1/5 red, 4/5 black) 
%	\item change the label in the key so black is `negative' and red is `positive'. 
%	\item can you make it so that the number of positive events (reds) are about 10, while the number of blacks are about 50 
%	\item can you use black dots and BrickRed dots so we don't burn readers retinas?
%	\item Next, under each plot can you also plot the PPV-NPV curve as alpha changes?  See fitness-plot-plan.jpeg for examples. 
%\end{enumerate}
%}
\begin{figure*}
\centering
\includegraphics[width=6in]{modelgraphs6.png}
\caption{Perfect, Precise, and Inverse Precise Anomaly Detectors Depicted}
\label{fig:models}
\floatfoot{\scriptsize{\textbf{Note:} Images of models as defined in \ref{defn:model-fit}. Bias class indicated by 5-to-1  negative-to-positive ratio.}}
\end{figure*}

As the problematic $PFP$ rate is exactly $1-PPV$, we see that a precise model is exactly the conditions for which $PFP$ can be made as small as possible by adjusting the threshold.  
This is stated rigorously in the next proposition. 
\prop
	Anomaly score $A$ is precise, in the sense of Definition \ref{defn:model-fit}, iff for all $\epsilon > 0, \exists \delta > 0$ such that $\alpha \geq 1-\delta$ implies $PFP(\alpha) \leq \epsilon$. 
\eprop 

In order to understand the attainable FARs, we assume the operator sets an initial threshold, e.g., by regulating the alert rate as described in Section~\ref{sec:prob}. 
Next we assume the operator labels only those events with anomaly score over the threshold, and plots the PPV-NPV curve to find the threshold optimizing PFP (maximizing the PPV). 
{\color{BlueViolet} Jessie, can we plot the PPV-NPV curves for the experimental data and include here. This will tell us how to set the optimal threshold to minimize the FAR.}

\subsection{Bounding the Expected Number of False Positives}
We expect that in practice the function $PPV$ can be estimated from labeled data via a domain expert, or at least determine whether $A$ is precise, as this only entails looking at the subset of most anomalous events, i.e., $\{A \geq \alpha\}$ for large $\alpha$. 
With access to both $PPV$ and $h$, Theorem~\ref{thm:fp} furnishes a strict bound on the expected number of false positives, and can be used analogously to Theorem~\ref{} in Algorithm~\ref{} to regulate the expected number of false positives over time. 
This allows operators with knowledge of the $PPV$ for at least high values of $\alpha$ to bound the false-alert rate a priori. 

\thm[FP Regulation Theorem]
	\label{thm:fp}
	Let $f$ be a PDF, $P$ the probability measure with density $f$, $h$ a decreasing bijection of $[0,1]$, and $A = h\circ f,$ an anomaly score that respects $f$. 
	Then 
	$$FP(\alpha) \leq h^{-1}(\alpha)(1-PPV(\alpha)),$$ and this inequality is sharp.
	In particular, $FP(\alpha) \leq h^{-1}(\alpha)$. 
\ethm

\begin{proof}
	Applying Definition~\ref{defn:functions} we have 
	\begin{align*}
		FP(\alpha) 	&= P(\{x: A(x) \geq \alpha \} \cap \{x: x \text{ negative } \})\\
					&= P(\{A \geq \alpha\}) (1-PPV(\alpha))\\
					& \leq h^{-1}(\alpha)(1-PPV(\alpha))
	\end{align*}
	with the last inequality provided by Lemma~\ref{lem:alert-rate}.
	Furthermore, this inequality is sharp as the conditions for equality are given in the Lemma, and Corollaries in Section~\ref{sec:prob}. 
	Finally, $FP(\alpha) \leq h^{-1}(\alpha)$ follows since $PPV(\alpha)\in [0,1].$ 
\end{proof}

Notice that the sharp bound on the number of false positives depends on two things, (1) $\alpha$ the threshold, and (2) $PPV(\alpha)$ for large $\alpha$, which measures how well the model fits the task.
Hence, this Theorem summarizes and mathematically relates the concepts in play. 

Consider the case when equality holds, for example, if $f$ is a Gaussian PDF. 
Then Theorem~\ref{thm:alert-rate} gives the expected number of alerts, so the operator can gauge how well the model fits the data by observing the quantity of alerts. 
Provide the model is well fit to the data, we move the number of false positives produces. 
As equality also holds in Theorem~\ref{thm:fp}, by labeling the alerts, the operator can pin down the model's precision telling if those events that are detected as anomalous are indeed the positive events (how well the model fits the classification task). 




