
- /alert-rate-paper - first stab at texing things in order

- /code - python scripts, has its own readme. 

- /code-probabilistic-alert-rate-paper - code that processed the skaion/graphprints data for the probabilistic-alert-rate-paper, all .py files

- /code-pvc-paper - 
	- digits.py - working w/ mnist data set, but got perfect classification, so abandoned
	- generalFunctions.py - serialization code
	- simulation.py 
		- creates labels data from two distributions (normal/anomalous)
		- trains a detector on a portion 
		- finds optimal thresholds
		- test results stored in ../data-pvc/paper 
	old/
        - skaion_rates.py & skaion_pvc_experiments.py - these were written by Jessie to get set thresholds and get some results from the skaion data. looks like they import modues from ../code-probabilistic-alert-rate-paper/ folder. this description needs to be redone! 
        - skaion_pvc_new    
            - Goal here was to test roc v. pvc method on skaion data. there aren't enough internal ips w/ attacks to work 
            - Note: this is meant to be run from the directory ../code-probabilistic-alert-rate-paper 
	- bobby-temp.py - where bobby develops code. 
	
- /data-graphlets 
	Contains graphlets data both with and without the ip scanning appliance. 
	NOTE: 
    	- indices 278-301 are the bittorrent traffic,
	
	subfolders: 
	- /no-appliance (w/o appliance)
		- graph_detection_info.json has the mu and cov for each fit MCD for graphs 150-600
        - m_dist.json is a list of 600 and has the mdist for each graph from 150-600 (0-148 have mdist == 0).
        - graphlet_list is the list of graphlet names
        - graphlets is a dict of {graph_num : {graphlet name: count } }
    - w-appliance (w/ appliance)
    	(same file descriptions as above.)

-/data-pvc-paper 
	- simulations/ contians results from each of 10 simulation runs and microaveraged results. See ../code-pvc-paper/pvc-simulation.py

- /data-skaion 
	- skaion_5s20_situ_output.txt 
		- from joel, has scores from models. 
	- binSizes.json - this has the number of flows per minute in one big list. 

- /data-untrained-experiment - directory containing data from ../code/one-off/untrained-data.py

- /deliverables - jj summer DHS poster and paper.

- /papers - publications in progress or submitted/accepted for publication 

- /refs - papers cited

