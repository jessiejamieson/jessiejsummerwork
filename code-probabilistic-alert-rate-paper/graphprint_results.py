# graphprint_results.py
import os
import numpy as np 
from scipy.stats import chi2
from matplotlib import pyplot as plt 
from generalFunctions import * 



# ---- function ---- # 
def p_inv_h(S): 
	""" 
	Common trick for inverting a matrix that's not invertible (on the subspace where it is invertible and making it 0 else. )

	Input: S np.array, n by n, symmetric
	Output: S's p-inverse. 
	Note here we don't use np.linalg.pinv b/c solving the eigenvalue problem is more stable using eigh when the matrix is symmetric
	So we do it manually w/ eigh. 
	""" 
	if not np.allclose(S.T, S): 
		print "S is not symmetric, nothing returned"
		return 

	values, U = np.linalg.eigh( S )  
	## so,  S = U np.diag(values) U.T. to check: 
	# np.allclose(S , np.dot(np.dot(U, np.diag(values)), U.T ) )
	l = np.zeros_like( values )
	for i in xrange(len(values)): 
		if  values[i] > 10**-8:  ## S is PSD matrix, hence eigenvalues non-neg. anyting less than zero is a rounding error. 
			l[i] = 1./values[i]
	S_pinv = np.dot( U, np.dot( np.diag(l), U.T ) ) 
	return S_pinv


# ---- paths ---- #
folder_path = os.path.join( os.path.abspath( os.path.join( os.getcwd(), '..')), 'data-graphprints')

# ---- unjsonify ---- # 
gdi = {int(x): y for x,y in unjsonify(os.path.join(os.path.join(folder_path, 'cisrc-paper-public-data'), 'graph_detection_info.json')).iteritems()}
## of the form {index: {mu: array, cov: array, m_dist: mahalanobis distance squared!} } note that this gives mahalanobis distance squared b/c mcd.mahalanobis gives m^2. 

p_values = {} ## to be populated
for i in xrange(150, 350): ## only use 200 data points like they did in the paper. 
	mahal_squared = gdi[i]['m-dist']
	mu, cov = np.array(gdi[i]['mu']), np.array(gdi[i]['cov'])
	k = len(mu) ## chi^2 degree of freedom. 
	# cov_inv = p_inv_h(cov) ## inverse of cov
	pv = 1-chi2.cdf(mahal_squared, k)  ## see http://stackoverflow.com/a/39985727/2487607 
	p_values[i] = pv 

jsonify(pv, os.path.join(folder_path, 'p-values.json'))

x = np.arange(0, .251,.001)
num_alerts = {}

for pv in x: 
	n = len([p for p in p_values.values() if p <= pv])
	num_alerts[pv] = n 

jsonify(num_alerts, os.path.join(folder_path, 'num_alerts.json'))

x = np.arange(0, .251,.001)
num_alerts = {float(a):b for a,b in  unjsonify(os.path.join(folder_path, 'num_alerts.json')).iteritems()}
y = [num_alerts[p] for p in x] 
plt.plot(x, y, 'b') ## number of alerts for each p-value
plt.plot(x, 200*x, 'k') ## expected number of alerts for each p-value

