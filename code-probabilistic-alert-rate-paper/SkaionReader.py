"""

Author: Jessie Jamieson 
Date Last Modified: 3/28/17

This code (SkaionReader.py) works together with SkaionOperations.py to both take in the Skaion data and perform various
operations on the data to run our anomaly detection. Note that since both the new and old skaion files are the same
format, these two code files may take in either of the data files.

Unit tests for the code exist, but are in another location and are not required.

The primary purpose of SkaionReader.py is to intake the Skaion data file (a .txt) and put it into a python format that
is readable for the DataOperations file. See below.

For an instance of how to use these two files, see the preamble/comments in the SkaionOperations file.
"""

import ast
from datetime import datetime as dt

class ScoreData:
    """
    This class initializes some functions, in particular, the getPvalue function and a function that deals with the
    instance when a key in a dictionary of data has no value. The immediate following variables will determine which
    score(s) one will see when doing analysis. These numbers, in particular, will be passed to the DataBase.AlertListBin
    function, along with a time value and an alert rate.
    """

    scorePcr              = 0
    scorePcr_T            = 1
    scorePrivPorts        = 2
    scorePrivPorts_T      = 3
    scoreBytesPerPacket   = 4
    scoreBytesPerPacket_T = 5

    def __init__(self,RawData):
        """
        In order for the init to be called, a RawData has to be passed.
        In a data dictionary, this will check to see what scores exist and will pull the appropriate score(s).
        """
        RawDataDict = {}
        if RawData != None:
            for Data in RawData:
                RawDataDict[Data['name']] = DataPointExist(Data,'score')

            self.Pcr              = DataPointExist(RawDataDict,'Pcr')
            self.Pcr_T            = DataPointExist(RawDataDict,'Pcr_T')
            self.PrivPorts        = DataPointExist(RawDataDict,'PrivPorts')
            self.PrivPorts_T      = DataPointExist(RawDataDict,'PrivPorts_T')
            self.BytesPerPacket   = DataPointExist(RawDataDict,\
                    'BytesPerPacket')
            self.BytesPerPacket_T = DataPointExist(RawDataDict,\
                    'BytesPerPacket_T')

    def getPvalue(self, scoreType):
        """
        This function will pull the appropriate scores and compute then return the P-value based on the 10^(-score) transformation.
        That is, scores are the -log_10(pvalue).
        """
        if scoreType == self.scorePcr:
            score = self.Pcr
        elif scoreType == self.scorePcr_T:
            score = self.Pcr_T
        elif scoreType == self.scorePrivPorts:
            score = self.PrivPorts
        elif scoreType == self.scorePrivPorts_T:
            score = self.PrivPorts_T
        elif scoreType == self.scoreBytesPerPacket:
            score = self.BytesPerPacket
        elif scoreType == self.scoreBytesPerPacket_T:
            score = self.BytesPerPacket_T
        else:
            score = None

        if score == None:
            Pvalue = None
        else:
            Pvalue = 10**(-score)

        return Pvalue

def DataPointExist(Data, key):
    """
    Checks: "Is there even data for the key?"
    """
    if key in Data:
        DataPoint = Data[key]
    else:
        DataPoint = None

    return DataPoint

class AlertData:
    """
    Alert Data Format
    """
    def __init__(self, RawData):
        self.srcIP     = DataPointExist(RawData,'srcIP')
        self.dstIP     = DataPointExist(RawData,'dstIP')
        self.time      = ExtractTime(DataPointExist(RawData,'stime'))
        self.flowPcr   = DataPointExist(RawData,'flowPcr')
        self.score     = DataPointExist(RawData,'score')
        self.scoreData = ScoreData(DataPointExist(RawData,'scores'))


def ReadDataFromFile(file_name):
    """ 
    Read Data from the Skaion Data files. 
    """

    Data = []
    with open(file_name) as fileData:
        for index, line in enumerate(fileData):
            correctedLine = LineCorrect(line)
            Data.append(ast.literal_eval(correctedLine))

    return Data

def CreateAlertDataList(file_name):
    """ 
    Read Data then create list of Alert Data.
    """

    RawDataList = ReadDataFromFile(file_name)

    AlertDataList = []
    for data in RawDataList:
        AlertDataList.append(AlertData(data))

    return AlertDataList

def LineCorrect(line):
    """
    Corrects a given line so that python will be able to evaluate it.
    """

    correctedLine = line.rstrip('\n')
    correctedLine = correctedLine.replace("\\n","")
    correctedLine = correctedLine.replace("false", "False")
    correctedLine = correctedLine.replace("true", "True")
    correctedLine = correctedLine.replace(" ", "")

    return correctedLine

def ExtractTime(RawTime):
    """ 
    Extract Date-Time python format from data.
    """

    if RawTime == None:
        pythonTime = None
    else:
        pythonTime = RawTime.replace("Z", "")
        pythonTime = dt.strptime(pythonTime, '%Y-%m-%dT%H:%M:%S.%f')

    return pythonTime

