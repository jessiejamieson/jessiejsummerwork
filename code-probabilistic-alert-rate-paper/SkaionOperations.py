"""
Author: Jessie Jamieson 
Date Last Modified: 3/28/17

This python3 file is dependent on the python file SkaionReader.py.

Once data is processed by the SkaionReader functions, this code will run anomaly detection. 
Below is an example of usage:

    import code-probabilistic-alert-rate-paper.SkaionOperations as sop
    import datetime as dt

    allData = sop.DataBase.PopulateDataBase("data_location_on_disk")  # populates a DataBase structure with Skaion data. 
                                                                        For more on the DataBase class, see below.

    deltaTime = dt.timedelta(minutes=1)                     # defines a time increment for analysis.

    binnedData = sop.BinnedDataBase(allData,deltaTime)      # bins the data into discrete deltaTime intervals.

    alertRate = sop.TimeRate(1, dt.timedelta(minutes = 1))  # defines how many alerts a user can be handled. In
                                                              this example, one per minute.

    threshold = sop.ThresholdType(alertRate, deltaTime)     # defines what type of threshold we wish to use, either 
                                                              adaptive or uniform. This example sets the threshold 
                                                              at one per minute. If a uniform p-value threshold of 
                                                              0.003 is desired, use sop.ThresholdType(0.003).

    alertCounts, alertThresholds, alertData = binnedData.GenerateAlertData(threshold, [1])   
                                                            
                                                            # finds the alerts in the data; the flows whose PCR_T
                                                              score (in this case) exceeds the adaptive threshold.
                                                              Now outputs will be lists where for the ith time interval:
                                                              alertCounts[i] = number of alerts in interval
                                                              alertThresholds[i] = threshold for that interval
                                                              alertData[i] = database of flows that were alerts for interval

"""

import SkaionReader as sr
import datetime as dt

class TimeRate:
    """ 
    Easy Class to represent a rate: number of instances per unit time.
    """

    def __init__(self, num_instance, delta_time):
        self.num       = num_instance
        self.deltaTime = delta_time

    def Ratio(self):
        """ Calculate Alert Rate Ratio inverse"""

        return self.deltaTime/float(self.num)

def RatioOfTimeRates(TimeRate1, TimeRate2):
    """
    Calculate Ratio of two Timerates, ie
        TimeRate1/TimeRate2
    """

    Top    = TimeRate2.Ratio()
    Bottom = TimeRate1.Ratio()

    return Top/Bottom

class DataBase:
    """ 
    Base DataBase Class
    """

    def __init__(self, data_list):
        self.dataList = data_list

    @classmethod
    def PopulateDataBase(self, file_name):
        """ 
        Populate Data Base From File. This method will generate all inputs for the init and returns
        init(inputs)
        """

        DataList = sr.CreateAlertDataList(file_name)

        return self(DataList)

    def OrderDataTemperal(self):
        """ 
        Order the Data in the DataBase by time
        """

        self.dataList.sort(key=lambda data: data.time)

    def Gen_srcIPDict(self):
        """ 
        Generate Dictionary of srcIP addresses and their flows.
        """

        IP_dict = {}
        for data in self.dataList:
            if data.srcIP in IP_dict.keys():
                IP_dict[data.srcIP].append(data)
            else:
                IP_dict[data.srcIP] = [data]

        return IP_dict

    def Gen_dstIPDict(self):
        """ 
        Generate Dictionary of dstIP addresses and their flows
        """

        IP_dict = {}
        for data in self.dataList:
            if data.dstIP in IP_dict.keys():
                IP_dict[data.dstIP].append(data)
            else:
                IP_dict[data.dstIP] = [data]

        return IP_dict

    def BinData(self, deltaTime):
        """ 
        Bin Data by time interval. 
        """

        self.OrderDataTemperal()

        DataBin = []
        TimeLimit = self.dataList[0].time + deltaTime
        CurrentDataBin = []
        for dataPoint in self.dataList:
            if dataPoint.time <= TimeLimit:
                CurrentDataBin.append(dataPoint)
            else:
                DataBin.append(DataBase(CurrentDataBin))

                CurrentDataBin = []
                CurrentDataBin.append(dataPoint)
                TimeLimit = TimeLimit + deltaTime

        DataBin.append(DataBase(CurrentDataBin))

        if DataBin[-1] == []:
            del DataBin[-1]

        return DataBin

    def FilterNullList(self, scoreType, Filtered = True):
        """ 
        MultiPass filter. Eliminates entries with a "Null" score. 
        """

        Data = self.dataList
        if Filtered:
            for score in scoreType:
                Data = filterNullScores(Data, score)

        return Data

    def Filter_srcIP(self, IPlist):
        """ 
        Filter out all the srcIP in IPlist
        """

        IP_dict = IPDict.srcIP(self)

        for IP in IPlist:
            del IP_dict.dataDict[IP]

        Data = []
        for key in IP_dict.dataDict.keys():
            Data += IP_dict.dataDict[key].dataList

        return Data

    def Filter_dstIP(self, IPlist):
        """ 
        Filter out all the dstIP in IPlist
        """

        IP_dict = IPDict.dstIP(self)

        for IP in IPlist:
            del IP_dict.dataDict[IP]

        Data = []
        for key in IP_dict.dataDict.keys():
            Data += IP_dict.dataDict[key].dataList

        return Data

    def AlertList(self, thresholdType, scoreType, \
            Filter = True, srcIPList = [], dstIPList = []):
        """ 
        Generate AlertList for the database. Multiple score types may be passed in list form.
        """

        Data = self.FilterNullList(scoreType, Filtered = Filter)

        if len(srcIPList) > 0:
            tmpDataBase = DataBase(Data)
            Data        = tmpDataBase.Filter_srcIP(srcIPList)

        if len(dstIPList) > 0:
            tmpDataBase = DataBase(Data)
            Data        = tmpDataBase.Filter_dstIP(dstIPList)

        threshold = thresholdType.Threshold(Data)

        alertList = []
        alertStatus = []

        for score in scoreType:
            for entry in Data:
                pvalue = entry.scoreData.getPvalue(score)
                if pvalue != None:
                    if pvalue <= threshold:
                        alertList.append(entry)
                        alertStatus.append(1)
                    else:
                        alertStatus.append(0)

        alertList = list(set(alertList))

        return alertList, alertStatus

def filterNullScores(DataList, scoreType):
    """ 
    Remove scores with no Data for scoreType 
    """

    Data = []
    for entry in DataList:
        if entry.scoreData.getPvalue(scoreType) != None:
            Data.append(entry)

    return Data

class ThresholdType:
    """ 
    Class to contain types of thresholds 
    """

    def __init__(self, arg1, deltaTime = None):

        self.thresholdOnly = False

        self.threshold = None
        self.alertRate = arg1
        self.deltaTime = deltaTime

        if self.deltaTime == None:
            self.alertRate = None
            self.threshold = arg1
            self.thresholdOnly = True

    def Threshold(self, Data):
        """ 
        Get the threshold for the database
        """

        if self.thresholdOnly:
            thresholdValue = self.threshold
        else:
            flowRate = TimeRate(len(Data), self.deltaTime)
            thresholdValue = RatioOfTimeRates(self.alertRate, flowRate)

        return thresholdValue

class IPDict:
    """ 
    Data orgainized by IP
    """

    def __init__(self, IP_dict):
        self.dataDict = {}
        for key in IP_dict:
            self.dataDict[key] = DataBase(IP_dict[key])

    @classmethod
    def srcIP(self, dataBase):
        """ 
        Generate IPDict using srcIP from DataBase 
        """

        IP_dict = dataBase.Gen_srcIPDict()

        return self(IP_dict)

    @classmethod
    def dstIP(self, dataBase):
        """ 
        Generate IPDict using dstIP from DataBase 
        """

        IP_dict = dataBase.Gen_dstIPDict()

        return self(IP_dict)

    def MaxFlowIP(self):
        """ 
        Find ip address(s) with most flows 
        """

        IP_list = list(self.dataDict.keys())
        # MaxIP = max(IP_list, key=lambda IP: \
        #         len(self.dataDict[IP].dataList))

        IP_size = []
        for key in IP_list:
            IP_size.append(len(self.dataDict[key].dataList))

        max_numFlow = max(IP_size)

        MaxIP = []
        for index, value in enumerate(IP_size):
            if value == max_numFlow:
                MaxIP.append(IP_list[index])
        return MaxIP

class BinnedDataBase:
    """ 
    Class containing a list of DataBases Binned By deltaTime. That is, each unit time interval will 
    yield a database consisting of each flow that occured in that time interval.
    """

    def __init__(self, dataBase, deltaTime):
        self.deltaTime = deltaTime
        self.dataBins = dataBase.BinData(self.deltaTime)

    def GenerateAlertData(self, thresholdType, scoreType, \
            Filter = True, srcIPList = [], dstIPList = []):
        """ 
        Generate AlertData (does not filter IP by default).  
        """

        srcIP_tmp = srcIPList
        dstIP_tmp = dstIPList

        AlertCounts     = []
        AlertThresholds = []
        AlertData       = []
        AlertStatus     = []
        for BIN in self.dataBins:
            tmpAlertData, tmpAlertStatus = BIN.AlertList(thresholdType, scoreType, \
                    Filter = True, srcIPList = srcIP_tmp, dstIPList = dstIP_tmp)

            AlertData.append(DataBase(tmpAlertData))
            AlertThresholds.append(thresholdType.Threshold(BIN.dataList))
            AlertCounts.append(len(tmpAlertData))
            AlertStatus.append(tmpAlertStatus)

        return (AlertCounts, AlertThresholds, AlertData, AlertStatus)
