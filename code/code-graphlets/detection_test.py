# detection_test.py
# adapted from graphlets cisrc paper code by RAB Aug 4 2016

import os
import numpy as np
from json2numpy import *
from robust_gaussian_detection import *
from sklearn.covariance import MinCovDet
from generalFunctions import *

##############################
### data file paths:
#############################
processedPath = os.path.join(os.path.join( os.path.join(os.getcwd(),".."), "data-graphlets"), "w-appliance")
if not os.path.isdir(processedPath):
	print 'Could not find processed path, ' + str(processedPath)
	quit()

#############################


##############################
### get data in numpy arrays:
#############################
# graphlet files
g_list_file = os.path.join(processedPath, "graphlet_list.json")
g_file = os.path.join(processedPath, "graphlets.json")
num_graphs = 600
gdv3, gdv4, graphlet_list = graphlet_json_2_numpy(g_list_file, g_file, num_graphs)

# set parameters for graph-level detection
N = 150
tol = np.inf # add everything to streaming model
# h = 0.85
h = 0.05


# for unfiltered data, N=150, tol = 6120, h= 0.93
# for filtered data, N=150, tol = 10. ** 6.5, h = 0.93

# check for possibility of a nonsingular matrix
n,p = gdv3.shape
if p >N:
	print "Warning, matrix will be singular because p > N"


gdv3 = gdv3 + 0.01*np.random.rand(n,p) # ADDING NOISE SO IT'S NOT SINGULAR
colvar = np.var(gdv3[:N,:], axis=0)
badcols = np.where(colvar==0)[0]
if badcols.size > 0:
	print "Warning, matrix will be singular because columns: " + str(badcols) + " are constant"

# run graph-level detection
anom, m_dist, graph_detection_info = robust_detection_graph_lvl(gdv3, N, tol, h)
#m_dist[358] = 0 # clean up this error - No longer an error!

jsonify( m_dist.tolist(), os.path.join(processedPath, "m_dist.json"))
jsonify( graph_detection_info, os.path.join(processedPath, "graph_detection_info.json")) 

# visualize the detection
bt_ind = np.array([278, 301]) # bit torrent indices
# tol = 10.0 ** 3.6# set the tolerance based on lowest anomalous value
# L=351 ## if you want it to stop at 350
tol = 10**5.8
L = m_dist.size
viz_detection_graph_lvl(m_dist, tol, N, bt_ind=bt_ind, L = L)

# # lasso regression path
# X = gdv3 # np.hstack((gdv3))
# n,p = X.shape
# y = np.ones(n)
# y[278:318] = -1
# logistic_regression_path(X, y)
