# robust_gaussian_detection.py
# adapted from graphlets cisrc paper code by RAB Aug 4 2016


import os
import numpy as np
from sklearn.covariance import MinCovDet
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.svm import l1_min_c

'''
robust_detection_graph_lvl()

INPUTS:
	feat_vec	==> numpy array of size n,p where
						n is number of observed graphs
						p is number of graphlets
	N 			==> the number of initial data points that are clean
	tol 		==> the tolerance for 'anomalous activity'
	quiet		==> set True to stop printing results

OUTPUTS:
	anom 	==> vector of booleans, True if anomalous and False otherwise
	m_dist 	==> updated mahalanobis distances
'''


def robust_detection_graph_lvl(feat_vec, N, tol, h, quiet=False):

	# # ====== TAKE THIS OUT IN FINAL VERSION!!!!!! ============
	# # this artificially reduces the number of variables so that MCD will work
	# k = N / 2  # number of variables to use
	# var = np.var(feat_vec, axis=0)
	# ind = np.argpartition(var, -k)[-k:]
	# feat_vec = feat_vec[:, ind]  # artificially reduce number of variables to make MCD work
	# # ====== TAKE THIS OUT IN FINAL VERSION!!!!!! ============

	n, p = feat_vec.shape  # get dimension of data

	# initialize output arrays
	anom = np.empty(n, dtype=bool)
	anom[0:N] = False # by assumption...
	m_dist = np.empty(n)
	m_dist[0:N] = 0

	graph_detection_info = {}

	# fit a Minimum Covariance Determinant (MCD) robust estimator to data
	X = feat_vec[0:N, ]  # initial training set
	mcd = MinCovDet(support_fraction=h).fit(X)  # initial fit
	graph_detection_info[N-1] = {}
	graph_detection_info[N-1]["mu"] = mcd.location_.tolist()
	graph_detection_info[N-1]["cov"] = mcd.covariance_.tolist()

	# go through each preceding vector,
	for i in range(N, n):

		# get mahalanobis distance of current graphlet degree vector
		x = feat_vec[i,].reshape(1, p)
		d = mcd.mahalanobis(x)[0]

		# update distance and anomaly arrays
		m_dist[i] = d
		anom[i] = d > tol

		# update model if current vector is not anomalous
		if not anom[i]:
			X = np.vstack((X,x))

			try:
				mcd = MinCovDet(support_fraction=h).fit(X)
			except RuntimeWarning:
				pass

		# update graph_detection_info: 
		graph_detection_info[i] = {}
		graph_detection_info[i]["mu"] = mcd.location_.tolist()
		graph_detection_info[i]["cov"] = mcd.covariance_.tolist()


		# print results
		if not quiet:
			print 'Graph ' + str(i) + " has distance d=" + str(d) + '\t-- ',
			if anom[i]:
				print 'anomalous'
			else:
				print ''

	return anom, m_dist, graph_detection_info


'''
robust_detection_node_lvl()

INPUTS:
	N 		==> the number of initial data points that are clean
	tol 	==> the tolerance for 'anomalous activity'
	quiet	==> set True to stop printing results

OUTPUTS:
	anom 	==> vector of booleans, True if anomalous and False otherwise
	m_dist 	==> updated mahalanobis distances
'''

'''
viz_detection_graph_lvl()

INPUTS:
	anom 	==> vector of booleans, True if anomalous and False otherwise
	m_dist 	==> updated mahalanobis distances
	tol 	==> the tolerance for 'anomalous activity'

OUTPUTS:
	none. A plot is produced
'''

def viz_detection_graph_lvl(m_dist, tol, N, anom=None, log=True, bt_ind=None, L=None, fig_size=(33.5, 16.5)):

	# specify endpoint if not given
	if L is None:
		L = m_dist.size

	# number of data points
	n = L-N

	# cut off unnecessary data
	m_dist = m_dist[N:L]

	# find anomalies
	if anom is None:
		anom = m_dist > tol

	# create time vector
	time = np.arange(N,L) # time vector
	a_ind = np.where(anom) # anomalous times

	fig = plt.figure(figsize=fig_size)
	ax = fig.add_subplot(1,1,1)

	# log plot or linear plot
	if log:
		plot_dist = np.log10(m_dist)
		plot_tol = np.log10(tol) * np.ones(n)
		ylabel = 'Mahalanobis Distance (log)'
	else:
		plot_dist = m_dist
		plot_tol = tol * np.ones(n)
		ylabel = 'Mahalanobis Distance'

	# plot the mahalanobis distance
	ax.plot(time, plot_dist, c='blue') # plot the distance
	ax.scatter(time[a_ind], plot_dist[a_ind], c='red') # scatter plot on anomalous data

	# plot the tolerance line (if finite)
	if tol < np.inf:
		ax.plot(time, plot_tol, 'r--', linewidth=2.5)
		ax.text(L-1, plot_tol[0], 'Anomaly Threshold', horizontalalignment='right', size='large')

	# plot the bit torrent traffic times (if given)
	if bt_ind is not None:
		ax.axvline(x=bt_ind[0], linestyle='-.', c="green", linewidth=4.0)
		ax.axvline(x=bt_ind[1], linestyle='-.', c="green", linewidth=4.0)

		# add label
		y_pos = (ax.get_ylim()[0] + np.min(plot_dist) ) / 2.0
		ax.text(bt_ind[0], y_pos, 'Known Anomalies', horizontalalignment='left', size='large')

	# label the graph
	plt.xlabel('Graph Sequence', fontsize='x-large')
	plt.ylabel(ylabel, fontsize='x-large')
	plt.title('IP Flow -- Network Anomaly Detection', fontsize = 'xx-large')
	plt.xlim((N, L))

	# show plot
	plt.show()

	return


def logistic_regression_path(X, y):

	cs = l1_min_c(X, y, loss='log') * np.logspace(-1, 5)


	clf = linear_model.LogisticRegression(C=1.0, penalty='l1', tol=1e-6)
	coefs_ = []
	for c in cs:
		clf.set_params(C=c)
		clf.fit(X, y)
		coefs_.append(clf.coef_.ravel().copy())

	coefs_ = np.array(coefs_)
	plt.plot(np.log10(cs), coefs_)
	ymin, ymax = plt.ylim()
	plt.xlabel('log(C)')
	plt.ylabel('Coefficients')
	plt.title('Logistic Regression Path')
	plt.axis('tight')
	plt.show()

	return