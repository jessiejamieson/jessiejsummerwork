/code-graphlets/
	This folder contains code used to prepare the graphlets data (from graphprints paper) for our use. 
	MAIN SCRIPT: 
	- detection_test.py is the main function that calls the other 4 .py files. 
		NOTE: switch the file path variable from "../data-graphlets/w-appliance/" to "../data-graphlets/no-appliance" to run on the one or the other. 
		NOTE: switch the "h" variable value if needed. 
		NOTE: change the "tol" variable above vis detection to move the anomaly threshold line. 
	SUPORTTING SCRIPTS: 
	- generalFunctions.py - serializaton code
	- json2numpy.py - serializatoin code
	- robust_gaussian_detection.py
		code for running streaming AD on graphlets (whole-graph level) using MCD. 

/data-graphlets 
	Contains graphlets data both with and without the ip scanning appliance. 
	NOTE: 
    	- indices 278-301 are the bittorrent traffic,
	
	subfolders: 
	- /no-appliance (w/o appliance)
		- graph_detection_info.json has the mu and cov for each fit MCD for graphs 150-600
        - m_dist.json is a list of 600 and has the mdist for each graph from 150-600 (0-148 have mdist == 0).
        - graphlet_list is the list of graphlet names
        - graphlets is a dict of {graph_num : {graphlet name: count } }
    - w-appliance (w/ appliance)
    	(same file descriptions as above.)