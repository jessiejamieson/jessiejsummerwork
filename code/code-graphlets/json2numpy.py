__author__ = 'Chris'

import numpy as np
import codecs, json, os

"""
graphlet_json_2_numpy()
Function that accepts json file paths and returns numpy
arrays of graphlet lists and graphlet counts

INPUT:
    g_list_file     ==> json file of list of graphlets
    g_file          ==> json file of graphlets
    num_graphs      ==> the number of graphs
"""
def graphlet_json_2_numpy(g_list_file, g_file, num_graphs):

    # get list of graphlets
    obj_text = codecs.open(g_list_file, 'r', encoding='utf-8').read()
    g_list = json.loads(obj_text)
    num_graphlets = len(g_list)

    # convert to dictionary (for Bobby's code)
    if type(g_list) is list:
        g_list_tmp = {}
        for i in range(num_graphlets):
            g_list_tmp[g_list[i]] = int(g_list[i][-1])
        g_list = g_list_tmp

    # order them and record the order
    g_ordered = sorted(g_list, key=lambda k: g_list[k])
    i = 0
    flag = True
    for graphlet in g_ordered:

        #
        g_size = g_list[graphlet]
        g_list[graphlet] = {'order':i, 'size': g_size}

        # search for where the first 4-size graphlet appears
        if flag and g_size == 4:
            size_4_ind = i
            flag = False

        i += 1 # increment the counter

    # get graphlet count
    obj_text = codecs.open(g_file, 'r', encoding='utf-8').read()
    g_array = json.loads(obj_text)

    # create numpy array
    gdv = np.empty((num_graphs, num_graphlets))
    for i in range(num_graphs):
        for j in range(num_graphlets):
            gdv[i,j] = int(g_array[str(i)][g_ordered[j]])

    # split the gdv matrix between 3 and 4
    gdv3 = gdv[:,:size_4_ind]
    gdv4 = gdv[:,size_4_ind:]
    return gdv3, gdv4, g_list


def orbit_json_2_numpy(o_list_file, o_file, num_graphs):

    # get list of orbits
    obj_text = codecs.open(o_list_file, 'r', encoding='utf-8').read()
    o_list = json.loads(obj_text)
    num_orbits = len(o_list)

    # order them and record the order
    o_ordered = sorted(o_list, key=lambda k: o_list[k])
    i = 0
    flag = True
    for orbit in o_ordered:

        #
        o_size = o_list[orbit]
        o_list[orbit] = {'order':i, 'size': o_size}

        # search for where the first 4-size orbit appears
        if flag and o_size == 4:
            size_4_ind = i
            flag = False

        i += 1 # increment the counter

    # get orbit vectors
    obj_text = codecs.open(o_file, 'r', encoding='utf-8').read()
    o_array = json.loads(obj_text)

    # create numpy array
    orb = np.empty((num_graphs, num_orbits))
    for i in range(num_graphs):
        for j in range(num_orbits):
            if o_ordered[j] in o_array[str(i)]:
                orb[i,j] = int(o_array[str(i)][o_ordered[j]])
            else:
                orb[i,j] = 0

    # split the gdv matrix between 3 and 4
    orb3 = orb[:,:size_4_ind]
    orb4 = orb[:,size_4_ind:]
    return orb3, orb4, o_list


# # main script
#
# # get the graphlet files
# dataPath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data"))
# processedPath = os.path.join(dataPath, "processed")
# g_list_file = os.path.join(processedPath, "graphlet_list.json")
# g_file = os.path.join(processedPath, "graphlets.json")
#
# # return the numpy arrays
# gdv3, gdv4, graphlet_list = graphlet_json_2_numpy(g_list_file, g_file)

