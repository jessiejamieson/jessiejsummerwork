# ctu-make-features.py

"""
To use this for the different CTU datasets you need to change the index of the data set (k, the first line after the module imports)

This code makes feature vector arrays out of the CTU data. 
Note the CTU dataset must be able to fit into memory! 

It 
1. makes time windows of length span seconds, 
where the span (int) variable is set in the first line of the "## ---- load data and make time windows---- ## " section 
2. It sorts the flows into timewindows and by source ip 
3. It makes a list of the 100 ips w/ the most flows and saves them (ips.json )
NOTE: for CTU data set 1, the botnet IP is '147.32.84.165'
4. Then it makes a feature vector dict 3d array, called "data" where 
data[c,i,j] is  of the form (time_window index, ip index, feature index ) 
The features list (telling you what feature has what index) is saved as is data 
5. It repeats #4. but now with joel's features, which are simply the bytes on each source port, and on each dest port for well-known ports. 
	Joel's features names and the corresponding 3d array of data are also saved. 


-RAB 10/10/2016
"""
import random, csv, os, copy, time
import numpy as np 
from datetime import datetime

from generalFunctions import *


if __name__ == '__main__':
	## ---- paths ---- ##
	k = 1  ## index of ctu file
	folderPath = os.path.abspath( os.path.join( os.path.join( os.getcwd(), ".." ), 'data-ctu-' + str( k ) ) )
	dataPath =  os.path.join( folderPath , "ctu-" + str( k ) + ".csv" )  


	## ---- time functions ---- ##
	def time_to_epoch(timestring):
		"""
		Input: timestring = string in the form %Y/%m/%d %H:%M:%S.%f 
		Output: float (epoch time)
		"""
		utc_time = datetime.strptime(timestring, "%Y/%m/%d %H:%M:%S.%f")
		epoch_time = (utc_time - datetime(1970, 1, 1)).total_seconds()
		return epoch_time


	def epoch_to_time(epoch):
		"""
		Input: epoch = string, epoch time 
		Output: string in form "%Y/%m/%d %H:%M:%S.%f"
		"""
		return time.strftime("%Y/%m/%d %H:%M:%S.%f", time.gmtime(epoch))


	## ---- open file ---- ## 
	f = open(dataPath,'r')
	reader = csv.reader(f)
	header = reader.next() 


	## ---- get indices for contents ---- ## 
	StartTime = header.index('StartTime') ## for timestamps
	SrcAddr = header.index("SrcAddr") ## source ip (node)
	DstAddr = header.index("DstAddr") ## dest ip (node)
	Sport = header.index('Sport') ## source port (edge color)
	Dport = header.index('Dport') ## dest. port (edge color)
	SrcBytes = header.index('SrcBytes')
	TotBytes = header.index('TotBytes') ## total bytes sent (edge weight)
	Label = header.index('Label') ## flows are labeled!! yay!


	## ---- load data and make time windows---- ## 
	span = 60 ## time span of a window in seconds. 
	line = reader.next()
	starttime = time_to_epoch(line[StartTime])

	c = 0 ## time_window counter
	time_windows = {} ## dict of the form {index: [window start time, window end time]}
	flows = {} ## dict of form { time window index: { source ip : {data }}}

	t0 = copy.copy(starttime)
	t1 = t0 + span 
	time_windows[c] = [t0, t1]
	flows[c] = {} 

	begin_clock = time.time()
	while line: 
		t = time_to_epoch(line[StartTime])
		if t >= t1: ## increment everything to the next time window 
			while t1 < t: ## need the while statement in case the time jumped more than one interval. 
				t0 += span
				t1 += span
				c += 1  
				time_windows[c] = [t0, t1]
				flows[c] = {}
				if c % 100 == 0: 
					print "Now on time_window %s" %c
		srcip = line[SrcAddr]
		if srcip not in flows[c]: 
			flows[c][srcip] = []
		flows[c][srcip].append(line)
		try: 
			line = reader.next()
		except: 
			break
	end_clock = time.time()
	print "Loading flows took %s minutes " %((end_clock - begin_clock) / 60.) 
	jsonify(time_windows, os.path.join(folderPath, 'time_windows.json'))

	# for c in time_windows: 
	# 	print c, sum( [ len( flows[c][ip] ) for ip in flows[c]   ])

	## which IPs have a lot of flows?
	ips = sorted(set([x for c in flows.keys() for x in flows[c].keys() ] ) )
	num_flows_total = { ip : sum( [ len( flows[c][ip] ) for c in time_windows.keys() if ip in flows[c]  ] ) for ip in ips }

	active_ips = sorted( num_flows_total.iteritems(), key = lambda (ip, n): n, reverse = True)
	ips, num_flows = zip(*active_ips[:100]) ## take the 100 ips with the most flows

	botnet_ip = '147.32.84.165'
	## check: 
	botnet_ip in ips

	jsonify(ips, os.path.join( folderPath, "ips.json") )

	## now make flows only include the ips we care about, then save 
	flows2 = { c: {ip: [] for ip in ips} for c in flows }
	for c, d in flows.iteritems(): 
		for ip in ips: 
			if ip in d.keys(): 
				flows2[c][ip] = d[ip]
	flows = flows2				
	jsonify(flows, os.path.join( folderPath , 'flows.json'))



	## build the feature vectors for each time window, ip
	begin_clock = time.time()
	vectors = {c: {ip: {} for ip in ips} for c,d in flows.iteritems()}
	for c in sorted( flows.keys() ): ## c is window counter
		if c %100 == 0: 
			print "Starting building feature vectors for window %s" %c
		for ip in ips: 

			try: 
				l = flows[c][ip] ## l is is list of flows
			except: 
				l = []

		 	d = vectors[c][ip] ## currently an empty dict 
			d['num_Mflows'] = len(l)/(10.**3) 
			d['num_Mips'] = len( set( [ f[DstAddr] for f in l ] ) )/(10.**3)
			
			high = 0
			low = 0
			for f in l: 
				dport = f[Dport]
				if dport: 
					try: 
						x = int[dport]
						if x >= 1024: 
							high += 1
						else :
							low += 1 
					except: 
						high += 1

			d['%_high_Dports'] = float( high ) / float( high + low  + .01) 
			
			high = 0
			low = 0
			for f in l: 
				sport = f[Sport]
				if sport: 
					try: 
						x = int[sport]
						if x >= 1024: 
							high += 1
						else :
							low += 1 
					except: 
						high += 1
			d['%_high_Sports'] = float( high ) / float( high + low  + .01) 

			d['src_Gbytes'] = sum([ int(f[SrcBytes]) for f in l  if f[SrcBytes]])/(10.**9)
			d['dst_Gbytes'] = sum([int(f[TotBytes]) - int(f[SrcBytes]) for f in l  if f[TotBytes]])/(10.**9)
			d['pcr'] = float( d['src_Gbytes'] - d[ 'dst_Gbytes' ] ) / (.00001 + (sum([ int(f[TotBytes]) for f in l  if f[TotBytes]])/(10.**9) ) )

	features = sorted([ 'num_Mflows', 'num_Mips', '%_high_Dports', '%_high_Sports', 'src_Gbytes', 'dst_Gbytes', 'pcr' ])
	# features = sorted(set( vectors[c][ip].keys() for c in time_windows.keys() for ip in ips ))
	jsonify(features, os.path.join(folderPath, 'features.json'))


	data = np.zeros( ( len(time_windows), len(ips), len(features) ) )
	for c in xrange(len(time_windows)): 
		for i, ip in enumerate(ips): 
			for j, f in enumerate(features): 
				data[c,i,j] = vectors[c][ip][f]

	jsonify(data.tolist() , os.path.join(folderPath, 'data.json'))

	end_clock = time.time()
	print "it took %s minutes to make the vectors" %( ( end_clock - begin_clock )/ 60. )



	## make joel's vectors: 
	# begin_clock = time.time()
	# joel_features = ["dst_bytes_" + str(n + 1) for n in range(1023)] + ["src_bytes_" + str( n+1 ) for n in range(1023)]
	# jsonify(joel_features, os.path.join(folderPath, 'joel_features.json')) 

	# joel_vectors = {c: {ip: { feature: 0 for feature in joel_features} for ip in ips } for c in flows.keys() }

	
	# def _int(s): 
	# 	try: 
	# 		x = int(s)
	# 	except: 
	# 		x = 0
	# 	return x 


	# def _sum(l): 
	# 	try: 
	# 		x = sum(l)
	# 	except: 
	# 		x = 0 
	# 	return x 


	# for c in time_windows.keys(): 
	# 	if c %100 == 0: 
	# 		print "Starting to build Joel_vector for window %s " %c 
	# 	for ip in ips: 	
	# 		try: 
	# 			l = flows[c][ip] ## list of flows
	# 			for n in range(1023): 
	# 				joel_vectors[c][ip]["dst_bytes_" + str(n + 1)] = _sum([ _int(f[TotBytes]) - _int( f[SrcBytes] )  for f in l  if f[Dport] == str(n+1)] )
	# 				joel_vectors[c][ip]["src_bytes_" + str(n + 1)] = _sum([ _int( f[SrcBytes] )  for f in l  if f[Sport] == str(n+1)] )
	# 		except: 
	# 			pass


	# joel_data = np.zeros( (len(time_windows), len(ips) , len(joel_features )))
	# for c in xrange(len(time_windows)):
	# 	for i, ip in enumerate(ips): 
	# 		for j, feature in enumerate(joel_features): 
	# 			joel_data[c , i, j ] = joel_vectors[c][ip][feature]

	# jsonify(joel_data.tolist(), os.path.join(folderPath, 'joel_data.json'))

	# end_clock = time.time() 
	# print "it took %s minutes to make Joel vectors " %(( end_clock - begin_clock ) / 60. )	



