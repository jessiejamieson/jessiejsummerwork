"""
    File    = DataGeneration.py
    Author  = Jessie Jamieson
    Email   = jdjamieson@huskers.unl.edu
    Created = 2016-06-16

    Last Modified = 2016-06-21
    version       = 1.0

    Description:
        This file is part of Jessie Jamieson's summer work at ORNL under the
        direction of Dr. Robert Bridges (Go Team Bridges!)

        The point of this file is to actually write data to a .csv file.

        The experiment runs for "count" days (see the bottom), and is currently
        set up to test 5 different "expectation" numbers. A .csv file is returned
        with a table of how many anomalies are detected each day as compared to
        each expectation value.

"""

#Importing Stuffs
import numpy as np
import sklearn as sk
import scipy
import scipy.stats
import numpy.linalg
#import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm
from sklearn import mixture
from sklearn.covariance import MinCovDet
import csv

"""
The code below is a general code that will generate test data for the
experiment. Initially, we will generate 200 points from a Gaussian mixed model
with parameters that we can decide, but this value can be raised.
"""

class ExperimentData (object):

    """
    Create a Set of Experiment Data. NumPoints should be the number of test
    points we wish to test our MCDs against.

    Inputs: NumPoints: The number of test-data points
            Distrb: The distributions (with parameters for the mixed Gaussian)
            we wish to sample from
            NumSamples: The number of points to build distributions from
    Outputs: self.Data: Experiment data from a mixed Gaussian distribution.

    Caution: for some reason, when python generates these random samples of
    distributions, NumSamples will cause an error unless it is sufficiently
    larger than NumPoints.
    """
    def __init__(self, NumPoints,Distrb, NumSamples = 5000):
        self.NumPoints    = NumPoints ### no. of points seen initially to fit model
        self.NumSamples   = NumSamples ### no. of points used to 
        self.Distribution = Distrb
        samplex = list()
        sampley = list()
        for dist in Distrb:
            mean  = dist['mean']
            cov   = dist['cov']
            x, y  = np.random.multivariate_normal(mean, cov, NumSamples).T
            samplex.append(x)
            sampley.append(y)
        #ENDFOR

        self.data = np.zeros((NumPoints,2),dtype = float)
        for i in xrange(NumPoints):
            p = np.random.uniform(0, 1)
            if p <= .9:
                self.data[i,0] = (samplex[0])[i]
                self.data[i,1] = (sampley[0])[i]
            else:
                self.data[i,0] = (samplex[1])[i]
                self.data[i,1] = (sampley[1])[i]
            #ENDIF
        #ENDFOR
    #ENDinitDEF

    def MCDDists(self,M):
        """
        Generates the MCD distributions, trained on points M to end. M is the
        number of MCD distributions you would like to generate.

        Inputs: M: The number of MCD Distributions you want to generate. The variable
                CheckList should also be an input, but I have not changed this yet.
                it is a list of different expected values you wish to iterate over.
        Outputs: Data: The title and initial conditions of the experiment
                 Check: The expected number of anomalies for the experiment given
                    CheckList
                 alpha: The Mah. distance we are using for a critical value
                 AnomalyCount: The number of detected anomalies for each
                    element of CheckList
                 MCDList: A list of vectors of the form [MCD Loc., MCD Cov.]
        """
        CheckList=[5, 10, 15, 20, 50] #Iterate over this with p, below.
        Data=[["Day, Expected"]]
        AnomalyCount = [count+1]
        Total=200                    #The total number of points per day. 86400 is 1 point/second
        #alpha = scipy.stats.chi2.isf(float(Check)/Total,2) ## gives H^{-1}(float(check)/total) , H(x) = (1-CDF_{\chi^2})(x)
        Pvalues = list()
        MCDList = list()
        #AnomalyCount = list()
        for i in xrange(M,self.NumPoints):
            #print i
            MCDs = sk.covariance.MinCovDet(support_fraction=0.9).fit(self.data[0:i,:])
            MCDList.append([MCDs.location_,MCDs.covariance_])
        print "MCDs Generated"
                # x, y = np.random.multivariate_normal(MCDs.location_, MCDs.covariance_, self.NumSamples).T

                #pvalue_tmp = list()
                #for j in xrange(self.NumPoints):
                #    distance = scipy.spatial.distance.mahalanobis(self.data[j,:], MCDs.location_, np.linalg.inv(MCDs.covariance_))
                #    pvalue_tmp.append(scipy.stats.chi2.sf(distance, 2))
                #    #ENDFOR
                #    Pvalues.append(pvalue_tmp)
                    #ENDFOR
            #value_temp=scipy.stats.chi2.isf(float(Check)/Total,2)
        for p in xrange(5):                 #Change this if you change CheckList
            Check=CheckList[p]
            Data[0].append(Check)
            alpha = scipy.stats.chi2.isf(float(Check)/Total,2) ## gives H^{-1}(float(check)/total) , H(x) = (1-CDF_{\chi^2})(x)
            ac_tmp = list()
            MCDDistances = list()
            for j in xrange(200,399):
                MCDDistances.append(pow(scipy.spatial.distance.mahalanobis([self.data[j+1][0],self.data[j+1][1]], MCDList[j-200][0], np.linalg.inv(MCDList[j-200][1])),2))
            #ENDFOR
            for k in xrange(199):
                if MCDDistances[k]>= alpha:
                    ac_tmp.append(1)
            #ENDIF
            AnomalyCount.append(len(ac_tmp))
         #ENDFOR
        #AnomalyCount.append(len(ac_tmp))
            #print Check
            #print alpha
        print AnomalyCount
        return Data, Check, alpha, AnomalyCount, MCDList
    #ENDDEF
#ENDCLASS

## Setup distribution for test ##
Dist = list()

# "good points"
GoodDist = {}
GoodDist['mean'] = [0,0]
GoodDist['cov']  = [[1,0],[0,1]]
Dist.append(GoodDist)

# "bad points"
BadDist = {}
BadDist['mean'] = [2,2]
BadDist['cov']  = [[1,0.5],[0.5,1]]
Dist.append(BadDist)

'''
This while loop will allow you to run the experiment over many days, 7 as it
is written now. Change the count bound if you wish to increase or decrease the
number of days it simulates.
'''

#Data_Temp=list()
Data_init=list()
count=0
while count<7:
    Experiment = ExperimentData(400,Dist)
    print "Initial Data Generated"
    DataStart, Check, Alpha, AnomalyCount, MCD = Experiment.MCDDists(200)
    #DataStart.append(AnomalyCount)
    if count==0:
        Data_init.append(DataStart)
        Data_init.append(AnomalyCount)
    else:
        Data_init.append(AnomalyCount)
    count=count+1

'''
This is the code that will output a .csv file of a table of data. Change the
path if necessary.
'''

#Data=[["Expected, Day", "Day 1", "Day 2", "Day 3", "Day 4", "Day 5", "Day 6", "Day 7"]]
#Data.append(Anomalies)
DataOutput=open('/Users/Jessie/jessiejsummerwork/data/MCDDataTable.csv', 'w')
F=csv.writer(DataOutput)
F.writerows(Data_init)
DataOutput.close
#print len(MCD)

#for index, plist in enumerate(pvalues):
#    print plist[index + 1]
    #if index == len(plist):
    #    break
    #ENDIF
#ENDFOR

#Check=100
#Total=200

#alpha = scipy.stats.chi2.ppf(1-Check/Total,2)
#alpha = scipy.stats.chi2.isf(float(Check)/Total,2) ## gives H^{-1}(float(check)) , H(x) = (1-CDF_{\chi^2})(x)
#print alpha
#ExperimentData = np.zeros((Total,2),dtype = float)
#Unif=np.random.multivariate_normal([0,0], [[1,0],[0,1]], Total).T
#OffCent=np.random.multivariate_normal([2,2], [[1,0.5],[0.5,1]], Total).T
#for i in xrange(Total):
    #p = np.random.uniform(0, 1)
    #if p <= .9:
    #    ExperimentData[i,0] = (Unif[0])[i]
    #    ExperimentData[i,1] = (Unif[1])[i]
    #else:
    #    ExperimentData[i,0] = (OffCent[0])[i]
    #    ExperimentData[i,1] = (OffCent[1])[i]
    #ENDIF
#ENDFOR



#AnomalyCount = list()
#for d in MCD:
#      ac_tmp = list()
#      MCDDistances = list()
#      for j in xrange(Total):
#          MCDDistances.append(pow(scipy.spatial.distance.mahalanobis([ExperimentData[j][0],ExperimentData[j][1]], d[0], np.linalg.inv(d[1])),2))
#      #ENDFOR
#      for k in xrange(Total):
#          if MCDDistances[k]>= alpha):
#              ac_tmp.append(1)
#          #ENDIF
#      #ENDFOR
#      AnomalyCount.append(len(ac_tmp))
#print alpha
#print AnomalyCount

#fig, ax = plt.subplots(1, 1)
#ax.hist(MCDDistances, bins=25, normed=True)
#x = np.linspace(scipy.stats.chi2.ppf(0.01, 2),scipy.stats.chi2.ppf(0.99, 2), 100)
#ax.plot(x, scipy.stats.chi2.pdf(x, 2),'r-', lw=5, alpha=0.6, label='chi2 pdf')
#plt.show()
