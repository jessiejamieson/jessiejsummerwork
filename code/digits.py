from sklearn import datasets, neighbors, linear_model
from random import shuffle

digits = datasets.load_digits()

fourindices = [i for i, x in enumerate(digits.target) if x == 4]

nineindices = [i for i, x in enumerate(digits.target) if x == 9]

fourdigits = [digits.data[k] for k in fourindices]

ninedigits = [digits.data[k] for k in nineindices]

foursnines = fourdigits+ninedigits

real_labels = [4]*len(fourdigits)+[9]*len(ninedigits)

zippeddata = list(zip(real_labels, foursnines))

shuffledzippeddata = list(zip(real_labels, foursnines))

shuffledtuples = shuffle(shuffledzippeddata)

#X_digits = digits.data
#y_digits = digits.target

shuffled_foursnines = [x[1] for x in shuffledzippeddata]
shuffled_labels = [x[0] for x in shuffledzippeddata]

n_samples = len(real_labels)
training_end = round(0.6*n_samples)
eighty_percent = round(0.8*n_samples)


X_train = shuffled_foursnines[:training_end] 
y_train = shuffled_labels[:training_end] 
X_test = shuffled_foursnines[training_end:eighty_percent]
y_test = shuffled_labels[training_end:eighty_percent]

logistic = linear_model.LogisticRegression()

logistic_fit = logistic.fit(X_train, y_train)

print('LogisticRegression score: %f'
      % logistic.fit(X_train, y_train).predict(X_test, y_test))