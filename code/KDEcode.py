import numpy as np
import time
from collections import defaultdict
import random, os
#import netaddr as na 
#from ipwhois import IPWhois as ipw
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import colors
from sklearn.covariance import MinCovDet as MCD
from sklearn.neighbors.kde import KernelDensity
#from generalFunctions import *


X = np.array([stuff]) ## take the x_i, p_i, and include x_i as many times as needed so the ratios of the p_i’s work out. 

## e.g. x_1, p_1 = 3/4, x_2, p_2 = 1/4, then use X = [x_1, x_1, x_1, x_2] 
## I think each data point is a row of X. 

width = float((max(X) - min(X))/2. + .1 ) ## bandwidth parameter. 
kernel = "cosine" ### choices are ['gaussian', 'tophat', 'epanechnikov', 'exponential', 'linear', 'cosine']
kde = KernelDensity(kernel = kernel, bandwidth = width).fit(X) ## fit model
p = float(np.exp(kde.score_samples(np.array(f[x]).reshape((1,1))))) # this is the likelihood of the new data (score_samples gives the log(pdf))
a, b = min(X) - width, max(X) + width ## [a,b] is the support of our kde pdf
S = np.linspace(a, b, 10000).reshape((10000,1)) ## sample points
delta = (b - a)/10000. ## interval length
A = np.exp(kde.score_samples(S)) ## probability of sample points
pv = float(np.sum(A[A <= p])*delta )

#xx = list(np.arange(0,1.05, .05))
plt.plot(A, pv, label = "A vs pv")
#plt.plot(xx, map(h0, xx), label = "Gold standard h")
plt.ylabel('Anomaly Scores')
plt.xlabel('P-values')
#plt.legend(bbox_to_anchor=(1, 1),bbox_transform=plt.gcf().transFigure)
plt.show()
continue