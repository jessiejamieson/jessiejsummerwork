# clustering.py

"""
Description: 
		k-means implementation from 
		https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/

		gap statistic implementation from 
		https://datasciencelab.wordpress.com/tag/gap-statistic/ 

		This file is a work in progress and was to be used in conjunction with ar_sim_gradient.py
		to do clustering using the create_f_h, ascent, and probability finding functions. 
"""

## ---- modules ---- ##
import numpy as np 
from numpy import linalg 
import random 
import matplotlib.pyplot as plt


## ---- data sampling ---- ## 
def init_board(N):
	X = np.array([(random.uniform(-1, 1), random.uniform(-1, 1)) for i in range(N)])
	return X


def init_board_gauss(N, k):
	n = float(N)/k
	X = []
	for i in range(k):
		c = (random.uniform(-1, 1), random.uniform(-1, 1))
		print "center %s = %s" %(i,c)
		s = random.uniform(0.05,0.3)
		x = []
		while len(x) < n:
			a, b = np.array([np.random.normal(c[0], s), np.random.normal(c[1], s)])
			# Continue drawing points from the distribution in the range [-1,1]
			if abs(a) < 1 and abs(b) < 1:
				x.append([a,b])
		X.extend(x)
	X = np.array(X)[:N]
	return X



## ---- k-means implementaton ---- ##  
def cluster_points(X, mu):
	clusters  = {}
	for x in X:
		bestmukey = min([(i[0], np.linalg.norm(x-mu[i[0]])) \
					for i in enumerate(mu)], key=lambda t:t[1])[0]
		try:
			clusters[bestmukey].append(x)
		except KeyError:
			clusters[bestmukey] = [x]
	return clusters


def reevaluate_centers(mu, clusters):
	newmu = []
	keys = sorted(clusters.keys())
	for k in keys:
		newmu.append(np.mean(clusters[k], axis = 0))
	return newmu


def has_converged(mu, oldmu):
	return (set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu]))


def find_centers(X, K):
	# Initialize to K random centers
	oldmu = random.sample(X, K)
	mu = random.sample(X, K)
	converged = False
	while not converged:
		oldmu = mu
		# Assign all points in X to clusters
		clusters = cluster_points(X, mu)
		# Reevaluate centers
		mu = reevaluate_centers(oldmu, clusters)
		converged = has_converged(mu, oldmu)
	return (mu, clusters)
	

## ---- gap-statistic implementation ---- ##
def Wk(mu, clusters):
	K = len(mu)
	return sum( [ np.linalg.norm( mu[i] - c)**2/(2*len(c)) for i in range(K) for c in clusters[i] ] )


def bounding_box(X):
	xmin, xmax = min( X, key=lambda a:a[0] )[0], max( X, key=lambda a : a[0] )[0]
	ymin, ymax = min( X, key=lambda a:a[1] )[1], max( X, key=lambda a : a[1] )[1]
	return (xmin,xmax), (ymin,ymax)


def gap_statistic(X, kmin, kmax, plot = False, verbose = False):
	""" 
	Input: 	X = np.array with a data point in each row 
			kmin = positive int, smallest k we should try 
			kmax = positive int, max k we should try 
			plot = bool, if True it makes plots. 
			verbose = bool, if True it prints updates as it runs
	Output: k = the k used for clustering
			ks = list of k's tried 
			Wks = list of sum(log(cluster variances)) (one for each k in ks)
			Wkbs = list of expected sum(log(cluster variances)) for data sampled from a uniform distribution 
			sk = standard deviation of the Wkbs's. 
	Description : runs gap statistic. Gap is the Wkbs - Wks (clusters should be farther apart in a uniform than in our data)
					We look for the first k such that gap[k] > gap[k+1] - sd[k+1]
	""" 
	(xmin,xmax), (ymin,ymax) = bounding_box(X)
	ks = range(kmin, kmax+2) ## ks is the range of the number of clusters to try, we need to go t kmax+2 b/c we have to check the gap between kmax and kmax +1 (and range(a,b) ends at b-1)
	Wks = np.zeros(len(ks)) ## to be populated log variance of each cluster of real data
	Wkbs = np.zeros(len(ks)) ## to be populated average log variance of each cluster of sampled data
	sk = np.zeros(len(ks)) ## to be populated standard deviation of Wkbs 
	
	for indk, k in enumerate(ks):
		if verbose: 
			print "\tGap Statistic code, beginning k = %s" %k
		mu, clusters = find_centers(X,k) ## mu's rows are the cluster centers

		Wks[indk] = np.log(Wk(mu, clusters))
		
		## Create B reference datasets
		B = 10
		BWkbs = np.zeros(B)
		for i in range(B):
			Xb = []
			for n in range(len(X)):
				Xb.append([random.uniform(xmin,xmax),
						  random.uniform(ymin,ymax)])
			Xb = np.array(Xb)
			mu, clusters = find_centers(Xb,k)
			BWkbs[i] = np.log(Wk(mu, clusters))
		
		Wkbs[indk] = sum(BWkbs)/B
		sk[indk] = np.sqrt(sum((BWkbs-Wkbs[indk])**2)/B)

	sk = sk * np.sqrt( 1 + 1/B )

	gaps = Wkbs - Wks	
	check = [gaps[i] - (gaps[i+1] - sk[i+1]) for i in xrange(len(ks)-1)] 	## we care about the first k where gap[k]>gap[k+1] - sd[k+1]


	if plot: 
		plt.figure(1)

		plt.subplot(211)
		plt.ylabel("Gaps E(log(Wb_k)) - W_k")
		plt.xlabel("k")
		plt.plot(ks, gaps, 'bo')


		plt.subplot(212)
		plt.plot(ks[:-1], check, 'go')
		plt.ylabel("Gap[k] - (Gap[k+1] - sd[k+1])")
		plt.xlabel("k")
		plt.title("First k above 0 is...")
		plt.axhline(y = 0)

		plt.show()

	for i in xrange(len(check)): 
		if check[i]>0: 
			break
	k  = ks[i] ## this is the k we want
	if verbose: 
		print "k = %s is the number of clusters to use" %k
	return (k, ks, Wks, Wkbs, sk)  


## ---- test it ---- ##
if __name__ == '__main__':
	X = init_board_gauss(300, 3) ## get data 
	(k, ks, Wks, Wkbs, sk)  = gap_statistic(X,1,3)
	mu, clusters = find_centers(X, k) ## cluster 
	c = {}
	for i in xrange(len(clusters)): 
		c[i] = np.array(clusters[i])

	plt.plot(c[0][:,0], c[0][:,1], 'bo')
	
	if k == 2: 
		plt.plot(c[1][:,0], c[1][:,1], 'ro')

	if k == 3: 
		plt.plot(c[2][:,0], c[2][:,1], "go" )
	plt.show()



##---- Mahalanobis -------##

def cluster_mahal(mu, cluster):
	xs=[]
	ys=[]
	for k in cluster.keys():
		xtemp=[]
		ytemp=[]
		clusterpoints=cluster[k]
		clustermean=mu[k]
		for p in clusterpoints:
			xtemp.append(p[0])
			ytemp.append(p[1])
		#endfor
		xs.append(xtemp)
		ys.append(ytemp)
	#endfor

	AllMahalDistances={}

	for x, y in xs, ys:
		mahaltemp=[]
		covariancetemp=np.cov(x,y)
		mutemp=mu[xs.index(x)]
		for i in xrange(len(x)):
			mahaltemp.append(scipy.spatial.distance.mahalanobis([x[i],y[i]],mutemp, np.linalg.inv(np.matrix(covariancetemp))))
		AllMahalDistances[xs.index(x)]=mahaltemp
		#endfor
	#endfor	

	return AllMahalDistances








