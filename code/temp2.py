# temp.py

"""

## for dataset 1,
initial time is 2011/08/10 09:46:53.f  
botnet infection occurs around 2011/08/10 10:50:00.f and 
occurs until about 2011/08/10 15:49:53.f'

w/ 60 second timespan per time_window, we start detection at 60 and go until the end. 

-RAB 10/10/2016
"""
import random, csv, os, copy, time
import numpy as np 
from datetime import datetime

from generalFunctions import *
from clustering import * 


## ---- paths ---- ##
k = 1  ## index of ctu file
folderPath = os.path.abspath( os.path.join( os.path.join( os.getcwd(), ".." ), 'data-ctu-' + str( k ) ) )
dataPath =  os.path.join( folderPath , "ctu-" + str( k ) + ".csv" )  


## ---- open file ---- ## 
time_windows = {int(c): d for c,d in unjsonify( os.path.join(folderPath, 'time_windows.json')).iteritems()}  ## dict of the form {index: [window start time, window end time]}
flows = { int(c): d for c,d in unjsonify(os.path.join(folderPath, 'flows.json')).iteritems()}
botnet_ip = '147.32.84.165'
ips = unjsonify( os.path.join( folderPath, "ips.json") )
## check: 
if not botnet_ip in ips: 
	print "botnet_ip not in ips!"

labels = unjsonify(os.path.join(folderPath, 'labels.json' ))				


## ---- load joel's features ---- ##
## joel's features: 
joel_features = unjsonify(os.path.join(folderPath, 'joel_features.json')) ### list of feature names
joel_data = np.array(unjsonify( os.path.join(folderPath, 'joel_data.json')) ) ### an 3-d array, timewindos x ip x features


## make 1 vector per ip for the first hour: 
X = np.sum(joel_data[:60, :,:], axis = 0) ## now of shape 100 ips by features
## remove rows that are constant (always 0 is really the only way this can happen)
var = np.var(X, axis = 1)
delete_rows = np.where(var == 0)[0] 
X = np.delete(X, delete_rows, axis = 0)

## remove cols that are constant (these features aren't observed)
var = np.var(X, axis = 0)
delete_columns = np.where(var == 0)[0] 
X = np.delete(X, delete_columns, axis = 1)
features = list(np.delete(np.array(joel_features).reshape(1, len(joel_features)), delete_columns, axis = 1))

## now normalize each vector: 
for i in xrange(X.shape[0]): 
	X[i,:] = X[i,:]/ np.sqrt(np.dot(X[i,:], X[i,:]))

## finally, cluster: 
# (k, ks, Wks, Wkbs, sk) = gap_statistic(X, 3, 20, plot = True, verbose = True)
# means, clusters = find_centers(X, k)
# d = {(i,j): np.sqrt( np.dot(x-y, x-y) ) for i,x in enumerate(means) for j,y in enumerate(means) }

## used k = 4 b/c distance between cluster centers is always greater than .5, and each cluster has at least 4 elements. If you go to 5, distances become smaller and singleton clusters emerge. 
means, clusters = find_centers(X, 4)
d = {(i,j): np.sqrt( np.dot(x-y, x-y) ) for i,x in enumerate(means) for j,y in enumerate(means) }

models = {} 
for cluster, points in clusters.iteritems(): 
	








# not used:
## gaussian detection for each ip: 
# c_start = 60 
# h = .98 ## mcd parameter
# mcd = MinCovDet(support_fraction = h)

# gaussians = {c: {ip: {} for ip in ips } for c in xrange(c_start, len(time_windows))} ##means and cov of history upto, not includeing c. 
# p_values = {c: {ip: "n/a" } for c in xrange(c_start, len(time_windows))} ##p-value of x_c given gaussian on history 

# c_start = 60 ## time interval to start on .


# for i, ip in enumerate(ips): 
# 	if i % 10 == 0: 
# 		print "beginning ip %s of 100" %i
# 	for c in xrange(c_start, len(time_windows)-1): 
# 		X = data[:c, i, :]
# 		x_new = data[ c , i, :].reshape(1, X.shape[-1]) ## test data

# 		## delete constant columns: 
# 		colvar = np.var(X, axis=0) ## these columns never change, making covariance matrix singular! usually they're always be 0!! It means this feature is useless in the observations 
# 		badcols = set(list(np.where(colvar < .0001 )[0]) + list(np.where( np.isnan(colvar))[0] ) )

# 		if len( badcols ) > 0:
# 			# print "removing columns: " + str(badcols) + "which are constant (else the cov. matrix will be singular)"
# 			## remove the bad columns by this awesome and semi-mysterious maneuver:
# 			for j in badcols: 
# 				X[0, j] = float("nan")  # give each such column a nan value (indicator to be nixed)
# 				x_new[0, j] = float("nan")
# 			X = np.ma.compress_cols(np.ma.masked_invalid(X)) ## the masked_invalid function switches the NaN's to dashes, the compress function deletes those columns. 
# 			x_new = np.ma.compress_cols(np.ma.masked_invalid(x_new)) ## the masked_invalid function switches the NaN's to dashes, the compress function deletes those columns. 

# 		## now we should be able to fit the mcd. Sometimes it doesn't converge, so we use try and except to add a little noise. If it still fails, we just use the mean, cov w/o mcd method. 
# 		try: 
# 			mcd.fit(X) 
# 			mean = mcd.location_
# 			cov = mcd.covariance_
# 		except: ## add a little noise to the samples b/c it's singular. 
# 			# try: 
# 			std = np.var(X, axis = 0)**.5	## 1 d array of standard deviations of each column 		
# 			E = .1 * std * np.random.rand( *X.shape ) ## an array w/ same shape as X. Made by drawing a uniform random sample in [0, standard deviation of the column * .1] and adds to entry 
# 			mcd.fit( X+E )
# 			mean = mcd.location_
# 			cov = mcd.covariance_
# 			# except: 
# 				# mean = np.mean( X, axis = 1)
# 				# cov = np.cov( X.T )

# 		gaussians[c][ip]['mean'] = mcd.location_.tolist()
# 		gaussians[c][ip]['cov'] = mcd.covariance_.tolist() 
		
# 		## now what's the p-value of x? 
# 		m_dist = mcd.mahalanobis(x_new)[0] ## this is the squared mahalanobis distance. It equals < cov^{-1} (x-mu), x-mu > 
# 		## note that 1-chi^2_CDF(alpha^2) = P(mahalanobis(x) > alpha ). This means to convert m_dist to a p_value, we use 
# 		##  p_value(x) = P(mahalanobis(X) >= mahalanobis(x)) = p(mahalanobis(X) > m_dist^.5) 1-chi2.cdf(m_dist) =  = p(mahalanobis(X) > mahalanobis(x)) where x is observed and X is a random variable. 
# 		pv = 1 - chi2.cdf(m_dist, x_new.shape[-1])
# 		p_values[c][ip] = pv 










