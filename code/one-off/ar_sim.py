# ar_sim.py

## Written by ßøßß¥

import numpy as np
import time
from collections import defaultdict
import random, os
import netaddr as na 
from ipwhois import IPWhois as ipw
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import colors
from sklearn.covariance import MinCovDet as MCD
from generalFunctions import *



## ---- functions ---- ##
def discrete_p_value(i ,f): 
	"""
	Input: 	i -- a key of d. 
			f -- dict, values are floats. Dict gives  (sample, probability) giving a probability mass funciton)
	Output: float: p-value of i under pmf f. 
	""" 
	pv = round(sum([v for v in f.values() if v <= f[i]]), 2)
	return pv 


def linearly_extend(h_):
	"""
	Input: 			h_ -- dict { float in [0,1]: float in [0,1]}. key-value pairs are points on the graph of a function
	Description: 	This linearly connects the dots to extend the h_ function to all of [0,1]
	Output:  		h -- function (input = x, float in [0,1], output float in [0,1]).
	"""
	def h(x):
		h_[1] = 0
		T = sorted(set(h_.keys()))
		s, t = T[0], T[1]
		i = 0
		while t < x: 
			i += 1
			s, t = T[i], T[i+1]
		return ((h_[t]-h_[s])/float(t-s))*(x-s) + h_[s]
	return h


# def create_f_h(a, n = 0, test = False): 
# 	""" 
# 	Input: 	a = dict. This gives the observed data and corresponding data and anomaly scores. Keys are samples, values are numbers in [0,1) (floats) giving the observed a_scores.
# 			n = 0 (default) or positive int. n > 0 will add a set of mass 1/n w/ small probability to account for unseen data. 
# 			test = bool. If true, it will print results of test code. 
# 	Description: The values of a are the anomaly scores. 
# 		This function creates a decreasing function h, and a probability mass function, f
# 		so that A(x) = h(p-value(x)). 
# 	Output: f = dict. Keys are samples, values are probabilities of that sample ( a prob. mass function)
# 			h = dict. Keys are p-values attained by f (floats) , and values are anomaly scores (floats). 
# 	""" 

# 	## remove the guys that have a-score = 1. They have 0 probability. 
# 	for x in a.keys():
# 		if a[x]==1:
# 			a.pop(x)

# 	## make sure the minimum is indeed 0:		
# 	a0 = min(a.values())
# 	if a0 != 0: 
# 		for x in a.keys(): 
# 			a[x] = a[x] - a0

# 	A = sorted(set( a.values()), reverse = True) ## just the set of values 
# 	m = len(A)
# 	reverse_a = {ai: [x for x in a.keys() if a[x] == ai] for ai in A} ## the reverse lookup.

# 	X = {i : reverse_a[A[i]] for i in xrange(m)}
# 	k = {i : len(X[i]) for i in xrange(m)}
# 	if n != 0: 
# 		k[0]  += 1./n ## add a little probability mass for the not-yet-seen points, called "other"

# 	## initiate dicts to be populated: 
# 	y = {i: 0 for i in xrange(-1, m)} ## p-values to be determined. 
# 	e = {i: 0 for i in xrange(m)} ## auxilary values that are needed to populate everything else. 
# 	p = {i: 0 for i in xrange(m)} ## probability masses (not per point, per levelset of A)

# 	for i in xrange(m): 
# 		## want to count this in reverse. 
# 		c = sum([k[j] for j in xrange(i, m)])
# 		e[i] = float(k[i])/ float(m*c)
# 		if i == 0: 
# 			y[0] = e[0]
# 			p[0] = y[0]/k[0]
# 		else: 
# 			y[i] = ( float(k[i])/k[i-1] ) * (y[i-1] - y[i-2])  + y[i-1] + e[i]
# 			p[i] = (y[i] - y[i-1]) /k[i] 


# 	## test code: : 
# 	if test: 
# 		print "Do the probabilities sum to 1?\n\t%s" %(round(1-sum([k[i]*p[i] for i in xrange(m)]), 1) == 0.0 )
		
# 		x = int(0 >= p[0]) ## if it's not true, store 1, else store 0
# 		for i in xrange(m-1): 
# 			x += int( p[i] >= p[i + 1] ) ## if it's not true, add 1
# 		print "Is 0 < p0 < p1 < ... ?\n\t%s" %(bool(not(x)))
		
# 		print "Do the p-values max at 1?\n\t%s" %(round(1-y[m-1],1) == 0. )

# 	## now make the probability mass function and decreasing function: 
# 	f = {}
# 	h_ = { 0.:1.}
# 	for i, l  in X.iteritems(): 
# 		for x in l: 
# 			f[x] = p[i]
# 			h_[y[i]] = a[x]


# 	if test: 
# 		for x in a.keys(): 
# 			print "\n\tx = %s" %x
# 			i = A.index(a[x])
# 			result = (round(discrete_p_value(x,f) - y[i], 1) == 0)
# 			print "\t p-value(x) matches y?\n\t\t%s" %(result)
# 			if not n and result: 
# 				result2 = (round( a[x] - h_[y[i]], 1) == 0 ) 
# 				print "\t h[p-value(x)] == a[x]?\n\t\t%s" %(result2)
# 	return f, h_


## generate data functions 
def create_continuous_data(n): 
	""" 
	Input: 	n = number of data points desired 
	Output: X = np.array of shape 2 by n (n data points in R^2)
			a_scores = np.array of shape n by 1, the corresponding anomaly scores in 0,1
	Description: GMM data generated .9 from standard bivariate, .1 from mean (2,2) cov [[1, .5,], [.5, 1]]. 
				MCD fit to an initial 100 points. h = .9
				new data points are scored, then mcd refit. 
				scoring is 1-exp(.1*mahalanobis distance)
	""" 

	mean = np.array([2, 2])
	cov = np.array([[1, 0.5], [0.5, 1]])
	x, y = np.random.multivariate_normal(mean, cov, 100 + n).T

	#Here I want regular old standard gaussian distribution
	mean2 = np.array([0, 0])
	cov2 = np.array([[1, 0], [0, 1]])
	x2, y2 = np.random.multivariate_normal(mean2, cov2, 100 + n).T

	#These are the plotting commands
	#fig = plt.figure()
	#ax1 = fig.add_subplot(111)
	#ax1.scatter(x, y, s=10, c='b', marker="s", label='skewed')
	#ax1.scatter(x2, y2, s=10, c='r', marker="o", label='normal')
	#plt.legend(loc='upper left');
	#plt.show()

	sampled_x=np.zeros(100 + n, dtype=float)
	sampled_y=np.zeros(100 + n, dtype=float)

	#gaussian mixture model. 
	for i in xrange(100 + n):
	    p = np.random.uniform(0, 1)
	    if p > 0.9:
	        sampled_x[i]=x[i]
	        sampled_y[i]=y[i]
	    if p < 0.9:
	        sampled_x[i]=x2[i]
	        sampled_y[i]=y2[i]

	X = np.array([sampled_x,sampled_y]).T ## x's down first column y's down second. 

	m_dist = {}
	# mcds = {}

	for i in xrange(100, n + 100):
	    #Xi=X[0:i]
	    Xi = X[: i , :]
	    mcd = MCD( support_fraction = .9 ).fit(Xi)
	    m_dist[i] = mcd.mahalanobis(X[i].reshape(1,2))[0] ##1-np.exp(-mcd.mahalanobis(X[i].reshape(1,2)))[0]

	a_score = 1-np.exp(-.1*np.array(m_dist.values()))
	# the histogram of the data
	# plt.hist(a_score)
	# plt.xlabel("a_score")
	# plt.ylabel("count")
	# plt.show()

	return X[100:,], a_score 


def create_discrete_data(n, p, h): 
	"""
	Input: 	n = positive int. number of data points desired
			p = list of positive floats summing to 1. these give the multinomial probability, for the model that occurs 95% of the time
			h = function. input and output are floats in [0,1], must be strictly decreasing. h(p-value) = a_score when creating the data 
	Output: X = np.array of shape n,. gives numbers in [0,1,2,3,...] that were sampled 
			a_score = np.array of shape n, . gives floats in [0,1).
	
	Description: Samples from a mixture of multinomials. Fits a multinomial and uses 1-pvalue to score, then updates. 
	""" 

	data = np.random.multinomial(1, p, size = n + 200)
	samples = {}
	for i in xrange(n + 200): 
		samples[i] = np.where(data[i])[0][0] ## write the side of the die that was rolled by model x on roll i
	l = len(p)
	f = {i : (1 + len( [x for x in samples.keys()[:200] if samples[x] == i]) )  for i in xrange(l) } ## divide by the sum of the values to get a probability
	X = np.array(samples.values()[200:]).reshape(n,)
	a_score = np.zeros(n,)
	for i in xrange(n):
		x = samples[200 + i]
		a_score[i] = h(discrete_p_value(x, { j : float(f[j])/sum( f.values() ) for j in xrange(l)} ))
		f[x] += 1
	return X, a_score


## run experiment functions
def discrete_experiment(start_min, dpm, minutes, p, h0, outpath = False): 
	""" Input: 	start_min = positive number. Number of minutes of data seen/used to set the inital probability model (f) and decreasing function (h), 
					and for calculating the data rate, we use the last start_min*dpm points. 
				dpm = positive int, gives number of data points per minute on average. 
				minutes = positive int. Gives the number of minutes the simulation should run 
				p  (see create_discrete_data for details) these are lists that give multinomial probabilities
				h0 = function, input/output both floats in [0,1]. must be decreasing. h(p-value) = a_score when creating the data 
				outpath = False or string to where the json file with the experiment input/output should be written. 

		Output: X = np.array of shape (N,). each entry is a number 0,1,2,... whatever was sampled from the multinomial
				a_score = np.array of shape (N,). each entry is the anomaly score (float in [0,1])
				times = list ## list of the times that data arrives in order 
				observations = {x : weighted running average a_score(x) for x in sample space.} ## input for create_f_h(). keys are the observed data points, and values are the weighted average of the a_scores for that key. 
				experiment_times = list,  just to store how long the building of the probability model takes, for our info. 	
				rates = list ## the rate of the data estimated after each point. 
				fs = { i : { x : p(x) for x in sample space} for i in xrange(n,N) } at each index i, it has 
				hs = {} ## we'll also store intermediate hs (the decreasing functions) 
				results = dict of form {minute index: {info }}
			
		Description: Generates simulated streaming multinomial data w/ a_scores probabilistically (timestamps are sampled, data is sampled, and a_scores applied)
					 (see create_discrete_data)
					 After n points are observed, it fits the f,h, and estimates the rate the data is streaming.
					 f,h, and r are reestimated after all subsequent data points, and stored.  
					 Finally, this function returns all the simulated and estimated data for analysis
	""" 

	## ---- generate the times ## 
	delta = 60./dpm ## sec. per 1 data point. 
	times = [0.]
	t = 0.
	while t < (60. * (start_min + minutes)) :
		t += 2*delta*np.random.beta(10,10) ## times 2 b/c need to center at 1 not at .5, sample 100 extra b/c we need it to go at least the required time. 
	 	## notice: beta(1, 1) is uniform distribution. 
	 	times.append(t)

	## --- initialize data ---- ##
	N = len(times) ## N = total number of data points. 
	X, a_score = create_discrete_data(N, p, h0) ## create the data

	## ---- estimated values ---- ##
	n = start_min*dpm ## number of data points 
	rates = { i : n * 60. /(times[i] - times[i-n])  for i in xrange(n,N)} ## number of data points per minute. 

	## we need to keep track of what data the system actually sees and have a single a_score for each point for input.
	observations = {} ## input for create_f_h(). keys are the observed data points, and values are the weighted average of the a_scores for that key. 
	experiment_times =[] ## just to store how long the building of the probability model takes. 
	fs = {} ## we'll also store intermediate fs (the probability distributions) 
	hs = {} ## we'll also store intermediate hs (the decreasing functions) 

	## populate observations, create the f's and h's:
	for i in xrange(N): 

		x, a = (X[i] , a_score[i])  	

		if x in observations.keys(): ## add to observatons: 
			observations[x] = .5 * observations[x] + .5 * a
		else: 
			observations[x] = a

		if i >= n :
			tstart = time.time()
			f, h_ = create_f_h(observations, n = i)
			tend = time.time()
			experiment_times.append(tend-tstart)
			fs[i] = f
			hs[i] = h_

	## now populate the results: 
	results = {}  ## to be populated, of form {minute: {threshold: ... }}
	t0 = 60*start_min
	intervals = [ ( t0 + (i * 60) , t0 + (i + 1)*60 ) for i in xrange(minutes)]
	for m in xrange(minutes): 
		s,t  = intervals[m]
		indices = [i for i in xrange(n,N) if times[i] >= s and times[i] <= t ]
		i0 = indices[0] 

		## load the estimated models at the begining of the minute: 
		f = fs[i0]
		h_ = hs[i0] 
		h = linearly_extend(h_)
		l = sorted(h_.values())
		epsilon = min( [ l[i] - l[i-1] for i in xrange( 1, len(l)-1) ]  )/2. 
		## For epsilon def: only go to len(l)-1 b/c h always has point h(0) = 1 (which never happens b/c no points have p-value 0), and often has a point w/ h(x) = .99. including both screws up the epsilon calculation!
		r = rates[i0]

		## what p-values, thresholds, results? 
		p_values = sorted(set([discrete_p_value(x,f) for x in f.keys()]))
		thresholds = map(h, p_values)
		results[m] = {h(pv) : { "p_value" : pv, "exp_alerts_per_min": r*pv, "actual_alerts_in_min": len([a_score[i] for i in indices if a_score[i] >= (h(pv) - epsilon) ]) }  for pv in p_values}

	xx = list(np.arange(0,1.05, .05))
	plt.plot(xx, map(h, xx), label = "Estimated h")
	plt.plot(xx, map(h0, xx), label = "Gold standard h")
	plt.legend()
	plt.show(block = False )

	if outpath: 
		if not os.path.isdir(outpath): 
			os.mkdir(outpath)
		picklify(h0, os.path.join(outpath, "input-h.pickle"))
		jsonify({"n":  n, "dpm": dpm, "minutes": minutes, "p": p}, os.path.join(outpath, "inputs.json")) 
		jsonify(X.tolist(), os.path.join(outpath,"X.json") )
		jsonify(a_score.tolist(), os.path.join(outpath,"a_score.json"))
		jsonify(times , os.path.join(outpath,"times.json")) 
		jsonify(experiment_times, os.path.join(outpath, "experiment_times.json"))
		jsonify(rates, os.path.join(outpath,"rates.json")) 
		jsonify(fs, os.path.join(outpath,"fs.json"))
		jsonify(hs, os.path.join(outpath, "hs.json"))
		jsonify(results, os.path.join(outpath, "results.json"))  

	return X, a_score, times, observations, experiment_times, rates, fs, hs, results


## ---- paths ---- ##
datapath = os.path.join(os.path.abspath(os.path.join(os.getcwd(), "..")), "data-discrete-experiments") 
if not os.path.isdir(datapath): 
	os.mkdir(datapath)


## ---- user inputs & experiment ---- ##

def h_a(x): 
	return 1-x**2

def h_b(x): 
	return 1-x**.5

hes= [h_a, h_b]

pes = [  [.6, .3, .1], [.5, .25, .15, .1],  [ 10./25,  8./25, 4./25, 2./25 , 1./25], [  60./140 ,40./140, 20./140, 10./140, 6./140, 3./140, 1./140] ]

p = np.array([5., 10, 15, 20, 25, 30, 35, 40, 45, 50])
p = list(p/float(sum(p)))


dpm = 60 ## no. data points per min. 
start_min = 5 ## time seen before starting algorithm (training data)
minutes = 10 ## how many minutes to run the experiment?
for i in xrange(len(pes)): 
	p = pes[i]
	for j in xrange(len(hes)):
		print "Starting experiment i = %s, j = %s " %(i,j)
		h0 = hes[j]
		outpath = os.path.join(datapath, "experiment_p%d_h%d" %(i,j))
		X, a_score, times, observations, experiment_times, rates, fs, hs, results = discrete_experiment(start_min, dpm, minutes, p, h0, outpath = outpath)

# ## see how the p-values change over time: 
# for i in xrange(len(p)): ## i = 0 (min p-value) to len(p1), max p-value. 
#     for m in results.keys():
#         print "i = %d, p-value = %s" %(i, sorted(results[m].keys())[i])


# for i in xrange(len(p1)): 
# 	exp_ave = np.average([ results[m][ sorted(results[m].keys())[i] ]["exp_alerts_per_min"] for m in results.keys()])
# 	act_ave = np.average([ results[m][ sorted(results[m].keys())[i] ]["actual_alerts_in_min"] for m in results.keys()])
# 	print "i = %d, exp_ave = %s, act_ave = %s" %(i,exp_ave, act_ave)
