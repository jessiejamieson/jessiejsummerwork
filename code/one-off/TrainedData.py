# temp.py


"""
This file will generate data upon which to use MCD methods to detect alerts over a period of a few days.

We decided not to use this because it seemed to only indicate that MCD methods were better than non-MCD methods (duh)
"""

import numpy as np
import sklearn as sk
import scipy, os
from scipy.stats import chi2
import numpy.linalg
#import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm
from sklearn import mixture
from sklearn.covariance import MinCovDet as MCD
import csv
from numpy import random
import json, pickle, codecs, copy


## ---- functions ---- ##

def jsonify(obj, out_file):
    """
    Inputs:
    - obj: the object to be jsonified
    - out_file: the file path where obj will be saved
    This function saves obj to the path out_file as a json file.
    """
    json.dump(obj, codecs.open(out_file, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)


def experiment(e, n, days, dps,  outpath = False, verbose = True):
	"""
	Input: 	e - positive int, gives expected no. of alerts per day
			n - initial number of points seen to fit the first gaussian
			dps - data points per second
			days - postive float, number of days to test it.
			outpath - False, or a string. If string, gives path to where to save results.
			verbose - True, gives print statements.
	Output: X - list of sampled data points
			m_dist - dict of form {data index: mahalanobis dist. }
			results - dict of form {day: alerts observed }

	"""
	## 1 data point per second.
	## run for 5 * 8 hr days.
	dpd = 8*60*60*dps  ## number of data points per day
	N = int(days*dpd + n)  ## total number of points
	print N

	mu = [np.array([0,0]), np.array([2,2])]
	Sigma = [np.eye(2), np.ones(2) + np.eye(2)]

	X = []
	for i in xrange(N):
		# j = np.where(random.multinomial(1, [.9,.1], size = 1) == 1) [0][0]
		j = np.where(random.multinomial(1, [1, 0], size = 1) == 1) [0][0]
		X.append( random.multivariate_normal(mu[j], Sigma[j], 1 ) )

	m_dist = {}
	for i in xrange(n, N-1):
		#if verbose and (i % 1000 == 0):
			#print "beginning fitting MCD %d of %d" %(i-n, N-1-n)
		train = np.array(X[i-n:i]).reshape(n, 2)
		Q = np.linalg.inv(Sigma[0])
		x = X[i+1]
		m_dist[i+1] = (np.dot(np.dot(Q,(x-mu[0]).T).T, (x-mu[0]).T)**.5)[0][0]
		# model = MCD( support_fraction = .9 ).fit(train )
		# m_dist[i+1] = (model.mahalanobis(X[i+1])**.5) ## model.mahalnobis needs to sqrooted to be the actual mahalanobist dist.

	k = X[0].shape[1] ## what dimension are we working in
	phi = lambda x: chi2.isf(x, k) ## (1 - cdf)^{-1}
	## p(m_dist(x) \geq alpha ) = 1-G(alpha*2) = phi^{-1}(alpha*2)
	## e alerts = dpd * p(m_dist(x) >= alpha ) = dpd * phi^{-1}(alpha*2)
	alpha = phi(float(e)/dpd)**.5
	# data.append(int(sum(M >= alpha)))

	num_alerts = {} ## {day : count }
	for i in xrange(days): ## 5 is number of 8 hour days.
		indices = range(n + (i * dpd )+1, n + ((i+1)* dpd))
		num_alerts[i] = sum(np.array([m_dist[j] for j in indices]) >= alpha )

	if outpath:
		outpath=str(outpath)
		if not os.path.isdir(outpath):
			os.mkdir(outpath)
		XX = [a.tolist() for a in X]
		jsonify(XX,  os.path.join(outpath, "X.json"))
		jsonify(m_dist, os.path.join(outpath, "m_dist.json"))
		jsonify(num_alerts, os.path.join(outpath, "num_alerts.json"))
		if verbose:
			print "Saved results to %s" %outpath
	print "day=%d, # points= %d, expected=%d, dps= %d" %(days, N, e, dps)
	for k,v in num_alerts.items():
		print (k)
		print (v)
	return X, m_dist, num_alerts


## ---- paths ---- ##
datapath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data-synthetic-experiment"))
if not os.path.isdir(datapath):
	os.mkdir(datapath)


## ---- user inputs ---- ##
e = 40  ## expected alerts per day
n = 100 ## initial number of points seen to fit the first gaussian
days=1
dps=1
outpath = os.path.join(datapath, "experiment-1")

## ----- function calls ---- ##

X1, m_dist1, num_alerts1 = experiment(e, n, days, dps, outpath= False, verbose= True)

