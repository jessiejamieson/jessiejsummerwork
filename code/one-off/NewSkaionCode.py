"""
Let's import the Skaion data from the folder. This will require navigating to the folder containing the data and then running the following code.
"""

import ast
from datetime import datetime
#import matplotlib.pyplot as plt
import collections


def Process_Skaion():
	"""
	The Skaion data set is almost 900,000 lines long. To process it we can either import all of the data at once or import it 
	a few lines at a time. The point is that once it's imported, we won't have to 
	import it again.
	"""

	all_skaion_data=[]

	replace_x_with_y = (" ", ""), ("false", "False"), ("true", "True"), ("\n'", "") # What do you want to replace?

	with open("skaion_5s20_situ_output.txt") as f:
		for line_number, line in enumerate(f, 1):
			all_skaion_data.append(ast.literal_eval(reduce(lambda a, kv: a.replace(*kv), replace_x_with_y, line)))

	"""
	Now let's pull out and use the times for the skaion data. To do this, we 
	need to convert the times to a format that's usable.
	"""

	for i in xrange(len(all_skaion_data)):
	#for key in sk_data.keys():
		temptime=all_skaion_data[i]['stime']
		temptimetrunk=temptime.replace("Z", "")
		temptimetrunk=datetime.strptime(temptimetrunk, '%Y-%m-%dT%H:%M:%S.%f')
		all_skaion_data[i]['stime']=temptimetrunk

	scores_times_ips_only=[]
	for i in xrange(len(all_skaion_data)):
		tempdict={}
		tempdict['score'] = all_skaion_data[i]['score']
		tempdict['scores']= all_skaion_data[i]['scores']
		tempdict['stime'] = all_skaion_data[i]['stime']
		tempdict['dstIP'] = all_skaion_data[i]['dstIP']
		tempdict['srcIP'] = all_skaion_data[i]['srcIP']
		scores_times_ips_only.append(tempdict)

	scores_times_ips_only= sorted(scores_times_ips_only, key=lambda k: k['stime'])
	
	return scores_times_ips_only
#########################

def ScorePuller(score_to_use):

	"""
	Input: score_to_use = which score we want to pull data for to check out
	Output: list_of_chosen_times = a list of the scores' times (in order)
			list_of_chosen_scores = a list of all the scores you wanted
									in chronological order
			list_of_chosen_times = a list of the scores' times (in order)
			list_of_chosen_srcIPs = the associated list of srcIPs
			list_of_chosen_destIPs = the associated list of dstIPs
	"""
	# score_to_use='Pcr'
	# Options: 'Pcr', 'Pcr_T', 'PrivPorts', 'PrivPorts_T',
	# 'BytesPerPacket', 'BytesPerPacket_T'

	list_of_chosen_scores=[]
	list_of_chosen_times=[]
	list_of_chosen_srcIPs=[]
	list_of_chosen_destIPs=[]

	for entry in scores_times_ips_only:
		tempvalues=[]
		for subentry in entry['scores']:
			tempvalues=tempvalues+dict.values()
			tempstrings=[el for el in tempvalues if isinstance(el, collections.Iterable)]
		if any(score_to_use in s for s in tempstrings):
			list_of_chosen_scores.append(entry['scores'][entry['scores'].index(subentry)]['score'])
			list_of_chosen_times.append(entry['stime'])
			list_of_chosen_srcIPs.append(entry['srcIP'])
			list_of_chosen_destIPs.append(entry['dstIP'])

	return list_of_chosen_times, list_of_chosen_scores, list_of_chosen_srcIPs, list_of_chosen_destIPs

###########################

def bin_by_time(bin_size, list_of_chosen_times, list_of_chosen_scores, list_of_chosen_srcIPs, list_of_chosen_destIPs):
	
	"""
	Input: bin_size: the gap that will serve as your bin size
			four vectors output by ScorePuller
	Output:
	"""

	#start_time=list_of_chosen_times[0]

	intervals=[(i, i+bin_size) for i in list_of_chosen_times]

	bins=[]
	bins.append([x] for x in list_of_chosen_times if i<=x<i+binsize for i in list_of_chosen_times)
















