"""
    File    = experiment_0.py
    Author  = Jessie Jamieson
    Email   = jdjamieson@huskers.unl.edu
    Created = 2016-06-22

    Last Modified = 2016-06-26

    Description:
        This file is part of Jessie Jamieson's summer work at ORNL under the
        direction of Dr. Robert Bridges (Go Team Bridges!)

       This code generates data from a multivariate normal distribution, finds
       the mahalanobis distances of these points, and calculates the number of
       anomalies using untrained methods.

"""

import numpy as np
import os
import sklearn as sk
import scipy
import scipy.stats as st
import numpy.linalg
#import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm
from sklearn import mixture
from sklearn.covariance import MinCovDet
import csv
from numpy import random
from scipy.stats import chi2
import json, pickle, codecs, copy


##-----functions----##

def jsonify(obj, out_file):
    """
    Inputs:
    - obj: the object to be jsonified
    - out_file: the file path where obj will be saved
    This function saves obj to the path out_file as a json file.
    """
    json.dump(obj, codecs.open(out_file, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)


def experiment_0(e, n, days, DPS, outpath = False, verbose = True):
    """
    Input: 	e - positive int, gives expected no. of alerts per day
			n - initial number of points seen to fit the gaussian
			days - postive float, number of days to test it.
			outpath - False, or a string. If string, gives path to where to save results.
			verbose - True, gives print statements.

    Barring path issues, these will be the outputs:
	Output: X - list of sampled data points
			m_dist - dict of form {data index: mahalanobis dist. }
			results - dict of form {day: alerts observed }

    """

    ######### 1D stuff, standard normal########################################

    #expected=(1000 data points)*p(pvalue(x)<=alpha)=1000*alpha

    #DataPoints=1000
    #Expected=100
    #alpha=float(Expected)/DataPoints #expected=datapoints*p(p-value(x)<=alpha)=datapoints*alpha
    #Points=np.random.normal(0,1,1000) #Set up the standard normal distribution

    #def pvalue(point):
    #    return 2*st.norm.cdf(-abs(point))

    #pvs=map(pvalue,Points)
    #pvpv=map(pvalue,pvs)
    #print pvpv

    #Actual= sum(i <= alpha for i in pvs)
    #alpha_obs =float(Actual)/float(DataPoints)
    #Ratio= float(Actual)/Expected

    #print "alpha_exp= %s, and we observed %d true outliers, for alpha_obs=%s ." %(alpha, Actual, alpha_obs)
    #print "Observed/Expected= %s" %Ratio

    ##########################################################################

    ###------2D Normal Dist. -----####

    ## 1 data point per second.
    ## run for 5 * 8 hr days.
    #e = 40  ## expected alerts per day
    #n = 100 ## initial number of points seen to fit the first gaussian
    #days = 4
    dpd = 8*60*60*DPS  ## number of data points per day
    N = int(days*dpd + n)  ## total number of points

    mu = [np.array([0,0]), np.array([2,2])]
    Sigma = [np.eye(2), np.ones(2) + np.eye(2)] #define the parameters for the multivariate

    X = []
    for i in xrange(N):
        # j = np.where(random.multinomial(1, [.9,.1], size = 1) == 1) [0][0]
        j = np.where(random.multinomial(1, [1, 0], size = 1) == 1) [0][0]
        X.append( random.multivariate_normal(mu[j], Sigma[j], 1 ) )

    def mdist(point):
        return np.linalg.norm(point) #Use the norm assuming a standard normal dist.
        #Otherwise, use the appropriate Mahalanobis distance

    m_dist = map(mdist,X)

    k = X[0].shape[1] ## what dimension are we working in
    phi = lambda x: chi2.isf(x, k) ## (1 - cdf)^{-1}
    ## p(m_dist(x) \geq alpha ) = 1-G(alpha*2) = phi^{-1}(alpha*2)
    ## e alerts = dpd * p(m_dist(x) >= alpha ) = dpd * phi^{-1}(alpha*2)
    alpha = phi(float(e)/dpd)**.5
    # data.append(int(sum(M >= alpha)))

    num_alerts= sum(i >= alpha for i in m_dist)
    #Observedratio=float(num_alerts)/N #quick ratio calclulations
    #Ratio= float(num_alerts)/(N*alpha)

    if verbose:
        print "We observed %d true outliers out of %d points and predicted %d (dps= %d, days=%d)." %(num_alerts, N, days*e, DPS, days)
        print "|Observed-Expected|= %s, Observed/Expected= %s" %(abs(float(num_alerts)-days*e),Ratio)

    if outpath:
        outpath=str(outpath)
        if not os.path.isdir(outpath):
            os.mkdir(outpath)
        XX = [a.tolist() for a in X]
        jsonify(XX,  os.path.join(outpath, "X.json"))
        jsonify(m_dist, os.path.join(outpath, "m_dist.json"))
        jsonify(num_alerts, os.path.join(outpath, "num_alerts.json"))
        if verbose:
            print "Saved results to %s" %outpath

    return X, m_dist, num_alerts

## ---- paths ---- ##
datapath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data-untrained-experiment"))
if not os.path.isdir(datapath):
	os.mkdir(datapath)

## ---- user inputs ---- ##
e = 40  ## expected alerts per day
n = 100 ## initial number of points seen to fit the first gaussian
#days = 4
dps=1
outpath = os.path.join(datapath, "experiment_0")

## ----- function calls ---- ##

#Here one can call the functions in a loop to generate lots of data.

i=0
while i<=9:
    Days=[1, 2, 3, 4, 5, 6, 7]
    for d in Days:
        X, m_dist, num_alerts = experiment_0(e, n, d, dps, outpath= True, verbose= True)
    i=i+1
