#		Import the skaion data from the folder. Navagate to the folder first to make this easier.

import ast
from datetime import datetime
import matplotlib.pyplot as plt

sk_data=[]
with open("skaion_5s20_situ_output.txt") as f:
	for line_number, line in enumerate(f, 1):
		temp1=line.replace(" ", "")
		temp2=temp1.replace("false", "False")
		temp3=temp2.replace("true", "True")
		sk_data.append(ast.literal_eval(temp3.replace("\n'", "")))

#		Convert date to something readable. All dates begin with "2017-01-18T", but we only 
# 		really care about the seconds if we're doing data flow per second.
#		Skaion data has three different times in each "flow"- stime, which is the time the
#		flow was started, flowLtime- the time it ended, and creationtime, when situ scored the 
#		flow. For this, I used the creation time since that would be what we would receive 
# 		from the anomaly detector/scoring system.

for i in xrange(len(sk_data)):
#for key in sk_data.keys():
	temptime=sk_data[i]['stime']
	temptimetrunk=temptime.replace("Z", "")
	temptimetrunk=datetime.strptime(temptimetrunk, '%Y-%m-%dT%H:%M:%S.%f')
	sk_data[i]['stime']=temptimetrunk

for i in xrange(len(sk_data)):
#for key in sk_data.keys():
	temptime=sk_data[i]['stime']
	temptimetrunk=temptime.replace("Z", "")
	sk_data[i]['stime']=temptimetrunk

for i in xrange(len(sk_data)):
#for key in sk_data.keys():
	temptime=sk_data[i]['stime']
	sk_data[i]['stime']=datetime.strptime(temptime, '%H:%M:%S.%f')

#		These pieces pull out only the relevant information to what we're doing immediately, 
#		namely the "score", "creation", and "scores" values/dictionaries. Scores and times
#		are held in the "scores_times" list.

################################################
# ignore everything until line 153-ish
################################################
 All_Times=[]
# score=[]
# PCR=[]
# PCR_T=[]
# PCR_Times=[]
PPorts=[]
# PPorts_T=[]
PPorts_Times=[]
# BPP=[]
# BPP_T=[]
# BPP_Times=[]

for l in scores_times:
	#temptime=scores_times[key]['time_object']
	#All_Times.append(temptime)
# 	score.append(scores_times[key]['score'])

	#if key % 10000 ==0:

	#	print key
	tempvalues=[]
	for dict in l['scores']:
		tempvalues+=dict.values()
		tempstrings=[el for el in tempvalues if isinstance(el, collections.Iterable)]

# 	if any("Pcr" in s for s in tempstrings):
# 		PCR.append(scores_times[key]['scores'][scores_times[key]['scores'].index(dict)]['score'])
# 		PCR_T.append(scores_times[key]['scores'][scores_times[key]['scores'].index(dict)]['score'])
# 		PCR_Times.append(temptime)
	if any("PrivPorts" in s for s in tempstrings):
		PPorts.append(l['scores'][l['scores'].index(dict)]['score'])
		#PPorts_T.append(l['scores'][l['scores'].index(dict)]['score'])
		PPorts_Times.append(temptime)
# 	if any("BytesPerPacket" in s for s in tempstrings):
# 		BPP.append(scores_times[key]['scores'][scores_times[key]['scores'].index(dict)]['score'])
# 		BPP_T.append(scores_times[key]['scores'][scores_times[key]['scores'].index(dict)]['score'])
# 		BPP_Times.append(temptime)


# rank_dict={}
# sortedscores=sorted(score)
# rank_dict={score.index(i):1-float(sortedscores.index(i)/len(score)) for i in score}

# rank_dict={}
# for i in xrange(100000):
# 	if i % 100 ==0:
# 		print i
# 	rank_dict[i]=[score[i],1-float(sortedscores.index(score[i]))/897998]

# exes=[]
# whys=[]

# for key in rank_dict.keys():
# 	exes.append(rank_dict[key][0])
# 	whys.append(rank_dict[key][1])

# number_of_alerts=[]
# pvals=[]
# tempalert=[]
# for i in xrange(10000):
# 	if i % 100 ==0:
# 		print i
# 	tempalert=[j for j in exes if whys[exes.index(j)]<=whys[i]]
# 	pvals.append(whys[i])
# 	number_of_alerts.append(len(tempalert))

#plt.hist will give me the counts per bin and the ends of each bin

#now let us create ordered pairs, (x,y) where x=pports score, y=pvalues

# SortedPPorts=sorted(PPorts)
# from itertools import islice

# PPortsPairs=[]
# for j in xrange(0,len(PPorts_bincounts)):
# 	print j
# 	if j==0:
# 		for i in xrange(int(PPorts_bincounts[j])+1):
# 			PPortsPairs.append([SortedPPorts[i],1-float(PPBinWeights[0])/sum(PPBinWeights), i, j])
# 		print len(PPortsPairs)
# 	elif (j>0 and j<len(PPorts_bincounts)-1):
# 		for i in xrange(int(sum(islice(PPorts_bincounts,j)))+1,int(sum(islice(PPorts_bincounts,j+1)))+1):
# 			PPortsPairs.append([SortedPPorts[i],1-float(sum(islice(PPBinWeights,j+1)))/sum(PPBinWeights), i, j])
# 		print "middle loop"
# 		print len(PPortsPairs)
# 	else: #j==len(PPorts_bincounts)-1:
# 		for i in xrange(int(sum(islice(PPorts_bincounts,j)))+1,len(SortedPPorts)):
# 			PPortsPairs.append([SortedPPorts[i],1-float(sum(PPBinWeights))/sum(PPBinWeights), i, j])
# 		print len(PPortsPairs)

# plist1=np.linspace(0,1,1000)
# plist2=np.linspace(0,0.1,100)

# expectedPPs=plist2*float(len(PPorts))

# actualPPs_2=[]

# justpvalsPP=[]

# for i in xrange(len(PPortsPairs)):
# 	if i % 10000==0:
# 		print i
# 	justpvalsPP.append(PPortsPairs[i][1])

# for i in xrange(len(plist2)):
# 	if i % 10==0:
# 		print i
# 	actualpps_2.append(sum(j<=plist2[i] for j in justpvalsPP))
############################################################################

"""
Turns out, the data in the file I was given is not sorted chronologically. We need to do
this first, then start scoring. First, break up the data in second-long chunks. Every second's
worth of data becomes a chunk. Then we'll run detection on that.
"""

interval=300 #seconds

scores_times_sorted= sorted(scores_times, key=lambda k: k['stime'])

bin_temp=[]
persecond_bins=[]
initial_time=list_for_ideal_ip[0]['stime']

current=0
max_current=len(list_for_ideal_ip)

while current<max_current-1:
	bin_temp=[]
	bin_temp.append(list_for_ideal_ip[current])
	time_1=list_for_ideal_ip[current]['stime']
	time_2=list_for_ideal_ip[current+1]['stime']
	delta_time=time_2-time_1
	#print delta_time
	while delta_time.seconds<interval and delta_time.days==0:
		bin_temp.append(list_for_ideal_ip[current+1])
		current=current+1
		if current==max_current-1:
			break
		time_2=list_for_ideal_ip[current+1]['stime']
		delta_time=time_2-time_1
		#print delta_time
	current=current+1
	persecond_bins.append(bin_temp)
#################
bin_temp=[]
persecond_bins=[]
initial_time=ip_pull_list[1]['stime']
interval=60
current=0
max_current=len(ip_pull_list)

while current<max_current-1:
	bin_temp=[]
	bin_temp.append(ip_pull_list[current])
	time_1=ip_pull_list[current]['stime']
	time_2=ip_pull_list[current+1]['stime']
	delta_time=time_2-time_1
	#print delta_time
	while delta_time.seconds<interval and delta_time.days==0:
		bin_temp.append(ip_pull_list[current+1])
		current=current+1
		if current==max_current-1:
			break
		time_2=ip_pull_list[current+1]['stime']
		delta_time=time_2-time_1
		#print delta_time
	current=current+1
	persecond_bins.append(bin_temp)

"""
Assume now that we can inspect at most M=100 points each second. Then M/N will be 
100/N, where N is the number of points per second that we see in our detector.

Then we want to know whose p-values are less than this ratio. The p-vals given were actually
the -log_{10} of the p-values. To undo this, take each score to the 10^{-score}. Then check 
whether this value is less than the ratio M/N. This is using h(x)=1-x.
"""

secondbinsizes=[len(b) for b in persecond_bins]

inspect_per_second=[1] #[50, 75, 100, 125, 150, 175, 200]

alerts_per_second=[]

rawpvals=[]

thresholds=[]

alphas_to_check=list(np.linspace(0,0.1, 1000))

#for i in xrange(len(scores_times_sorted)):
#	rawpvals.append(scores_times_sorted[i]['score'])
name ='Pcr' ## what model? 
alerts_for_alphas=[]
for p in xrange(len(inspect_per_second)): ## p is the user input  = max number alerts per second 
#for p in xrange(len(alphas_to_check)):
	tempthresholds=[]
	alerts_per_second=[]
	for i in xrange(len(secondbinsizes)):
		
		ratepersecond=secondbinsizes[i]/float(interval)
		ratio=inspect_per_second[p]/float(ratepersecond)
		#ratio=alphas_to_check[p]
		# print "interval = %s " %interval 
		# print "secondbinsizes[%s]= %s" %( i, secondbinsizes[i] )
		# print "inspect_per_second[p] = %s" %inspect_per_second[p]
		# print "ratepersecond = %s"  %ratepersecond
		# print "ratio = %s" %ratio 
		tempthresholds.append(ratio)

		#alerts_per_threshold=[sum()]

		end_index = secondbinsizes[i]
		if i==0:
			start_index = 0
		else: ## i>0
			start_index = secondbinsizes[ i-1]

		for j in xrange(start_index, end_index):
			l = [d for d in list_for_ideal_ip[j]['scores'] if d['name'] == name ] ## list of dicts 
			if l: ## l has length 1 or 0
				s = l[0]['score']
				#print s
				#PrivPortsScores[l[0]]['stime']=l[0]['score']
			else: 
			#	s = 1.
				secondbinsizes[i]=secondbinsizes[i]-1
			# index=0
			# while key not in scores_times_sorted[j]['scores'][index].values():
			# 	index=index+1
			# 	if index>=len(scores_times_sorted[j]['scores'])-1:
					# rawpvals.append(1)
			# rawpvals.append(10**(-scores_times_sorted[j]['scores'][index].values()[0]))
			rawpvals.append( float(10)** ( -s ) )
			print rawpvals ## list of pvalues (floats)
		ratepersecond=secondbinsizes[i]/float(interval)
		ratio=inspect_per_second[p]/float(ratepersecond)
		print ratio
		alerts_per_second.append(sum( pv <= ratio for pv in rawpvals))
		rawpvals=[]

		# else:
		# 	ratepersecond=secondbinsizes[i]
		# 	ratio=inspect_per_second[p]/float(ratepersecond)
		# 	tempthresholds.append(ratio)
		# 	for j in xrange(secondbinsizes[i-1],sum(secondbinsizes[i-1:i+1])):
		# 		key='PrivPorts'
		# 		index=0
		# 		while key not in scores_times_sorted[j]['scores'][index].values():
		# 			index=index+1
		# 			if index>=len(scores_times_sorted[j]['scores']-1):
		# 				rawpvals.append(1)
		# 		rawpvals.append(10**(-scores_times_sorted[j]['scores'][index].values()[0]))
		# 	alerts_per_second.append(sum(k <= ratio for k in rawpvals))
	thresholds.append(tempthresholds)
	alerts_for_alphas.append(alerts_per_second)

#thresholds=[]
# for i in xrange(len(secondbinsizes)):
# 	rawpvals=[]
# 	indices=[]
# 	trimdices=[]
# 	if i==0:
# 		for j in xrange(secondbinsizes[0]):
# 			rawpvals.append(10**(-scores_times_sorted[j]['score']))
# 		indices=zip(*heapq.nsmallest(100, enumerate(rawpvals), key=operator.itemgetter(1)))[0]
# 		for k in xrange(len(indices)):
# 			trimdices.append(rawpvals[indices[k]])
# 		thresholds.append(min(trimdices))
# 	else:
# 		for j in xrange(secondbinsizes[i-1],sum(secondbinsizes[i-1:i+1])):
# 			rawpvals.append(10**(-scores_times_sorted[j]['score']))
# 		indices=zip(*heapq.nsmallest(100, enumerate(rawpvals), key=operator.itemgetter(1)))[0]
# 		for k in xrange(len(indices)):
# 			trimdices.append(rawpvals[indices[k]])
# 		thresholds.append(min(trimdices))

###########################################

av_p_s=[]
for p in xrange(len(inspect_per_second)):
	av_p_s.append(sum(alerts_for_alphas[p])/float(len(alerts_for_alphas[p])))

x_index=[]
for i in xrange(len(secondbinsizes)):
	x_index.append(i)

ylist=[]
for p in av_p_s:
	ylist.append([p]*len(alerts_per_second))

"""
The below first plot will plot the seconds after the scoring began vs. the number of alerts in 
that second given a few different ratios.

The below second plotting will plot (a) the seconds after the scoring began vs. the number of alerts
in that second given the ratio above. It also includes a line that is the average of all # of 
alerts over the entire scoring period.

The third plot will show how the threshold changes in each case.
"""

# do rate and alpha on the same ones, y axis, alert rate threshold
plt.plot(x_index, alerts_per_second)
plt.ylabel("Bin Sizes")
plt.xlabel("Seconds After Start")
plt.title("Per Second Bin Sizes, Skaion Data Set")
plt.show(block=False)

#########
def plot_me():
	dashes=[5,5,5,5]
	fig, ax = plt.subplots()

	ax.grid(True)

	ticklines = ax.get_xticklines() + ax.get_yticklines()
	gridlines = ax.get_xgridlines() + ax.get_ygridlines()
	#ticklabels = ax.get_xticklabels() + ax.get_yticklabels()

	for line in ticklines:
	    line.set_linewidth(3)

	for line in gridlines:
	    line.set_linestyle('-.')

	#line1, = ax.plot(x_index, alerts_for_alphas[0], 'b', label='0.5')
	#line2, = ax.plot(x_index, alerts_for_alphas[1], 'g', label='1')
	line3, = ax.plot(x_index, alerts_for_alphas[0], 'ro', label="1")
	line4, = ax.plot(x_index, [sum(alerts_for_alphas[0])/float(len(alerts_for_alphas[0]))]*len(x_index))
	#line4, = ax.plot(x_index, alerts_for_alphas[3], 'k', label="10")
	#line5, = ax.plot(x_index, alerts_for_alphas[4], 'c', label='20')
	#line6, = ax.plot(x_index, alerts_for_alphas[5], 'm', label="100")
	#line7, = ax.plot(x_index, alerts_for_alphas[6], 'y', label="200")
	line4.set_dashes(dashes)

	ax.legend(title="Max Alerts Per 5 Minutes", loc='upper right', ncol=2)
	plt.ylabel("Actual Alerts Per 5 Minutes")
	plt.xlabel("Time After Start (x5 Minutes)")
	plt.title("Alerts Per 5 Minutes, Skaion Data Set")
	#plt.axis([0, 34, 0, 135])
	plt.show(block=False)
#############################################################

fig, ax = plt.subplots()

ax.grid(True)

ticklines = ax.get_xticklines() + ax.get_yticklines()
gridlines = ax.get_xgridlines() + ax.get_ygridlines()
#ticklabels = ax.get_xticklabels() + ax.get_yticklabels()

for line in ticklines:
    line.set_linewidth(3)

for line in gridlines:
    line.set_linestyle('-.')

line1, = ax.plot(x_index, thresholds[0], 'b', label='50')
line2, = ax.plot(x_index, thresholds[1], 'g', label='75')
line3, = ax.plot(x_index, thresholds[2], 'r', label="100")
line4, = ax.plot(x_index, thresholds[3], 'k', label="125")
line5, = ax.plot(x_index, thresholds[4], 'c', label='150')
line6, = ax.plot(x_index, thresholds[5], 'm', label="175")
line7, = ax.plot(x_index, thresholds[6], 'y', label="200")

ax.legend(title="Max Alerts Per Day", loc='upper left', ncol=2)
plt.ylabel("Actual Alerts Per Second")
plt.xlabel("Seconds After Start")
plt.title("Alerts Per Second, Skaion Data Set")
plt.axis([0, 34, 0, .015])
plt.draw()

#############################################################

dashes=[5,5,5,5]
fig, ax = plt.subplots()
line1, = ax.plot(x_index, thresholds[0], 'b')
#line1.set_dashes(dashes)

line2, =ax.plot(x_index,thresholds[0],'ro')

#line3, = ax.plot(x_index, alerts_per_second, 'ro', label="Alerts Per Second")
ax.legend(loc='upper left')
plt.ylabel("Alert Rate Threshold")
plt.xlabel("Seconds After Start")
plt.title("Alert Rate Threshold Over Time")
plt.show()

"""
Now what if we didn't care about the flow rate and just passed it p-values willy nilly? Let's
plot that.
"""
import numpy as np

inspect_per_second=np.linspace(1/3, 1, num=100)

alerts_per_second=[]

rawpvals=[]

thresholds=[]

#for i in xrange(len(scores_times_sorted)):
#	rawpvals.append(scores_times_sorted[i]['score'])
alerts_for_alphas=[]
for p in xrange(len(inspect_per_second)):
	tempthresholds=[]
	alerts_per_second=[]
	for i in xrange(len(secondbinsizes)):
		rawpvals=[]
		if i==0:
			ratepersecond=secondbinsizes[0]
			ratio=inspect_per_second[p]/float(ratepersecond)
			tempthresholds.append(ratio)
			for j in xrange(secondbinsizes[0]):
				rawpvals.append(10**(-scores_times_sorted[j]['score']))
			alerts_per_second.append(sum(k <= ratio for k in rawpvals))
		else:
			ratepersecond=secondbinsizes[i]
			ratio=inspect_per_second[p]/float(ratepersecond)
			tempthresholds.append(ratio)
			for j in xrange(secondbinsizes[i-1],sum(secondbinsizes[i-1:i+1])):
				rawpvals.append(10**(-scores_times_sorted[j]['score']))
			alerts_per_second.append(sum(k <= ratio for k in rawpvals))
	thresholds.append(tempthresholds)
	alerts_for_alphas.append(alerts_per_second)

alerts_vary_alpha=[]
for s in xrange(len(secondbinsizes)):
	rawpvals_temp=[]
	alerts_temp=[]
	if s==0:
		ratepersecond=secondbinsizes[0]
		for j in xrange(secondbinsizes[0]):
			rawpvals_temp.append(10**(-scores_times_sorted[j]['score']))
	else:
		ratepersecond=secondbinsizes[s]
		for j in xrange(secondbinsizes[s-1],sum(secondbinsizes[s-1:s+1])):
			rawpvals_temp.append(10**(-scores_times_sorted[j]['score']))
	for a in xrange(len(inspect_per_second)):
		ratio=inspect_per_second[a]/float(ratepersecond)
		alerts_temp.append(sum(k <= ratio for k in rawpvals_temp))
	print s
	alerts_vary_alpha.append(alerts_temp)

from mpl_toolkits.mplot3d import Axes3D 
fig=plt.figure()
ax=fig.add_subplot(111, projection='3d')
for k in alerts_vary_alpha:
	ax.scatter(inspect_per_second, k, )
plt.ylabel("Actual Alerts Per Second")
plt.xlabel("Max Can Inspect")
plt.title("Alerts Per Second, Skaion Data Set")
ax.set_zlim(0, 0.02)
plt.show()















"""
Out of curiosity, the code below does the same thing as above except in half-second chunks 
instead of second sized chunks.
"""

scores_times_sorted= sorted(scores_times, key=lambda k: k['stime'])

bin_temp=[]
per_half_second_bins=[]
initial_time=scores_times_sorted[1]['stime']

current=0
max_current=len(scores_times_sorted)

while current<max_current-1:
	bin_temp=[]
	bin_temp.append(scores_times_sorted[current])
	time_1=scores_times_sorted[current]['stime']
	time_2=scores_times_sorted[current+1]['stime']
	delta_time=time_2-time_1
	#print delta_time
	while delta_time.microseconds<500000:
		bin_temp.append(scores_times_sorted[current+1])
		current=current+1
		if current==max_current-1:
			break
		time_2=scores_times_sorted[current+1]['stime']
		delta_time=time_2-time_1
		#print delta_time
	current=current+1
	per_half_second_bins.append(bin_temp)


half_secondbinsizes=[len(b) for b in per_half_second_bins]

inspect_per_half_second=50

alerts_per_half_second=[]

rawpvals=[]

#for i in xrange(len(scores_times_sorted)):
#	rawpvals.append(scores_times_sorted[i]['score'])

for i in xrange(len(half_secondbinsizes)):
	rawpvals=[]
	if i==0:
		rateperhalfsecond=half_secondbinsizes[0]
		ratio=inspect_per_half_second/float(rateperhalfsecond)
		for j in xrange(half_secondbinsizes[0]):
			rawpvals.append(10**(-scores_times_sorted[j]['score']))
		alerts_per_half_second.append(sum(k <= ratio for k in rawpvals))
	else:
		rateperhalfsecond=half_secondbinsizes[i]
		ratio=inspect_per_half_second/float(rateperhalfsecond)
		for j in xrange(half_secondbinsizes[i-1],sum(half_secondbinsizes[i-1:i+1])):
			rawpvals.append(10**(-scores_times_sorted[j]['score']))
		alerts_per_half_second.append(sum(k <= ratio for k in rawpvals))

x_index_half_everysecond=[]
for i in xrange(len(half_secondbinsizes)):
	if i % 2 == 0:
		x_index_half_everysecond.append(i/float(2))
ys=[50]*len(x_index_half_everysecond)

x_index_half=[]
for i in xrange(len(half_secondbinsizes)):
	x_index_half.append(i)

plt.plot([i/float(2) for i in x_index_half],alerts_per_half_second,'r', x_index_half_everysecond, ys, 'k' )
#plt.plot(x_index, [100]*len(alerts_per_second))
plt.xticks([0, 5, 10, 15, 20, 25, 30, 35])
plt.ylabel("Actual Alerts Per Second")
plt.xlabel("Seconds After Start")
plt.show()



#####
set_of_ip_pvals_pcr=set([k['scores'][0]['score'] for k in ip_pull_list])

counts=[]
for score in alphas_to_check:
	counts.append(sum(pv<=score for pv in [10**(-k['scores'][0]['score']) for k in ip_pull_list]))

plt.hist([10**(-k['scores'][0]['score']) for k in ip_pull_list])
plt.ylabel('Counts for PCR Score, IP ''100.5.111.85''')
plt.xlabel('10**(-PCR score)')
plt.show(block=False)

plt.plot(alphas_to_check,counts, 'bo')
plt.xlabel('Threshold as set by user')
plt.ylabel('Number of Alerts at Threshold Overall, for Particular IP')
plt.show(block=False)
###############################################################

ip_pull_list=[k for k in sk_data if k['srcIP']==ip_pull]

srcIPs=[sk_data[k]['srcIP'] for k in xrange(len(sk_data))]

ip_flows=[]

for ip in set_srcIPs:
	ip_flows_temp=[k for k in sk_data if k['srcIP']==ip]
	ip_flows.append(ip_flows_temp)

