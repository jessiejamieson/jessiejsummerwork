# temp.py

import os, re
import numpy as np
from sklearn.covariance import MinCovDet
from generalFunctions import *
import matplotlib.pyplot as plt

## ---- file paths ---- ##
dataPath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data-graphprints"))

graphlets = {int(x): y for x,y in unjsonify(graphletsPath).iteritems()}


## ---- node-level streaming data (orbits) ---- ## 
orbitListPath = os.path.join(dataPath, "orbit_list.json")

# initialize variables: 
N = 150 # number of times to train on
L = 350 # only consider orbit vectors up to 350
num_graphs = 600
len(three_orbits)


## need a list of the nodes  in a fixed order: 
culprit_node = 6893 # culprint node
bt_ind = np.array([278, 301]) # bit torrent indices
node_paths = [os.path.join(processedPath, x) for x in os.listdir(processedPath) if
                 x.startswith("orbits_") and not x.endswith(str(culprit_node) + ".json")]  # grab orbit_x.json but not culprit
node_paths.append(os.path.join(processedPath, "orbits_6893.json")) ## make the culprit node the last of the list. 
nodes = [regex.findall(p.split("/")[-1])[0] for p in node_paths]

## need a list of the three-orbits in a fixed order
orbit_list = unjsonify(orbitListPath)
orbits = { }
for o in orbit_list: 
	orbits[o] = 4 
	for g in graphlets: 
		if g.startswith(o[:-1]) and g.endswith("3"): 
			orbits[o] = 3
three_orbits = [o for o,s in orbits.iteritems() if s == 3]

data = np.zeros( ( len(nodes), L,  len(three_orbits)) ) ## to be populated, axis 0 = nodes, axis 1 = graphs, axis 2 = orbits

for i, n in enumerate(nodes): 
	p = node_paths[i]
	d = {int(g): v for g,v in unjsonify(p).iteritems()}
	for g in xrange(L): 
		for k,o in enumerate(three_orbits): 
			try: 
				data[ i, g, k] = int(d[g][o])
			except: 
				data[ i, g, k] = 0

for i in xrange(150, L-1): 
	### cluster data[:i, ,] use gap statistic but it should be eitehr 5 or 6, probably 6.  
	### sould give centroids.
	### vectors = data[i, , ]
	### for j,n in enumerate(nodes): 
	###		v = vectors[j] ## graph i, node j's 3-orbit count vector	
 	###		distances[i][j] = dist(node i to nearest centroid)



