
"""
This is a simple test that generates gaussian data, calculates the m-distances for the data,
and uses chi2 statistics to determine the number of alerts in that set of data if we want n alerts
"""

import numpy as np
from scipy.stats import chi2, norm 
from numpy import random 

data = []
for i in xrange(100):
	mu = np.array([0])
	s = np.array([1]) ## S^{-1}
	Sigma = np.array([1])


	X = random.normal(mu, Sigma,100).reshape(100,1)

	m = lambda x: (np.dot(np.dot(s,x-mu),x-mu))**.5 ## mahalnobis dist. 

	M = np.array(map(m, list(X))).reshape(100,1)

	n = 10 ## number of desired alerts 

	y = float(n)/X.shape[0] ## no. desired/ no. of data points 

	phi = lambda x: chi2.isf(x, X.shape[1])

	alpha = phi(y)**.5 

	data.append(int(sum(M >= alpha)))
	print "Run %d .... \nActual no. of alerts is %s" %(i, data[-1])