"""
    File    = MCDexperiment.py
    Author  = Jessie Jamieson
    Email   = jdjamieson@huskers.unl.edu
    Created = 2016-06-16

    Last Modified = 2016-06-21
    version       = a billion

    Description:
        This file is part of Jessie Jamieson's summer work at ORNL under the
        direction of Dr. Robert Bridges (Go Team Bridges!)

        1) This code first creates a set of experiment data to test minimum
        covariance determinant methods on anomaly detection. Initially, the
        data set is sampled from a mixed multivariate distribution: the first
        with mean 0 and covariance [[1,0],[0,1]], and the second with mean (2,2)
        and covariance [[1,0.5],[0.5,1]].

        2) Next, we run MCDs on the data points. MCD[i] will run on data[i-1].
        We generate points in each MCD distribution.

        3) Then, for each MCD, we calculate the Mahalanobis distances for the
        points generated. These points satisfy a chi-squared distribution from
         which we will calculate p-values of our initial data set.

"""
"""Importing stuffs"""
import numpy as np
import sklearn as sk
import scipy
import scipy.stats
import numpy.linalg
#import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm
from sklearn import mixture
from sklearn.covariance import MinCovDet

"""The code below is a general code that will generate test data for the
experiment. Initially, we will generate 200 points from a Gaussian mixed model
with parameters that we can decide."""

class ExperimentData (object):
    """Create a Set of Experiment Data. NumPoints should be the number of test
    points we wish to test our MCDs against. Caution: for some reason, when
    python generates these random samples of distributions, NumSamples will
    cause an error unless it is sufficiently larger than NumPoints. """
    def __init__(self,NumPoints,Distrb, NumSamples = 5000):
        self.NumPoints    = NumPoints
        self.NumSamples   = NumSamples
        self.Distribution = Distrb
        samplex = list()
        sampley = list()
        for dist in Distrb:
            mean  = dist['mean']
            cov   = dist['cov']
            x, y  = np.random.multivariate_normal(mean, cov, NumSamples).T
            samplex.append(x)
            sampley.append(y)
        #ENDFOR
        """self.data below is where the running experiment data can be stored
        or called as needed"""

        self.data = np.zeros((NumPoints,2),dtype = float)
        for i in xrange(NumPoints):
            p = np.random.uniform(0, 1)
            if p <= .9:
                self.data[i,0] = (samplex[0])[i]
                self.data[i,1] = (sampley[0])[i]
            else:
                self.data[i,0] = (samplex[1])[i]
                self.data[i,1] = (sampley[1])[i]
            #ENDIF
        #ENDFOR
    #ENDinitDEF

    def MCDDists(self,M):
        """ Generates the MCD distributions, trained on points M to end. M is the
        number of MCD distributions you would like to generate. """
        Predicted=100 #I have hard set these values here, but these should be moved.
        Rate=200 #Predicted is the number of points that should be anomalies, Rate is the rate/day.
        alpha = scipy.stats.chi2.isf(float(Predicted)/Rate,2) ## gives H^{-1}(float(Predicted)/Rate) , H(x) = (1-CDF_{\chi^2})(x)
        print alpha
        AnomalyCount = list()
        Pvalues = list()
        MCDList = list()
        #AnomalyCount = list()
        for i in xrange(M,self.NumPoints):
            MCDs = sk.covariance.MinCovDet().fit(self.data[0:i,:])
            MCDList.append([MCDs.location_,MCDs.covariance_])
            # x, y = np.random.multivariate_normal(MCDs.location_, MCDs.covariance_, self.NumSamples).T

            pvalue_tmp = list()
            for j in xrange(self.NumPoints):
                distance = scipy.spatial.distance.mahalanobis(self.data[j,:], MCDs.location_, np.linalg.inv(MCDs.covariance_))
                pvalue_tmp.append(scipy.stats.chi2.sf(distance, 2))
            #print distance
            #ENDFOR
            Pvalues.append(pvalue_tmp)
        #ENDFOR
        #print AnomalyCount
        #return Pvalues
        """At this point, for each MCD, we find how many outliers there are in
        the set of points we did not test against. This will change per MCD"""
        for d in MCDList:
            #S=np.linalg.inv(scipy.linalg.sqrtm(d[1]))
            #alpha_temp=scipy.stats.chi2.isf(float(Predicted)/Rate,2)*pow(float(np.linalg.norm(scipy.linalg.fractional_matrix_power(d[1],0.5))),-2)+np.linalg.norm(d[0])
            value_temp=scipy.stats.chi2.isf(float(Predicted)/Rate,2)
            #vectortemp=[np.sqrt(value_temp),np.sqrt(value_temp)]
            #alpha_temp=scipy.linalg.norm(S*vectortemp+d[0])
            #print alpha_temp
            ac_tmp = list()
            MCDDistances = list()
            for j in xrange(100,200):
                MCDDistances.append(pow(scipy.spatial.distance.mahalanobis([self.data[j][0],self.data[j][1]], d[0], np.linalg.inv(d[1])),2))
            #ENDFOR
            for k in xrange(100):
                if MCDDistances[k]>= value_temp:
                    ac_tmp.append(1)
                #ENDIF
            #ENDFOR
            AnomalyCount.append(len(ac_tmp))
            #print alpha
        print AnomalyCount
        return MCDList
    #ENDDEF
#ENDCLASS

## Setup distribution for test ##
Dist = list()

# "good points"
GoodDist = {}
GoodDist['mean'] = [0,0]
GoodDist['cov']  = [[1,0],[0,1]]
Dist.append(GoodDist)

# "bad points"
BadDist = {}
BadDist['mean'] = [2,2]
BadDist['cov']  = [[1,0.5],[0.5,1]]
Dist.append(BadDist)


Experiment = ExperimentData(200,Dist)
MCD = Experiment.MCDDists(100)
#print len(MCD)

#for index, plist in enumerate(pvalues):
#    print plist[index + 1]
    #if index == len(plist):
    #    break
    #ENDIF
#ENDFOR

#Check=100
#Total=200

#alpha = scipy.stats.chi2.ppf(1-Check/Total,2)
#alpha = scipy.stats.chi2.isf(float(Check)/Total,2) ## gives H^{-1}(float(check)) , H(x) = (1-CDF_{\chi^2})(x)
#print alpha
#ExperimentData = np.zeros((Total,2),dtype = float)
#Unif=np.random.multivariate_normal([0,0], [[1,0],[0,1]], Total).T
#OffCent=np.random.multivariate_normal([2,2], [[1,0.5],[0.5,1]], Total).T
#for i in xrange(Total):
    #p = np.random.uniform(0, 1)
    #if p <= .9:
    #    ExperimentData[i,0] = (Unif[0])[i]
    #    ExperimentData[i,1] = (Unif[1])[i]
    #else:
    #    ExperimentData[i,0] = (OffCent[0])[i]
    #    ExperimentData[i,1] = (OffCent[1])[i]
    #ENDIF
#ENDFOR



#AnomalyCount = list()
#for d in MCD:
#      ac_tmp = list()
#      MCDDistances = list()
#      for j in xrange(Total):
#          MCDDistances.append(pow(scipy.spatial.distance.mahalanobis([ExperimentData[j][0],ExperimentData[j][1]], d[0], np.linalg.inv(d[1])),2))
#      #ENDFOR
#      for k in xrange(Total):
#          if MCDDistances[k]>= alpha):
#              ac_tmp.append(1)
#          #ENDIF
#      #ENDFOR
#      AnomalyCount.append(len(ac_tmp))
#print alpha
#print AnomalyCount

#fig, ax = plt.subplots(1, 1)
#ax.hist(MCDDistances, bins=25, normed=True)
#x = np.linspace(scipy.stats.chi2.ppf(0.01, 2),scipy.stats.chi2.ppf(0.99, 2), 100)
#ax.plot(x, scipy.stats.chi2.pdf(x, 2),'r-', lw=5, alpha=0.6, label='chi2 pdf')
#plt.show()
