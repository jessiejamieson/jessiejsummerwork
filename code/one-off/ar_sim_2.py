# ar_sim.py

## Written by ßøßß¥

import numpy as np
import time
from collections import defaultdict
import random, os
#import netaddr as na 
#from ipwhois import IPWhois as ipw
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import colors
from sklearn.covariance import MinCovDet as MCD
#from generalFunctions import *



## ---- functions ---- ##
def discrete_p_value(i ,f): 
	"""
	Input: 	i -- a key of d. 
			f -- dict, values are floats. Dict gives  (sample, probability) giving a probability mass funciton)
	Output: float: p-value of i under pmf f. 
	""" 
	pv = round(sum([v for v in f.values() if v <= f[i]]), 2)
	return pv 


def linearly_extend(h_):
	"""
	Input: 			h_ -- dict { float in [0,1]: float in [0,1]}. key-value pairs are points on the graph of a function
	Description: 	This linearly connects the dots to extend the h_ function to all of [0,1]
	Output:  		h -- function (input = x, float in [0,1], output float in [0,1]).
	"""
	def h(x):
		h_[1] = 0
		T = sorted(set(h_.keys()))
		s, t = T[0], T[1]
		i = 0
		while t < x: 
			i += 1
			s, t = T[i], T[i+1]
		return ((h_[t]-h_[s])/float(t-s))*(x-s) + h_[s]
	return h

def CreateInputData(X, a_Score):
    """ Convert X and aScore into the input data I assumed when I wrote my functions """
    InputData = list()
    temp=min(a_Score)
    aScore=a_Score-temp
    for i, sample in enumerate(X):
        #if aScore[i] != 0:
            InputData.append(([sample],aScore[i]))
    #ENDFOR
    return InputData
#ENDDEF

def ReadData(InputData):
    """ Reads Input data """

    DataDict = {}
    for data in InputData:
        if data[1] in DataDict:
            for observ in data[0]:
                DataDict[data[1]].append(observ)
            #ENDFOR
        else:
            DataDict[data[1]] = list()
            for observ in data[0]:
                DataDict[data[1]].append(observ)
            #ENDFOR
        #ENDIF
    #ENDFOR
    return DataDict
#ENDDEF

def DataPreProcesser(InputData):
    """ Extracts Observations """
    DataDict = ReadData(InputData)

    ObservePreDict = {}

    for key in DataDict:
        for observ in DataDict[key]:
            if observ in ObservePreDict:
                ObservePreDict[observ].append(key)
            else:
                ObservePreDict[observ] = list()
                ObservePreDict[observ].append(key)
            #ENDIF
        #ENDFOR
    #ENDFOR
    return ObservePreDict
#ENDDEF

def DataAgroScoreProcessor(InputData):
    """ Creates one a-score as a running average for each observation """
    ObservePreDict = DataPreProcesser(InputData)

    DataAgroDict = {}
    for observ in ObservePreDict:
        NumScore = float(len(ObservePreDict[observ]))
        DataAgroDict[observ] = float(0)
        for score in ObservePreDict[observ]:
            DataAgroDict[observ] += float(score)
        #ENDFOR
        DataAgroDict[observ] = DataAgroDict[observ]/NumScore
    #ENDFOR
    return DataAgroDict
#ENDDEF

def K_counts(InputData):
    """ Counts number of points  with given agro score, returns K"""
    DataAgroDict = DataAgroScoreProcessor(InputData)

    AgroCountSingleDict = {}

    for observ in DataAgroDict:
        if DataAgroDict[observ] in AgroCountSingleDict:
            AgroCountSingleDict[DataAgroDict[observ]] += 1
        else:
            AgroCountSingleDict[DataAgroDict[observ]] = 1
        #ENDIF
    #ENDFOR
    return AgroCountSingleDict
#ENDDEF

def N_counts(InputData):
    """ Counts number of observations with given agro score"""
    DataAgroDict = DataAgroScoreProcessor(InputData)
    ObservePreDict = DataPreProcesser(InputData)

    AgroCountMultiDict = {}

    for observ in DataAgroDict:
        if DataAgroDict[observ] in AgroCountMultiDict:
            AgroCountMultiDict[DataAgroDict[observ]] += len(ObservePreDict[observ])
        else:
            AgroCountMultiDict[DataAgroDict[observ]] = len(ObservePreDict[observ])
        #ENDIF
    #ENDFOR
    return AgroCountMultiDict
#ENDDEF

def probabilities(K, N):
    """
    inputs: K- dictionary of k_i values of the form anomaly_score:k_i
            N- dictionary of n_i values of the form anomaly_score:n_i
    outputs: Probabilities- a list of the probabilities calculated
    """

    K_values=[]
    N_values=[]
    KeyList=[]


    for key in K:
        K_values.append(K[key])
        N_values.append(N[key])
        KeyList.append(key)

    L=len(K_values)

    Coeff=np.zeros((L,L))
    Const=np.zeros(L)

    #Set up m-th row and column
    for j in xrange(0,L):
        Coeff[L-1,j]=(K_values[j])
    #endfor

    for i in xrange(L-1):
        Const[i]=N_values[i]
        for j in xrange(L-1):
            if i==j:
                Coeff[i,j]=N_values[i]*(K_values[i])+N_values[L-1]*(K_values[i])
            else:
                Coeff[i,j]=N_values[i]*(K_values[j])
            #endif
        #endfor
    #endfor

    Const[L-1]=1

    Probs = np.matmul(np.linalg.inv(Coeff), Const)
    ProbSort=sorted(Probs)

    return ProbSort, KeyList, K_values

def isvalid(P, K):
    """
    This function will check to see if our calculated P vector satisfies the probability 
    constraints. If it does not, we will average P with P_hat, those expected from a uniform
    distribution.

    inputs: P: the list of probabilities
            n: The total observations from the above calculation
            K: the list of k_i values from the above calculation

    outputs: Bad: =False if P is a valid prob. vector, and True otherwise


    """

    n = sum(K)

    m=len(P)
    Bad = True


    if 0<P[0]<1/float(n):           #Check P_1 against (0,1/n)
        #print "P_1 is valid!"
        for j in xrange(1,m,1):     #Now check P_2:P_{m-1}
            kj=K[0:j]
            pj=P[0:j]
            kj=map(float,kj)
            pj=map(float, pj)
            k=float(sum(k for k in kj))
            s=np.dot(kj,pj)
            #print s
            if (j<m-1) and (P[j-1]<=P[j]<=(1-float(s))/float(n-k)):
                continue
                #print "The next P is valid!"
            elif (j==m-1) and (P[j-1]<=P[j]<=(1-float(s))/K[j]):
               # print "P is a valid probability vector! :)"
                P[j]=(1-float(s))/K[j]
                Bad=False
            else:
                #print "This vector is not a valid probability vector."  #if any sequential P does not work, break
                Bad=True
                break
    else:                       #If P_1 doesn't work, this thing breaks.
        #print "P is not a valid probability vector. :("
        Bad=True
        #break
    #endif

    return Bad

def expected_p(K):
    """
    This function will calculate the expected P vector given n and K.

    inputs: n: The total observations from the above calculation
            K: the list of k_i values from the above calculation

    outputs: ExpectedP: the expected probability vector

    """

    n = sum(K)
    m=len(K)

    ExpectedP=[1/float(2*n)]

    for i in xrange(1,m,1):
        kj=K[0:i-1]
        Ej=ExpectedP[0:i-1]
        k=sum(k for k in kj)
        sE=np.dot(kj,Ej)
        ExpectedP.append((1-sE)/float(n-k))
    #endfor

    return ExpectedP

def create_f_h(X, aScore, observations):
    """ Creates f and h stuff """

    InputData = CreateInputData(X, aScore)
    K = K_counts(InputData)
    N = N_counts(InputData)

    P, Keylist, K_values = probabilities(K, N)

    m=len(K_values)

    Bad=isvalid(P,K_values)
    count=0
    while (Bad) and (count<1000):
        count=count+1
        P_temp=[]
        ExpectedP=expected_p(K_values)
        for i in xrange(m):
            P_temp.append((P[i]+ExpectedP[i])/float(2))
        P=P_temp
        #print P_temp
        Bad=isvalid(P,K_values)
    Count=count
    #else:
        #continue
        #print "The Probability vector is acceptable already!"

    f = {}
    for i, x in enumerate(list(set(X))):
        Pr=P[::-1]
        f[x] = Pr[i]
    #ENDFOR

    #ScoreDict_tmp = DataAgroScoreProcessor(InputData)
    ScoreDict = {}
    for observ in observations:
        if observations[observ] in ScoreDict:
            ScoreDict[observations[observ]].append(observ)
        else:
            ScoreDict[observations[observ]] = list()
            ScoreDict[observations[observ]].append(observ)
        #ENDIF
    #ENDFOR
    h = {0:[1]}
    for avalue in ScoreDict:
        #Prob = 0
        #for observ in ScoreDict[avalue]:
           # Prob = max(0, observ)
        #ENDFOR
        pvalue = 0
        for observ in f:
            if f[observ]<= f[np.mean(ScoreDict[avalue])]:
            #if f[observ] <=Prob:
                pvalue += f[observ]
                #print pvalue
            #ENDIF
        #ENDFOR
        if pvalue in h:
            h[pvalue].append(avalue)
            h[pvalue]=[np.mean(h[pvalue])]
        else:
            h[pvalue] = list()
            h[pvalue].append(avalue)
            h[pvalue]=[np.mean(h[pvalue])]
        #ENDIF
    for pvalue in h:
        h[pvalue]=h[pvalue][0]
    #ENDFOR
    if 1 not in h.keys():
        h[1]=0
    #ENDIF
    return f,h, Count
#ENDDEF

# def create_f_h(a, n = 0, test = False): 
# 	""" 
# 	Input: 	a = dict. This gives the observed data and corresponding data and anomaly scores. Keys are samples, values are numbers in [0,1) (floats) giving the observed a_scores.
# 			n = 0 (default) or positive int. n > 0 will add a set of mass 1/n w/ small probability to account for unseen data. 
# 			test = bool. If true, it will print results of test code. 
# 	Description: The values of a are the anomaly scores. 
# 		This function creates a decreasing function h, and a probability mass function, f
# 		so that A(x) = h(p-value(x)). 
# 	Output: f = dict. Keys are samples, values are probabilities of that sample ( a prob. mass function)
# 			h = dict. Keys are p-values attained by f (floats) , and values are anomaly scores (floats). 
# 	""" 

# 	## remove the guys that have a-score = 1. They have 0 probability. 
# 	for x in a.keys():
# 		if a[x]==1:
# 			a.pop(x)

# 	## make sure the minimum is indeed 0:		
# 	a0 = min(a.values())
# 	if a0 != 0: 
# 		for x in a.keys(): 
# 			a[x] = a[x] - a0

# 	A = sorted(set( a.values()), reverse = True) ## just the set of values 
# 	m = len(A)
# 	reverse_a = {ai: [x for x in a.keys() if a[x] == ai] for ai in A} ## the reverse lookup.

# 	X = {i : reverse_a[A[i]] for i in xrange(m)}
# 	k = {i : len(X[i]) for i in xrange(m)}
# 	if n != 0: 
# 		k[0]  += 1./n ## add a little probability mass for the not-yet-seen points, called "other"

# 	## initiate dicts to be populated: 
# 	y = {i: 0 for i in xrange(-1, m)} ## p-values to be determined. 
# 	e = {i: 0 for i in xrange(m)} ## auxilary values that are needed to populate everything else. 
# 	p = {i: 0 for i in xrange(m)} ## probability masses (not per point, per levelset of A)

# 	for i in xrange(m): 
# 		## want to count this in reverse. 
# 		c = sum([k[j] for j in xrange(i, m)])
# 		e[i] = float(k[i])/ float(m*c)
# 		if i == 0: 
# 			y[0] = e[0]
# 			p[0] = y[0]/k[0]
# 		else: 
# 			y[i] = ( float(k[i])/k[i-1] ) * (y[i-1] - y[i-2])  + y[i-1] + e[i]
# 			p[i] = (y[i] - y[i-1]) /k[i] 


# 	## test code: : 
# 	if test: 
# 		print "Do the probabilities sum to 1?\n\t%s" %(round(1-sum([k[i]*p[i] for i in xrange(m)]), 1) == 0.0 )
		
# 		x = int(0 >= p[0]) ## if it's not true, store 1, else store 0
# 		for i in xrange(m-1): 
# 			x += int( p[i] >= p[i + 1] ) ## if it's not true, add 1
# 		print "Is 0 < p0 < p1 < ... ?\n\t%s" %(bool(not(x)))
		
# 		print "Do the p-values max at 1?\n\t%s" %(round(1-y[m-1],1) == 0. )

# 	## now make the probability mass function and decreasing function: 
# 	f = {}
# 	h_ = { 0.:1.}
# 	for i, l  in X.iteritems(): 
# 		for x in l: 
# 			f[x] = p[i]
# 			h_[y[i]] = a[x]


# 	if test: 
# 		for x in a.keys(): 
# 			print "\n\tx = %s" %x
# 			i = A.index(a[x])
# 			result = (round(discrete_p_value(x,f) - y[i], 1) == 0)
# 			print "\t p-value(x) matches y?\n\t\t%s" %(result)
# 			if not n and result: 
# 				result2 = (round( a[x] - h_[y[i]], 1) == 0 ) 
# 				print "\t h[p-value(x)] == a[x]?\n\t\t%s" %(result2)
# 	return f, h_


## generate data functions 
def create_continuous_data(n): 
	""" 
	Input: 	n = number of data points desired 
	Output: X = np.array of shape 2 by n (n data points in R^2)
			a_scores = np.array of shape n by 1, the corresponding anomaly scores in 0,1
	Description: GMM data generated .9 from standard bivariate, .1 from mean (2,2) cov [[1, .5,], [.5, 1]]. 
				MCD fit to an initial 100 points. h = .9
				new data points are scored, then mcd refit. 
				scoring is 1-exp(.1*mahalanobis distance)
	""" 

	mean = np.array([2, 2])
	cov = np.array([[1, 0.5], [0.5, 1]])
	x, y = np.random.multivariate_normal(mean, cov, 100 + n).T

	#Here I want regular old standard gaussian distribution
	mean2 = np.array([0, 0])
	cov2 = np.array([[1, 0], [0, 1]])
	x2, y2 = np.random.multivariate_normal(mean2, cov2, 100 + n).T

	#These are the plotting commands
	#fig = plt.figure()
	#ax1 = fig.add_subplot(111)
	#ax1.scatter(x, y, s=10, c='b', marker="s", label='skewed')
	#ax1.scatter(x2, y2, s=10, c='r', marker="o", label='normal')
	#plt.legend(loc='upper left');
	#plt.show()

	sampled_x=np.zeros(100 + n, dtype=float)
	sampled_y=np.zeros(100 + n, dtype=float)

	#gaussian mixture model. 
	for i in xrange(100 + n):
	    p = np.random.uniform(0, 1)
	    if p > 0.9:
	        sampled_x[i]=x[i]
	        sampled_y[i]=y[i]
	    if p < 0.9:
	        sampled_x[i]=x2[i]
	        sampled_y[i]=y2[i]

	X = np.array([sampled_x,sampled_y]).T ## x's down first column y's down second. 

	m_dist = {}
	# mcds = {}

	for i in xrange(100, n + 100):
	    #Xi=X[0:i]
	    Xi = X[: i , :]
	    mcd = MCD( support_fraction = .9 ).fit(Xi)
	    m_dist[i] = mcd.mahalanobis(X[i].reshape(1,2))[0] ##1-np.exp(-mcd.mahalanobis(X[i].reshape(1,2)))[0]

	a_score = 1-np.exp(-.1*np.array(m_dist.values()))
	# the histogram of the data
	# plt.hist(a_score)
	# plt.xlabel("a_score")
	# plt.ylabel("count")
	# plt.show()

	return X[100:,], a_score 


def create_discrete_data(n, p, h): 
	"""
	Input: 	n = positive int. number of data points desired
			p = list of positive floats summing to 1. these give the multinomial probability, for the model that occurs 95% of the time
			h = function. input and output are floats in [0,1], must be strictly decreasing. h(p-value) = a_score when creating the data 
	Output: X = np.array of shape n,. gives numbers in [0,1,2,3,...] that were sampled 
			a_score = np.array of shape n, . gives floats in [0,1).
	
	Description: Samples from a mixture of multinomials. Fits a multinomial and uses 1-pvalue to score, then updates. 
	""" 

	data = np.random.multinomial(1, p, size = n + 200)
	samples = {}
	for i in xrange(n + 200): 
		samples[i] = np.where(data[i])[0][0] ## write the side of the die that was rolled by model x on roll i
	l = len(p)
	f = {i : (1 + len( [x for x in samples.keys()[:200] if samples[x] == i]) )  for i in xrange(l) } ## divide by the sum of the values to get a probability
	X = np.array(samples.values()[200:]).reshape(n,)
	a_score = np.zeros(n,)
	for i in xrange(n):
		x = samples[200 + i]
		a_score[i] = h(discrete_p_value(x, { j : float(f[j])/sum( f.values() ) for j in xrange(l)} ))
		f[x] += 1
	return X, a_score


    ## run experiment functions
def discrete_experiment(start_min, dpm, minutes, p, h0, outpath = False): 
    """ Input: 	start_min = positive number. Number of minutes of data seen/used to set the inital probability model (f) and decreasing function (h), 
    				and for calculating the data rate, we use the last start_min*dpm points. 
    			dpm = positive int, gives number of data points per minute on average. 
    			minutes = positive int. Gives the number of minutes the simulation should run 
    			p  (see create_discrete_data for details) these are lists that give multinomial probabilities
    			h0 = function, input/output both floats in [0,1]. must be decreasing. h(p-value) = a_score when creating the data 
    			outpath = False or string to where the json file with the experiment input/output should be written. 

    	Output: X = np.array of shape (N,). each entry is a number 0,1,2,... whatever was sampled from the multinomial
    			a_score = np.array of shape (N,). each entry is the anomaly score (float in [0,1])
    			times = list ## list of the times that data arrives in order 
    			observations = {x : weighted running average a_score(x) for x in sample space.} ## input for create_f_h(). keys are the observed data points, and values are the weighted average of the a_scores for that key. 
    			experiment_times = list,  just to store how long the building of the probability model takes, for our info. 	
    			rates = list ## the rate of the data estimated after each point. 
    			fs = { i : { x : p(x) for x in sample space} for i in xrange(n,N) } at each index i, it has 
    			hs = {} ## we'll also store intermediate hs (the decreasing functions) 
    			results = dict of form {minute index: {info }}
                Counts =  dict of the form {iteration: int} where int is the number of times our estimated P values were averaged
    		
    	Description: Generates simulated streaming multinomial data w/ a_scores probabilistically (timestamps are sampled, data is sampled, and a_scores applied)
    				 (see create_discrete_data)
    				 After n points are observed, it fits the f,h, and estimates the rate the data is streaming.
    				 f,h, and r are reestimated after all subsequent data points, and stored.  
    				 Finally, this function returns all the simulated and estimated data for analysis
    """ 

    ## ---- generate the times----## 
    Counts={}
    delta = 60./dpm ## sec. per 1 data point. 
    times = [0.]
    t = 0.
    while t < (60. * (start_min + minutes)) :
        t += 2*delta*np.random.beta(10,10) ## times 2 b/c need to center at 1 not at .5, sample 100 extra b/c we need it to go at least the required time. 
        ## notice: beta(1, 1) is uniform distribution. 
        times.append(t)

    ## --- initialize data ---- ##
    N = len(times) ## N = total number of data points. 
    X, a_score = create_discrete_data(N, p, h0) ## create the data
    #a_score = np.around(a_score, decimals=2)
   

    ## ---- estimated values ---- ##
    n = start_min*dpm ## number of data points 
    rates = { i : n * 60. /(times[i] - times[i-n])  for i in xrange(n,N)} ## number of data points per minute. 

    ## we need to keep track of what data the system actually sees and have a single a_score for each point for input.
    observations = {} ## input for create_f_h(). keys are the observed data points, and values are the weighted average of the a_scores for that key. 
    experiment_times =[] ## just to store how long the building of the probability model takes. 
    fs = {} ## we'll also store intermediate fs (the probability distributions) 
    hs = {} ## we'll also store intermediate hs (the decreasing functions) 

    ## populate observations, create the f's and h's:
    for i in xrange(N): 

        x, a = (X[i] , a_score[i])      

        if x in observations.keys(): ## add to observatons: 
            observations[x] = .5 * observations[x] + .5 * a
        else: 
            observations[x] = a

        if i >= n :
            tstart = time.time()
            #f, h_ = create_f_h(observations.keys(),observations.values())
            f,h_, Count_temp=create_f_h(X,a_score,observations)
            tend = time.time()
            experiment_times.append(tend-tstart)
            fs[i] = f
            hs[i] = h_
            Counts[i]=Count_temp
        

    ## now populate the results: 
    results = {}  ## to be populated, of form {minute: {threshold: ... }}
    t0 = 60*start_min
    intervals = [ ( t0 + (i * 60) , t0 + (i + 1)*60 ) for i in xrange(minutes)]
    for m in xrange(minutes): 
    	s,t  = intervals[m]
    	indices = [i for i in xrange(n,N) if times[i] >= s and times[i] <= t ]
    	i0 = indices[0]

    	## load the estimated models at the begining of the minute: 
    	f = fs[i0]
    	h_ = hs[i0]
    	h = linearly_extend(h_)
    	l = sorted(h_.values())

    	epsilon = min( [ l[i+1] - l[i] for i in xrange( 0, len(l)-1) ]  )/2. 
    	# For epsilon def: only go to len(l)-1 b/c h always has point h(0) = 1 (which never happens b/c no points have p-value 0), and often has a point w/ h(x) = .99. including both screws up the epsilon calculation!
    	r = rates[i0]

    	## what p-values, thresholds, results? 
    	p_values = sorted(set([discrete_p_value(x,f) for x in f.keys()]))
    	thresholds = map(h, p_values)
    	results[m] = {h(pv) : { "p_value" : pv, "exp_alerts_per_min": r*pv, "actual_alerts_in_min": len([a_score[i] for i in indices if a_score[i] >= (h(pv) - epsilon) ]) }  for pv in p_values}

    xx = list(np.arange(0,1.05, .05))
    plt.plot(xx, map(h, xx), label = "Estimated h")
    plt.plot(xx, map(h0, xx), label = "Gold standard h")
    plt.ylabel('Anomaly Scores')
    plt.xlabel('P-values')
    plt.legend(bbox_to_anchor=(1, 1),bbox_transform=plt.gcf().transFigure)
    plt.show()

    if outpath: 
    	if not os.path.isdir(outpath): 
    		os.mkdir(outpath)
    	picklify(h0, os.path.join(outpath, "input-h.pickle"))
    	jsonify({"n":  n, "dpm": dpm, "minutes": minutes, "p": p}, os.path.join(outpath, "inputs.json")) 
    	jsonify(X.tolist(), os.path.join(outpath,"X.json") )
    	jsonify(a_score.tolist(), os.path.join(outpath,"a_score.json"))
    	jsonify(times , os.path.join(outpath,"times.json")) 
    	jsonify(experiment_times, os.path.join(outpath, "experiment_times.json"))
    	jsonify(rates, os.path.join(outpath,"rates.json")) 
    	jsonify(fs, os.path.join(outpath,"fs.json"))
    	jsonify(hs, os.path.join(outpath, "hs.json"))
    	jsonify(results, os.path.join(outpath, "results.json"))  

    return X, a_score, times, observations, experiment_times, rates, fs, hs, results, Counts


## ---- paths ---- ##
datapath = os.path.join(os.path.abspath(os.path.join(os.getcwd(), "..")), "data-discrete-experiments") 
if not os.path.isdir(datapath): 
	os.mkdir(datapath)

## ---- user inputs & experiment ---- ##

def h_c(x): 
    return 1-x**2

def h_b(x): 
    return 1-x**.5

def h_a(x):
    return 1-x

#hes= [h_a, h_b]
def h_0(x):
    return np.piecewise(x, [x<=0.9, x>0.9], [lambda x: (-2./3)*x+1, lambda x: -4.*x+4])
hes=[h_a, h_b, h_c]
pes=[[0.5001,0.4999]]

pes = [  [.6, .3, .1], [.5, .25, .15, .1],  [ 10./25,  8./25, 4./25, 2./25 , 1./25], [  35./140 ,30./140, 25./140, 20./140, 15./140, 10./140, 5./140] ]
pes = [[.6, .3, .1]]

p = np.array([5., 10, 15, 20, 25, 30, 35, 40, 45, 50])
p = list(p/float(sum(p)))

hes=[h_b]
pes=[[  35./140 ,30./140, 25./140, 20./140, 15./140, 10./140, 5./140]]


dpm = 60 ## no. data points per min. 
start_min = 5 ## time seen before starting algorithm (training data)
minutes = 60 ## how many minutes to run the experiment?
for i in xrange(len(pes)): 
    p = pes[i]
    for j in xrange(len(hes)):
        print "Starting experiment i = %s, j = %s " %(i,j)
        h0 = hes[j]
        #outpath = os.path.join(datapath, "experiment_p%d_h%d" %(i,j))
        X, a_score, times, observations, experiment_times, rates, fs, hs, results, Counts = discrete_experiment(start_min, dpm, minutes, p, h0)
        print Counts.values()
        break
    for k in xrange(len(p)): 
        exp_ave = np.average([ results[m][ sorted(results[m].keys())[k] ]["exp_alerts_per_min"] for m in results.keys()])
        act_ave = np.average([ results[m][ sorted(results[m].keys())[k] ]["actual_alerts_in_min"] for m in results.keys()])
        print "%s-%s," %(exp_ave, act_ave)


# ## see how the p-values change over time: 
# for i in xrange(len(p)): ## i = 0 (min p-value) to len(p1), max p-value. 
#     for m in results.keys():
#         print "i = %d, p-value = %s" %(i, sorted(results[m].keys())[i])


for i in xrange(len(p)): 
  exp_ave = np.average([ results[m][ sorted(results[m].keys())[i] ]["exp_alerts_per_min"] for m in results.keys()])
  act_ave = np.average([ results[m][ sorted(results[m].keys())[i] ]["actual_alerts_in_min"] for m in results.keys()])
  print "i = %d, exp_ave = %s, act_ave = %s" %(i,exp_ave, act_ave)
