import numpy as np
import sklearn as sk
import scipy
import scipy.stats
import numpy.linalg
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from sklearn import mixture
from sklearn.covariance import MinCovDet

#here I want to set my skewed gaussian distribution
#numpy.random.multivariate_normal(mean, cov[, size])
mean = [2, 2]
cov = [[1, 0.5], [0.5, 1]]
x, y = np.random.multivariate_normal(mean, cov, 5000).T

#Here I want to st my regular old standard gaussian distribution
mean2 = [0, 0]
cov2 = [[1, 0], [0, 1]]

x2, y2 = np.random.multivariate_normal(mean2, cov2, 5000).T

#These are the plotting commands
#fig = plt.figure()
#ax1 = fig.add_subplot(111)

#ax1.scatter(x, y, s=10, c='b', marker="s", label='skewed')
#ax1.scatter(x2, y2, s=10, c='r', marker="o", label='normal')
#plt.legend(loc='upper left');
#plt.show()

#Now I want to initialize my "sample" space. Sample points from the mixed
# disribution.

# sampled_x=np.zeros(200, dtype=float) #86400
# sampled_y=np.zeros(200, dtype=float)
X = np.zeros((200,2), dtype=float)


#sampled_centered_x=numpy.zeros(100, dtype=float)
#sampled_centered_y=numpy.zeros(100, dtype=float)

#Here let's use "random" numbers to pick our data set

# for i in xrange(200):
#     p=np.random.uniform(0, 1)
#     if p>0.9:
#         sampled_x[i]=x[i]
#         sampled_y[i]=y[i]
#     if p<0.9:
#         sampled_x[i]=x2[i]
#         sampled_y[i]=y2[i]

for i in xrange(200):
    p=np.random.uniform(0, 1)
    if p>0.9:
        X[i,0]=x[i]
        X[i,1]=y[i]
    if p<0.9:
        X[i,0]=x2[i]
        X[i,1]=y2[i]

#These are the points I'm going to try to fit my MCDs to
# X=[sampled_x,sampled_y]

#N=200
# calc_MCDs=np.zeros(100,dtype=float)
calc_MCDs=list()
NewDist  =list()

for i in xrange(101,200):
    Xi=X[0:i,:]
    # Xi=np.asarray(X[0:i,:])
    Cov = sk.covariance.MinCovDet().fit(Xi)
    distx, disty = np.random.multivariate_normal(Cov.location_, Cov.covariance_, 5000).T
    calc_MCDs.append(Cov)
    NewDist.append((distx,disty))

#print NewDist[1].shape
#print sampled_x
#calculating p values. To do this, we need to calculate the mahalanobis distances for points, and fit
# a chi squared distribution to these.

mal_dist= list()
Calculated_chi =list()

#xcoord=NewDist[1]
#ycoord=NewDist[2]


for i in xrange(100):
    x,y =NewDist[i]
    for j in xrange(5000):
        #print NewDistI[j]
        point= (x[j],y[j])
        mal_dist.append(scipy.spatial.distance.mahalanobis(point, calc_MCDs[i].location_, np.linalg.inv(calc_MCDs[i].covariance_) ))
#Calculated_chi.append(scipy.stats.chisquare(mal_dist[i]))
