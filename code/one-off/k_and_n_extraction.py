"""
    File    = MCDexperiment.py
    Author  = Jessie Jamieson
    Email   = jdjamieson@huskers.unl.edu
    Created = 2016-06-28

    Last Modified = 2016-06-28
    version       = 1.0

    Description:
        This file is part of Jessie Jamieson's summer work at ORNL under the
        direction of Dr. Robert Bridges (Go Team Bridges!)

        This code takes as input a list of tuples, with the first entry a set of points/sides and the
        second entry their associated anomaly score.

        It outputs the K-values and N-values in the form of dictionaries.
"""

#Importing Stuffs
import numpy as np
import sklearn as sk
import scipy
import scipy.stats
import numpy.linalg
#import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm
from sklearn import mixture
from sklearn.covariance import MinCovDet
import csv

def ReadData(InputData):
    """ Reads Input data """

    DataDict = {}
    for data in InputData:
        if data[1] in DataDict:
            for observ in data[0]:
                DataDict[data[1]].append(observ)
            #ENDFOR
        else:
            DataDict[data[1]] = list()
            for observ in data[0]:
                DataDict[data[1]].append(observ)
            #ENDFOR
        #ENDIF
    #ENDFOR
    return DataDict
#ENDDEF

def DataPreProcesser(InputData):
    """ Extracts Observations """
    DataDict = ReadData(InputData)

    ObservePreDict = {}

    for key in DataDict:
        for observ in DataDict[key]:
            if observ in ObservePreDict:
                ObservePreDict[observ].append(key)
            else:
                ObservePreDict[observ] = list()
                ObservePreDict[observ].append(key)
            #ENDIF
        #ENDFOR
    #ENDFOR
    return ObservePreDict
#ENDDEF

def DataAgroScoreProcessor(InputData):
    """ Creates one a-score as a running average for each observation """
    ObservePreDict = DataPreProcesser(InputData)

    DataAgroDict = {}
    for observ in ObservePreDict:
        NumScore = float(len(ObservePreDict[observ]))
        DataAgroDict[observ] = float(0)
        for score in ObservePreDict[observ]:
            DataAgroDict[observ] += float(score)
        #ENDFOR
        DataAgroDict[observ] = DataAgroDict[observ]/NumScore
    #ENDFOR
    return DataAgroDict
#ENDDEF

def K_counts(InputData):
    """ Counts number of points  with given agro score, returns K"""
    DataAgroDict = DataAgroScoreProcessor(InputData)

    AgroCountSingleDict = {}

    for observ in DataAgroDict:
        if DataAgroDict[observ] in AgroCountSingleDict:
            AgroCountSingleDict[DataAgroDict[observ]] += 1
        else:
            AgroCountSingleDict[DataAgroDict[observ]] = 1
        #ENDIF
    #ENDFOR
    return AgroCountSingleDict
#ENDDEF

def N_counts(InputData):
    """ Counts number of observations with given agro score"""
    DataAgroDict = DataAgroScoreProcessor(InputData)
    ObservePreDict = DataPreProcesser(InputData)

    AgroCountMultiDict = {}

    for observ in DataAgroDict:
        if DataAgroDict[observ] in AgroCountMultiDict:
            AgroCountMultiDict[DataAgroDict[observ]] += len(ObservePreDict[observ])
        else:
            AgroCountMultiDict[DataAgroDict[observ]] = len(ObservePreDict[observ])
        #ENDIF
    #ENDFOR
    return AgroCountMultiDict
#ENDDEF

###--- Test case ------###

TestData = list()
TestData.append(({1,2,3}, 0.3))
TestData.append(({4,5,6}, 0.5))
TestData.append(({1,4,5}, 0.2))
TestData.append(({7}, 0.7))


ObservDict = DataPreProcesser(TestData)
DataAgroDict = DataAgroScoreProcessor(TestData)
K = K_counts(TestData)
N = N_counts(TestData)

print ObservDict
print K # dictionary of K's
print N # dictionary of N's


##--------Test post pls ignore-------##


Test1 = 0
for score in AgroCountSingleDict:
    Test1 = Test1 + AgroCountSingleDict[score]
#ENDFOR

Test2 = 0
for score in AgroCountMultiDict:
    Test2 = Test2 + AgroCountMultiDict[score]
#ENDFOR

Check1 = len(ObservDict)
Check2 = 0
for data in TestData:
    Check2 = Check2 + len(data[0])
#ENDFOR

print Test1
print Check1
print Test2
print Check2













