"""
    File    = MCDexperiment.py
    Author  = Jessie Jamieson
    Email   = jdjamieson@huskers.unl.edu
    Created = 2016-06-16

    Last Modified = 2016-06-16
    version       = 1.0

    Description:
        This file is part of Jessie Jamieson's summer work at ORNL under the
        direction of Dr. Robert Bridges (Go Team Bridges!)

        1) This code first creates a set of experiment data to test minimum
        covariance determinant methods on anomaly detection. Initially, the
        data set is sampled from a mixed multivariate distribution: the first
        with mean 0 and covariance [[1,0],[0,1]], and the second with mean (2,2)
        and covariance [[1,0.5],[0.5,1]].

        2) Next, we run MCDs on the data points. MCD[i] will run on data[i-1].
        We generate points in each MCD distribution.

        3) Then, for each MCD, we calculate the Mahalanobis distances for the
        points generated. These fit a chi-squared distribution from which we will
        calculate p-values of our initial data set.

"""

import numpy as np
import sklearn as sk
import scipy
import scipy.stats
import numpy.linalg
#import matplotlib.pyplot as plt
#from matplotlib.colors import LogNorm
from sklearn import mixture
from sklearn.covariance import MinCovDet

class ExperimentData (object):
    """
    This python object simply creates experiment data to the specifications desired. We pass
    the object the "good data" parameters and the "bad data" parameters (see below).
    From these parameters, we generate a set of gaussian distributed data and a (skewed) 
    set of gaussian distributed data.
    """
    def __init__(self,NumPoints,Distrb, NumSamples = 5000):
        self.NumPoints    = NumPoints
        self.NumSamples   = NumSamples
        self.Distribution = Distrb
        samplex = list()
        sampley = list()
        for dist in Distrb:
            mean  = dist['mean']
            cov   = dist['cov']
            x, y  = np.random.multivariate_normal(mean, cov, NumSamples).T
            samplex.append(x)
            sampley.append(y)
        #ENDFOR

        self.data = np.zeros((NumPoints,2),dtype = float)
        for i in xrange(NumPoints):
            p = np.random.uniform(0, 1)
            if p <= .9:
                self.data[i,0] = (samplex[0])[i]
                self.data[i,1] = (sampley[0])[i]
            else:
                self.data[i,0] = (samplex[1])[i]
                self.data[i,1] = (sampley[1])[i]
            #ENDIF
        #ENDFOR
    #ENDinitDEF

    def mcdDists(self,M):
        """ Generate list of MCDs using previous data"""
        #alpha = scipy.stats.chi2.ppf(1-k/N,2)
        #print alpha
        Pvalues = list()
        MCDList = list()
        #AnomalyCount = list()
        for i in xrange(M,self.NumPoints):
            MCDs = sk.covariance.MinCovDet().fit(self.data[0:i,:])
            MCDList.append([MCDs.location_,MCDs.covariance_])
            # x, y = np.random.multivariate_normal(MCDs.location_, MCDs.covariance_, self.NumSamples).T

            pvalue_tmp = list()
            for j in xrange(self.NumPoints):
                distance = scipy.spatial.distance.mahalanobis(self.data[j,:], MCDs.location_, np.linalg.inv(MCDs.covariance_))
                pvalue_tmp.append(scipy.stats.chi2.sf(distance, 2))
            #print distance
            #ENDFOR
            Pvalues.append(pvalue_tmp)
        #ENDFOR
        #print AnomalyCount
        #return Pvalues
        return MCDList
    #ENDDEF
#ENDCLASS

## Setup distribution for test ##
Dist = list()

# "good points"
GoodDist = {}
GoodDist['mean'] = [0,0]
GoodDist['cov']  = [[1,0],[0,1]]
Dist.append(GoodDist)

# "bad points"
BadDist = {}
BadDist['mean'] = [2,2]
BadDist['cov']  = [[1,0.5],[0.5,1]]
Dist.append(BadDist)


Experiment = ExperimentData(200,Dist)
MCD = Experiment.mcdDists(101)


Check=200
Total=1000

#alpha = scipy.stats.chi2.ppf(1-Check/Total,2)
alpha = scipy.stats.chi2.ppf(1-float(Check)/Total,2)
print alpha

ExperimentData = np.zeros((Total,2),dtype = float)
Unif=np.random.multivariate_normal([0,0], [[1,0],[0,1]], Total).T
OffCent=np.random.multivariate_normal([2,2], [[1,0.5],[0.5,1]], Total).T
for i in xrange(Total):
    p = np.random.uniform(0, 1)
    if p <= .9:
        ExperimentData[i,0] = (Unif[0])[i]
        ExperimentData[i,1] = (Unif[1])[i]
    else:
        ExperimentData[i,0] = (OffCent[0])[i]
        ExperimentData[i,1] = (OffCent[1])[i]
    #ENDIF
#ENDFOR



AnomalyCount = list()
for d in MCD:
      ac_tmp = list()
      MCDDistances = list()
      for j in xrange(Total):
          MCDDistances.append(scipy.spatial.distance.mahalanobis([ExperimentData[j][0],ExperimentData[j][1]], d[0], np.linalg.inv(d[1])))
      #ENDFOR
      for k in xrange(Total):
          if MCDDistances[k]>= alpha:
              ac_tmp.append(1)
          #ENDIF
      #ENDFOR
      AnomalyCount.append(len(ac_tmp))
#print alpha
print AnomalyCount
