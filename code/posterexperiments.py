"""
Graph Prints Data Analysis

This file runs the experiments for my poster, whose files are:
teambridgesposter.tex
teambridgesposter.pdf
teambridgesposterstyle.sty


Experiment 2 is a probabilistic estimation of anomaly counts and false positive rates, 
using graph prints data from the files in the paths below.

Experiment 1 is a non-probabilistic estimation of anomaly counts and fp rates using the same data.

figures are saved in the same location as the poster files, for easy compiling with latex.

File requires:
generalFunctions.py
ar_sim_gradient.py
"""

#####----- Import --------######
import numpy as np
import time
from collections import defaultdict
import random, os
#import netaddr as na 
#from ipwhois import IPWhois as ipw
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import colors
from sklearn.covariance import MinCovDet as MCD
import operator
import json
import scipy
from pprint import pprint
from generalFunctions import *
from ar_sim_gradient import *
from math import *
import re


###### ---- file paths ---- #######
dataPath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data/public-data"))
noappPath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data/no-appliance/graphlets.json"))
appPath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data/w-appliance/graphlets.json"))
graphletsPath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data/public-data/graph_info.json"))
mahalPath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data/public-data/graph_detection_info.json"))
noAppMahalPath = os.path.abspath(os.path.join(os.path.join(os.getcwd(), ".."), "data/no-appliance/m_dist.json"))

##### ----- unjsonify the files ----- #####
graphlets = {int(x): y for x,y in unjsonify(graphletsPath).iteritems()}
mahalinformation = {int(x): y for x,y in unjsonify(mahalPath).iteritems()}


## ---- node-level streaming data (orbits) ---- ## 
orbitListPath = os.path.join(dataPath, "orbit_list.json")



""""""""""""""""""""""""""""""""""""""""""

## ----- extract the mahalanobis distances ---- ###
"""
The data in the files comes with a mahalanobis distance. Here we extract them and their associated covariances.
only the mahalanobis distances that were extracted are used in the poster experiments. The information in 
lines 73-93 was only used for testing and does not come up in the poster (see other info below).
"""

# Task 1: show we can regulate the alert rate (via the theorem) if we know the p-values. Convert m-dist
# to p-values 

mahaldistances={z:mahalinformation[z]["m-dist"] for z in mahalinformation.keys() }
mahalcovariances={z:mahalinformation[z]["cov"] for z in mahalinformation.keys() }
mahalmeans={z:mahalinformation[z]["mu"] for z in mahalinformation.keys() }


# what is the p-value for this experiment?
mdist=10**3.6
p=scipy.stats.chi2.sf(mdist,37)

# Use this p-value to find the mahal. distance that corresponds to the threshold.

anomalythreshold=scipy.stats.chi2.sf(p,37)**0.5 # the m-distance^2 after which p(x=>x0)=p. isf=(1-cdf)^(-1)

anomalycount=sum(i > anomalythreshold for i in mahaldistances.values()) #how many have distances greater than the threshold?

sqrrootdist={k:mahaldistances[k]**0.5 for k in mahaldistances.keys()}

#mahaldistancescutoff={k:mahaldistances[k] for k in mahaldistances.keys() if mahaldistances[k]<1000}

# plt.hist(mahaldistances.values(), bins=np.linspace(0,1000,1000))
# plt.xlabel("M-distances")
# plt.ylabel("Frequency")
# plt.show()
#fig = plt.gcf()

###################################################################
"""
Below, we use the data from the graphprints files after information about the monitoring appliance(s)
has been removed. 

Experiment 2 requires the calculation of means/covariances from the graphlet vectors, as compared
to exp. 1 in which information is derived entirely from mahalanobis distances. The prior
distribution assumed for this data is a normal distribution. 

"""

#####----- unjsonify the graphlet data ------#####
noAppGraphlets = {int(x): y for x,y in unjsonify(noappPath).iteritems()}

######----- get the graphlet vectors -------- ####
# note: only using the information about the graphlets with 3 vertices

graphlet_counts={z: {k:v for k,v in noAppGraphlets[z].iteritems()} for z in noAppGraphlets.keys() }

graphlet_counts_3={}

#graphlet_counts_3_totals={}

for key in graphlet_counts.keys():
	graphlet_counts_3[key]={k:v for k,v in graphlet_counts[key].items() if k.endswith('_3') }
	tempdict=graphlet_counts_3[key]
	graphlet_counts_3[key]={u:int(v) for u,v in tempdict.items()}

for i in xrange(150):
	del graphlet_counts_3[i]

#### ----- store the vectors in a matrix and perform operations on those ----#####

dataforgauss=np.zeros((len(graphlet_counts_3[200].keys()), len(graphlet_counts_3.keys())))
keys=graphlet_counts_3[200].keys()

for key in graphlet_counts_3.keys():
	for i in xrange(len(keys)):
		dataforgauss[i,key-150]=graphlet_counts_3[key][keys[i]]


means=np.zeros((len(graphlet_counts_3[200].keys()), len(graphlet_counts_3.keys())))

covariances=[0]

for j in xrange(1,len(graphlet_counts_3.keys())):
	for i in xrange(len(graphlet_counts_3[200].keys())):
		means[i,j]=np.mean(dataforgauss[i, 0:j])
	covariances.append(np.cov(dataforgauss[:,0:j]))

###---- calculate the m-distances using the mean/cov from above----####

calculatedmdist=[0, 0,0]

for j in xrange(3,len(graphlet_counts_3.keys())):
	workingmean=means[:,j-1]
	workingcovariance=covariances[j-1]
	workingvector=dataforgauss[:,j]
	if np.linalg.det(workingcovariance)!=0:
		calculatedmdist.append(scipy.spatial.distance.mahalanobis(workingvector,workingmean,np.linalg.inv(workingcovariance)))
	else:
		calculatedmdist.append(scipy.spatial.distance.mahalanobis(workingvector,workingmean,np.linalg.pinv(workingcovariance)))
		#print j

###-----Get the anomaly thresholds given probabilities, and plot -----####

plist=np.linspace(0,0.2,20000)
shortplist=[0.1, 0.04]
#plist=shortplist

anomalythreshold={p:scipy.stats.chi2.isf(p,2)**0.5 for p in plist} # the m-distance^2 after which p(x=>x0)=p. isf=(1-cdf)^(-1)

#sqrddist={k:mahaldistances[k]**2 for k in mahaldistances.keys()}

#ps={k**2:scipy.stats.chi2.sf(k,2) for k in calculatedmdist[282:601]}
allps={k**2:scipy.stats.chi2.sf(k,2) for k in calculatedmdist}

#anomalycount={p:sum(i > anomalythreshold[p] for i in calculatedmdist[282:601]) for p in plist}
allanomalies={p:sum(i > anomalythreshold[p] for i in calculatedmdist) for p in plist}


sortedacounts= sorted(allanomalies.items(), key=operator.itemgetter(0))

#xvals=anomalycount.keys()
#yvals=[(anomalycount[x]-33)/float(anomalycount[x]) for x in xvals]

xvals1=allanomalies.keys()
yvals1=[abs((allanomalies[x]-33)/float(allanomalies[x])) for x in xvals]

#yvalsactual=[abs((p*float(601)-33)/(p*float(601))) for p in xvals]

#xvalsl=[log(i) for i in xvals2]
#yvalsl=[log(i) for i in yvals2]

matplotlib.rcParams.update({'font.size': 22})

plt.scatter(xvals1,yvals1)
plt.title("Recovered False Positive Rate as a Function of p-Value")
plt.xlabel("p-Value")
plt.ylabel("False Positive Rate")
ax=plt.gca()
ax.set_xlim(ax.get_xlim()[::-1])
ax.set_xlim(plist[-1],-0.01)
ax.set_yscale('linear')
ax.set_xscale('linear')
plt.savefig("pvals_vs_fpr.png")
plt.show()


""""""""""""""""""""""""""""""""""""""""""


"""
Experiment 1- a non-probabilistic experiment. Here we do not assume a gaussian (or any underlying distribution).
Instead, we run the previous create_f_h algorithms (and related algorithms) to calculate the prob.
distribution that is expected using the maximum a posteriori function. Then we check if it is valid,
and if not, run our gradient ascent method to find the most maximum element from the acceptable space.

Then we calculate anomaly counts and fp rates.

Note: this file does not depend on create_f_h because I have pulled the relevant and required code from that file and included them here 
for easier calculations. It does require the probability calculator and validity checker from ar_sim_gradient.
"""


####----unjsonify the mahalanobis distance graphs-----####

noAppMdist = unjsonify(noAppMahalPath)

###--- M-dist=a-scores!-----###

anomalyscores={noAppMdist.index(x):sqrt(x) for x in noAppMdist}
del anomalyscores[0]
sortedmahal= sorted(anomalyscores.items(), key=operator.itemgetter(1))


###--- "cluster" the distances ---- ###

separation=1
splitindex=[]

for i in xrange(1,len(sortedmahal)):
	if sortedmahal[i][1]-sortedmahal[i-1][1]>separation:
		splitindex.append(i)
#### --- build N and K ----- ###

Ntemp={}

for i in xrange(1,len(splitindex)):
	Ntemp[i]=sortedmahal[splitindex[i-1]:splitindex[i]] 

Nreduced={}

for key in Ntemp.keys():
	valuetemp=Ntemp[key]
	listtemp=[]
	for i in valuetemp:
		listtemp.append(i[1])
	Nreduced[key]=listtemp

averages=[]
Ntemp={}

for v in Nreduced.values():
	averages.append(sum(v)/float(len(v)))
	Ntemp[Nreduced.values().index(v)]=len(v)

Nwithaverages={i:averages[i] for i in xrange(len(averages))}

N={Nwithaverages[i]:Ntemp[i] for i in xrange(len(Nwithaverages.values()))}

K={k:1 for k in N.keys()}

####---- Probability calculation ---- ####

ProbSort, KeyList, K_values = probabilities(K,N)

#### ---- Does it lie in our region? ----###

Bad =isbad(ProbSort, K, N)   #if true, then it is a bad prob. vector. If False, it is an ok prob vector

#### If necessary, run gradient ascent to find acceptable point.

if Bad:
	InteriorPoints=interiorpoints(N, K, ProbSort)
	ProbSortNew=prob_grad_ascent(ProbSort, InteriorPoints, K, N)

###-----Generate f and h -----#####

"""
Recall: f: a_score---> probabilities
		h: pvalue---> a_score 
"""

f={}
reverseprobs=ProbSort
#reverseprobs.reverse()
for i in xrange(len(Nreduced.keys())):
        for j in xrange(len(Nreduced[i+1])):
                f[Nreduced[i+1][j]]=reverseprobs[i]/float(sorted(N.keys())[i])


minuspvals={}

sfk=sorted(f.keys())
sfv=[]

for j in xrange(len(sfk)):
        sfv.append(f[sfk[j]])

for i in xrange(len(f.values())):
	minuspvals[sfk[i]]=sum(sfv[0:i+1])

h={}

htemp={k:max(minuspvals.values())-minuspvals[k] for k in minuspvals.keys()}
h={htemp[k]/float(max(htemp.values())):k for k in minuspvals.keys()}


###---- Now we can get some anomaly counts! ----###


plist=np.linspace(0,0.2,10000) #split the interval as desired

anomalycounts={p:sum(i<p for i in h.keys()) for p in plist}

sortedacounts=sorted(anomalycounts.items(), key=operator.itemgetter(0))

xvals2=plist
yvals2=[(anomalycounts[p]-33)/float(anomalycounts[p]) for p in xvals2 ]

for i in xrange(len(yvals2)):
	if yvals2[i]<0:
		yvals2[i]=0

#---- Plot the information ----#

plt.scatter(xvals2,yvals2)
#plt.scatter(xvals1,yvals1)
plt.title("Recovered False Positive Rate as a Function of p-Value")
plt.xlabel("p-Value")
plt.ylabel("False Positive Rate")
ax=plt.gca()
ax.set_xlim(ax.get_xlim()[::-1])
ax.set_xlim(0.2,-0.01)
ax.set_ylim(0,0.4)
ax.set_yscale('linear')
ax.set_xscale('linear')
#plt.savefig("pvals_vs_fpr.png")
plt.show()



