# skaion-rate-detector.py 

import os, copy
import numpy as np 
from scipy.stats import norm 
from matplotlib import pyplot as plt 

from generalFunctions import * 


datapath = os.path.abspath(os.path.join( os.path.join( os.getcwd(), '..'), 'data-skaion')) 
os.path.isdir(datapath)

# fpm = unjsonify(os.path.join(datapath, 'binSizes.json'))
fps = unjsonify(os.path.join(datapath, 'onesecondbinsizes.json'))
fps = [sum(fps[i:i+10]) for i in np.arange(0,len(fps), 10)] ## so each entry is the count of the flows in a 6 second window. 
# x,bins = np.hist(fps)
l = 100 #  25
x = np.arange(l,9*l,l)
bins = {0: (0, l)}
for i in xrange(1,len(x)): 
	bins[i]= (x[i-1], x[i])
bins[i+1] = (x[i], 100000)

m = len(bins)
counts = {-1 : {i: 1 for i in xrange(m)}} 


def denom(i): 
	return i + 1 + m

pvals = {}
for i in xrange(len(fps)): 
	obs = fps[i] 	
	counts[i] = copy.copy(counts[i-1]) 
	for j in xrange(m): 
		if bins[j][0] <= obs and obs < bins[j][1]: 
			p = counts[i-1][j]/float( denom(i-1)) 
			pv = sum( counts[i-1][k]/float(denom(i-1)) for k in xrange(m) if counts[i-1][k] <= counts[i-1][j] )
			pvals[i] = pv
			counts[i][j] += 1 ## updates the new probability 

x = sorted(pvals.keys())
y = [pvals[i] for i in x]
plt.plot(x[100:],y[100:])
plt.xlabel('10-Second Intervals')
plt.ylabel('Flow Rate P-Value')
plt.show(block = False)

N = 1./10. ## upper bound on expected alerts per minute
r = 6. ## scores per minute
## N = expected num alerts per min = is prob(pv <= alpha) * #scores/min <= alpha * r
## so N <=  alpha * r  or alpha = N/r
alpha = N / r 

alerts = np.where( np.array(y[int(2*r):]) <= alpha, 1, 0 ) ## 1 if there's an alert in that interval, after the first two minutes
ave = r * (sum(alerts)/float(len(alerts)) ) ## scores per minute * percent scores that are alerts = aver num alerts per min
ave_b = r * float(sum(alerts[:int(223*r)]))/ len(alerts[:int(223*r)]) ## num_alerts per min 

print "Expected number of alerts per minute is N = %s\n\t Ave per min is %s\n\t Ave per min before attack is %s" %(N, ave, ave_b)

## to see this is not Gaussian! 

def pv_from_z(z): 
	return 2* scipy.stats.norm.sf(z)

models = {i: {'mu': np.mean(fps[:i]), 'var': np.var(fps[:i]) } for i in xrange(20,len(fps)) }
zscores = {i: np.abs(fps[i] - d['mu'])/d['var'] for i, d in models.iteritems()}
pvals2 = {i: pv_from_z(z) for i,z in zscores.iteritems() }

x = sorted(pvals2.keys())
y = [pvals2[i] for i in x]
N = 1./3. ## alerts per minute
r = 60 ## scores per minute
## want .1 alerts per minute. 
## N = expected num alerts per min = is prob(pv <= alpha) * #scores/min <= alpha * r
## so N <=  alpha * r  or alpha = N/r
alpha = N / r 
alerts = np.where( np.array(y) <= alpha, 1, 0 ) ## 1 if there's an alert in that second
ave = 60* sum(alerts)/float(len(alerts)) ## num alerts per min
ave_b = 60* float(sum(alerts[:225*60]))/ len(alerts[:225*60]) ## num_alerts per min 










