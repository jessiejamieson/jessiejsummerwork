readme.txt

This is a readme for jj-bb/code/ directory. 

Last updated August 12, 2016 -- JJ  

Files: 
	- ar_sim_gradient.py - script contains code to simulate the alert rate regulation without knowing the underlying distribution; this code implements the new "gradient ascent" method. Includes the create_f_h, interiorpoints, and probabilities functions. This file is called by the experiment files.

	- generalFunctions.py - script contains functions for serialization

	- TrainedData.py -  script used to generate data to compare with that from an untrained anomaly detection method when the underlying distribution is known

	- UntrainedData.py - script used to generate data to compare with that from a trained anomaly detection method when the underlying distribution is known

	- KDEcode.py - script in progress that was supposed to use the new nonprobabilistic methods for a clustering application. Not complete.

	- clustering.py - script in progress that was supposed to use the new nonprobabilistic methods (ar_sim_gradient.py in particular) for a clustering application. Not complete.

	- posterexperiments.py - Every experiment on the teambridgesposter by Jessie Jamieson was done using this code. See teambridgesposter.tex and/or teambridgesposter.pdf for more information.

Folders: 
	- code-graphlets - Contains GraphPrints data and associated unpacking scripts with some code for data extraction. See the readme inside the directory for more info.

	- one-off - just about everything in this folder has been ruled obsolete by new scripts, or is just not useful for this project, but is worth keeping.

