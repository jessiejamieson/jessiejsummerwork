"""

Author: Jessie Jamieson (with help from William Jamieson)
Date Last Modified: 2/12/17

This code (DataReader.py) works together with DataOperations.py to both take in the Skaion data and perform various
operations on the data to run our anomaly detection. Note that since both the new and old skaion files are the same
format, these two code files may take in either of the data files.

Tests for the code may be found in another folder.

The primary purpose of DataReader.py is to intake the Skaion data file (a .txt) and put it into a python format that
is readable for the DataOperations file. See below.

Python is object oriented, so that structure is (theoretically) nice and makes code easier to use across many
projects. Modules are things like matplotlib or datetime. You can think about a module as a specialized dictionary
that can store Python code so you can access it with the [.] operator. Python also has another construct that
serves a similar purpose called a "class". A class is a way to take a grouping of functions and data and place
them inside a container so you can access them with the [.] (dot) operator. You will see a few classes below. The
best way I think of it is that while every dog has a color, the class of dogs itself does not. Classes let us
create many many instances of things as we need them.

For an instance of how to use these two files, see the preamble/comments in the DataOperations file.
"""

import ast
from datetime import datetime as dt

class ScoreData:
    """
    This class initializes some funcitons, in particular, the getPvalue function and a function that deals with the
    instance when a key in a dictionary of data has no value. The immediate following variables will determine which
    score(s) one will see when doing analysis. These numbers, in particular, will be passed to the DataBase.AlertListBin
    function, along with a time value and an alert rate.

    Note that for classes, order in which functions are listed does not matter. All functions are initialized.
    """

    scorePcr              = 0
    scorePcr_T            = 1
    scorePrivPorts        = 2
    scorePrivPorts_T      = 3
    scoreBytesPerPacket   = 4
    scoreBytesPerPacket_T = 5

    def __init__(self,RawData):
        """
        In order for the init to be called, a RawData has to be passed.
        In a data dictionary, this will check to see what scores exist and will pull the appropriate score(s).
        RawData is a list of
        """
        RawDataDict = {}
        if RawData != None:
            for Data in RawData:
                RawDataDict[Data['name']] = DataPointExist(Data,'score')

            self.Pcr              = DataPointExist(RawDataDict,'Pcr')
            self.Pcr_T            = DataPointExist(RawDataDict,'Pcr_T')
            self.PrivPorts        = DataPointExist(RawDataDict,'PrivPorts')
            self.PrivPorts_T      = DataPointExist(RawDataDict,'PrivPorts_T')
            self.BytesPerPacket   = DataPointExist(RawDataDict,\
                    'BytesPerPacket')
            self.BytesPerPacket_T = DataPointExist(RawDataDict,\
                    'BytesPerPacket_T')

    def getPvalue(self, scoreType):
        """
        This function will pull the appropriate scores and compute then return the P-value based on the 10^(-score) transformation.
        That is, scores are the -log_10(pvalue).
        """
        if scoreType == self.scorePcr:
            score = self.Pcr
        elif scoreType == self.scorePcr_T:
            score = self.Pcr_T
        elif scoreType == self.scorePrivPorts:
            score = self.PrivPorts
        elif scoreType == self.scorePrivPorts_T:
            score = self.PrivPorts_T
        elif scoreType == self.scoreBytesPerPacket:
            score = self.BytesPerPacket
        elif scoreType == self.scoreBytesPerPacket_T:
            score = self.BytesPerPacket_T
        else:
            score = None

        if score == None:
            Pvalue = None
        else:
            Pvalue = 10**(-score)

        return Pvalue

def DataPointExist(Data, key):
    """
    Is there even data for the key?
    """
    if key in Data:
        DataPoint = Data[key]
    else:
        DataPoint = None

    return DataPoint

class AlertData:
    """ Alert Data Format"""
    def __init__(self, RawData):
        self.srcIP     = DataPointExist(RawData,'srcIP')
        self.dstIP     = DataPointExist(RawData,'dstIP')
        self.time      = ExtractTime(DataPointExist(RawData,'stime'))
        self.flowPcr   = DataPointExist(RawData,'flowPcr')
        self.score     = DataPointExist(RawData,'score')
        self.scoreData = ScoreData(DataPointExist(RawData,'scores'))

def ReadDataFromFile(file_name):
    """ Read Data from the Skalion Data formated files """

    Data = []
    with open(file_name) as fileData:
        for index, line in enumerate(fileData):
            correctedLine = LineCorrect(line)
            Data.append(ast.literal_eval(correctedLine))

    return Data

def CreateAlertDataList(file_name):
    """ Read Data then create list of Alert Data """

    RawDataList = ReadDataFromFile(file_name)

    AlertDataList = []
    for data in RawDataList:
        AlertDataList.append(AlertData(data))

    return AlertDataList

def LineCorrect(line):
    """ Correct a given line so that python will be able to evaluate it """

    correctedLine = line.rstrip('\n')
    correctedLine = correctedLine.replace("\\n","")
    correctedLine = correctedLine.replace("false", "False")
    correctedLine = correctedLine.replace("true", "True")
    correctedLine = correctedLine.replace(" ", "")

    return correctedLine

def ExtractTime(RawTime):
    """ Extract Date-Time python format from data"""

    if RawTime == None:
        pythonTime = None
    else:
        pythonTime = RawTime.replace("Z", "")
        pythonTime = dt.strptime(pythonTime, '%Y-%m-%dT%H:%M:%S.%f')

    return pythonTime

