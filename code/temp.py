# temp.py

"""

## for dataset 1,
initial time is 2011/08/10 09:46:53.f  
botnet infection occurs around 2011/08/10 10:50:00.f and 
occurs until about 2011/08/10 15:49:53.f'

w/ 60 second timespan per time_window, we start detection at 60 and go until the end. 

-RAB 10/10/2016
"""
import random, csv, os, copy, time
import numpy as np 
from datetime import datetime

from generalFunctions import *
from clustering import * 


## ---- paths ---- ##
k = 1  ## index of ctu file
folderPath = os.path.abspath( os.path.join( os.path.join( os.getcwd(), ".." ), 'data-ctu-' + str( k ) ) )
dataPath =  os.path.join( folderPath , "ctu-" + str( k ) + ".csv" )  


## ---- functions ---- ##
def detect_points(N, p_values):
	""" 
	Input:	N = int, This is the operator's input giving the desired no. of points per hour 
			p_values = dict, {time_window index: {ip: pv}}
	Output: detected_points = {c: [ (ip, p_value)] for c in times} ## the ips and their pv's that were below the threshold
			alphas = {c: alpha} the p_valuse thresholds for each time window. 
	"""

	alphas = {}
	detected_points = {c: [] for c in times}

	for c in times: 
		d = p_values[c]
		data = [ (ip, pv) in d.iteritems() if pv != "na" ]
		n = len(data) ## Number of data points in this minute. 
		rate = n * 60 ## rate (points per hour) as of this minute. 
		alpha = N / (60. * n)  ## math here is  N >= E(alerts per hour ) = p( pv <= alpha ) * rate = alpha *  n*60. 
		alphas[c] = alpha
		## note we know equality of p(pv <= alpha) = alpha b/c kde is continuous. 
		detected_points[c] = [x in data if x[1] <= alpha]
	return detect_points, alphas 



## ---- open file ---- ## 
time_windows = {int(c): d for c,d in unjsonify( os.path.join(folderPath, 'time_windows.json')).iteritems()}  ## dict of the form {index: [window start time, window end time]}
flows = { int(c): d for c,d in unjsonify(os.path.join(folderPath, 'flows.json')).iteritems()}
botnet_ip = '147.32.84.165'
ips = unjsonify( os.path.join( folderPath, "ips.json") )
## check: 
if not botnet_ip in ips: 
	print "botnet_ip not in ips!"

# all_labels = sorted(set( [ f[-1] for c,d in flows.iteritems() for ip, l in d.iteritems() for f in l ]  ))

## make labels for the data points: 
labels = {c: {ip: "normal" for ip in ips} for c in time_windows.keys() }
for c in flows.keys(): 
	for ip in ips: 
		N = .001 + len(flows[c][ip]) ## how many flows in that period? 
		n = len([f for f in flows[c][ip] if "Botnet" in f[-1] ] )  ## n = no. of flow labels indicating botnet
		if (float(n) / N ) > .05: 	
			labels[c][ip] = 'botnet'

jsonify(labels, os.path.join(folderPath, 'labels.json' ))				

## load the data: 
baseline_parameters = unpickle(os.path.join(dataPath, 'baseline_parameters.pkl'))
p_values = { int(c): d for c,d in unjsonify(os.path.join(dataPath, 'p_values.json')).iteritems() }
times = sorted( p_values.keys() ) ## just the indices for the time windows 

alerts = {}
thresholds = {}
## find and save which points get detected (note, this has a variable data rate)
for N in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]: ## number of expected alerts per hour 
	print "--- Beginning alert analysis for N = %s ----" %N
	detected_points, alphas = detect_points(N, p_values)
	alerts[N] = detected_points 
	thresholds[N] = alphas 

jsonify(alerts, os.path.join(dataPath, 'alerts.json'))
jsonify(thresholds, os.path.join(dataPath, 'alphas.json'))


## find num of alerts per hour, and write as a table (csv file. )
num_alerts = []
for N in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]: 
	item = {'N' : N}
	d = alerts[N]
	for hour in range(1,5):
		n = 0
		for c in xrange(hour * 60, hour * 60 + 60 ): 
			n += d[c]
		item["hour "+ hour] = n
	num_alerts.append(item)

with open( os.path.join(dataPath , 'alert_count_table.csv') , 'w') as csvfile:
    
    writer = csv.DictWriter(csvfile, fieldnames = num_alerts[0].keys())
    writer.writeheader()
    for d in num_alerts: 
	    writer.writerow(d)






## ---- joel's features and clustering ---- ##
## joel's features: 
# joel_features = unjsonify(os.path.join(folderPath, 'joel_features.json'))
# joel_data = np.array(unjsonify( os.path.join(folderPath, 'joel_data.json')) )

## cluster by joel's features 
# X = np.vstack([joel_data[c, :, :] for c in xrange(joel_data.shape[0])])
# (k, ks, Wks, Wkbs, sk) = gap_statistic(X, 3, 15, plot = True, verbose = True)



# not used:
## gaussian detection for each ip: 
# c_start = 60 
# h = .98 ## mcd parameter
# mcd = MinCovDet(support_fraction = h)

# gaussians = {c: {ip: {} for ip in ips } for c in xrange(c_start, len(time_windows))} ##means and cov of history upto, not includeing c. 
# p_values = {c: {ip: "n/a" } for c in xrange(c_start, len(time_windows))} ##p-value of x_c given gaussian on history 

# c_start = 60 ## time interval to start on .


# for i, ip in enumerate(ips): 
# 	if i % 10 == 0: 
# 		print "beginning ip %s of 100" %i
# 	for c in xrange(c_start, len(time_windows)-1): 
# 		X = data[:c, i, :]
# 		x_new = data[ c , i, :].reshape(1, X.shape[-1]) ## test data

# 		## delete constant columns: 
# 		colvar = np.var(X, axis=0) ## these columns never change, making covariance matrix singular! usually they're always be 0!! It means this feature is useless in the observations 
# 		badcols = set(list(np.where(colvar < .0001 )[0]) + list(np.where( np.isnan(colvar))[0] ) )

# 		if len( badcols ) > 0:
# 			# print "removing columns: " + str(badcols) + "which are constant (else the cov. matrix will be singular)"
# 			## remove the bad columns by this awesome and semi-mysterious maneuver:
# 			for j in badcols: 
# 				X[0, j] = float("nan")  # give each such column a nan value (indicator to be nixed)
# 				x_new[0, j] = float("nan")
# 			X = np.ma.compress_cols(np.ma.masked_invalid(X)) ## the masked_invalid function switches the NaN's to dashes, the compress function deletes those columns. 
# 			x_new = np.ma.compress_cols(np.ma.masked_invalid(x_new)) ## the masked_invalid function switches the NaN's to dashes, the compress function deletes those columns. 

# 		## now we should be able to fit the mcd. Sometimes it doesn't converge, so we use try and except to add a little noise. If it still fails, we just use the mean, cov w/o mcd method. 
# 		try: 
# 			mcd.fit(X) 
# 			mean = mcd.location_
# 			cov = mcd.covariance_
# 		except: ## add a little noise to the samples b/c it's singular. 
# 			# try: 
# 			std = np.var(X, axis = 0)**.5	## 1 d array of standard deviations of each column 		
# 			E = .1 * std * np.random.rand( *X.shape ) ## an array w/ same shape as X. Made by drawing a uniform random sample in [0, standard deviation of the column * .1] and adds to entry 
# 			mcd.fit( X+E )
# 			mean = mcd.location_
# 			cov = mcd.covariance_
# 			# except: 
# 				# mean = np.mean( X, axis = 1)
# 				# cov = np.cov( X.T )

# 		gaussians[c][ip]['mean'] = mcd.location_.tolist()
# 		gaussians[c][ip]['cov'] = mcd.covariance_.tolist() 
		
# 		## now what's the p-value of x? 
# 		m_dist = mcd.mahalanobis(x_new)[0] ## this is the squared mahalanobis distance. It equals < cov^{-1} (x-mu), x-mu > 
# 		## note that 1-chi^2_CDF(alpha^2) = P(mahalanobis(x) > alpha ). This means to convert m_dist to a p_value, we use 
# 		##  p_value(x) = P(mahalanobis(X) >= mahalanobis(x)) = p(mahalanobis(X) > m_dist^.5) 1-chi2.cdf(m_dist) =  = p(mahalanobis(X) > mahalanobis(x)) where x is observed and X is a random variable. 
# 		pv = 1 - chi2.cdf(m_dist, x_new.shape[-1])
# 		p_values[c][ip] = pv 










