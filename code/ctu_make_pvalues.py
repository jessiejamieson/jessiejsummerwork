# ctu_make_pvalues.py

"""
NOTES: 
## for dataset 1,
initial time is 2011/08/10 09:46:53.f  
botnet infection occurs around 2011/08/10 10:50:00.f and 
occurs until about 2011/08/10 15:49:53.f'

w/ 60 second timespan per time_window, we start detection at 60 and go until the end. 



-RAB 10/10/2016
"""
import random, csv, os, copy, time
import numpy as np 
from datetime import datetime
from sklearn.covariance import MinCovDet 
from scipy.stats import chi2
from sklearn.neighbors import KernelDensity

from generalFunctions import *
from clustering import * 




## ---- time functions ---- ##
def time_to_epoch(timestring):
	"""
	Input: timestring = string in the form %Y/%m/%d %H:%M:%S.%f 
	Output: float (epoch time)
	"""
	utc_time = datetime.strptime(timestring, "%Y/%m/%d %H:%M:%S.%f")
	epoch_time = (utc_time - datetime(1970, 1, 1)).total_seconds()
	return epoch_time


def epoch_to_time(epoch):
	"""
	Input: epoch = string, epoch time 
	Output: string in form "%Y/%m/%d %H:%M:%S.%f"
	"""
	return time.strftime("%Y/%m/%d %H:%M:%S.%f", time.gmtime(epoch))

if __name__ == '__main__':
	## ---- paths ---- ##
	k = 1  ## index of ctu file
	folderPath = os.path.abspath( os.path.join( os.path.join( os.getcwd(), ".." ), 'data-ctu-' + str( k ) ) )
	dataPath =  os.path.join( folderPath , "ctu-" + str( k ) + ".csv" )  


	## ---- open file ---- ## 
	time_windows = {int(c): d for c,d in unjsonify( os.path.join(folderPath, 'time_windows.json')).iteritems()}  ## dict of the form {index: [window start time, window end time]}
	flows = { int(c): d for c,d in unjsonify(os.path.join(folderPath, 'flows.json')).iteritems()}
	botnet_ip = '147.32.84.165'
	ips = unjsonify( os.path.join( folderPath, "ips.json") )
	## check: 
	if not botnet_ip in ips: 
		print "botnet_ip not in ips!"


	## ---- joel's features and clustering ---- ##
	## joel's features: 
	# joel_features = unjsonify(os.path.join(folderPath, 'joel_features.json'))
	# joel_data = np.array(unjsonify( os.path.join(folderPath, 'joel_data.json')) )

	## cluster by joel's features 
	# X = np.vstack([joel_data[c, :, :] for c in xrange(joel_data.shape[0])])
	# (k, ks, Wks, Wkbs, sk) = gap_statistic(X, 3, 15, plot = True, verbose = True)


	## load the data: 
	## first set of features to try: 
	features = unjsonify( os.path.join(folderPath, 'features.json')) ## list of feature names corresponding to columns
	data = np.array( unjsonify( os.path.join(folderPath, 'data.json')))

	## ---- kernel density estimate ---- ##
	c_start = 60 
	kernel = "gaussian" ### choices are ['gaussian', 'tophat', 'epanechnikov', 'exponential', 'linear', 'cosine']

	baseline_parameters = {c: { } for c in xrange(c_start, len(time_windows)-1 )}
	p_values = {c: {ip:0 for ip in ips} for c in xrange(c_start , len(time_windows)-1)}

	for c in xrange( c_start, len(time_windows) -1 ): 
		if c %100 == c_start : 
			print "Starting computing p-values for c = %s of  %s " %(c - c_start, len(time_windows)- 1-c_start)
		X = np.vstack([data[t, :, :] for t in xrange(c)] )
		## delete the rows that are 0: 
		var = np.var(X, axis = 1)
		delete_rows = np.where(var == 0)[0] 
		X = np.delete(X, delete_rows, axis = 0)
		baseline_parameters[c]['delete_rows'] = delete_rows

		## delete columns that are constant: 
		var = np.var(X, axis = 0)
		delete_columns = np.where(var == 0)[0] 
		X = np.delete(X, delete_columns, axis = 1)
		baseline_parameters[c]['delete_columns'] = delete_columns

		## normalize: 
		mean = np.mean(X, axis = 0)
		std = np.var(X, axis = 0)**.5	
		X = (X - mean)/std
		baseline_parameters[c]["mean"] = mean ## save these so we can normalize future vectors. 
		baseline_parameters[c]["std"] = std

		## fit kde:
		width = np.mean(std)
		
		##NOT USED ANY MORE make a bounding box: 
		# m = np.min(X, axis = 0) - 2*std
		# M = np.max(X, axis = 0) + 2*std	
		## sample 10000 points in the box for a monte-carlo simulation: 
		# S = np.array([m + (M-m)* np.random.random(len(m)) for t in xrange(10000)]).reshape(10000, len(m))

		## fit kde
		kde = KernelDensity(kernel = kernel, bandwidth = width).fit(X) ## fit model						
		## sample and evaluate probabilities for a monte carlo: 
		S = kde.sample(10000)
		A = np.exp(kde.score_samples(S)) ## probability of sample points
		baseline_parameters[c]['kde'] = kde 
		baseline_parameters[c]['A'] = A 	

		for i,ip in enumerate(ips): 
			x = data[c, i,:].reshape(1,data.shape[-1])
			
			if np.all(x == 0): # if x is the zero vector, skip
				pv = "na"			
			
			else: 		
				## delete columns that were 0 in observations: 
				x = np.delete(x, delete_columns, axis = 1)

				## normalize x: 
				x = (x - mean)/std

				## get p-value: 
				# try: 
				p = float(np.exp(kde.score_samples(x ))) # likelihood of the new data (score_samples gives the log(pdf))
				pv = len(np.where(A <= p)[0])/10000.
				# except: 
				# 	pv = 'na'
				## store: 
				p_values[c][ip] = pv

	picklify( baseline_parameters, os.path.join(folderPath, 'baseline_parameters.pkl') ) 
	jsonify(p_values, os.path.join(folderPath, 'p_values.json'))



##########
	# not used:
	## gaussian detection for each ip: 
	# c_start = 60 
	# h = .98 ## mcd parameter
	# mcd = MinCovDet(support_fraction = h)

	# gaussians = {c: {ip: {} for ip in ips } for c in xrange(c_start, len(time_windows))} ##means and cov of history upto, not includeing c. 
	# p_values = {c: {ip: "n/a" } for c in xrange(c_start, len(time_windows))} ##p-value of x_c given gaussian on history 

	# c_start = 60 ## time interval to start on .


	# for i, ip in enumerate(ips): 
	# 	if i % 10 == 0: 
	# 		print "beginning ip %s of 100" %i
	# 	for c in xrange(c_start, len(time_windows)-1): 
	# 		X = data[:c, i, :]
	# 		x_new = data[ c , i, :].reshape(1, X.shape[-1]) ## test data

	# 		## delete constant columns: 
	# 		colvar = np.var(X, axis=0) ## these columns never change, making covariance matrix singular! usually they're always be 0!! It means this feature is useless in the observations 
	# 		badcols = set(list(np.where(colvar < .0001 )[0]) + list(np.where( np.isnan(colvar))[0] ) )

	# 		if len( badcols ) > 0:
	# 			# print "removing columns: " + str(badcols) + "which are constant (else the cov. matrix will be singular)"
	# 			## remove the bad columns by this awesome and semi-mysterious maneuver:
	# 			for j in badcols: 
	# 				X[0, j] = float("nan")  # give each such column a nan value (indicator to be nixed)
	# 				x_new[0, j] = float("nan")
	# 			X = np.ma.compress_cols(np.ma.masked_invalid(X)) ## the masked_invalid function switches the NaN's to dashes, the compress function deletes those columns. 
	# 			x_new = np.ma.compress_cols(np.ma.masked_invalid(x_new)) ## the masked_invalid function switches the NaN's to dashes, the compress function deletes those columns. 

	# 		## now we should be able to fit the mcd. Sometimes it doesn't converge, so we use try and except to add a little noise. If it still fails, we just use the mean, cov w/o mcd method. 
	# 		try: 
	# 			mcd.fit(X) 
	# 			mean = mcd.location_
	# 			cov = mcd.covariance_
	# 		except: ## add a little noise to the samples b/c it's singular. 
	# 			# try: 
	# 			std = np.var(X, axis = 0)**.5	## 1 d array of standard deviations of each column 		
	# 			E = .1 * std * np.random.rand( *X.shape ) ## an array w/ same shape as X. Made by drawing a uniform random sample in [0, standard deviation of the column * .1] and adds to entry 
	# 			mcd.fit( X+E )
	# 			mean = mcd.location_
	# 			cov = mcd.covariance_
	# 			# except: 
	# 				# mean = np.mean( X, axis = 1)
	# 				# cov = np.cov( X.T )

	# 		gaussians[c][ip]['mean'] = mcd.location_.tolist()
	# 		gaussians[c][ip]['cov'] = mcd.covariance_.tolist() 
			
	# 		## now what's the p-value of x? 
	# 		m_dist = mcd.mahalanobis(x_new)[0] ## this is the squared mahalanobis distance. It equals < cov^{-1} (x-mu), x-mu > 
	# 		## note that 1-chi^2_CDF(alpha^2) = P(mahalanobis(x) > alpha ). This means to convert m_dist to a p_value, we use 
	# 		##  p_value(x) = P(mahalanobis(X) >= mahalanobis(x)) = p(mahalanobis(X) > m_dist^.5) 1-chi2.cdf(m_dist) =  = p(mahalanobis(X) > mahalanobis(x)) where x is observed and X is a random variable. 
	# 		pv = 1 - chi2.cdf(m_dist, x_new.shape[-1])
	# 		p_values[c][ip] = pv 










